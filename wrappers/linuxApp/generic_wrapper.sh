#!/usr/bin/env bash
#!----------------------------------------------------------------------------
#!
#! generic_wrap.sh
#!
#! A generic wrapper script for any linux application that needs a clean env
#!
#! Usage:
#!     This is to be sym linked 
#!
#! History:
#!   29Jan19: A. De Silva, First version
#!   04Apr19: A. Solodkov, changes for centos7 warnings and ssh display exports
#!
#!----------------------------------------------------------------------------


alrb_wrapWorkdir=`\mktemp -d ${ALRB_tmpScratch}/wrapperXXXXXX 2>&1`
if [ $? -ne 0 ]; then
    \echo "Error: unable to create wrapper work dir"
    exit 64
fi

alrb_appName=`basename $0`

env | \grep -e "^SSH_AUTH_SOCK=" -e "^SSH_CONNECTION=" -e "^DBUS_SESSION_BUS_ADDRESS=" -e "^USER=" -e "^KRB[[:alnum:]]*=" -e "^HOME=" -e "^TERM=" -e "^DISPLAY=" |  env LC_ALL=C \sort | \awk '{print "export "$0}' | \sed -e 's|=|="|1' -e 's|$|"|1' > $alrb_wrapWorkdir/wrapScript.sh 

\cat <<EOF >> $alrb_wrapWorkdir/wrapScript.sh

cd $PWD
alrb_cmd="$alrb_appName $@"
eval \$alrb_cmd

EOF

alrb_thisEnv=`env | \grep -e "^HOME=" -e "^LANG=" -e "^KRB5CCNAME="| \tr '\n' ' '`
env -i $alrb_thisEnv bash -l -c "source $alrb_wrapWorkdir/wrapScript.sh"
alrb_rc=$?

\rm -rf $alrb_wrapWorkdir

exit $alrb_rc
