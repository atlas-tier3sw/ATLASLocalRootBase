#!/usr/bin/env bash
#!----------------------------------------------------------------------------
#!
#! rucio_wrapper.sh
#!
#! A generic wrapper script for rucio-clients setups
#!
#! Usage:
#!     This is to be sym linked 
#!
#! History:
#!   16Feb16: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_wrapWorkdir=`\mktemp -d ${ALRB_tmpScratch}/wrapperXXXXXX 2>&1`
if [ $? -ne 0 ]; then
    \echo "Error: unable to create wrapper work dir"
    exit 64
fi

alrb_appName=`basename $0`

if [ ! -z $AtlasProject ]; then
    if [ "$AtlasReleaseType" = "nightly" ]; then
	export ALRB_RELSETUP="$AtlasProject,$AtlasBuildBranch,$AtlasBuildStamp --platform=$CMTCONFIG"
    else
	export ALRB_RELSETUP="$AtlasProject,$AtlasVersion --platform=$CMTCONFIG"
    fi
fi

if [ $ALRB_OSMAJORVER -ge 7 ]; then
    if [ -z $ALRB_pythonVersion ]; then
	alrb_py3Setup="lsetup \"python recommended\" -q"
    else
	alrb_py3Setup="lsetup \"python $ALRB_pythonVersion\" -q"
    fi
    alrb_setupATLASMod="-3"
else
    alrb_py3Setup=""
    alrb_setupATLASMod=""
fi

env | \grep -e "^ALRB" -e "^ATLAS_LOCAL_" -e "^X509_USER_PROXY" -e "^RUCIO_ACCOUNT" -e "ALRB_[[:alnum:]]*Version="  -e "ALRB_RELSETUP" -e "^TZ=" -e "^KRB[[:alnum:]]*=" |  env LC_ALL=C \sort | \awk '{print "export "$1""}' | \sed -e 's|=|="|g' -e 's|$|"|g' > $alrb_wrapWorkdir/wrapScript.sh 

alrb_wrapperArgs=""
for alrb_item  in "$@"; do    
    \echo $alrb_item | \grep -e "[[:space:]]" > /dev/null  2>&1 
    if [ $? -eq 0 ]; then
	alrb_wrapperArgs="$alrb_wrapperArgs \"$alrb_item\""
    else
	alrb_wrapperArgs="$alrb_wrapperArgs $alrb_item"
    fi
done

\cat <<EOF >> $alrb_wrapWorkdir/wrapScript.sh

if [ ! -e  \$ALRB_rucioWrapperEnvScript ]; then
  env | \sort > $alrb_wrapWorkdir/env.0
  eval source \$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh --quiet $alrb_setupATLASMod
  $alrb_py3Setup
  eval source \${ATLAS_LOCAL_ROOT_BASE}/packageSetups/localSetup.sh rucio --quiet
  env | \sort > $alrb_wrapWorkdir/env.1
  diff --unchanged-line-format= --old-line-format= --new-line-format='%L' $alrb_wrapWorkdir/env.0 $alrb_wrapWorkdir/env.1 | \sed -e 's|^|export |' -e 's|=|="|' -e 's|$|"|' > \$ALRB_rucioWrapperEnvScript
else
  source \$ALRB_rucioWrapperEnvScript
fi

if [ ! -z "\$ALRB_RELSETUP" ]; then
  type -a insertPath > /dev/null 2>&1
  if [ \$? -ne 0 ]; then
    source \${ATLAS_LOCAL_ROOT_BASE}/utilities/setupAliases.sh
  fi
  insertPath PATH $ATLAS_LOCAL_ROOT_BASE/wrappers/asetup
fi

$alrb_appName $alrb_wrapperArgs

EOF

alrb_thisEnv=`env | \grep -e "^HOME=" -e "^LANG=" -e "^KRB5CCNAME="| \tr '\n' ' '`
env -i $alrb_thisEnv bash -l -c "source $alrb_wrapWorkdir/wrapScript.sh"
alrb_rc=$?

\rm -rf $alrb_wrapWorkdir

exit $alrb_rc

