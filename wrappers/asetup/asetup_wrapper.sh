#!/usr/bin/env bash
#!----------------------------------------------------------------------------
#!
#! asetup_wrapper.sh
#!
#! A generic wrapper script for any asetup application that needs a clean env
#!
#! Usage:
#!     This is to be sym linked 
#!
#! History:
#!   07Mar19: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


alrb_wrapWorkdir=`\mktemp -d ${ALRB_tmpScratch}/wrapperXXXXXX 2>&1`
if [ $? -ne 0 ]; then
    \echo "Error: unable to create wrapper work dir"
    exit 64
fi

alrb_appName=`basename $0`

env | \grep -e "^ALRB" -e "^ATLAS_LOCAL_" -e "^X509_USER_PROXY" -e "^RUCIO_ACCOUNT" -e "ALRB_[[:alnum:]]*Version="  -e "ALRB_RELSETUP" -e "^KRB[[:alnum:]]*=" |  env LC_ALL=C \sort | \awk '{print "export "$1""}' | \sed -e 's|=|="|g' -e 's|$|"|g' > $alrb_wrapWorkdir/wrapScript.sh 

\cat <<EOF >> $alrb_wrapWorkdir/wrapScript.sh

cd $PWD
eval source \$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh --quiet 
eval asetup $ALRB_RELSETUP --quiet

alrb_cmd="$alrb_appName $@"
eval \$alrb_cmd

EOF

alrb_thisEnv=`env | \grep -e "^HOME=" -e "^LANG=" -e "^KRB5CCNAME="| \tr '\n' ' '`
env -i $alrb_thisEnv bash -l -c "source $alrb_wrapWorkdir/wrapScript.sh"
alrb_rc=$?

\rm -rf $alrb_wrapWorkdir

exit $alrb_rc
