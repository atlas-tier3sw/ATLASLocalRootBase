#!----------------------------------------------------------------------------
#!
#! singularityFunctions.sh
#!
#! Code specific to Singularity that is called by startContainer 
#!
#! Usage:
#!     used by startContainer.sh 
#!
#! History:
#!    11Dec17: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


alrb_fn_singularitySetEnvVar()
{
    export SINGULARITYENV_$1="$2"
    return 0
}


alrb_fn_singularityInitialize()
{
    alrb_runtimeBindMountOpt="-B"
    alrb_runtimeWorkdirOpt="--pwd "
    alrb_runtimeInitialized="singularity"
    alrb_runTimeErrMsg="Error"
    alrb_proxyCachedContainer=""
    alrb_pullFromRegistry="NO"
    
    if [ -e $HOME/alrb_override_singularity.sh ]; then
	source $HOME/alrb_override_singularity.sh
	return $?
    elif [ -e $alrb_localConfigDir/alrb_singularity.sh ]; then
	source $alrb_localConfigDir/alrb_singularity.sh
	return $?
    fi

    return 0
}


alrb_fn_singularityVersion()
{

# for now, use only if excplicitly asked for cvmfs version and exit on error
    if [ ! -z "$ALRB_CONT_SWVERSION" ]; then
	alrb_fn_singularityGetCvmfsExe
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi
    
    which singularity > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	local alrb_tmpVal="current"
	if [ ! -z $ALRB_cvmfsSingularityVersion ]; then
	    alrb_tmpVal="$ALRB_cvmfsSingularityVersion"
	fi		
	local alrb_tmpPath="$ALRB_cvmfs_repo/containers/sw/singularity/${alrb_archType}-el${ALRB_RHVER}/${alrb_tmpVal}/bin"
	if [ -e "$alrb_tmpPath/singularity" ]; then
	    local alrb_tmpRealPath=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_tmpPath`
	    export PATH="$PATH:$alrb_tmpRealPath"
	else
	    \echo "Warning: singularity not found in $alrb_tmpPath, so not added"
	    \echo "         \$ALRB_cvmfsSingularityVersion was defined as $ALRB_cvmfsSingularityVersion"
	fi
    fi

    alrb_contVer=`singularity --version 2>&1`
    if [ $? -ne 0 ]; then
	\echo "PATH searched: $PATH"
	\echo $alrb_contVer
	\echo "Error: Singularity is not installed or fails; cannot create container" 1>&2
	return 64
    else
	alrb_contVer=`\echo $alrb_contVer | \sed -e 's/.* version //g'`
    fi
    alrb_contVer=`\echo $alrb_contVer | \cut -f 1 -d "-" 2>&1`
    let alrb_contVerN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_contVer 3 -p`
    if [ "$alrb_contVerN" -lt 20601 ]; then
	\echo "Warning: Singularity version is $alrb_contVer; 2.6.1 or newer recommended."
    fi
    
    return 0
}


alrb_fn_singularityGetCvmfsExe()
{
    
    local alrb_rc=0

    # singularity executables should be os version independent
    #  use el7 version as a minimal
    local alrb_tmpPath="$ALRB_cvmfs_repo/containers/sw/singularity/${alrb_archType}-el7/$ALRB_CONT_SWVERSION"
    if [ ! -e "$alrb_tmpPath/bin/singularity" ]; then
	\echo "Error: singularity executable does not exist on $alrb_tmpPath"
	return 64
    fi
    local alrb_tmpRealPath=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_tmpPath`

    alrb_fn_singularityTestCvmfsExe "$alrb_tmpRealPath/bin"
    alrb_rc=$?
    if [ $alrb_rc -eq 0 ]; then
	export PATH="$alrb_tmpRealPath/bin:$PATH"
    fi
    
    return $alrb_rc
}


alrb_fn_singularityTestCvmfsExe()
{

    local alrb_rc=0
    local alrb_result
   
    local alrb_singularityExecPath="$1"
    
    local alrb_testContainer="$ALRB_cvmfs_repo/containers/fs/singularity/${alrb_archType}-busybox"
    if [ -z "$PWD" ]; then
	PWD=`pwd`
    fi

    local alrb_contOpts=""
    if [ ! -z $KRB5CCNAME ]; then
	local alrb_krbType="`\echo $KRB5CCNAME | \cut -f 1 -d ':'`"
	if [[ "$alrb_krbType" = "FILE" ]] || [[ "$alrb_krbType" = "DIR" ]]; then
	    local alrb_krbNameHost="`\echo $KRB5CCNAME | \cut -f 2 -d ':'`"
	    alrb_contOpts="-B $alrb_krbNameHost:$alrb_krbNameHost --env KRB5CCNAME=$KRB5CCNAME"
	fi
    fi
    
    local alrb_cmd="$alrb_singularityExecPath/singularity exec -c $alrb_contOpts -B $PWD:/whatever --pwd /whatever -e $alrb_testContainer id"    
    alrb_result=`eval $alrb_cmd 2>&1`
    alrb_rc=$?
    if [ $alrb_rc -ne 0 ]; then
	\echo "Error: singularity from cvmfs did not work ..."
	if [ -e /proc/sys/user/max_user_namespaces ]; then
	    \echo "/proc/sys/user/max_user_namespaces has: `\cat /proc/sys/user/max_user_namespaces`"
	else
	    \echo "/proc/sys/user/max_user_namespaces does not exist."
	fi
	\echo "command: $alrb_cmd"
	\echo $alrb_result	
    fi
    
    return $alrb_rc
}


alrb_fn_singularityOptsParser()
{
    if [ ! -z "$ALRB_CONT_CMDOPTS" ]; then
	eval set -- "$ALRB_CONT_CMDOPTS"
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-c|--contain|-C|--containall)
		    alrb_containMinimal="YES"
		    alrb_copyProxy="YES"
		    shift
		    ;;
		--pwd)
		    alrb_setContainerPwd="$2"
		    shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi
    
    return 0
    
}

alrb_fn_singularityBuildDockerSandbox()
{
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} build sandbox"

    alrb_fn_getProxyCachedRegistryURL "$alrb_containerCandidate"
    
    if [ ! -z "$SINGULARITY_TMPDIR" ]; then
	\echo "Using (\$SINGULARITY_TMPDIR defined) $SINGULARITY_TMPDIR for sandbox ..."
    elif [ ! -z "$PILOT_CONTAINER_SANDBOX" ]; then
	\echo "Using (\$PILOT_CONTAINER_SANDBOX) $PILOT_CONTAINER_SANDBOX for sandbox ..."
	export SINGULARITY_TMPDIR="$PILOT_CONTAINER_SANDBOX"
    elif [ ! -z "$PILOT_HOME" ]; then
	\echo "Using (\$PILOT_HOME) $PILOT_HOME/singularityTmp for sandbox ..."
	export SINGULARITY_TMPDIR="$PILOT_HOME/singularityTmp"
    else
	\echo -e "
\033[1m\033[4mSINGULARITY_TMPDIR is undefined\033[0m
 Defining SINGULARITY_TMPDIR to point to a non-tmp dir will save time if you
 run the container more than once
" 
	\echo "Container will not be built in a separate stage but in exec ..."
	alrb_pullFromRegistry="YES"
	return 0
    fi

    \echo "Now building container ..."
    mkdir -p $SINGULARITY_TMPDIR
        
    local alrb_rc

    local alrb_dirName=`\echo $alrb_containerCandidate | \sed -e 's|docker://||' | \sed -e 's|[^[:alnum:].-]|\_|g'`    
    local alrb_containerDir="$SINGULARITY_TMPDIR/$alrb_dirName"
    if [ "$alrb_labelBuild" != "" ]; then
	alrb_containerDir="$SINGULARITY_TMPDIR/${alrb_labelBuild}_${alrb_dirName}"
    elif [ "$alrb_labelUse" != "" ]; then
	alrb_containerDir="$SINGULARITY_TMPDIR/${alrb_labelUse}_${alrb_dirName}"
	if [ -e $alrb_containerDir/noupdate ]; then
	    alrb_container="$alrb_containerDir/image"
	    return 0
	else
	    \echo "Error: $alrb_containerCandidate label ${alrb_labelUse} does not seem to be built in $alrb_containerDir"
	    return 64
	fi
    fi
    mkdir -p $alrb_containerDir
    if [ "$alrb_contVerN" -ge 30502 ]; then
	export SINGULARITY_DISABLE_CACHE=true
    else
	export SINGULARITY_CACHEDIR="$alrb_containerDir/cache"
	mkdir -p $alrb_containerDir/cache
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi
    
    \echo "Locking dir ..." 
    $ATLAS_LOCAL_ROOT_BASE/utilities/createLockFile.sh -f $alrb_containerDir/lockfile -s "$ALRB_CONT_TIMEOUTS" -r "$ALRB_CONT_TIMEOUTR" -d "$ALRB_CONT_TIMEOUTD" -v 
    if [ $? -ne 0 ]; then
	\echo "Error: There may be a concurrent build in progress.  Exiting"
	return 64
    fi
    local alrb_optSingularityBuild="$alrb_optContainerBuilld"
    if [ -e $alrb_containerDir/image/.singularity.d/runscript ]; then
	alrb_optSingularityBuild="$alrb_optSingularityBuild -u"
	if [ "$alrb_forceBuild" = "YES" ]; then
	    \rm -f $alrb_containerDir/noupdate
	    \rm -f $alrb_containerDir/building
	fi
	if [ -e "$alrb_containerDir/noupdate" ]; then
	    \echo " $alrb_containerDir/noupdate found; will not do 'singularity build -u'"
	    alrb_container="$alrb_containerDir/image"
	    \echo "Removing lock dir ..."
	    \rm -f $alrb_containerDir/lockfile
	    return 0
	elif [ -e "$alrb_containerDir/building" ]; then
	    \echo "Error: $alrb_containerDir/building found; will exit immediately."
	    return 64
	fi
    else
	\rm -rf $alrb_containerDir/image
    fi
    if [ $? -eq 0 ]; then
	local alrb_cmd
	if [ "$alrb_contVerN" -ge 30500 ]; then
	    alrb_optSingularityBuild="$alrb_optSingularityBuild --fix-perms"
	fi
	alrb_cmd="singularity $ALRB_CONT_OPTS build $alrb_optSingularityBuild --sandbox $alrb_containerDir/image $alrb_proxyCachedContainer"
	singularity --version
	\echo " from `which singularity`"
	\echo "$alrb_cmd"
	\echo " suppressing \"warn xattr{*} ignoring forbidden xattr\" warnings"
	alrb_fn_printMsg 1 "$alrb_cmd"
	touch $alrb_containerDir/building
	eval $alrb_cmd 2>&1 | \sed -e '/warn xattr{.*} ignoring forbidden xattr/d'
	alrb_rc=${PIPESTATUS[0]}
	alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} finished building sandbox"
	\rm -f $alrb_containerDir/building
	\echo "Removing lock dir ..."
	\rm -f $alrb_containerDir/lockfile
	if [ $alrb_rc -ne 0 ]; then
	    return 64
	fi
	alrb_container="$alrb_containerDir/image"
	touch $alrb_containerDir/noupdate
    else
	\echo "Error: Cannot get lockfile $alrb_containerDir/lockfile"
	return 64
    fi

    return 0
}


alrb_fn_singularitySetContainer()
{
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} determine container"
    local alrb_tmpVal

    alrb_container=$alrb_containerCandidate
    alrb_containerAsSingularity="$alrb_container"

# docker container specified explicitly    
    \echo $alrb_containerCandidate | \grep -e "^docker://" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_fn_singularityBuildDockerSandbox
	return $?
    fi

    if [ -e $alrb_container ]; then
# these allow for direct paths for containers to be specified
	alrb_tmpVal=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_container`	
	if [[ -f $alrb_tmpVal ]] && [[ "${alrb_tmpVal##*.}" = "img" ]]; then
	    return 0
	elif [ -d $alrb_tmpVal/.singularity.d ]; then
	    return 0
	fi
    fi

# search for unpacked dir images ...
    local alrb_suffixAr=( "-${alrb_archType}" "" )
    local alrb_tmpAr=( )
    if [ ! -z "$ALRB_CONT_UNPACKEDDIR" ]; then
	alrb_tmpAr=( $ALRB_CONT_UNPACKEDDIR )
    fi
    if [ ! -z "$ALRB_cvmfs_unpacked_repo" ]; then
	alrb_tmpAr=( ${alrb_tmpAr[@]}
		     "$ALRB_cvmfs_unpacked_repo"
		     "$ALRB_cvmfs_unpacked_repo/registry.cern.ch"
		     "$ALRB_cvmfs_unpacked_repo/gitlab-registry.cern.ch"
		     "$ALRB_cvmfs_unpacked_repo/registry.hub.docker.com"
		   )
    fi
    local alrb_unpackedDir
    for alrb_unpackedDir in "${alrb_tmpAr[@]}"; do
	for alrb_suffix in "${alrb_suffixAr[@]}"; do
	    alrb_tmpVal="$alrb_unpackedDir/$alrb_containerCandidate$alrb_suffix"
	    if [ -d $alrb_tmpVal/.singularity.d ]; then
		alrb_container="$alrb_tmpVal"
		return 0
	    elif [[ -f $alrb_tmpVal ]] && [[ "${alrb_tmpVal##*.}" = "img" ]]; then
		alrb_container="$alrb_tmpVal"
		return 0
	    fi
	done
    done

    # at this point, these are default containers and should not have a '/'
    \echo $alrb_containerCandidate | \grep -e "/" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	\echo "Error: $alrb_containerCandidate is not found as directory"
	return 64
    fi
    
    if [ "$alrb_imageType" = "images" ]; then
	local alrb_singRepo="$ALRB_cvmfs_repo/containers/images/singularity"
	local alrb_imageSuffix=".img"
    else
	local alrb_singRepo="$ALRB_cvmfs_repo/containers/fs/singularity"
	local alrb_imageSuffix=""
    fi

    local alrb_oldifs="$IFS"
    IFS=$'\n'
    local alrb_defaultContAr=(
	'slc5|atlas-default|-e "slc5"'
	'centos6|atlas-default|-e "sl6" -e "slc6" -e "centos6" -e "rhel6"'
	'centos7|atlas-default|-e "sl7" -e "slc7" -e "centos7" -e "rhel7"'
	'almalinux8|atlas-default|-e "sl8" -e "slc8" -e "centos8" -e "centos8s" -e "rhel8" -e "almalinux8" -e "alma8"'
	'almalinux9|atlas-default|-e "sl9" -e "slc9" -e "centos9" -e "almalinux9" -e "alma9" -e "rhel9" -e "el9"'
    )

    local alrb_defaultCont
    for alrb_defaultCont in ${alrb_defaultContAr[@]}; do
	local alrb_queryCmd="\echo $alrb_containerCandidate | \grep -i `\echo $alrb_defaultCont | \cut -f 3 -d '|'`"
	alrb_tmpVal=`eval $alrb_queryCmd 2>&1`
	if [ $? -eq 0 ]; then
	    alrb_containerType="`\echo  $alrb_defaultCont | \cut -f 2 -d '|'`"
	    local alrb_contNamePrefix=`\echo $alrb_defaultCont | \cut -f 1 -d '|'`
	    alrb_containerAsSingularity="$alrb_singRepo/${alrb_archType}-${alrb_contNamePrefix}${alrb_imageSuffix}"
	    alrb_container="$alrb_containerAsSingularity"
	    if [ ! -z $ALRB_CONT_DEFAULTCONTDIR ]; then
		alrb_tmpVal="`\echo $alrb_containerAsSingularity | \sed -e 's|'$alrb_singRepo'|'$ALRB_CONT_DEFAULTCONTDIR'|g'`" 
		if [ -e $alrb_tmpVal ]; then
		    alrb_container="$alrb_tmpVal"
		fi
	    fi		     	
	    
	    if [ "$alrb_contNamePrefix" = "almalinux9" ]; then		
		\echo "$alrb_patch" | \grep -e ",none," > /dev/null 2>&1
		if [ $? -ne 0 ]; then
		    alrb_patch="$alrb_patch,openssh,"
		fi
	    fi
	    
	    break
	fi
    done
    IFS="$alrb_oldifs"
    
    return 0
}


alrb_fn_singularityPatchBindContainer()
{    
    
    \echo "$alrb_patch" | \grep -e ",none," > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	return 0
    fi

    \echo "$alrb_patch" | \grep -e ",openssh," > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	# fix el9 default containers for openssh issue
	# applies only to singularity unpacked default el9 containers on cvmfs
	# https://its.cern.ch/jira/browse/ATLINFR-5092
    
	local alrb_patchDir="$ATLAS_LOCAL_ROOT_BASE/containerPatch/el9/current"
	if [ -d $alrb_patchDir ]; then
	    # this has to be owned by the user ...
	    \mkdir -p $alrb_contDummyHome/patchDir
	    \cp -LR $alrb_patchDir/* $alrb_contDummyHome/patchDir/
    	    \find  $alrb_contDummyHome/patchDir/ -type f  -exec chmod go-w {} \;
	    alrb_patchBindStr="$alrb_patchBindStr -B $alrb_contDummyHome/patchDir/50-redhat.conf:/etc/ssh/ssh_config.d/50-redhat.conf -B $alrb_contDummyHome/patchDir/openssh.config:/etc/crypto-policies/back-ends/openssh.config"
	fi
    fi
    
    return 0
}


alrb_fn_singularityGetPathLookup()
{
    
    ALRB_CONT_SED2HOST=""
    ALRB_CONT_SED2CONT=""

    local alrb_bindMountAr=()

    local alrb_isNestedContainer=""
    if [ ! -z "$SINGULARITY_ENVIRONMENT" ]; then
	if [ -d "/.singularity.d/env" ]; then
	    alrb_isNestedContainer="YES"
	fi
    fi
	 
    local alrb_bindStr=""
    if [ ! -z "$SINGULARITY_BIND" ]; then
	if [ "$alrb_containerSkipCvmfs" = "YES" ]; then
	    \echo $SINGULARITY_BIND | \grep -e "/cvmfs" > /dev/null 2>&1
	    if [ $? -eq 0 ]; then
		if [ "$alrb_isNestedContainer" != "YES" ]; then
		    \echo "Warning: \$SINGULARITY_BIND defined has /cvmfs which should not be mounted."
		else
		    \echo "Warning: inhertited \$SINGULARITY_BIND defined has /cvmfs.
	  /cvmfs will be removed from it as it should not be mounted."
		    local alrb_tmpVal=`\echo $SINGULARITY_BIND | \tr ',' '\n' | sed -e '/^\/cvmfs/d' | tr '\n' ',' | sed -e 's|,$||g' -e 's|[[:space:]]||g'`
		    if [ "$alrb_tmpVal" != "" ]; then
			export SINGULARITY_BIND="$alrb_tmpVal"
		    else
			unset SINGULARITY_BIND
		    fi
		fi
	    fi
	fi
	alrb_bindStr="$alrb_bindStr,$SINGULARITY_BIND"
    fi

    if [ ! -z "$SINGULARITY_BINDPATH" ]; then
	if [ "$alrb_containerSkipCvmfs" = "YES" ]; then
	    \echo $SINGULARITY_BINDPATH | \grep -e "/cvmfs" > /dev/null 2>&1
	    if [ $? -eq 0 ]; then
		if [ "$alrb_isNestedContainer" != "YES" ]; then
		    \echo "Warning: \$SINGULARITY_BINDPATH defined has /cvmfs which should not be mounted."
		else
		    \echo "Warning: inhertited \$SINGULARITY_BINDPATH defined has /cvmfs.
	  /cvmfs will be removed from it as it should not be mounted."
		    local alrb_tmpVal=`\echo $SINGULARITY_BINDPATH | \tr ',' '\n' | sed -e '/^\/cvmfs/d' | tr '\n' ',' | sed -e 's|,$||g' -e 's|[[:space:]]||g'`
		    if [ "$alrb_tmpVal" != "" ]; then
			export SINGULARITY_BINDPATH="$alrb_tmpVal"
		    else
			unset SINGULARITY_BINDPATH
		    fi
		fi
	    fi
	fi
	alrb_bindStr="$alrb_bindStr,$SINGULARITY_BINDPATH"
    fi
    
    local alrb_confFile="`singularity buildcfg |\grep -e SINGULARITY_CONFDIR | \cut -f 2 -d "="`/singularity.conf"
    if [ ! -e $alrb_confFile ]; then
	\echo "Warning: unable to find singularity conf file but will continue."
    else
	local alrb_tmpVal="`\grep -e "^[[:space:]]*bind path" $alrb_confFile  | \cut -f 2 -d "=" | \tr '\n' ','`"
	if [[ "$alrb_containerSkipCvmfs" = "YES" ]] && [[ "$alrb_containMinimal" != "YES" ]]; then
	    \echo "$alrb_tmpVal" | \grep -e "/cvmfs" > /dev/null 2>&1
	    if [ $? -eq 0 ]; then
		\echo "Error: Sigularity config file ($alrb_confFile) has /cvmfs.
       /cvmfs may be mounted because of this; but continue despite this error."
	    fi
	fi
	alrb_bindStr="$alrb_bindStr,$alrb_tmpVal"
    fi   

    if [ ! -z "$ALRB_CONT_STARTCMD" ]; then
	eval set -- "$ALRB_CONT_STARTCMD"
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-B|--bind|-H|--home)
		    local alrb_tmp1=`\echo $2 | \cut -f 1 -d ":"`
		    local alrb_tmp2=`\echo $2 | \cut -f 2 -d ":"`
		    alrb_bindMountAr=( ${alrb_bindMountAr[@]} $alrb_tmp2 )
		    alrb_fn_buildSedPath "$alrb_tmp1" "$alrb_tmp2"
		    shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi

    local alrb_tmpAr=( `\echo $alrb_bindStr | \sed -e 's/,/ /g'` )
    local alrb_item
    for alrb_item in "${alrb_tmpAr[@]}"; do
	local alrb_tmp1=`\echo $alrb_item | \cut -f 1 -d ":"`
	local alrb_tmp2=`\echo $alrb_item | \cut -f 2 -d ":"`
	alrb_bindMountAr=( ${alrb_bindMountAr[@]} $alrb_tmp2 )
	alrb_fn_buildSedPath "$alrb_tmp1" "$alrb_tmp2"
    done
    
    if [ "$ALRB_CONT_SED2HOST" != "" ]; then
	export ALRB_CONT_SED2HOST
    fi
    if [ "$ALRB_CONT_SED2CONT" != "" ]; then
	export ALRB_CONT_SED2CONT
    fi
    
    if [ "$alrb_labelBuild" != "" ]; then
	local alrb_item
	for alrb_item in "${alrb_bindMountAr[@]}"; do
	    \mkdir -p $alrb_container/$alrb_item
	done
    fi
    
    return 0    
}


alrb_fn_singularityConstructCommand()
{
    if [ -z $SINGULARITY_CACHEDIR ]; then
	export SINGULARITY_CACHEDIR="`pwd`/singularity"
    fi

    alrb_fn_singularityPatchBindContainer
    
    local alrb_optStr="$alrb_patchBindStr -H $alrb_contDummyHome:$ALRB_CONT_DUMMYHOME"

    if [ ! -z $ALRB_CONT_DUMMYALRB ]; then 
	alrb_optStr="$alrb_optStr -B $ATLAS_LOCAL_ROOT_BASE:$ALRB_CONT_DUMMYALRB"
    fi
    
    if [ "$alrb_containerSkipCvmfs" != "YES" ]; then
	\echo "Info: /cvmfs mounted; do 'setupATLAS -d -c ...' to skip default mounts."
	alrb_optStr="$alrb_optStr -B $alrb_cvmfs_mount:/cvmfs"
    fi	

    if [ "$alrb_containerSkipHome" != "YES" ]; then
	\echo "Info: \$HOME mounted; do 'setupATLAS -d -c ...' to skip default mounts."     
	alrb_optStr="$alrb_optStr -B $alrb_userParentMnt:$alrb_userParentMntCont"
    fi

    alrb_optStr="$alrb_optStr -B $alrb_pwd:/srv"

    if [[ ! -z $ALRB_CONT_REALTMPDIR ]] && [[ "$ALRB_CONT_REALTMPDIR" = "/scratch" ]]; then
	alrb_optStr="$alrb_optStr -B $TMPDIR:/scratch"
    fi

    if [ "$alrb_bindKRB5CCNAME" = "YES" ]; then
	alrb_optStr="$alrb_optStr -B $alrb_krbNameHost:$alrb_krbNameContainer"
    fi

    alrb_cmdRunPayload="singularity $ALRB_CONT_OPTS exec $ALRB_CONT_CMDOPTS"
# versions 2.3.1 and newer have -e option
    if [ $alrb_contVerN -ge 20301 ]; then
	alrb_cmdRunPayload="$alrb_cmdRunPayload -e "
    fi

# writable for build job if labelled as such    
    if [ "$alrb_labelBuild" != "" ]; then
	alrb_cmdRunPayload="$alrb_cmdRunPayload -w "
    fi
    
    if [ "$alrb_cont_payload" = "" ]; then
	alrb_cont_payload="$alrb_shellPath/$ALRB_SHELL"
    fi
    alrb_cmdRunPayload="$alrb_cmdRunPayload $alrb_optStr $alrb_container $alrb_cont_payload"

    export SINGULARITYENV_ENV="$ALRB_CONT_DUMMYHOME/$alrb_rcFile"

    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Singularity: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `which singularity`"
	\echo "ContainerType: $alrb_containerType"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi

    return 0
}


alrb_fn_singularityGuessModifiers()
{

    local alrb_useContainer="$alrb_container"
    if [ "$alrb_pullFromRegistry" = "YES" ]; then
	alrb_useContainer="$alrb_proxyCachedContainer"
    fi
    
    env -u SINGULARITY_BIND -u SINGULARITY_BINDPATH singularity $ALRB_CONT_OPTS exec $ALRB_CONT_CMDOPTS -c -e -H $alrb_contDummyHome:$ALRB_CONT_DUMMYHOME $alrb_useContainer $ALRB_CONT_DUMMYHOME/probeContainer.sh
    return $?
}


alrb_fn_singularityConstructCommandDumb()
{

    # docker container specified explicitly    
    \echo $alrb_containerCandidate | \grep -e "^docker://" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_fn_singularityBuildDockerSandbox
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi
    
    alrb_cmdRunPayload="singularity $ALRB_CONT_OPTS exec $ALRB_CONT_CMDOPTS $alrb_container $ALRB_CONT_RUNPAYLOAD"
    
    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Singularity: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `which singularity`"
	\echo "Note: This is running with -x/--dumb option and bypasses ALRB features."
	\echo "      If you have issues, check first that it works without -x/--dumb"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi
        
    return $?
}


