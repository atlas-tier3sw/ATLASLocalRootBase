#!----------------------------------------------------------------------------
#!
#! apptainerFunctions.sh
#!
#! Code specific to Apptainer that is called by startContainer 
#!
#! Usage:
#!     used by startContainer.sh 
#!
#! History:
#!    04Mar22: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


alrb_fn_apptainerSetEnvVar()
{
    export APPTAINERENV_$1="$2"   
    return 0
}


alrb_fn_apptainerTry()
{
    local alrb_savedPath="$PATH"
    alrb_fn_${ALRB_CONT_SWTYPE}Initialize
    alrb_runTimeErrMsg="Info"
    alrb_fn_${ALRB_CONT_SWTYPE}Version
    if [ $? -ne 0 ]; then
	export PATH="$alrb_savedPath"
	return 64
    fi
    alrb_runTimeErrMsg="Error"
    
    return 0
}


alrb_fn_apptainerInitialize()
{
    alrb_fn_apptainerMigrateSingularityEnvs
    
    alrb_runtimeBindMountOpt="-B"
    alrb_runtimeWorkdirOpt="--pwd "
    alrb_runtimeInitialized="apptainer"
    alrb_runTimeErrMsg="Error"
    alrb_proxyCachedContainer=""
    alrb_pullFromRegistry="NO"
    
    if [ -e $HOME/alrb_override_apptainer.sh ]; then
	source $HOME/alrb_override_apptainer.sh
	return $?
    elif [ -e $alrb_localConfigDir/alrb_apptainer.sh ]; then
	source $alrb_localConfigDir/alrb_apptainer.sh
	return $?
    fi

    return 0
}

	
alrb_fn_apptainerVersion()
{
    local alrb_rc=0
    if [ -e $ALRB_cvmfs_repo/containers/sw/apptainer ]; then
	alrb_fn_apptainerGetCvmfsExe
	alrb_rc=$?
    else
	\echo "Info: apptainer not found (not mounted) cvmfs"
    fi
    if [[ $alrb_rc -ne 0 ]] && [[ ! -z "$ALRB_CONT_SWVERSION" ]]; then
	return 64
    else
	type -p apptainer > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	    \echo "${alrb_runTimeErrMsg}: apptainer not found locally"
	    return 64
	fi
    fi
    
    alrb_contVer="`apptainer --version 2>&1 | \sed -e 's/.* version //g' | \cut -f 1 -d '-'`"
    let alrb_contVerN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_contVer 3 -p`
    
    return 0
}


alrb_fn_apptainerMigrateSingularityEnvs()
{
    local alrb_tmpAr=( `env | \grep -e "^SINGULARITY" | \cut -f 1 -d "="` )
    local alrb_item
    for alrb_item in ${alrb_tmpAr[@]}; do
        local alrb_name=`\echo $alrb_item | \sed -e 's|^SINGULARITY|APPTAINER|g'`
        local alrb_value="${!alrb_item}"	
	if [[ ! -z "${!alrb_name+x}" ]] && [[ "${!alrb_name}" != "$alrb_value" ]] ; then
	   \echo "Warning: $alrb_name (${!alrb_name}) exists"
	   \echo "         different from $alrb_item ($alrb_value)"
	   \echo "         $alrb_name unchanged."
	   continue
	fi
	\echo "Info: Migrating env variable $alrb_item to $alrb_name"
	export $alrb_name="$alrb_value"
	unset $alrb_item
    done

    return 0

}


alrb_fn_apptainerGetCvmfsExe()
{
    
    local alrb_rc=0
    if [ ! -z "$ALRB_CONT_SWVERSION" ]; then
	local alrb_cvmfsApptainerVersion="$ALRB_CONT_SWVERSION"
    elif [ ! -z "$ALRB_cvmfsApptainerVersion" ]; then
	local alrb_cvmfsApptainerVersion="$ALRB_cvmfsApptainerVersion"
    else
	local alrb_cvmfsApptainerVersion="current"
    fi
    local alrb_tmpPath="$ALRB_cvmfs_repo/containers/sw/apptainer/${alrb_archType}-el${ALRB_OSMAJORVER}/$alrb_cvmfsApptainerVersion"
    if [ ! -e "$alrb_tmpPath/bin/apptainer" ]; then
	\echo "Error: apptainer $alrb_cvmfsApptainerVersion executable does not exist on $alrb_tmpPath"
        alrb_fn_cleanup
        exit 64
    fi
    local alrb_tmpRealPath=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_tmpPath`

    alrb_fn_apptainerTestCvmfsExe "$alrb_tmpRealPath/bin"
    alrb_rc=$?
    if [ $alrb_rc -eq 0 ]; then
	export PATH="$alrb_tmpRealPath/bin:$PATH"
    fi
    
    return $alrb_rc
}


alrb_fn_apptainerTestCvmfsExe()
{

    local alrb_rc=0
    local alrb_result
   
    local alrb_apptainerExecPath="$1"
    
    local alrb_testContainer="$ALRB_cvmfs_repo/containers/fs/singularity/${alrb_archType}-busybox"
    if [ -z "$PWD" ]; then
	PWD=`pwd`
    fi

    local alrb_contOpts=""
    if [ ! -z $KRB5CCNAME ]; then
	local alrb_krbType="`\echo $KRB5CCNAME | \cut -f 1 -d ':'`"
	if [[ "$alrb_krbType" = "FILE" ]] || [[ "$alrb_krbType" = "DIR" ]]; then
	    local alrb_krbNameHost="`\echo $KRB5CCNAME | \cut -f 2 -d ':'`"
	    alrb_contOpts="-B $alrb_krbNameHost:$alrb_krbNameHost --env KRB5CCNAME=$KRB5CCNAME"
	fi
    fi

    local alrb_cmd="$alrb_apptainerExecPath/apptainer exec -c $alrb_contOpts -B $PWD:/whatever --pwd /whatever -e $alrb_testContainer id"    
    alrb_result=`eval $alrb_cmd 2>&1`
    alrb_rc=$?
    if [ $alrb_rc -ne 0 ]; then
	\echo "${alrb_runTimeErrMsg}: apptainer from cvmfs will not work ..."
	if [ -e /proc/sys/user/max_user_namespaces ]; then
	    \echo " /proc/sys/user/max_user_namespaces has: `\cat /proc/sys/user/max_user_namespaces`"
	else
	    \echo " /proc/sys/user/max_user_namespaces does not exist."
	fi
	if [ "$alrb_runTimeErrMsg" = "Error" ]; then
	    \echo "command: $alrb_cmd"
	    \echo $alrb_result	
	fi
    fi
    
    return $alrb_rc
}


alrb_fn_apptainerOptsParser()
{
    if [ ! -z "$ALRB_CONT_CMDOPTS" ]; then
	eval set -- "$ALRB_CONT_CMDOPTS"
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-c|--contain|-C|--containall)
		    alrb_containMinimal="YES"
		    alrb_copyProxy="YES"
		    shift
		    ;;
		--pwd)
		    alrb_setContainerPwd="$2"
		    shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi
    
    return 0
    
}

alrb_fn_apptainerBuildDockerSandbox()
{
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} build sandbox"

    alrb_fn_getProxyCachedRegistryURL "$alrb_containerCandidate"
    
    if [ ! -z "$APPTAINER_TMPDIR" ]; then
	\echo "Using (\$APPTAINER_TMPDIR defined) $APPTAINER_TMPDIR for sandbox ..."
    elif [ ! -z "$PILOT_CONTAINER_SANDBOX" ]; then
	\echo "Using (\$PILOT_CONTAINER_SANDBOX) $PILOT_CONTAINER_SANDBOX for sandbox ..."
	export APPTAINER_TMPDIR="$PILOT_CONTAINER_SANDBOX"
    elif [ ! -z "$PILOT_HOME" ]; then
	\echo "Using (\$PILOT_HOME) $PILOT_HOME/apptainerTmp for sandbox ..."
	export APPTAINER_TMPDIR="$PILOT_HOME/apptainerTmp"
    else
	\echo -e "
\033[1m\033[4mAPPTAINER_TMPDIR is undefined\033[0m
 Defining APPTAINER_TMPDIR to point to a non-tmp dir will save time if you
 run the container more than once
" 
	\echo "Container will not be built in a separate stage but in exec ..."
	alrb_pullFromRegistry="YES"
	return 0
    fi

    \echo "Now building container ..."
    mkdir -p $APPTAINER_TMPDIR
        
    local alrb_rc

    local alrb_dirName=`\echo $alrb_containerCandidate | \sed -e 's|docker://||' | \sed -e 's|[^[:alnum:].-]|\_|g'`    
    local alrb_containerDir="$APPTAINER_TMPDIR/$alrb_dirName"
    if [ "$alrb_labelBuild" != "" ]; then
	alrb_containerDir="$APPTAINER_TMPDIR/${alrb_labelBuild}_${alrb_dirName}"
    elif [ "$alrb_labelUse" != "" ]; then
	alrb_containerDir="$APPTAINER_TMPDIR/${alrb_labelUse}_${alrb_dirName}"
	if [ -e $alrb_containerDir/noupdate ]; then
	    alrb_container="$alrb_containerDir/image"
	    return 0
	else
	    \echo "Error: $alrb_containerCandidate label ${alrb_labelUse} does not seem to be built in $alrb_containerDir"
	    return 64
	fi
    fi
    mkdir -p $alrb_containerDir
    if [ -z "$APPTAINER_DISABLE_CACHE" ]; then
	export APPTAINER_DISABLE_CACHE=true
    else
	export APPTAINER_CACHEDIR="$alrb_containerDir/cache"
	mkdir -p $alrb_containerDir/cache
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi
    
    \echo "Locking dir ..." 
    $ATLAS_LOCAL_ROOT_BASE/utilities/createLockFile.sh -f $alrb_containerDir/lockfile -s "$ALRB_CONT_TIMEOUTS" -r "$ALRB_CONT_TIMEOUTR" -d "$ALRB_CONT_TIMEOUTD" -v 
    if [ $? -ne 0 ]; then
	\echo "Error: There may be a concurrent build in progress.  Exiting"
	return 64
    fi
    local alrb_optApptainerBuild="$alrb_optContainerBuilld"
    if [[ -e $alrb_containerDir/image/.singularity.d/runscript ]] || [[ -e $alrb_containerDir/image/.apptainer.d/runscript ]]; then
	alrb_optApptainerBuild="$alrb_optApptainerBuild -u"
	if [ "$alrb_forceBuild" = "YES" ]; then
	    \rm -f $alrb_containerDir/noupdate
	    \rm -f $alrb_containerDir/building
	fi
	if [ -e "$alrb_containerDir/noupdate" ]; then
	    \echo " $alrb_containerDir/noupdate found; will not do 'apptainer build -u'"
	    alrb_container="$alrb_containerDir/image"
	    \echo "Removing lock dir ..."
	    \rm -f $alrb_containerDir/lockfile
	    return 0
	elif [ -e "$alrb_containerDir/building" ]; then
	    \echo "Error: $alrb_containerDir/building found; will exit immediately."
	    return 64
	fi
    else
	\rm -rf $alrb_containerDir/image
    fi
    if [ $? -eq 0 ]; then
	local alrb_cmd
	alrb_optApptainerBuild="$alrb_optApptainerBuild --fix-perms"
	alrb_cmd="apptainer $ALRB_CONT_OPTS build $alrb_optApptainerBuild --sandbox $alrb_containerDir/image $alrb_proxyCachedContainer"
	apptainer --version
	\echo " from `type -p apptainer`"
	\echo "$alrb_cmd"
	\echo " suppressing \"warn xattr{*} ignoring forbidden xattr\" warnings"
	alrb_fn_printMsg 1 "$alrb_cmd"
	touch $alrb_containerDir/building
	eval $alrb_cmd 2>&1 | \sed -e '/warn xattr{.*} ignoring forbidden xattr/d'
	alrb_rc=${PIPESTATUS[0]}
	alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} finished building sandbox"
	\rm -f $alrb_containerDir/building
	\echo "Removing lock dir ..."
	\rm -f $alrb_containerDir/lockfile
	if [ $alrb_rc -ne 0 ]; then
	    return 64
	fi
	alrb_container="$alrb_containerDir/image"
	touch $alrb_containerDir/noupdate
    else
	\echo "Error: Cannot get lockfile $alrb_containerDir/lockfile"
	return 64
    fi

    return 0
}


alrb_fn_apptainerSetContainer()
{
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} determine container"
    local alrb_tmpVal

    alrb_container=$alrb_containerCandidate
    alrb_containerAsSingularity="$alrb_container"

# docker container specified explicitly    
    \echo $alrb_containerCandidate | \grep -e "^docker://" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_fn_apptainerBuildDockerSandbox
	return $?
    fi

    if [ -e $alrb_container ]; then
# these allow for direct paths for containers to be specified
	alrb_tmpVal=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_container`	
	if [[ -f $alrb_tmpVal ]] && [[ "${alrb_tmpVal##*.}" = "img" ]]; then
	    return 
	elif [[ -d $alrb_tmpVal/.singularity.d ]] || [[ -d $alrb_tmpVal/.apptainer.d ]]; then	    
	    return 0
	fi
    fi

# search for unpacked dir images ...
    local alrb_suffixAr=( "-${alrb_archType}" "" )
    local alrb_tmpAr=( )
    if [ ! -z "$ALRB_CONT_UNPACKEDDIR" ]; then
	alrb_tmpAr=( $ALRB_CONT_UNPACKEDDIR )
    fi
    if [ ! -z "$ALRB_cvmfs_unpacked_repo" ]; then
	alrb_tmpAr=( ${alrb_tmpAr[@]}
		     "$ALRB_cvmfs_unpacked_repo"
		     "$ALRB_cvmfs_unpacked_repo/registry.cern.ch"
		     "$ALRB_cvmfs_unpacked_repo/gitlab-registry.cern.ch"
		     "$ALRB_cvmfs_unpacked_repo/registry.hub.docker.com"
		   )
    fi
    local alrb_unpackedDir
    local alrb_suffix
    for alrb_unpackedDir in "${alrb_tmpAr[@]}"; do
	for alrb_suffix in "${alrb_suffixAr[@]}"; do
	    alrb_tmpVal="$alrb_unpackedDir/$alrb_containerCandidate$alrb_suffix"
	    if [[ -d $alrb_tmpVal/.singularity.d ]] || [[ -d $alrb_tmpVal/.apptainer.d ]]; then
		alrb_container="$alrb_tmpVal"
		return 0
	    elif [[ -f $alrb_tmpVal ]] && [[ "${alrb_tmpVal##*.}" = "img" ]]; then
		alrb_container="$alrb_tmpVal"
		return 0
	    fi
	done
    done

    # at this point, these are default containers and should not have a '/'
    \echo $alrb_containerCandidate | \grep -e "/" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        \echo "Error: $alrb_containerCandidate is not found as directory"
        return 64
    fi
    
    if [ "$alrb_imageType" = "images" ]; then
	local alrb_singRepo="$ALRB_cvmfs_repo/containers/images/singularity"
	local alrb_imageSuffix=".img"
    else
	local alrb_singRepo="$ALRB_cvmfs_repo/containers/fs/singularity"
	local alrb_imageSuffix=""
    fi

    local alrb_oldifs="$IFS"
    IFS=$'\n'
    local alrb_defaultContAr=(
	'slc5|atlas-default|-e "slc5"'
	'centos6|atlas-default|-e "sl6" -e "slc6" -e "centos6" -e "rhel6"'
	'centos7|atlas-default|-e "sl7" -e "slc7" -e "centos7" -e "rhel7"'
	'almalinux8|atlas-default|-e "sl8" -e "slc8" -e "centos8" -e "centos8s" -e "rhel8" -e "almalinux8" -e "alma8"'
	'almalinux9|atlas-default|-e "sl9" -e "slc9" -e "centos9" -e "almalinux9" -e "alma9" -e "rhel9" -e "el9"'
    )

    local alrb_defaultCont
    for alrb_defaultCont in ${alrb_defaultContAr[@]}; do
	local alrb_queryCmd="\echo $alrb_containerCandidate | \grep -i `\echo $alrb_defaultCont | \cut -f 3 -d '|'`"
	alrb_tmpVal=`eval $alrb_queryCmd 2>&1`
	if [ $? -eq 0 ]; then
	    alrb_containerType="`\echo  $alrb_defaultCont | \cut -f 2 -d '|'`"
	    local alrb_contNamePrefix=`\echo $alrb_defaultCont | \cut -f 1 -d '|'`
	    alrb_containerAsSingularity="$alrb_singRepo/${alrb_archType}-${alrb_contNamePrefix}${alrb_imageSuffix}"
	    alrb_container="$alrb_containerAsSingularity"
	    if [ ! -z $ALRB_CONT_DEFAULTCONTDIR ]; then
		alrb_tmpVal="`\echo $alrb_containerAsSingularity | \sed -e 's|'$alrb_singRepo'|'$ALRB_CONT_DEFAULTCONTDIR'|g'`" 
		if [ -e $alrb_tmpVal ]; then
		    alrb_container="$alrb_tmpVal"
		fi
	    fi		     	
	    
	    if [ "$alrb_contNamePrefix" = "almalinux9" ]; then		
		\echo "$alrb_patch" | \grep -e ",none," > /dev/null 2>&1
		if [ $? -ne 0 ]; then
		    alrb_patch="$alrb_patch,openssh,"
		fi
	    fi
	    
	    break
	fi
    done
    IFS="$alrb_oldifs"
    
    return 0
}


alrb_fn_apptainerPatchBindContainer()
{    
    
    \echo "$alrb_patch" | \grep -e ",none," > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	return 0
    fi

    \echo "$alrb_patch" | \grep -e ",openssh," > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	# fix el9 default containers for openssh issue
	# applies only to apptainer unpacked default el9 containers on cvmfs
	# https://its.cern.ch/jira/browse/ATLINFR-5092
    
	local alrb_patchDir="$ATLAS_LOCAL_ROOT_BASE/containerPatch/el9/current"
	if [ -d $alrb_patchDir ]; then
	    # this has to be owned by the user ...
	    \mkdir -p $alrb_contDummyHome/patchDir
	    \cp -LR $alrb_patchDir/* $alrb_contDummyHome/patchDir/
    	    \find  $alrb_contDummyHome/patchDir/ -type f  -exec chmod go-w {} \;
	    alrb_patchBindStr="$alrb_patchBindStr -B $alrb_contDummyHome/patchDir/50-redhat.conf:/etc/ssh/ssh_config.d/50-redhat.conf -B $alrb_contDummyHome/patchDir/openssh.config:/etc/crypto-policies/back-ends/openssh.config"
	fi
    fi
    
    return 0
}


alrb_fn_apptainerGetPathLookup()
{
    
    ALRB_CONT_SED2HOST=""
    ALRB_CONT_SED2CONT=""

    local alrb_bindMountAr=()

    local alrb_isNestedContainer=""
    if [ ! -z "$APPTAINER_ENVIRONMENT" ]; then
	if [[ -d "/.singularity.d/env" ]] || [[  -d "/.apptainer.d/env" ]]; then
	    alrb_isNestedContainer="YES"
	fi
    fi
	 
    local alrb_bindStr=""
    if [ ! -z "$APPTAINER_BIND" ]; then
	if [ "$alrb_containerSkipCvmfs" = "YES" ]; then
	    \echo $APPTAINER_BIND | \grep -e "/cvmfs" > /dev/null 2>&1
	    if [ $? -eq 0 ]; then
		if [ "$alrb_isNestedContainer" != "YES" ]; then
		    \echo "Warning: \$APPTAINER_BIND defined has /cvmfs which should not be mounted."
		else
		    \echo "Warning: inhertited \$APPTAINER_BIND defined has /cvmfs.
	  /cvmfs will be removed from it as it should not be mounted."
		    local alrb_tmpVal=`\echo $APPTAINER_BIND | \tr ',' '\n' | sed -e '/^\/cvmfs/d' | tr '\n' ',' | sed -e 's|,$||g' -e 's|[[:space:]]||g'`
		    if [ "$alrb_tmpVal" != "" ]; then
			export APPTAINER_BIND="$alrb_tmpVal"
		    else
			unset APPTAINER_BIND
		    fi
		fi
	    fi
	fi
	alrb_bindStr="$alrb_bindStr,$APPTAINER_BIND"
    fi

    if [ ! -z "$APPTAINER_BINDPATH" ]; then
	if [ "$alrb_containerSkipCvmfs" = "YES" ]; then
	    \echo $APPTAINER_BINDPATH | \grep -e "/cvmfs" > /dev/null 2>&1
	    if [ $? -eq 0 ]; then
		if [ "$alrb_isNestedContainer" != "YES" ]; then
		    \echo "Warning: \$APPTAINER_BINDPATH defined has /cvmfs which should not be mounted."
		else
		    \echo "Warning: inhertited \$APPTAINER_BINDPATH defined has /cvmfs.
	  /cvmfs will be removed from it as it should not be mounted."
		    local alrb_tmpVal=`\echo $APPTAINER_BINDPATH | \tr ',' '\n' | sed -e '/^\/cvmfs/d' | tr '\n' ',' | sed -e 's|,$||g' -e 's|[[:space:]]||g'`
		    if [ "$alrb_tmpVal" != "" ]; then
			export APPTAINER_BINDPATH="$alrb_tmpVal"
		    else
			unset APPTAINER_BINDPATH
		    fi
		fi
	    fi
	fi
	alrb_bindStr="$alrb_bindStr,$APPTAINER_BINDPATH"
    fi
    
    local alrb_confFile="`apptainer buildcfg |\grep -e APPTAINER_CONFDIR | \cut -f 2 -d "="`/apptainer.conf"
    if [ ! -e $alrb_confFile ]; then
	\echo "Warning: unable to find apptainer conf file but will continue."
    else
	local alrb_tmpVal="`\grep -e "^[[:space:]]*bind path" $alrb_confFile  | \cut -f 2 -d "=" | \tr '\n' ','`"
	if [[ "$alrb_containerSkipCvmfs" = "YES" ]] && [[ "$alrb_containMinimal" != "YES" ]]; then
	    \echo "$alrb_tmpVal" | \grep -e "/cvmfs" > /dev/null 2>&1
	    if [ $? -eq 0 ]; then
		\echo "Error: Sigularity config file ($alrb_confFile) has /cvmfs.
       /cvmfs may be mounted because of this; but continue despite this error."
	    fi
	fi
	alrb_bindStr="$alrb_bindStr,$alrb_tmpVal"
    fi   

    if [ ! -z "$ALRB_CONT_STARTCMD" ]; then
	eval set -- "$ALRB_CONT_STARTCMD"
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-B|--bind|-H|--home)
		    local alrb_tmp1=`\echo $2 | \cut -f 1 -d ":"`
		    local alrb_tmp2=`\echo $2 | \cut -f 2 -d ":"`
		    alrb_bindMountAr=( ${alrb_bindMountAr[@]} $alrb_tmp2 )
		    alrb_fn_buildSedPath "$alrb_tmp1" "$alrb_tmp2"
		    shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi

    local alrb_tmpAr=( `\echo $alrb_bindStr | \sed -e 's/,/ /g'` )
    local alrb_item
    for alrb_item in "${alrb_tmpAr[@]}"; do
	local alrb_tmp1=`\echo $alrb_item | \cut -f 1 -d ":"`
	local alrb_tmp2=`\echo $alrb_item | \cut -f 2 -d ":"`
	alrb_bindMountAr=( ${alrb_bindMountAr[@]} $alrb_tmp2 )
	alrb_fn_buildSedPath "$alrb_tmp1" "$alrb_tmp2"
    done
    
    if [ "$ALRB_CONT_SED2HOST" != "" ]; then
	export ALRB_CONT_SED2HOST
    fi
    if [ "$ALRB_CONT_SED2CONT" != "" ]; then
	export ALRB_CONT_SED2CONT
    fi
    
    if [ "$alrb_labelBuild" != "" ]; then
	local alrb_item
	for alrb_item in "${alrb_bindMountAr[@]}"; do
	    \mkdir -p $alrb_container/$alrb_item
	done
    fi
    
    return 0    
}


alrb_fn_apptainerConstructCommand()
{
    if [ -z $APPTAINER_CACHEDIR ]; then
	export APPTAINER_CACHEDIR="`pwd`/apptainer"
    fi

    alrb_fn_apptainerPatchBindContainer
    
    local alrb_optStr="$alrb_patchBindStr -H $alrb_contDummyHome:$ALRB_CONT_DUMMYHOME"

    if [ ! -z $ALRB_CONT_DUMMYALRB ]; then 
	alrb_optStr="$alrb_optStr -B $ATLAS_LOCAL_ROOT_BASE:$ALRB_CONT_DUMMYALRB"
    fi
    
    if [ "$alrb_containerSkipCvmfs" != "YES" ]; then
	\echo "Info: /cvmfs mounted; do 'setupATLAS -d -c ...' to skip default mounts."
	alrb_optStr="$alrb_optStr -B $alrb_cvmfs_mount:/cvmfs"
    fi	

    if [ "$alrb_containerSkipHome" != "YES" ]; then
	\echo "Info: \$HOME mounted; do 'setupATLAS -d -c ...' to skip default mounts."     
	alrb_optStr="$alrb_optStr -B $alrb_userParentMnt:$alrb_userParentMntCont"
    fi

    alrb_optStr="$alrb_optStr -B $alrb_pwd:/srv"

    if [[ ! -z $ALRB_CONT_REALTMPDIR ]] && [[ "$ALRB_CONT_REALTMPDIR" = "/scratch" ]]; then
	alrb_optStr="$alrb_optStr -B $TMPDIR:/scratch"
    fi

    if [ "$alrb_bindKRB5CCNAME" = "YES" ]; then
	alrb_optStr="$alrb_optStr -B $alrb_krbNameHost:$alrb_krbNameContainer"
    fi
    
    alrb_cmdRunPayload="apptainer $ALRB_CONT_OPTS exec $ALRB_CONT_CMDOPTS"
    alrb_cmdRunPayload="$alrb_cmdRunPayload -e "

# writable for build job if labelled as such    
    if [ "$alrb_labelBuild" != "" ]; then
	alrb_cmdRunPayload="$alrb_cmdRunPayload -w "
    fi
    
    if [ "$alrb_cont_payload" = "" ]; then
	alrb_cont_payload="$alrb_shellPath/$ALRB_SHELL"
    fi
    alrb_cmdRunPayload="$alrb_cmdRunPayload $alrb_optStr $alrb_container $alrb_cont_payload"

    export APPTAINERENV_ENV="$ALRB_CONT_DUMMYHOME/$alrb_rcFile"
    
    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Apptainer: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `type -p apptainer`"
	\echo "ContainerType: $alrb_containerType"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi

    return 0
}


alrb_fn_apptainerGuessModifiers()
{

    local alrb_useContainer="$alrb_container"
    if [ "$alrb_pullFromRegistry" = "YES" ]; then
	alrb_useContainer="$alrb_proxyCachedContainer"
    fi

    env -u APPTAINER_BIND -u APPTAINER_BINDPATH apptainer $ALRB_CONT_OPTS exec $ALRB_CONT_CMDOPTS -c -e -H $alrb_contDummyHome:$ALRB_CONT_DUMMYHOME $alrb_useContainer $ALRB_CONT_DUMMYHOME/probeContainer.sh
    return $?
}


alrb_fn_apptainerConstructCommandDumb()
{

    # docker container specified explicitly    
    \echo $alrb_containerCandidate | \grep -e "^docker://" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_fn_apptainerBuildDockerSandbox
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi
    
    alrb_cmdRunPayload="apptainer $ALRB_CONT_OPTS exec $ALRB_CONT_CMDOPTS $alrb_container $ALRB_CONT_RUNPAYLOAD"
    
    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Apptainer: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `type -p apptainer`"
	\echo "Note: This is running with -x/--dumb option and bypasses ALRB features."
	\echo "      If you have issues, check first that it works without -x/--dumb"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi
        
    return $?
}


