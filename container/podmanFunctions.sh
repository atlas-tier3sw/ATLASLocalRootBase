#!----------------------------------------------------------------------------
#!
#! podmanFunctions.sh
#!
#! Code specific to Podman that is called by startContainer
#!
#! Usage:
#!     used by startContainer.sh
#!
#! History:
#!    11Dec17: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_fn_podmanInitialize()
{
    alrb_runtimeBindMountOpt="-v"
    alrb_runtimeWorkdirOpt="-w "
    alrb_runtimeInitialized="podman"
    alrb_runTimeErrMsg="Error"
    alrb_defaultPlatform=""
    alrb_localhostPrefix=""
    alrb_platformName=""
    alrb_containerNameTag=""
    
    alrb_runtimeExeName="podman"
    for alrb_runtimeAltStr in "${alrb_runtimeAltAr[@]}"; do
	alrb_runtimeAlt=`\echo $alrb_runtimeAltStr | \cut -f 1 -d "|" | \tr '[:upper:]' '[:lower:]'`
	if [ "$alrb_runtimeAlt" = "${ALRB_CONT_SWTYPE}" ]; then
	    alrb_runtimeExeName="`\echo $alrb_runtimeAltStr | \cut -f 2- -d "|"`"
	    break
	fi
    done    
    
    alrb_fn_podmanGetPlatform
    if [ $? -ne 0 ]; then
        return 64
    fi

    local alrb_recommendedMessage=""
    local alrb_tmpBuffer
    if [ -e $HOME/.config/containers/storage.conf ]; then
	alrb_tmpBuffer="`\sed -e 's/[[:space:]]//g' -e '/^$/d' -e '/^#/d' $HOME/.config/containers/storage.conf`"
    else
	alrb_recommendedMessage="YES"
    fi
    if [ "$alrb_recommendedMessage" != "YES" ]; then
	\echo "$alrb_tmpBuffer" | \awk '/^\[storage.options.overlay\]/,/^mount_program="\/usr\/bin\/fuse-overlayfs"/' 2>&1 | \grep -e '^mount_program="/usr/bin/fuse-overlayfs"' > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	    alrb_recommendedMessage="YES"
	fi
    fi
    if [ "$alrb_recommendedMessage" != "YES" ]; then
	\echo "$alrb_tmpBuffer" | \awk '/^\[storage\]/,/^driver="overlay"/' 2>&1 | \grep -e 'driver="overlay"' > /dev/null 2>&1 
	if [ $? -ne 0 ]; then
	    alrb_recommendedMessage="YES"
	fi
    fi
    if [ "$alrb_recommendedMessage" = "YES" ]; then
	\echo "
     *      *      *   RECOMMENDATION  ONLY   *     *      *
Info : Your \$HOME/.config/containers/storage.conf is missing one of more
       of these to avoid delays in the startup.  If you see a slow startup,
       try creating \$HOME/.config/containers/storage.conf with:

[storage]
driver = \"overlay\"

[storage.options.overlay]
mount_program = \"/usr/bin/fuse-overlayfs\"
     *      *      *      *      *

"
	
    fi

    if [ -e $HOME/alrb_override_podman.sh ]; then
	source $HOME/alrb_override_podman.sh
	return $?
    elif [ -e $alrb_localConfigDir/alrb_podman.sh ]; then
	source $alrb_localConfigDir/alrb_podman.sh
	return $?
    fi

    return 0
}


alrb_fn_podmanGetPlatform()
{

    local alrb_platform0="$alrb_platform"
    local alrb_setDefaultPlatform="YES"
    
    \echo $ALRB_CONT_CMDOPTS | \grep -e "--platform " > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_platform0=`\echo $ALRB_CONT_CMDOPTS | \sed -e 's|.*--platform ||' | \cut -f 1 -d " "`
	alrb_setDefaultPlatform="NO"
    fi

    if [ "$alrb_platform0" = "linux/arm64" ]; then
	alrb_defaultPlatform="--platform $alrb_platform"	
	alrb_archType="aarch64"
    elif [ "$alrb_platform0" = "linux/amd64" ]; then
	alrb_defaultPlatform="--platform $alrb_platform"	
	alrb_archType="x86_64"	
    elif [[ "$ATLAS_LOCAL_ROOT_ARCH" = "arm64-MacOS" ]] || [[ "$ATLAS_LOCAL_ROOT_ARCH" = "aarch64-Linux" ]]; then 
	alrb_defaultPlatform="--platform linux/arm64"
    else
	alrb_defaultPlatform="--platform linux/amd64"
    fi

    if [ "$alrb_setDefaultPlatform" = "NO" ]; then
	alrb_defaultPlatform=""
    fi

    if [ "$alrb_defaultPlatform" != "" ]; then
	alrb_platformName="_`\echo $alrb_defaultPlatform | \sed -e 's|--platform ||g' -e 's|/|-|g'`"
    fi
        
    return 0
}


alrb_fn_podmanVersion()
{
    alrb_contVer=`$alrb_runtimeExeName version 2>&1`
    if [ $? -ne 0 ]; then
	\echo "Error: Podman is not installed and so cannot create container" 1>&2
	return 64
    fi
    alrb_contVer=`$alrb_runtimeExeName version --format '{{.Client.Version}}' | \cut -f 1 -d "-"`

    return 0
}


alrb_fn_podmanOptsParser()
{
    if [ ! -z "$ALRB_CONT_CMDOPTS" ]; then
	eval set -- "$ALRB_CONT_CMDOPTS"
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-w|--workdir)
		    alrb_setContainerPwd="$2"
		    shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi
    
    return 0
    
}


alrb_fn_podmanSetContainer()
{
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} determine container"
    local alrb_tmpVal

    let alrb_podmanBuildPass=0
    
    alrb_container="$alrb_containerCandidate"

# docker container specified explicitly    
    \echo $alrb_containerCandidate | \grep -e "^docker://" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	\echo "Error: container name has docker:// but podman URL should be docker.io/"
	\echo "       Please fix and retry."
	return 64
    fi

# dir like path specified; assume it is docker / podman
    \echo $alrb_containerCandidate | \grep -e "/" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_container="$alrb_containerCandidate"
	alrb_containerAsSingularity="docker://$alrb_container"
	return 0
    fi

    local alrb_singRepo="$ALRB_cvmfs_repo/containers/fs/singularity"

    local alrb_oldifs="$IFS"
    IFS=$'\n'
    local alrb_defaultContAr=(
	'slc5|atlas-default|-e "slc5"'
	'centos6|atlas-default|-e "sl6" -e "slc6" -e "centos6" -e "rhel6"'
	'centos7|atlas-default|-e "sl7" -e "slc7" -e "centos7" -e "rhel7"'
	'almalinux8|atlas-default|-e "sl8" -e "slc8" -e "centos8" -e "centos8s" -e "rhel8" -e "almalinux8" -e "alma8"'
	'almalinux9|atlas-default|-e "sl9" -e "slc9" -e "centos9" -e "almalinux9" -e "alma9" -e "rhel9" -e "el9"'
    )

    local alrb_defaultCont
    for alrb_defaultCont in ${alrb_defaultContAr[@]}; do
	local alrb_queryCmd="\echo $alrb_containerCandidate | \grep -i `\echo $alrb_defaultCont | \cut -f 3 -d '|'`"
	alrb_tmpVal=`eval $alrb_queryCmd 2>&1`
	if [ $? -eq 0 ]; then
	    alrb_containerType="`\echo  $alrb_defaultCont | \cut -f 2 -d '|'`"
	    local alrb_contNamePrefix=`\echo $alrb_defaultCont | \cut -f 1 -d '|'`
	    alrb_containerAsSingularity="$alrb_singRepo/${alrb_archType}-${alrb_contNamePrefix}"
	    if [ -z $ALRB_CONT_DEFAULTCONTREPO ]; then
		alrb_container="registry.cern.ch/atlasadc/atlas-grid-$alrb_contNamePrefix"
	    else
		alrb_container="$ALRB_CONT_DEFAULTCONTREPO/atlasadc/atlas-grid-$alrb_contNamePrefix"
	    fi
	    break
	fi
    done
    IFS="$alrb_oldifs"
    
    return 0
}


alrb_fn_podmanJoinRunning()
{
    
    local let alrb_rc=0    
    local alrb_result
    alrb_result=`\echo $ALRB_CONT_CONDUCT | \grep -e ",podmanJoin,"`
    if [ $? -eq 0 ]; then
	alrb_result=`$alrb_runtimeExeName ps -f ancestor=$alrb_containerNameTag -f status=running --format "{{.Names}}" | env LC=ALL \sort | \tail -n 1`
	if [[ $? -eq 0 ]] && [[ "$alrb_result" != "" ]]; then
	    \echo "Podman: Join already running $alrb_result"
	    \echo "        Remember that if you exit that container, this session will die"
	    alrb_joinedRunningContainer="YES"
	    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} joining running container $alrb_result"
	    $alrb_runtimeExeName exec -it $alrb_result $alrb_shellPath/$ALRB_SHELL
	    alrb_rc=$?
	fi
    fi

    return $alrb_rc
}


alrb_fn_podmanBuild()
{

    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} build"
    
    local alrb_rc

    let alrb_podmanBuildPass+=1

    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} build ($alrb_podmanBuildPass)"
    
    local alrb_optPodmanBuild="$alrb_optContainerBuilld"
    
    local alrb_podmanContDir="$alrb_contWorkdir/files/$alrb_container"
    alrb_podmanContDir=`\echo $alrb_podmanContDir | sed -e 's|docker://|docker.io|g'`
    mkdir -p $alrb_podmanContDir

    local alrb_proxyCachedContainer
    alrb_fn_getProxyCachedRegistryURL "$alrb_container"
    \rm -f ${alrb_podmanContDir}/Dockerfile.new
    \cat << EOF > ${alrb_podmanContDir}/Dockerfile.new
FROM $alrb_proxyCachedContainer

USER root

# First one works for RHEL, Ubuntu, SUSE; second one is in case of uid conflict
# Third one for dash, ash
RUN groupadd -g $alrb_gid $alrb_whoami || groupadd $alrb_whoami || addgroup -g $alrb_gid $alrb_whoami
RUN useradd --no-create-home --home-dir $ALRB_CONT_DUMMYHOME --uid $alrb_uid --gid $alrb_whoami --shell $alrb_shellPath/$ALRB_SHELL $alrb_whoami || useradd --no-create-home --home-dir $ALRB_CONT_DUMMYHOME --shell $alrb_shellPath/$ALRB_SHELL $alrb_whoami || adduser -D -H -h $ALRB_CONT_DUMMYHOME -u $alrb_uid -s $alrb_shellPath/$ALRB_SHELL -G $alrb_whoami $alrb_whoami 

#RUN mkdir -p $ALRB_CONT_DUMMYHOME $ALRB_CONT_DUMMYALRB /srv /scratch /tmp /cvmfs /home $alrb_targetMount

EOF

    if [ "$alrb_buildFile" != "" ]; then
	\cat $alrb_buildFile >> ${alrb_podmanContDir}/Dockerfile.new 
    fi
    
    \cat << EOF >> ${alrb_podmanContDir}/Dockerfile.new

USER $alrb_whoami

CMD [ "$alrb_shellPath/$ALRB_SHELL" ]

ENTRYPOINT [ "" ]

EOF

    if [[ "$alrb_podmanBuildPass" -gt 1 ]] && [[ -e ${alrb_podmanContDir}/Dockerfile ]]; then
	diff ${alrb_podmanContDir}/Dockerfile ${alrb_podmanContDir}/Dockerfile.new > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    return 0
	fi
    fi
    \mv ${alrb_podmanContDir}/Dockerfile.new ${alrb_podmanContDir}/Dockerfile
    if [ "$alrb_buildFile" != "" ]; then
	\echo "     * * * modified user Dockerfile * * * "
	\cat ${alrb_podmanContDir}/Dockerfile
	\echo "                   * * * "
    fi

    alrb_containerNameTag="$alrb_containerName$alrb_platformName"
    alrb_cmd="$alrb_runtimeExeName $ALRB_CONT_OPTS build $alrb_optPodmanBuild $alrb_defaultPlatform --pull $alrb_QuietOpt -t $alrb_containerNameTag ${alrb_podmanContDir}"
    \echo "$alrb_cmd"
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} $alrb_cmd"
    eval $alrb_cmd 2>&1
    alrb_rc=$?

    alrb_localhostPrefix="localhost/"

    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} finished build"
    
    return $alrb_rc
}


alrb_fn_podmanGetPathLookup()
{

    ALRB_CONT_SED2HOST=""
    ALRB_CONT_SED2CONT=""

    if [ ! -z "$ALRB_CONT_STARTCMD" ]; then
	eval set -- "$ALRB_CONT_STARTCMD"
	local alrb_item
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-v|--volume)
		    local alrb_tmp1=`\echo $2 | \cut -f 1 -d ":"`
		    local alrb_tmp2=`\echo $2 | \cut -f 2 -d ":"`
		    alrb_fn_buildSedPath "$alrb_tmp1" "$alrb_tmp2"
		    shift 2
		    ;;
		--mount)
		    local alrb_source=""
		    local alrb_target=""
		    for alrb_item in `\echo $2 | \sed -e 's|,| |g'`; do
			local alrb_tmp1=`\echo $alrb_item | \cut -f 1 -d "="`
			local alrb_tmp2=`\echo $alrb_item | \cut -f 2 -d "="`
			if [[ "$alrb_tmp1" = "source" ]] || [[ "$alrb_tmp1" = "src" ]] ; then
			    alrb_source="$alrb_tmp2"
			elif [[ "$alrb_tmp1" = "target" ]] || [[ "$alrb_tmp1" = "destination" ]] || [[ "$alrb_tmp1" = "dst" ]]; then
			    alrb_target="$alrb_tmp2"
			fi
		    done
		    if [[ "$alrb_source" != "" ]] && [[ "$alrb_target" != "" ]]; then
			alrb_fn_buildSedPath "$alrb_source" "$alrb_target"
		    fi
		    shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi

    if [ "$ALRB_CONT_SED2HOST" != "" ]; then
	export ALRB_CONT_SED2HOST
    fi
    if [ "$ALRB_CONT_SED2CONT" != "" ]; then
	export ALRB_CONT_SED2CONT
    fi
    
    return 0    
}


alrb_fn_podmanConstructCommand()
{

    # docker does not handle aliases properly on MacOS so fix this ...
    if [ "$alrb_cvmfs_mount" != "" ]; then
	alrb_cvmfs_mount=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_cvmfs_mount`
    fi
    
    local alrb_targetMount=""
    local alrb_optStr="--mount \"type=bind,source=$alrb_contDummyHome,target=$ALRB_CONT_DUMMYHOME\""

    if [ ! -z $ALRB_CONT_DUMMYALRB ]; then 
	alrb_optStr="$alrb_optStr --mount \"type=bind,source=$ATLAS_LOCAL_ROOT_BASE,target=$ALRB_CONT_DUMMYALRB\""
    fi
    
    if [ "$alrb_containerSkipCvmfs" != "YES" ]; then
	\echo "Info: /cvmfs mounted; do 'setupATLAS -d -c ...' to skip default mounts."
	local alrb_bindProp=""
	local alrb_tmpVal
	if [ "$ALRB_OSTYPE" != "MacOSX" ]; then
	    alrb_tmpVal="`findmnt -n /cvmfs 2>&1`"
	    if [ $? -eq 0 ]; then
		\echo $alrb_tmpVal | \grep -e " autofs " > /dev/null 2>&1
		if [ $? -eq 0 ]; then
		    alrb_bindProp=",bind-propagation=shared"
		fi
	    else
		alrb_tmpVal="`\cat /proc/1/mountinfo 2>&1 | \grep cvmfs.atlas.cern.ch | \grep -e 'shared'`"
		if [ "$alrb_tmpVal" != "" ]; then
		    alrb_bindProp=",bind-propagation=shared"
		fi
	    fi
	fi
	# need to do this for each repo to avoid "Too many levels of symbolic links" error
	local alrb_thisRepo
	for alrb_thisRepo in ${alrb_cvmfsRepoAr[@]}; do
	    alrb_optStr="$alrb_optStr --mount \"type=bind,source=$alrb_cvmfs_mount/$alrb_thisRepo,target=/cvmfs/$alrb_thisRepo,readonly,consistency=cached$alrb_bindProp\""
	done

	# special cvmfs repo that is available only to certain sites
	alrb_thisRepo="projects.cern.ch"
	local alrb_domain=`hostname -d`
	\echo $alrb_domain | \grep -e "cern\.ch" > /dev/null 2>&1
	if [[ $? -eq 0 ]] || [[ -d $alrb_cvmfs_mount/$alrb_thisRepo/lcg ]]; then
	    alrb_optStr="$alrb_optStr --mount \"type=bind,source=$alrb_cvmfs_mount/$alrb_thisRepo,target=/cvmfs/$alrb_thisRepo,readonly,consistency=cached$alrb_bindProp\""
	fi
	
    fi

    if [ "$alrb_containerSkipHome" != "YES" ]; then
	\echo "Info: \$HOME mounted; do 'setupATLAS -d -c ...' to skip default mounts."
    	alrb_optStr="$alrb_optStr --mount \"type=bind,source=$alrb_userParentMnt,target=$alrb_userParentMntCont\""
	if [[ "$alrb_userParentMntCont" != "/home" ]] && [[ "$alrb_userParentMntCont" != "/" ]]; then
	    alrb_targetMount="$alrb_targetMount $alrb_userParentMntCont"
	fi				    
    fi
    
    alrb_optStr="$alrb_optStr --mount \"type=bind,source=$alrb_pwd,target=/srv\""
    
    if [[ ! -z $ALRB_CONT_REALTMPDIR ]] && [[ "$ALRB_CONT_REALTMPDIR" = "/scratch" ]]; then
	alrb_optStr="$alrb_optStr --mount \"type=bind,source=$TMPDIR,target=/scratch\""
    fi

    if [ "$alrb_bindKRB5CCNAME" = "YES" ]; then
	alrb_optStr="$alrb_optStr -v $alrb_krbNameHost:$alrb_krbNameContainer:Z"
    fi
        
    # need to possibly rebuild in case we changed shells etc from first pass.
    alrb_fn_podmanBuild
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    alrb_podmanName="$alrb_containerName-`date +%s`" 
    alrb_cmdRunPayload="$alrb_runtimeExeName $ALRB_CONT_OPTS run $ALRB_CONT_CMDOPTS $alrb_defaultPlatform -e \"ENV=$ALRB_CONT_DUMMYHOME/$alrb_rcFile\""

    alrb_cmdRunPayload="$alrb_cmdRunPayload $alrb_optStr --mount \"type=bind,source=/tmp,target=/tmp\" -it --rm --userns=keep-id --security-opt label=disable --name $alrb_podmanName $alrb_localhostPrefix$alrb_containerNameTag"
    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Podman: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `which $alrb_runtimeExeName`"
	\echo "ContainerType: $alrb_containerType"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi
    
    return 0
}


alrb_fn_podmanGuessModifiers()
{

    $alrb_runtimeExeName $ALRB_CONT_OPTS run $ALRB_CONT_CMDOPTS --mount "type=bind,source=$alrb_contDummyHome,target=$ALRB_CONT_DUMMYHOME" -it --rm --userns=keep-id --security-opt label=disable $alrb_localhostPrefix$alrb_containerNameTag /bin/sh $ALRB_CONT_DUMMYHOME/probeContainer.sh
    return $?
}


alrb_fn_podmanConstructCommandDumb()
{
    
    alrb_cmdRunPayload="$alrb_runtimeExeName $ALRB_CONT_OPTS run $ALRB_CONT_CMDOPTS $alrb_container $ALRB_CONT_RUNPAYLOAD"
    
    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Podman: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `which $alrb_runtimeExeName`"
	\echo "Note: This is running with -x/--dumb option and bypasses ALRB features."
	\echo "      If you have issues, check first that it works without -x/--dumb"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi

    return $?

}
