#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! printContainerList.sh
#!
#! prints out a list of containers and URL for the unpacked cvmfs dir
#!
#! Usage:
#!     printContainerList.sh
#!
#! History:
#!   29Jan24: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

if [ -z $ALRB_NOCOLORPRINT ]; then
    alrb_colReset="\033[0m"
    alrb_colRed="\033[1;31m"
    alrb_colCyan="\033[1;36m"
    alrb_colBlue="\033[1;34m"
else
    alrb_colReset=""
    alrb_colRed=""
    alrb_colCyan=""
    alrb_colBlue=""
fi

alrb_fn_printInstructions()
{
    \echo -e " 
These are containers available on cvmfs unpacked repository; these are
installed from other registries.  

 Note: ${alrb_colRed}cvmfs is not needed to use these containers.${alrb_colReset}

${alrb_colCyan}Format${alrb_colReset}
 The format of the listings are aliases independent of runtime (apptainer,
  docker, podman, etc).
 Simply cut and paste as the argument for setupATLAS -c.

  The format consists of a prefix alias A/B% where
  A, the registry, can be one of these:
`\cat $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/registries.txt | \grep -e '^,' | \cut -f 2-3 -d ',' | \sed -e 's|,| is |g' -e 's|^|   |g'`
  B, the path in the registry, can be one of these:
   00 is /
`\cat $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/containerPath.txt | \grep -e '^,' | \cut -f 2-3 -d ',' | \sed -e 's|,| is /|g' -e 's|^|   |g'`   

${alrb_colCyan}More Information${alrb_colReset}
 For more information, or for other containers,  visit
  https://hub.docker.com
  https://registry.cern.ch/harbor/projects
  https://gitlab.cern.ch/atlas/athena/container_registry
   (and other gitlab.cern.ch registries for non-atlas/athena projects)
 (Shifter runtime images can be listed by doing 'shifterimg images'.)


${alrb_colCyan}Usage${alrb_colReset}
 To use, type:
   setupATLAS -c <container URL from the list below>

 Instructions:
   https://twiki.atlas-canada.ca/bin/view/AtlasCanada/Containers
 and, for help, 
   setupATLAS -c -h


${alrb_colCyan}Interactive searching in setupATLAS -c${alrb_colReset}
 For an interactive search of an ATLAS standalone container release, you can do
   setupATLAS -c find <and other options>
 or
   setupATLAS -c find=comma-delimited-search-args <and other options>
 which will let you select and run an ATLAS standalone release container.


${alrb_colCyan}Command: queryC${alrb_colReset}
 You can also do (after setupATLAS)
   queryC  # -h for details
 eg
   queryC find
   queryC whatis 


${alrb_colCyan}crane${alrb_colReset}	
 crane is also available (after setupATLAS)
   lsetup crane
"
    return 0
}


if [ -z $ALRB_cvmfs_unpacked_repo ]; then
    export ALRB_cvmfs_unpacked_repo="/cvmfs/unpacked.cern.ch"
fi
alrb_containerListFile="$ALRB_cvmfs_unpacked_repo/logDir/list.txt"

if [ ! -e $alrb_containerListFile ]; then
    \echo "
Please look at:
 https://atlas-tier3-sw.web.cern.ch/containers/listing.txt
"
    exit 64
fi
#\grep -e "# Date Completed" $alrb_containerListFile > /dev/null 2>&1
#if [ $? -ne 0 ]; then
#    \echo "Error: $alrb_containerListFile is incomplete"
#    exit 64
#fi

alrb_fn_printInstructions
\echo -e "${alrb_colBlue}Containers found:${alrb_colReset}
"

alrb_result=`type -t alrb_fn_getAliasSubStr`
if [[ $? -ne 0 ]] || [[ "$alrb_result" != "function" ]]; then
    source $ATLAS_LOCAL_ROOT_BASE/container/findFunctions.sh
    alrb_fn_initializeFindFunctions
fi
alrb_fn_getAliasSubStr

for alrb_reg in ${alrb_cvmfsRegList[@]}; do     

    # this is a temporary workaround until the container log file is fixed
    # https://cern.service-now.com/service-portal?id=ticket&table=incident&n=INC3744884    
    alrb_contList=( `\find $ALRB_cvmfs_unpacked_repo/$alrb_reg -type l | eval $alrb_changeStr | \sed -E -e 's|^.*/cvmfs/unpacked.cern.ch/||g' | sort -V` )
    for alrb_cont in ${alrb_contList[@]}; do
	\echo "  $alrb_cont"
    done
done

alrb_fn_printInstructions

exit 0
