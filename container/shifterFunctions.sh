#!----------------------------------------------------------------------------
#!
#! shifterFunctions.sh
#!
#! Code specific to Shifter that is called by startContainer
#!
#! Usage:
#!     used by startContainer.sh
#!
#! History:
#!    11Dec17: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_fn_shifterInitialize()
{
    alrb_runtimeBindMountOpt="-V"
    alrb_runtimeWorkdirOpt="--workdir="
    alrb_runtimeInitialized="shifter"
    alrb_runTimeErrMsg="Error"
    return 0
}


alrb_fn_shifterVersion()
{
    which shifter > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	\echo "Error: Shifter is not instaled and so cannot run" 1>&2
	return 64
    fi
    alrb_contVer=`rpm -qa | \grep -i shifter-runtime | \sed -e 's/shifter-runtime-//' -e 's/.x86_64//'`
    
    return 0
}


alrb_fn_shifterOptsParser()
{
    if [ ! -z "$ALRB_CONT_CMDOPTS" ]; then
	eval set -- "$ALRB_CONT_CMDOPTS"
	
	while [ $# -gt 0 ]; do
	    case $1 in
		--workdir=*)
		    alrb_setContainerPwd="`\echo $1 | \cut -f 2 -d '='`"
		    shift
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi
    
    return 0
    
}


alrb_fn_shifterSetContainer()
{
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} determine container"
    local alrb_tmpVal

# Shifter is an odd duck                                                       
    alrb_homeDirNew=$alrb_homeDir

    alrb_container="$alrb_containerCandidate"
    alrb_containerAsSingularity=""    

    shifterimg lookup $alrb_container > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	return 0
    fi

    local alrb_singRepo="$ALRB_cvmfs_repo/containers/fs/singularity"

    local alrb_oldifs="$IFS"
    IFS=$'\n'
    local alrb_defaultContAr=(
	'slc5|atlas-default|-e "slc5"'
	'centos6|atlas-default|-e "sl6" -e "slc6" -e "centos6" -e "rhel6"'
	'centos7|atlas-default|-e "sl7" -e "slc7" -e "centos7" -e "rhel7"'
	'almalinux8|atlas-default|-e "sl8" -e "slc8" -e "centos8" -e "centos8s" -e "almalinux8" -e "alma8"  -e "rhel8"'
	'almalinux9|atlas-default|-e "sl9" -e "slc9" -e "centos9" -e "almalinux9" -e "alma9" -e "rhel9" -e "el9"'
    )

    local alrb_defaultCont
    for alrb_defaultCont in ${alrb_defaultContAr[@]}; do
	local alrb_queryCmd="\echo $alrb_containerCandidate | \grep -i `\echo $alrb_defaultCont | \cut -f 3 -d '|'`"
	alrb_tmpVal=`eval $alrb_queryCmd 2>&1`
	if [ $? -eq 0 ]; then
	    alrb_containerType="`\echo  $alrb_defaultCont | \cut -f 2 -d '|'`"
	    local alrb_contNamePrefix=`\echo $alrb_defaultCont | \cut -f 1 -d '|'`
	    alrb_containerAsSingularity="$alrb_singRepo/${alrb_archType}-${alrb_contNamePrefix}"
	    alrb_container="registry.cern.ch/atlasadc/atlas-grid-$alrb_contNamePrefix"
	    break
	fi
    done
    IFS="$alrb_oldifs"
    
    return 0
}


alrb_fn_shifterGetPathLookup()
{
    
    ALRB_CONT_SED2HOST=""
    ALRB_CONT_SED2CONT=""
    
    if [ ! -z "$ALRB_CONT_STARTCMD" ]; then
	eval set -- "$ALRB_CONT_STARTCMD"
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-V|--volume)
		    local alrb_tmp1=`\echo $2 | \cut -f 1 -d ":"`
		    local alrb_tmp2=`\echo $2 | \cut -f 2 -d ":"`
		    alrb_fn_buildSedPath "$alrb_tmp1" "$alrb_tmp2"
	            shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
	            shift
		    ;;
	    esac
	done
    fi
    
    if [ "$ALRB_CONT_SED2HOST" != "" ]; then
	export ALRB_CONT_SED2HOST
    fi
    if [ "$ALRB_CONT_SED2CONT" != "" ]; then
	export ALRB_CONT_SED2CONT
    fi
    
    return 0    
}


alrb_fn_shifterConstructCommand()
{

    local alrb_optStr="-V $alrb_contDummyHome:$ALRB_CONT_DUMMYHOME"

    # Shifter relies on mount points already existing so ...
    if [ ! -z $ALRB_CONT_DUMMYALRB ]; then 
	export ALRB_CONT_DUMMYALRB="/ATLASLocalRootBase"
	alrb_optStr="$alrb_optStr -V $ATLAS_LOCAL_ROOT_BASE:$ALRB_CONT_DUMMYALRB"
    fi
        
    if [ "$alrb_containerSkipCvmfs" != "YES" ]; then
	\echo "Info: /cvmfs mounted; do 'setupATLAS -d -c ...' to skip default mounts."
	alrb_optStr="$alrb_optStr  -m cvmfs "
    fi
    
    alrb_optStr="$alrb_optStr -V $alrb_pwd:/srv"
    if [[ ! -z $ALRB_CONT_REALTMPDIR ]] && [[ "$ALRB_CONT_REALTMPDIR" = "/scratch" ]]; then
	alrb_optStr="$alrb_optStr -V $TMPDIR:/scratch"
    fi

    alrb_cmdRunPayload="shifter $ALRB_CONT_OPTS $ALRB_CONT_CMDOPTS --image=$alrb_container $alrb_optStr "
    
    if [ "$alrb_cont_payload" = "" ]; then
	if [ "$ALRB_SHELL" = "bash" ]; then
	    alrb_cmdRunPayload="$alrb_cmdRunPayload $alrb_shellPath/bash --rcfile $ALRB_CONT_DUMMYHOME/.bashrc"
	elif [ "$ALRB_SHELL" = "zsh" ]; then
	    alrb_cmdRunPayload="$alrb_cmdRunPayload env ZDOTDIR=$ALRB_CONT_DUMMYHOME $alrb_shellPath/zsh" 
	elif [ "$ALRB_SHELL" = "sh" ]; then
	    alrb_cmdRunPayload="$alrb_cmdRunPayload env ENV=$ALRB_CONT_DUMMYHOME/$alrb_rcFile $alrb_shellPath/sh" 
	fi
    else
	alrb_cmdRunPayload="$alrb_cmdRunPayload $alrb_cont_payload"
    fi

    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Shifter: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `which shifter`"
	\echo "ContainerType: $alrb_containerType"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi

    return 0
}


alrb_fn_shifterGuessModifiers()
{
    shifter $ALRB_CONT_OPTS $ALRB_CONT_CMDOPTS --clearenv --image=$alrb_container -V $alrb_contDummyHome:$ALRB_CONT_DUMMYHOME $ALRB_CONT_DUMMYHOME/probeContainer.sh
    return $?
}


alrb_fn_shifterConstructCommandDumb()
{
    
    alrb_cmdRunPayload="shifter $ALRB_CONT_OPTS $ALRB_CONT_CMDOPTS --image=$alrb_container $ALRB_CONT_RUNPAYLOAD"
    
    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Shifter: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `which shifter`"
	\echo "Note: This is running with -x/--dumb option and bypasses ALRB features."
	\echo "      If you have issues, check first that it works without -x/--dumb"
	\echo "ContainerType: $alrb_containerType"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi

    return $?
}
