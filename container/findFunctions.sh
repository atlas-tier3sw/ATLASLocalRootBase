#!----------------------------------------------------------------------------
#!
#! findFunctions.sh
#!
#! Container functionality for finding containers
#!
#! Usage:
#!     internal usage in ALRB
#!
#! History:
#!    02Feb24: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_fn_initializeFindFunctions()
{
    # initialize

    # registry alias information
    source $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/releaseFind.sh
    
    # These are the dirs searched on /cvmfs/unpacked.cern.ch/
    alrb_cvmfsRegList=(
	"registry.cern.ch"
	"gitlab-registry.cern.ch"
	"registry.hub.docker.com"
    )

    # These are positions for container registry URLs in A$LAS_LOCAL_ROOT_BASE/etc/containerInfo/registries.txt
    alrb_registryUrlPos="alias:2,crane:3,cvmfs:4,docker:5,podman:6,apptainer:4,singularity:4"

    alrb_projectAr=( `\echo ${alrb_relProjAr[@]} | \sed -E -e 's|&[[:alnum:]/]*||g'` )
    
    source $ATLAS_LOCAL_ROOT_BASE/utilities/checkAtlasLocalRoot.sh
    alrb_repoListingDir="$ALRB_tmpScratch/repoListings"
    mkdir -p $alrb_repoListingDir

    alrb_craneDir="$ATLAS_LOCAL_ROOT/crane/current"
    if [ "$ALRB_OSTYPE" = "Linux" ] && [[ -e "${alrb_craneDir}-SL${ALRB_OSMAJORVER}" ]]; then
	alrb_craneDir="$ATLAS_LOCAL_ROOT/crane/current-SL${ALRB_OSMAJORVER}"
    fi

    return 0
}


alrb_fn_fixMultiArchCvmfs()
{

    # multi arch containers may be on registries but the containers may have
    # only a platform suffix on cvmfs unpacked.  So fix this here..
    # may change alrb_foundOnCvmfs_*, alrb_runtimeUrl
    
    if [ "$alrb_skipCheckCvmfs" = "YES" ]; then
	return 0
    elif [ "$alrb_foundOnCvmfs_x86_64$alrb_foundOnCvmfs_aarch64" != "" ]; then
	return 0
    fi

    local alrb_pos=`alrb_fn_getRegistryUrlPos cvmfs`
    local alrb_reg=`\echo "$alrb_contRegInfo" | \cut -f $alrb_pos -d ','`
    local alrb_tmpPath="$ALRB_cvmfs_unpacked_repo/$alrb_reg/`\echo $alrb_registryUrl | \cut -f 2- -d '/'`"

    local alrb_isGetArch=""
    \echo $alrb_tmpPath | \grep -e "getarch" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_isGetArch="YES"
    fi

    local alrb_tmpVal_x86_64="${alrb_tmpPath}-x86_64"
    local alrb_tmpVal_aarch64="${alrb_tmpPath}-aarch64"
    if [ "$alrb_isGetArch" = "YES" ]; then
	alrb_tmpVal_x86_64=`\echo $alrb_tmpPath | \sed -e 's|getarch|x86_64|'`
	alrb_tmpVal_aarch64=`\echo $alrb_tmpPath | \sed -e 's|getarch|aarch64|'`
    fi
    if [ -e "$alrb_tmpVal_x86_64" ]; then
	alrb_foundOnCvmfs_x86_64="$alrb_tmpVal_x86_64"
    fi
    if [ -e "$alrb_tmpVal_aarch64" ]; then
	alrb_foundOnCvmfs_aarch64="$alrb_tmpVal_aarch64"
    fi

    # cvmfs is preferred if available for apptainer
    if [[ "$ALRB_CONT_SWTYPE" = "apptainer" ]] || [[ "$ALRB_CONT_SWTYPE" = "singularity" ]]; then
	if [ "$alrb_archType" = "" ]; then
	    alrb_archType=`uname -m`
	fi
	local alrb_tmpVal=$(\echo alrb_foundOnCvmfs_${alrb_archType})
	if [ -e "${!alrb_tmpVal}" ]; then
	    alrb_runtimeUrl="${!alrb_tmpVal}"
	fi
    fi
    
    return 0
}


alrb_fn_queryContainerArch()
{

    # set
    alrb_hasArch="$alrb_hasArch"
    
    local alrb_rc0=0
    
    if [[ "$alrb_registryUrl" = "" ]] || [[ "$alrb_hasArch" != "" ]]; then
	return 0
    fi

    local alrb_manifest
    alrb_manifest="`$alrb_craneDir/crane manifest $alrb_registryUrl 2>&1`"
    alrb_rc0=$?

    if [ $alrb_rc0 -eq  0 ]; then
	\echo $alrb_manifest | \grep -E -e amd64 > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    alrb_hasArch="$alrb_hasArch x86_64"
	fi
	\echo $alrb_manifest | \grep -E -e arm64 > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    alrb_hasArch="$alrb_hasArch aarch64"
	fi	
    fi

    # if cannot find ...
    if [ "$alrb_hasArch" = "" ]; then
        \echo "$alrb_aliasUrl" | \grep -e "-aarch64$" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            alrb_hasArch="$alrb_hasArch aarch64"
        else
            alrb_hasArch="$alrb_hasArch x86_64"
        fi
    fi
    alrb_hasArch="`\echo $alrb_hasArch | \sed -E -e 's|^[[:space:]]||' -e 's|[[:space:]]$||'`"
    
    return 0
}


alrb_fn_getInfoFromAliasUrlDefaultOS()
{
    # sets registry, runtime, cvmfs URL

    local alrb_aliasURL="$1"

    local alrb_aliasCont=`\echo $alrb_aliasURL | \cut -f 2 -d '%'`

    alrb_runtimeUrl="$alrb_aliasCont"
    alrb_registryUrl="registry.cern.ch/atlasadc/atlas-grid-$alrb_aliasCont"
    alrb_foundOnCvmfs_x86_64="$ALRB_cvmfs_repo'/containers/fs/singularity/x86_64-$alrb_aliasCont"
    alrb_foundOnCvmfs_aarch64="$ALRB_cvmfs_repo'/containers/fs/singularity/aarch_64-$alrb_aliasCont"

    alrb_hasArch="x86_64, aarch64"    
    if [ "$alrb_aliasCont" = "slc5" ]; then
	alrb_hasArch="x86_64"
	alrb_foundOnCvmfs_aarch64="Not found - unavailable"
    elif [ "$alrb_aliasCont" = "centos6" ]; then
	alrb_hasArch="x86_64"
	alrb_foundOnCvmfs_aarch64="Not found - unavailable"
    fi
    
    return 0
}


alrb_fn_getInfoFromAliasUrl()
{
    # sets registry, runtime, cvmfs URL
    # also sets
    alrb_contRegAlias=""
    alrb_contRegPathAlias=""
    alrb_contTag=""
    alrb_contRegistry=""
    alrb_contRegPath=""
    alrb_contRegInfo=""
    
    local alrb_aliasURL="$1"
    
    local alrb_prefix=`\echo $alrb_aliasURL | \cut -f 1 -d '%'`

    if [ "$alrb_prefix" = "00/00" ]; then
	alrb_fn_getInfoFromAliasUrlDefaultOS "$alrb_aliasURL"
	return $?
    fi

    alrb_contTag=`\echo $alrb_aliasURL | \cut -f 2- -d '%'`
    alrb_contRegAlias=`\echo $alrb_prefix | \cut -f 1 -d '/'`
    alrb_contRegPathAlias=`\echo $alrb_prefix | \cut -f 2- -d '/'`

    alrb_contRegInfo=`\grep -E -e ",$alrb_contRegAlias," $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/registries.txt`
    local alrb_pos=`alrb_fn_getRegistryUrlPos crane`
    alrb_contRegistry=`\echo "$alrb_contRegInfo" | \cut -f $alrb_pos -d ','`
    
    local alrb_contRegPathAlias=`\echo $alrb_prefix | \cut -f 2- -d '/'`
    if [ "$alrb_contRegPathAlias" = "00" ]; then
        alrb_contRegPath=""
    else
        alrb_contRegPath="`\grep -E -e \",$alrb_contRegPathAlias,\"  $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/containerPath.txt | \cut -f 3 -d ","`/"
    fi

    alrb_fn_constructRegistryURLFromPrefix
    alrb_fn_constructCvmfsPathFromPrefix
    alrb_fn_constructRuntimeURLFromPrefix
    alrb_fn_queryContainerArch
    alrb_fn_fixMultiArchCvmfs
    
    return 0
}


alrb_fn_constructRegistryURLFromPrefix()
{
    # sets
    alrb_registryUrl="$alrb_contRegistry/$alrb_contRegPath$alrb_contTag"
    return 0
}


alrb_fn_constructCvmfsPathFromPrefixGetArch()
{
    local alrb_tmpVal="$1"

    local alrb_foundIt=""
    alrb_foundOnCvmfs_aarch64="Not found - unavailable"
    alrb_foundOnCvmfs_x86_64="Not found - unavailable"

    # multi-arch container unpacked with platform tags ...
    if [ -e "${alrb_tmpVal}-aarch64" ]; then
	alrb_foundOnCvmfs_aarch64="${alrb_tmpVal}-aarch64"
	alrb_foundIt="Y"
    fi
    if [ -e "${alrb_tmpVal}-x86_64" ]; then
	alrb_foundOnCvmfs_x86_64="${alrb_tmpVal}-x86_64"
	alrb_foundIt="Y"
    fi

    # maintain backward compatibility; no platform suffix means x86_64 ...
    if [ "$alrb_foundIt" = "" ]; then
	if [ ! `\echo $alrb_tmpVal | \grep -e "aarch64"` ]; then
	    alrb_foundOnCvmfs_x86_64="$alrb_tmpVal"
	else
	    alrb_foundOnCvmfs_aarch64="$alrb_tmpVal"
	fi
    fi

    alrb_foundOnCvmfs="$alrb_tmpVal"
    
    return 0
}


alrb_fn_constructCvmfsPathFromPrefix()
{

    if [ "$alrb_skipCheckCvmfs" = "YES" ]; then
	return 0
    fi

    if [[ "$alrb_foundOnCvmfs" != "" ]] && [[ `\echo "$alrb_foundOnCvmfs" | \grep -E -e "^$ALRB_cvmfs_unpacked_repo/"` ]] && [[ -e "$alrb_foundOnCvmfs" ]]; then
	alrb_fn_constructCvmfsPathFromPrefixGetArch "$alrb_foundOnCvmfs"
	return 0
    fi

    local alrb_pos=`alrb_fn_getRegistryUrlPos cvmfs`
    local alrb_reg=`\echo "$alrb_contRegInfo" | \cut -f $alrb_pos -d ','`
    local alrb_tmpVal="$ALRB_cvmfs_unpacked_repo/$alrb_reg/$alrb_contRegPath$alrb_contTag"
    if [ -e $alrb_tmpVal ]; then
	alrb_fn_constructCvmfsPathFromPrefixGetArch $alrb_tmpVal
    fi

    return 0
}

	    
alrb_fn_constructRuntimeURLFromPrefix()
{
    # sets
    alrb_runtimeUrl=""
    # can change alrb_foundOnCvmfs

    local alrb_pos=`alrb_fn_getRegistryUrlPos $ALRB_CONT_SWTYPE`
    local alrb_reg=`\echo "$alrb_contRegInfo" | \cut -f $alrb_pos -d ','`

    if [ "$alrb_reg" != "" ]; then
	alrb_runtimeUrl="$alrb_reg/$alrb_contRegPath$alrb_contTag"
    else
	alrb_runtimeUrl="$alrb_contRegPath$alrb_contTag"
    fi

    alrb_fn_constructCvmfsPathFromPrefix

    # cvmfs is preferred if available for apptainer
    if [[ "$ALRB_CONT_SWTYPE" = "apptainer" ]] || [[ "$ALRB_CONT_SWTYPE" = "singularity" ]]; then
	if [[ "$alrb_foundOnCvmfs" = "" ]] || [[ `\echo "$alrb_foundOnCvmfs" | \grep -E -e "Not found"` ]]; then
	    # cvmfs version missing/not found, use  docker:// url
	    alrb_pos=`alrb_fn_getRegistryUrlPos docker`
	    local alrb_reg=`\echo $alrb_contRegInfo | \cut -f $alrb_pos -d ','`
	    if [ "$alrb_reg" != "" ]; then
		alrb_runtimeUrl="docker://$alrb_reg/$alrb_contRegPath$alrb_contTag"
	    else
		alrb_runtimeUrl="docker://$alrb_contRegPath$alrb_contTag"
	    fi
	else
	    alrb_runtimeUrl="$alrb_contRegPath$alrb_contTag"
	fi
    fi
    
    return 0
}


alrb_fn_getRegistryUrlPos()
{
    # returns as a string a position of the string for the runtime
    local alrb_type="$1"

    \echo ",$alrb_registryUrlPos," | \sed -E -e 's|.*,'$alrb_type':([[:digit:]]+),.*|\1|g'    
    return 0
}


alrb_fn_getAliasSubStr()
{

    # sets alrb_changeStr for changing path into aliases
    alrb_changeStr="\sed -E -f $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/sedConvertToAlias.txt"
        
    return 0
}


alrb_fn_findReleaseValidateProject()
{

    # sets:
    alrb_searchStr=""
    alrb_searchProj=""

    local alrb_searchPattern="$1"

    local alrb_proj
    if [ "$alrb_searchPattern" != "" ]; then
	for alrb_proj in ${alrb_projectAr[@]}; do
	    \echo ",$alrb_searchPattern," | \grep -E -e ",$alrb_proj," > /dev/null 2>&1
	    if [ $? -eq 0 ]; then
		alrb_searchProj="$alrb_proj"
		alrb_searchStr="`\echo ,$alrb_searchPattern, | \sed -E -e 's|[,]*'$alrb_proj'||g' | \sed -e 's|,,|,|g' | \sed -E -e 's|\.|\\\.|g' -e 's|,|\" \| \\\grep -E -e \"|g' | \sed -e 's|^\"||' -e 's|$|\"|'`"
	    fi	
	done		
    fi	
    
    if [ "$alrb_searchProj" = "" ]; then
	\echo "
Error: Search string does not contain a valid project (ie one of these):
  ${alrb_projectAr[@]}
"
	return 1
    fi  	

    return 0
}


alrb_fn_getRegistryList()
{

    # sets:
    alrb_resultAr=()
    alrb_fetchListingFailure=()
    
    local alrb_searchProj="$1"
    local alrb_searchStr="$2"

    local let alrb_rc0=0
    
    local alrb_tmpVal    
    local alrb_projInfo
    for alrb_projInfo in ${alrb_relProjAr[@]}; do
	\echo $alrb_projInfo | \grep -E -e "^$alrb_searchProj\&" > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	    continue
	fi

    	local alrb_proj="`\echo $alrb_projInfo | \cut -f 1 -d '&'`"
	alrb_tmpVal="`\echo $alrb_projInfo | \cut -f 2- -d '&'`"
	local alrb_registryAlias="`\echo $alrb_tmpVal | \cut -f 1 -d '/'`"
	local alrb_pathAlias="`\echo $alrb_tmpVal | \cut -f 2 -d '/'`"
	local alrb_pos=`alrb_fn_getRegistryUrlPos crane`
	local alrb_registry="`\grep -e ",$alrb_registryAlias," $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/registries.txt | \cut -f $alrb_pos  -d ','`"
	local alrb_registryPath="`\grep -e ",$alrb_pathAlias," $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/containerPath.txt | \cut -f 3 -d ','`"

	local let alrb_lastUpdatedTime=0
	local alrb_repoListingCached="$alrb_repoListingDir/`\echo ${alrb_registry}_${alrb_registryPath}_${alrb_proj} | \sed -E -e 's|\/|-|g'`"
	local alrb_repoListingCachedTime="$alrb_repoListingCached-time"
	local alrb_repoListingCachedList="$alrb_repoListingCached-list"
	if [[ -e $alrb_repoListingCachedTime ]] && [[ -e $alrb_repoListingCachedList ]]; then
	    source $alrb_repoListingCachedTime
	fi
	local let alrb_timeDiff=`expr $alrb_now - $alrb_lastUpdatedTime`
	if [ $alrb_timeDiff -gt 3600 ]; then
	    local alrb_registryProjPath="$alrb_registry/$alrb_registryPath/$alrb_proj"
	    alrb_fn_fetchRegistryListingCrane "$alrb_registryProjPath" "$alrb_repoListingCachedList" "$alrb_repoListingCachedTime"
	    if [ $? -ne 0 ]; then
		if [ ! -e $alrb_repoListingCachedList ]; then
		    \echo "
Error: Unable to get dir listing for $alrb_registryProjPath
       Information may be incomplete.
"
		    let alrb_rc0=64
		    continue
		else
		    \echo "
Warning: Unable to get fresh listing for $alrb_registryProjPath
	 Using last cached update from $alrb_timeDiff s
"
		fi
	    fi
	fi

	if [[ "$alrb_searchStr" = "" ]] && [[ -e $alrb_repoListingCachedList ]]; then
	    alrb_resultAr=( ${alrb_resultAr[@]} `\cat $alrb_repoListingCachedList | \sed -E -e 's|^|'$alrb_registryAlias'/'$alrb_pathAlias'%'$alrb_searchProj':|g' | \sort -V `)
	else
	    alrb_resultAr=( ${alrb_resultAr[@]} `eval \cat $alrb_repoListingCachedList $alrb_searchStr | \sed -E -e 's|^|'$alrb_registryAlias'/'$alrb_pathAlias'%'$alrb_searchProj':|g' | \sort -V ` )
	fi
    done

    return $alrb_rc0
}


alrb_fn_fetchRegistryListingCrane()
{
    # fetch the registry listings, if stale, using crane
    # sets alrb_fetchListingFailure()
    local alrb_craneProjectPath="$1"
    local alrb_craneListFile="$2"
    local alrb_craneTimeFile="$3"
    
    eval $alrb_craneDir/crane ls $alrb_craneProjectPath > $alrb_craneListFile.tmp

    if [ $? -eq 0 ]; then
	\echo "let alrb_lastUpdatedTime=$alrb_now" > $alrb_repoListingCachedTime.tmp
	\mv $alrb_craneListFile.tmp $alrb_craneListFile
	\mv $alrb_craneTimeFile.tmp $alrb_craneTimeFile
    else
	\echo "Warning: unable to fetch $alrb_registry listing $alrb_craneProjectPath"
	alrb_fetchListingFailure=( ${alrb_fetchListingFailure[@]} "$alrb_craneProjectPath" )
	return 64
    fi

    return 0
}


alrb_fn_findRelease()
{
    # sets alrb_aliasUrl
    alrb_aliasUrl=""
    alrb_resultAr=()
    
    local alrb_findShowUnique="$1"
    alrb_inputStr="$2"

    let local alrb_rc=0
    
    local alrb_searchPattern="`\echo $alrb_inputStr | \sed -E -e 's|[[:space:]]||g' -e 's|,,||g' -e 's|find[=]*||g' -e 's|^,||g' -e 's|,$||'`"
    alrb_fn_findReleaseValidateProject "$alrb_searchPattern"     
    alrb_rc=$?
    if [ $alrb_rc -ne 0 ]; then
	return $alrb_rc
    fi

    local alrb_tmpVal

    alrb_fn_getRegistryList "$alrb_searchProj" "$alrb_searchStr"
    alrb_rc=$?
    if [ $alrb_rc -ne 0 ]; then
        return $alrb_rc
    fi
    
    local let alrb_arsize=${#alrb_resultAr[@]}
    if [[ $alrb_arsize -eq 1 ]] && [[ "$alrb_findShowUnique" = "YES" ]]; then
	alrb_aliasUrl="${alrb_resultAr[0]}"
	alrb_tmpVal=`\echo "${alrb_resultAr[0]}" | \cut -f 2 -d ":"`
	alrb_requestedRel="$alrb_searchProj,$alrb_tmpVal"
	return 0
    elif [ $alrb_arsize -eq 0 ]; then
	\echo "Error: Search did not return any results"
	return 1	
    else
	local alrb_index
	local alrb_index1
	printf "\t%5s\t%s\n" "#" "Release"
	for alrb_index in "${!alrb_resultAr[@]}"; do
	    let alrb_index1=$alrb_index+1
	    alrb_tmpVal=`\echo "${alrb_resultAr[$alrb_index]}" | \cut -f 2 -d ":"`
	    printf "\t%5s\t%s\n" "$alrb_index1" "$alrb_tmpVal"
	done
	if [ $alrb_arsize -gt 20 ]; then
	    printf "\t%5s\t%s\n" "#" "Release"
	fi
    fi

    local let alrb_inputStrN=0
    while [ $alrb_inputStrN -eq 0 ]; do
	read -p "Enter # (1st column above) or new search string [exit to abort]: " alrb_inputStr 
	\echo $alrb_inputStr | \grep -E -e "^[[:digit:]]+$" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    let alrb_inputStrN=$alrb_inputStr 
	    if [ $alrb_inputStr -gt $alrb_index1 ]; then
		\echo "Error: # is outside the bounds of the listing."
		let alrb_inputStrN=0
		continue
	    else
		let alrb_index=$alrb_inputStrN-1
		alrb_aliasUrl="${alrb_resultAr[$alrb_index]}"
		alrb_tmpVal=`\echo "${alrb_resultAr[$alrb_index]}" | \cut -f 2 -d ":"`
		alrb_requestedRel="$alrb_searchProj,$alrb_tmpVal"
	    fi       
	elif [ "$alrb_inputStr" = "exit" ]; then
	    return 64
	else
	    return 1
	fi
    done
    
    return 0

}


alrb_fn_confirmFindRequirements()
{

    if [ "$ALRB_CONT_SWTYPE" = "shifter" ]; then
	\echo "Error: Cannot use container search functionality for Shifter."
	return 64
    fi	

    ping -c 1 lxplus.cern.ch > /dev/null 2>&1 || curl -s -m 1 www.cern.ch > /dev/null 2>&1 || wget -q -T 1 www.cern.ch > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	\echo "
Error: Network is unavailable.  Cannot use container search functionality."
	return 64
    fi
    
    if [ ! -d "$ALRB_cvmfs_unpacked_repo/logDir" ]; then
	alrb_foundOnCvmfs="Not found - cvmfs unpacked repo is missing"
	alrb_foundOnCvmfs_x86_64="$alrb_foundOnCvmfs"
	alrb_foundOnCvmfs_aarch64="$alrb_foundOnCvmfs"
	alrb_skipCheckCvmfs="YES"
    fi
    
    return 0 
}


alrb_fn_findReleaseUI()
{

    local alrb_findShowUnique="$1"
    local alrb_inputStr="`\echo $2 | \sed -E -e 's|^find=||'`"
    
    # set
    alrb_registryUrl=""
    alrb_runtimeUrl=""
    alrb_aliasUrl=""
    alrb_foundOnCvmfs=""
    alrb_foundOnCvmfs_x86_64=""
    alrb_foundOnCvmfs_aarch64=""
    alrb_requestedRel=""
    alrb_hasArch=""
    
    local alrb_contRegAlias=""
    local alrb_contRegPathAlias=""
    local alrb_contTag=""
    local alrb_contRegistry=""
    local alrb_contRegPath=""
    local alrb_contRegInfo=""
    
    local alrb_changeStr=""
    local alrb_searchStr=""
    local alrb_searchProj=""
    
    local alrb_resultAr=()
    local alrb_fetchListingFailure=()
    local alrb_skipCheckCvmfs=""
    
    local let alrb_rc=1

    alrb_fn_confirmFindRequirements
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    local let alrb_now=`date +%s`
    
    while [ $alrb_rc -eq 1 ]; do
	if [ "$alrb_inputStr" = "" ]; then
	    \echo "Find an ATLAS release; for example analysisbase,24.2."
	    read -p "Search for [exit to abort]: " alrb_inputStr 
	fi
	if [ "$alrb_inputStr" = "exit" ]; then
	    return 64
	fi
	alrb_inputStr=`\echo $alrb_inputStr | tr '[:upper:]' '[:lower:]'`
	alrb_fn_findRelease "$alrb_findShowUnique" "$alrb_inputStr"
	alrb_rc=$?
	if [ $alrb_rc -eq 1 ]; then
	    alrb_inputStr=""
	fi
    done

    if [[ $alrb_rc -eq 0 ]] && [[ "$alrb_aliasUrl" != "" ]]; then
	alrb_fn_getInfoFromAliasUrl "$alrb_aliasUrl"
	\echo " "
	printf " %12s: %s\n" \
	       "selected" "$alrb_requestedRel" \
	       "aliased" "$alrb_aliasUrl" \
	       "registry" "$alrb_registryUrl" \
	       "$ALRB_CONT_SWTYPE" "$alrb_runtimeUrl" \
	       "path x86_64" "$alrb_foundOnCvmfs_x86_64" \
	       "path aarch64" "$alrb_foundOnCvmfs_aarch64" \
	       "architecture" "$alrb_hasArch"
	\echo " "
    fi
    
    return $alrb_rc	  
}


alrb_fn_constructReleaseURLFromPrefix()
{
    # sets alrb_runtimeUrl
    alrb_runtimeUrl=""

    local alrb_contRegInfo="$1"
    local alrb_contRegPath="$2"
    local alrb_contTag="$3"

    local alrb_pos=""
    if [[ "$ALRB_CONT_SWTYPE" = "apptainer" ]] || [[ "$ALRB_CONT_SWTYPE" = "singularity" ]]; then
	alrb_pos=`alrb_fn_getRegistryUrlPos cvmfs`
	local alrb_contRegCvmfs=`\echo $alrb_contRegInfo | \cut -f $alrb_pos -d ','`
	local alrb_cvmfsContPath="$ALRB_cvmfs_unpacked_repo/$alrb_contRegCvmfs/$alrb_contRegPath/$alrb_contTag"
	if [ -e $alrb_cvmfsContPath ]; then
	    alrb_runtimeUrl="$alrb_contRegPath/$alrb_contTag"
	else	    
# if cvmfs unpacked is missing, need docker:// url for apptainer ...
	    alrb_pos=`alrb_fn_getRegistryUrlPos docker`
	    local alrb_contReg=`\echo $alrb_contRegInfo | \cut -f $alrb_pos -d ','`
	    if [ "$alrb_contReg" != "" ]; then
		alrb_contReg="$alrb_contReg/"
	    fi
	    alrb_runtimeUrl="docker://$alrb_contReg$alrb_contRegPath/$alrb_contTag"
	fi

    else

	alrb_pos=`alrb_fn_getRegistryUrlPos $ALRB_CONT_SWTYPE`
	local alrb_contReg=`\echo "$alrb_contRegInfo" | \cut -f $alrb_pos -d ','`
	if [ "$alrb_contReg" != "" ]; then
	    alrb_contReg="$alrb_contReg/"
	fi
	alrb_runtimeUrl="$alrb_contReg$alrb_contRegPath/$alrb_contTag"
    fi
    
    return 0
}


alrb_fn_getInfoFromPath()
{

    local alrb_inquiry="$1"
    local let alrb_rc0=1
    
    alrb_fn_getInfoFromPathCvmfs "$alrb_inquiry"
    alrb_rc0=$?

    if [ $alrb_rc0 -ne 0 ]; then
	alrb_fn_getInfoFromPathRegistry "$alrb_inquiry"
	alrb_rc0=$?
    fi

    return $alrb_rc0
}


alrb_fn_getRegistryCandidatePath()
{
    # set alrb_regCandidatePath if docker://

    alrb_regCandidatePath="$1"

    local alrb_inquiry="$1"

    local alrb_regCandidate=""
    if [ `\echo "$alrb_inquiry" | \grep -E -e "^docker://"` ]; then
	alrb_regCandidate="`\echo $alrb_inquiry | \sed -e 's|docker://||' | \cut -f 1 -d '/'`"
	if [ ! `\grep -E -e ",$alrb_regCandidate," $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/registries.txt` ]; then
	    alrb_regCandidate="registry.hub.docker.com/"
	else
	    alrb_regCandidate=""
	fi
	alrb_regCandidatePath="`\echo $1 | \sed -E -e 's|docker://|'$alrb_regCandidate'|'`"
    fi	

    return 0
}


alrb_fn_getInfoFromPathRegistry()
{
    local alrb_inquiry="$1"
    local let alrb_rc0=1

    local  alrb_regCandidatePath=""
    alrb_fn_getRegistryCandidatePath "$alrb_inquiry"
    alrb_rc0=$?
    if [ $alrb_rc0 -eq 0 ]; then
	alrb_inquiry="$alrb_regCandidatePath"
    fi

    local alrb_tmpVal="`\echo $alrb_inquiry | \cut -f 1 -d '/'`"
    if [ `\grep -E -e  ",$alrb_tmpVal," $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/registries.txt` ]; then
	if [ "$alrb_changeStr" = "" ]; then
	    alrb_fn_getAliasSubStr
	fi
	alrb_aliasUrl="`\echo $alrb_inquiry | eval $alrb_changeStr`"
	alrb_rc0=$?
    fi
    
    return $alrb_rc0
}


alrb_fn_getInfoDefaultOS()
{
    
    local alrb_inquiry="$1"

    local let alrb_rc0=1
        
    alrb_defOSStr5="-E 
-e '^slc5$'
-e '^'$ALRB_cvmfs_repo'/containers/fs/singularity/x86_64-slc5$'
-e 'atlasadc/atlas-grid-slc5$'   
"
    alrb_defOSStr6=" -E 
-e '^slc6$' -e '^sl6$' -e '^centos6$' -e '^rhel6$' 
-e '^'$ALRB_cvmfs_repo'/containers/fs/singularity/x86_64-centos6$' 
-e 'atlasadc/atlas-grid-centos6$'
"        
    alrb_defOSStr7=" -E 
-e '^slc7$' -e '^sl7$' -e '^centos7$' -e '^rhel7$' 
-e '^'$ALRB_cvmfs_repo'/containers/fs/singularity/x86_64-centos7$'
-e '^'$ALRB_cvmfs_repo'/containers/fs/singularity/aarch64-centos7$' 
-e 'atlasadc/atlas-grid-centos7$'
" 
    alrb_defOSStr8=" -E 
-e '^centos8$' -e '^centos8s$' -e '^almalinux8$' -e '^alma8$' -e '^rhel8$' 
-e '^'$ALRB_cvmfs_repo'/containers/fs/singularity/x86_64-almalinux8$'
-e '^'$ALRB_cvmfs_repo'/containers/fs/singularity/aarch64-almalinux8' 
-e 'atlasadc/atlas-grid-almalinux8$'
   "    

    alrb_defOSStr9=" -E 
-e '^centos9$' -e '^el9$'  -e '^el9$'  -e '^alma9$' -e '^rhel9$' 
-e '^'$ALRB_cvmfs_repo'/containers/fs/singularity/x86_64-almalinux9$'
-e '^'$ALRB_cvmfs_repo'/containers/fs/singularity/aarch64-almalinux9$' 
-e 'atlasadc/atlas-grid-almalinux9$'
"    

    if [ `\echo $alrb_inquiry | eval \grep $alrb_defOSStr5` ]; then
	alrb_aliasUrl="00/00%slc5"
    elif [ `\echo $alrb_inquiry | eval \grep $alrb_defOSStr6` ]; then
	alrb_aliasUrl="00/00%centos6"
    elif [ `\echo $alrb_inquiry | eval \grep $alrb_defOSStr7` ]; then
	alrb_aliasUrl="00/00%centos7"
    elif [ `\echo $alrb_inquiry | eval \grep $alrb_defOSStr8` ]; then
	alrb_aliasUrl="00/00%almalinux8"
    elif [ `\echo $alrb_inquiry | eval \grep $alrb_defOSStr9` ]; then
	alrb_aliasUrl="00/00%almalinux9"
    fi

    if [ "$alrb_aliasUrl" != "" ]; then
	let alrb_rc0=0
    fi
    
    return $alrb_rc0
}


alrb_fn_getInfoFromPathCvmfs()
{
    local alrb_inquiry="$1"

    local let alrb_rc0=1

    local  alrb_regCandidatePath=""
    alrb_fn_getRegistryCandidatePath "$alrb_inquiry"
    alrb_rc0=$?
    if [ $alrb_rc0 -eq 0 ]; then
	alrb_inquiry="$alrb_regCandidatePath"
    fi

    local alrb_cvmfsCand=""
    if [ "$alrb_skipCheckCvmfs" != "YES" ]; then
       	if [[ `\echo "$alrb_inquiry" | \grep -E -e "$ALRB_cvmfs_unpacked_repo/"` ]] && [[ -e "$alrb_inquiry" ]]; then
	    alrb_foundOnCvmfs="$alrb_inquiry"
	    alrb_cvmfsCand="$alrb_inquiry"
	elif [ -e "$ALRB_cvmfs_unpacked_repo/$alrb_inquiry" ]; then
	    alrb_foundOnCvmfs="$ALRB_cvmfs_unpacked_repo/$alrb_inquiry"
	    alrb_cvmfsCand="$alrb_foundOnCvmfs"
	else
	    local alrb_cvmfsRegDir
	    for alrb_cvmfsRegDir in ${alrb_cvmfsRegList[@]}; do
		if [ -e "$ALRB_cvmfs_unpacked_repo/$alrb_cvmfsRegDir/$alrb_inquiry" ]; then
		    alrb_foundOnCvmfs="$ALRB_cvmfs_unpacked_repo/$alrb_cvmfsRegDir/$alrb_inquiry"
		    alrb_cvmfsCand="$alrb_foundOnCvmfs"
		    break
		fi
	    done	
	fi	    
    fi

    if [ "$alrb_cvmfsCand" != "" ]; then
	if [ "$alrb_changeStr" = "" ]; then
	    alrb_fn_getAliasSubStr
	fi
	local alrb_tmpVal=`\echo $alrb_cvmfsCand | eval $alrb_changeStr | \sed -E -e 's|^.*/cvmfs/unpacked.cern.ch/||g'`
	if [ "$alrb_tmpVal" != "" ]; then
	    alrb_aliasUrl="$alrb_tmpVal"
	    alrb_fn_constructRegistryURLFromPrefix
	    alrb_rc0=$?
	fi
    else
	let alrb_rc0=1
    fi
    
    return $alrb_rc0
}


alrb_fn_whatIs()
{
    local alrb_inquiry="$1"

    # set
    alrb_registryUrl=""
    alrb_runtimeUrl=""
    alrb_aliasUrl=""
    alrb_foundOnCvmfs=""
    alrb_foundOnCvmfs_x86_64=""
    alrb_foundOnCvmfs_aarch64=""
    alrb_requestedRel=""
    alrb_hasArch=""
    
    local alrb_contRegAlias=""
    local alrb_contRegPathAlias=""
    local alrb_contTag=""
    local alrb_contRegistry=""
    local alrb_contRegPath=""
    local alrb_contRegInfo=""
    
    local alrb_changeStr=""
    local alrb_searchStr=""
    local alrb_searchProj=""
    
    local alrb_resultAr=()
    local alrb_fetchListingFailure=()
    local alrb_skipCheckCvmfs=""
    
    local let alrb_rc=1

    alrb_fn_confirmFindRequirements
    if [ $? -ne 0 ]; then
	return 64
    fi

    local let alrb_now=`date +%s`
    
    \echo "$alrb_inquiry" | \grep -E -e "\%" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	alrb_fn_getInfoDefaultOS "$alrb_inquiry"
	alrb_rc=$?
	if [ $alrb_rc -ne 0 ]; then	    
	    alrb_fn_getInfoFromPath "$alrb_inquiry"
	fi
    else
	alrb_aliasUrl="$alrb_inquiry"
    fi
    alrb_rc=$?
    
    if [[ $alrb_rc -eq 0 ]] && [[ "$alrb_aliasUrl" != "" ]]; then
	alrb_fn_getInfoFromAliasUrl "$alrb_aliasUrl"
	\echo " "
	printf " %12s: %s\n" \
	       "inquired" "$alrb_inquiry" \
	       "alrb alias" "$alrb_aliasUrl" \
	       "registry" "$alrb_registryUrl" \
	       "$ALRB_CONT_SWTYPE" "$alrb_runtimeUrl" \
	       "path x86_64" "$alrb_foundOnCvmfs_x86_64" \
	       "path aarch64" "$alrb_foundOnCvmfs_aarch64" \
	       "architecture" "$alrb_hasArch"
	\echo " "
    else
	\echo "Error: whatis - no information about $alrb_inquiry;
                use find or provide full registry url."
    fi
    
    return $alrb_rc
}


alrb_fn_createConvertToAliasSedFile()
{
    # creates file to use with sed -E -f to change path to aliases
    
    local alrb_sedFile="$1"
    \rm -f $alrb_sedFile.new
    touch $alrb_sedFile.new
    
    local alrb_prefix
    local alrb_repoPrefix
    local alrb_pathPrefix
    local alrb_repoURL
    local alrb_dirPath
    local alrb_pos=`alrb_fn_getRegistryUrlPos crane`
    for alrb_prefix in `( IFS=$'\n'; \echo "${alrb_relProjAr[*]}" | \cut -f 2 -d '&' | sort -u )`; do
	alrb_repoPrefix="`\echo $alrb_prefix | \cut -f 1 -d '/'`"
	alrb_pathPrefix="`\echo $alrb_prefix | \cut -f 2 -d '/'`"
	alrb_repoURL="`\grep -E -e \",$alrb_repoPrefix,\" $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/registries.txt | \cut -f $alrb_pos -d ','`" 
	alrb_dirPath="`\grep -E -e \",$alrb_pathPrefix,\" $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/containerPath.txt | \cut -f 3 -d ','`"
	\echo "s|(^\|/)$alrb_repoURL/$alrb_dirPath/|\1$alrb_prefix%|g" >> $alrb_sedFile.new
    done

    local alrb_regInfo
    local alrb_repoURLList=()
    local alrb_repoURLChange
    local alrb_repoURL
    for alrb_regInfo in `\grep -e "^," $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/registries.txt`; do
	alrb_repoPrefix="`\echo $alrb_regInfo | \cut -f 2 -d ','`/00"
	alrb_repoURLList=( `\echo $alrb_regInfo | \cut -f 3- -d ',' | \tr ',' '\n' | \sed '/^[[:space:]]*$/d' | \sort -u ` )
	for alrb_repoURL in ${alrb_repoURLList[@]}; do
	    \echo "s|(^\|/)$alrb_repoURL/|\1$alrb_repoPrefix%|g" >> $alrb_sedFile.new
	done
    done

    \mv $alrb_sedFile.new $alrb_sedFile
    
    return 0
}

