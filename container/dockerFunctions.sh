#!----------------------------------------------------------------------------
#!
#! dockerFunctions.sh
#!
#! Code specific to Docker that is called by startContainer
#!
#! Usage:
#!     used by startContainer.sh
#!
#! History:
#!    11Dec17: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_fn_dockerInitialize()
{
    alrb_runtimeBindMountOpt="-v"
    alrb_runtimeWorkdirOpt="-w "
    alrb_runtimeInitialized="docker"
    alrb_runTimeErrMsg="Error"
    alrb_defaultPlatform=""
    alrb_platformName=""
    alrb_containerNameTag=""
    
    alrb_fn_dockerGetPlatform
    if [ $? -ne 0 ]; then
        return 64
    fi

    if [ -e $HOME/alrb_override_docker.sh ]; then
	source $HOME/alrb_override_docker.sh
	return $?
    elif [ -e $alrb_localConfigDir/alrb_docker.sh ]; then
	source $alrb_localConfigDir/alrb_docker.sh
	return $?
    fi
    
    return 0
}


alrb_fn_dockerGetPlatform()
{

    local alrb_platform0="$alrb_platform"
    local alrb_setDefaultPlatform="YES"
    
    \echo $ALRB_CONT_CMDOPTS | \grep -e "--platform " > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_platform0=`\echo $ALRB_CONT_CMDOPTS | \sed -e 's|.*--platform ||' | \cut -f 1 -d " "`
	alrb_setDefaultPlatform="NO"
    fi

    if [ "$alrb_platform0" = "linux/arm64" ]; then
	alrb_defaultPlatform="--platform $alrb_platform"	
	alrb_archType="aarch64"
    elif [ "$alrb_platform0" = "linux/amd64" ]; then
	alrb_defaultPlatform="--platform $alrb_platform"	
	alrb_archType="x86_64"	
    elif [[ "$ATLAS_LOCAL_ROOT_ARCH" = "arm64-MacOS" ]] || [[ "$ATLAS_LOCAL_ROOT_ARCH" = "aarch64-Linux" ]]; then 
	alrb_defaultPlatform="--platform linux/arm64"
    else
	alrb_defaultPlatform="--platform linux/amd64"
    fi

    if [ "$alrb_setDefaultPlatform" = "NO" ]; then
	alrb_defaultPlatform=""
    fi

    if [ "$alrb_defaultPlatform" != "" ]; then
        alrb_platformName="_`\echo $alrb_defaultPlatform | \sed -e 's|--platform ||g' -e 's|/|-|g'`"
    fi
    
    return 0
}


alrb_fn_dockerVersion()
{
    alrb_contVer=`docker --version 2>&1`
    if [ $? -ne 0 ]; then
	\echo "Error: Docker is not installed and so cannot create container" 1>&2
	return 64
    fi
    alrb_contVer=`docker version --format '{{.Client.Version}}' | \cut -f 1 -d "-"`
    let alrb_contVerN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_contVer 3`
    if [ "$alrb_contVerN" -lt 171200 ]; then
	\echo "Warning: Docker version is $alrb_contVer; 17.12.0  or newer recommended."
    fi

    return 0
}


alrb_fn_dockerOptsParser()
{
    if [ ! -z "$ALRB_CONT_CMDOPTS" ]; then
	eval set -- "$ALRB_CONT_CMDOPTS"
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-w|--workdir)
		    alrb_setContainerPwd="$2"
		    shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi
    
    return 0
    
}


alrb_fn_dockerSetContainer()
{
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} determine container"
    local alrb_tmpVal

    let alrb_dockerBuildPass=0
    
    alrb_container="$alrb_containerCandidate"

# docker container specified explicitly    
    \echo $alrb_containerCandidate | \grep -e "^docker://" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_container=`\echo $alrb_containerCandidate | \sed -e 's|docker://||'`
	alrb_containerAsSingularity="$alrb_containerCandidate"
	return 0
    fi

# dir like path specified; assume it is docker
    \echo $alrb_containerCandidate | \grep -e "/" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_container="$alrb_containerCandidate"
	alrb_containerAsSingularity="docker://$alrb_container"
	return 0
    fi

    local alrb_singRepo="$ALRB_cvmfs_repo/containers/fs/singularity"

    local alrb_oldifs="$IFS"
    IFS=$'\n'
    local alrb_defaultContAr=(
	'slc5|atlas-default|-e "slc5"'
	'centos6|atlas-default|-e "sl6" -e "slc6" -e "centos6" -e "rhel6"'
	'centos7|atlas-default|-e "sl7" -e "slc7" -e "centos7" -e "rhel7"'
	'almalinux8|atlas-default|-e "sl8" -e "slc8" -e "centos8" -e "centos8s" -e "rhel8" -e "almalinux8" -e "alma8"'
	'almalinux9|atlas-default|-e "sl9" -e "slc9" -e "centos9" -e "almalinux9" -e "alma9" -e "rhel9" -e "el9"'
    )

    local alrb_defaultCont
    for alrb_defaultCont in ${alrb_defaultContAr[@]}; do
	local alrb_queryCmd="\echo $alrb_containerCandidate | \grep -i `\echo $alrb_defaultCont | \cut -f 3 -d '|'`"
	alrb_tmpVal=`eval $alrb_queryCmd 2>&1`
	if [ $? -eq 0 ]; then
	    alrb_containerType="`\echo  $alrb_defaultCont | \cut -f 2 -d '|'`"
	    local alrb_contNamePrefix=`\echo $alrb_defaultCont | \cut -f 1 -d '|'`
	    alrb_containerAsSingularity="$alrb_singRepo/${alrb_archType}-${alrb_contNamePrefix}"
	    if [ -z $ALRB_CONT_DEFAULTCONTREPO ]; then
		alrb_container="registry.cern.ch/atlasadc/atlas-grid-$alrb_contNamePrefix"
	    else
		alrb_container="$ALRB_CONT_DEFAULTCONTREPO/atlasadc/atlas-grid-$alrb_contNamePrefix"
	    fi
	    break
	fi
    done
    IFS="$alrb_oldifs"
    
    return 0
}


alrb_fn_dockerJoinRunning()
{
    
    local let alrb_rc=0    
    local alrb_result
    alrb_result=`\echo $ALRB_CONT_CONDUCT | \grep -e ",dockerJoin,"`
    if [ $? -eq 0 ]; then
	alrb_result=`docker ps -f ancestor=$alrb_containerNameTag -f status=running --format "{{.Names}}" | env LC=ALL \sort | \tail -n 1`
	if [[ $? -eq 0 ]] && [[ "$alrb_result" != "" ]]; then
	    \echo "Docker: Join already running $alrb_result"
	    \echo "        Remember that if you exit that container, this session will die"
	    alrb_joinedRunningContainer="YES"
	    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} joining running container $alrb_result"
	    docker exec -it $alrb_result $alrb_shellPath/$ALRB_SHELL
	    alrb_rc=$?
	fi
    fi

    return $alrb_rc
}


alrb_fn_dockerBuild()
{

    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} build"
    
    local alrb_rc

    let alrb_dockerBuildPass+=1

    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} build ($alrb_dockerBuildPass)"
    
    local alrb_optDockerBuild="$alrb_optContainerBuilld"
    
    local alrb_dockerContDir="$alrb_contWorkdir/files/$alrb_container"
    mkdir -p $alrb_dockerContDir

    local alrb_proxyCachedContainer
    alrb_fn_getProxyCachedRegistryURL "$alrb_container"
    \rm -f ${alrb_dockerContDir}/dockerfile.new
    \cat << EOF > ${alrb_dockerContDir}/dockerfile.new
FROM $alrb_proxyCachedContainer

USER root

# First one works for RHEL, Ubuntu, SUSE; second one is in case of uid conflict
# Third one for dash, ash
RUN useradd --no-create-home --home-dir $ALRB_CONT_DUMMYHOME --uid $alrb_uid --shell $alrb_shellPath/$ALRB_SHELL $alrb_whoami || useradd --no-create-home --home-dir $ALRB_CONT_DUMMYHOME --shell $alrb_shellPath/$ALRB_SHELL $alrb_whoami || adduser -D -H -h $ALRB_CONT_DUMMYHOME -u $alrb_uid -s $alrb_shellPath/$ALRB_SHELL $alrb_whoami

RUN mkdir -p $ALRB_CONT_DUMMYHOME $ALRB_CONT_DUMMYALRB /srv /scratch /tmp /cvmfs /home $alrb_targetMount

EOF

    if [ "$alrb_buildFile" != "" ]; then
	\cat $alrb_buildFile >> ${alrb_dockerContDir}/dockerfile.new 
    fi
    
    \cat << EOF >> ${alrb_dockerContDir}/dockerfile.new

USER $alrb_whoami

CMD [ "$alrb_shellPath/$ALRB_SHELL" ]

ENTRYPOINT [ "" ]

EOF

    if [[ "$alrb_dockerBuildPass" -gt 1 ]] && [[ -e ${alrb_dockerContDir}/dockerfile ]]; then
	diff ${alrb_dockerContDir}/dockerfile ${alrb_dockerContDir}/dockerfile.new > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    return 0
	fi
    fi
    \mv ${alrb_dockerContDir}/dockerfile.new ${alrb_dockerContDir}/dockerfile
    if [ "$alrb_buildFile" != "" ]; then
	\echo "     * * * modified user dockerfile * * * "
	\cat ${alrb_dockerContDir}/dockerfile
	\echo "                   * * * "
    fi

    local alrb_platform0=""
    if [ "$alrb_defaultPlatform" != "" ]; then
        alrb_platform0="_`\echo $alrb_defaultPlatform | \sed -e 's|--platform ||g' -e 's|/|-|g'`"
    fi

    alrb_containerNameTag="$alrb_containerName$alrb_platformName"
    alrb_cmd="docker $ALRB_CONT_OPTS build $alrb_optDockerBuild $alrb_defaultPlatform --pull $alrb_QuietOpt -t $alrb_containerNameTag ${alrb_dockerContDir}"
    \echo "$alrb_cmd"
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} $alrb_cmd"
    eval $alrb_cmd 2>&1
    alrb_rc=$?
    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} finished build"
    
    return $alrb_rc
}


alrb_fn_dockerGetPathLookup()
{

    ALRB_CONT_SED2HOST=""
    ALRB_CONT_SED2CONT=""

    if [ ! -z "$ALRB_CONT_STARTCMD" ]; then
	eval set -- "$ALRB_CONT_STARTCMD"
	local alrb_item
	
	while [ $# -gt 0 ]; do
	    case $1 in
		-v|--volume)
		    local alrb_tmp1=`\echo $2 | \cut -f 1 -d ":"`
		    local alrb_tmp2=`\echo $2 | \cut -f 2 -d ":"`
		    alrb_fn_buildSedPath "$alrb_tmp1" "$alrb_tmp2"
		    shift 2
		    ;;
		--mount)
		    local alrb_source=""
		    local alrb_target=""
		    for alrb_item in `\echo $2 | \sed -e 's|,| |g'`; do
			local alrb_tmp1=`\echo $alrb_item | \cut -f 1 -d "="`
			local alrb_tmp2=`\echo $alrb_item | \cut -f 2 -d "="`
			if [[ "$alrb_tmp1" = "source" ]] || [[ "$alrb_tmp1" = "src" ]] ; then
			    alrb_source="$alrb_tmp2"
			elif [[ "$alrb_tmp1" = "target" ]] || [[ "$alrb_tmp1" = "destination" ]] || [[ "$alrb_tmp1" = "dst" ]]; then
			    alrb_target="$alrb_tmp2"
			fi
		    done

		    if [[ "$alrb_source" != "" ]] && [[ "$alrb_target" != "" ]]; then
			alrb_fn_buildSedPath "$alrb_source" "$alrb_target"
		    fi
		    shift 2
		    ;;
		--)
		    shift
		    break
		    ;;
		*)
		    shift
		    ;;
	    esac
	done
    fi

    if [ "$ALRB_CONT_SED2HOST" != "" ]; then
	export ALRB_CONT_SED2HOST
    fi
    if [ "$ALRB_CONT_SED2CONT" != "" ]; then
	export ALRB_CONT_SED2CONT
    fi
    
    return 0    
}


alrb_fn_dockerConstructCommand()
{

    # docker does not handle aliases properly on MacOS so fix this ...
    if [ "$alrb_cvmfs_mount" != "" ]; then
	alrb_cvmfs_mount=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_cvmfs_mount`
    fi
    
    local alrb_targetMount=""
    local alrb_optStr="--mount \"type=bind,source=$alrb_contDummyHome,target=$ALRB_CONT_DUMMYHOME\""

    if [ ! -z $ALRB_CONT_DUMMYALRB ]; then 
	alrb_optStr="$alrb_optStr --mount \"type=bind,source=$ATLAS_LOCAL_ROOT_BASE,target=$ALRB_CONT_DUMMYALRB\""
    fi
    
    if [ "$alrb_containerSkipCvmfs" != "YES" ]; then
	\echo "Info: /cvmfs mounted; do 'setupATLAS -d -c ...' to skip default mounts."
	local alrb_bindProp=""
	local alrb_tmpVal
	if [ "$ALRB_OSTYPE" != "MacOSX" ]; then
	    alrb_tmpVal="`findmnt -n /cvmfs 2>&1`"
	    if [ $? -eq 0 ]; then
		\echo $alrb_tmpVal | \grep -e " autofs " > /dev/null 2>&1
		if [ $? -eq 0 ]; then
		    alrb_bindProp=",bind-propagation=shared"
		fi
	    else
		alrb_tmpVal="`\cat /proc/1/mountinfo 2>&1 | \grep cvmfs.atlas.cern.ch | \grep -e 'shared'`"
		if [ "$alrb_tmpVal" != "" ]; then
		    alrb_bindProp=",bind-propagation=shared"
		fi
	    fi
	fi
	alrb_optStr="$alrb_optStr --mount \"type=bind,source=$alrb_cvmfs_mount,target=/cvmfs,readonly,consistency=cached$alrb_bindProp\""
    fi

    if [ "$alrb_containerSkipHome" != "YES" ]; then
	\echo "Info: \$HOME mounted; do 'setupATLAS -d -c ...' to skip default mounts."
    	alrb_optStr="$alrb_optStr --mount \"type=bind,source=$alrb_userParentMnt,target=$alrb_userParentMntCont\""
	if [[ "$alrb_userParentMntCont" != "/home" ]] && [[ "$alrb_userParentMntCont" != "/" ]]; then
	    alrb_targetMount="$alrb_targetMount $alrb_userParentMntCont"
	fi				    
    fi
    
    alrb_optStr="$alrb_optStr --mount \"type=bind,source=$alrb_pwd,target=/srv\""
    
    if [[ ! -z $ALRB_CONT_REALTMPDIR ]] && [[ "$ALRB_CONT_REALTMPDIR" = "/scratch" ]]; then
	alrb_optStr="$alrb_optStr --mount \"type=bind,source=$TMPDIR,target=/scratch\""
    fi

    if [ "$alrb_bindKRB5CCNAME" = "YES" ]; then
	alrb_optStr="$alrb_optStr -v $alrb_krbNameHost:$alrb_krbNameContainer:Z"
    fi
    
    # need to possibly rebuild in case we changed shells etc from first pass.
    alrb_fn_dockerBuild
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    alrb_dockerName="$alrb_containerName-`date +%s`" 
    alrb_cmdRunPayload="docker $ALRB_CONT_OPTS run $ALRB_CONT_CMDOPTS $alrb_defaultPlatform -e \"ENV=$ALRB_CONT_DUMMYHOME/$alrb_rcFile\""

    alrb_cmdRunPayload="$alrb_cmdRunPayload $alrb_optStr --mount \"type=bind,source=/tmp,target=/tmp\" -it --rm --name $alrb_dockerName $alrb_containerNameTag"
    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Docker: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `which docker`"
	\echo "ContainerType: $alrb_containerType"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi
    
    return 0
}


alrb_fn_dockerGuessModifiers()
{

    docker $ALRB_CONT_OPTS run $ALRB_CONT_CMDOPTS --mount "type=bind,source=$alrb_contDummyHome,target=$ALRB_CONT_DUMMYHOME" -it --rm $alrb_containerNameTag /bin/sh $ALRB_CONT_DUMMYHOME/probeContainer.sh
    return $?
}


alrb_fn_dockerConstructCommandDumb()
{
    
    alrb_cmdRunPayload="docker $ALRB_CONT_OPTS run $ALRB_CONT_CMDOPTS $alrb_container $ALRB_CONT_RUNPAYLOAD"
    
    if [ "$alrb_Quiet" = "NO" ]; then
	\echo "------------------------------------------------------------------------------"
	\echo "Docker: $alrb_contVer"
	\echo "Host: $ALRB_CONT_HOSTINFO"
	\echo "From: `which docker`"
	\echo "Note: This is running with -x/--dumb option and bypasses ALRB features."
	\echo "      If you have issues, check first that it works without -x/--dumb"
	\echo "$alrb_cmdRunPayload"
	\echo "------------------------------------------------------------------------------"
    fi

    return $?

}
