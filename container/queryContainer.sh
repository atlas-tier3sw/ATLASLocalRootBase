#! /bin/bash 
#!----------------------------------------------------------------------------
#!
#! queryContainer.sh
#!
#! finds a container
#!
#! Usage:
#!     queryContainer.sh  -h
#!
#! History:
#!   10Feb24: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_progname="queryC"

alrb_fn_queryContainerHelp()
{
    \cat <<EOF

    Notes:
      - Not supported for shifter runtime.
      - The search here is mainly meant for ATLAS containers; others
	 are best-effort basis.

    $alrb_progname find <comma delimited search string> [options]
      find is a command to search for an ATLAS standalone release
       eg: $alrb_progname find analysisbase,alma9,24.2

    $alrb_progname whatis <container name or alias> [options]
      whatis is a commmand to lookup a release
       eg  $alrb_progname whatis CG/AA%analysisbase:22.2.59
	   $alrb_progname whatis atlas/athena:23.0.27

    Options to override default:
     -h --help      	Print this help message
     -s --swtype=STRING Overwrites \$ALRB_CONT_SWTYPE or the default
			(apptainer for Linux, docker for MacOS)

    The inquiry returns (exit code 0 if successful):
      alrb alias	a run-time independent container URL
                 	 eg to run anywhere:
			  setupATLAS -c <alrb alias>
			The format of the alrb alias is
			  <prefix>%<container alias or tag>
			where prefix is a code for
			  <registry>/<registry path>
			with 00/00 meaning the default ATLAS OS containers
			 
      registry   	URL to the registry package and tag
      			 eg crane ls <registry>
      			  
      <runtime>         <runtime> can be any of the supported runtimes
      			 This can also be used on this machine as
			  setupATLAS -c <runtime>
      
      path x86_64	path to unpacked container on cvmfs for x86_64 
      	    		 This works only for apptainer

      path aarch64	path to unpacked container on cvmfs for aarch64
      	    		 This works only for apptainer

      architecture	container architectures supported by container
      			 from crane manifest <registry>
EOF

    return 0
}


alrb_fn_doFind()
{
    alrb_result=`type -t alrb_fn_findReleaseUI`
    if [[ $? -ne 0 ]] || [[ "$alrb_result" != "function" ]]; then
	source $ATLAS_LOCAL_ROOT_BASE/container/findFunctions.sh
	alrb_fn_initializeFindFunctions
    fi

    # for this, stop and use if search results are = 1 
    # if not set, will ask for user confirmation which is good in some cases
    alrb_findShowUnique="YES"
    alrb_fn_findReleaseUI "$alrb_findShowUnique" "$@"
        
    return $?
}


alrb_fn_doWhatIs()
{
    local alrb_inquiry="$1"

    alrb_result=`type -t alrb_fn_whatIs`
    if [[ $? -ne 0 ]] || [[ "$alrb_result" != "function" ]]; then
	source $ATLAS_LOCAL_ROOT_BASE/container/findFunctions.sh
	alrb_fn_initializeFindFunctions
    fi
    
    alrb_fn_whatIs "$@"
    return $?
    
}


# * * * main * * * 

alrb_shortopts="h,s:"
alrb_longopts="help,swtype:"
alrb_result=`getopt -T >/dev/null 2>&1`
if [ $? -eq 4 ] ; then # New longopts getopt.
    alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_progname" -- "$@")
    alrb_returnVal=$?
else # use wrapper
    alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt.sh $alrb_shortopts $alrb_longopts $*`
    alrb_returnVal=$?
fi
    
# do we have an error here ?
if [ $alrb_returnVal -ne 0 ]; then
    \echo $alrb_opts 1>&2    
    \echo "$alrb_progname --help for usage" 1>&2
    exit 1
fi

eval set -- "$alrb_opts"

if [ ! -z $ALRB_CONT_SWTYPE ]; then
    alrb_swtype="$ALRB_CONT_SWTYPE"
elif [ ! -z $ALRB_OSTYPE ]; then
    if [ "$ALRB_OSTYPE" = "Linux" ]; then
	alrb_swtype="apptainer"
    else
	alrb_swtype="docker"
    fi  
fi

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
	    alrb_fn_queryContainerHelp
	    exit 0
            ;;
	-s|--swtype)
	    alrb_swtype="$2"
	    shift 2
	    ;;
	--)
	    shift
	    break
	    ;;
        *)
	    \echo "Internal Error: option processing error: $1" 1>&2
	    exit 1
	    ;;
    esac
done

export ALRB_CONT_SWTYPE="$alrb_swtype"
let alrb_retCode=0
alrb_action=""
alrb_actionVal=""
if [ "$#" -ne 2 ] ; then 
    alrb_fn_queryContainerHelp
    \echo "
Error: missing arguments; you need to specify 
       $alrb_progname find whatever
       $alrb_progname whatis whatever
"
    exit 64
else
    alrb_action="$1"
    alrb_actionVals="$2"
fi

alrb_skipCheckCvmfs=""
if [ ! -d "$ALRB_cvmfs_unpacked_repo/logDir" ]; then
    alrb_foundOnCvmfs="Not found - cvmfs unpacked repo is missing"
    alrb_skipCheckCvmfs="YES"
fi

if [ "$alrb_action" = "find" ]; then
    \echo "$alrb_actionVals" | \grep -E -e '%' -e ':' -e '/' > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	\echo "
Error: find is to look for specific ATLAS projects.
         eg: $alrb_progname find analysisbase,alma9,24.2
       where supported projects are:
        `\echo ${alrb_relProjAr[@]} | \sed -E -e 's|&[[:alnum:]/]*||g'`
       Did you mean to do instead:
        $alrb_progname whatis $alrb_actionVals
"
	exit 64
    fi
    alrb_fn_doFind "$alrb_actionVals"
    alrb_retCode=$?
elif [ "$alrb_action" = "whatis" ]; then
    alrb_fn_doWhatIs "$alrb_actionVals"
    alrb_retCode=$?
else
    \echo "Error: action $alrb_action is unknown"
    alrb_fn_queryContainerHelp
    exit 64
fi

exit $alrb_retCode
