#!/bin/bash
#!----------------------------------------------------------------------------
#!
#! createConvertToAliasSedFile
#!
#! create a sed command file to use for changing URl to aliases
#!
#! Usage:
#!     internal usage in ALRB
#!
#! History:
#!   13Sep24: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

source $ATLAS_LOCAL_ROOT_BASE/container/findFunctions.sh

alrb_fn_initializeFindFunctions

alrb_fn_createConvertToAliasSedFile $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/sedConvertToAlias.txt

exit $?
