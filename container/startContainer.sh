#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! startContainer.sh
#!
#! Start up an appropriate container and setupATLAS
#!
#! Usage:
#!     startContainer.sh -h
#!
#! History:
#!    11Dec17: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_progname=startContainer.sh

if [ -z "$ALRB_progDisplayName" ]; then
    alrb_progDisplayName="setupATLAS -c"
else
    alrb_progDisplayName="$ALRB_progDisplayName"
fi

alrb_fn_startContainerHelp()
{

    local alrb_result
    alrb_result=`type -t alrb_fn_startContainerHelpMe`
    if [[ $? -ne 0 ]] || [[ "$alrb_result" != "function" ]]; then
	source $ATLAS_LOCAL_ROOT_BASE/container/helpFunctions.sh
    fi
    alrb_fn_startContainerHelpMe

    return 0
}


set_ALRB_ContainerEnv() 
{
    
    local alrb_delimit=""
    local alrb_setIt=""
    if [ "$1" = "ALRB_CONT_OPTS" -o "$1" = "ALRB_CONT_CMDOPTS" ]; then
	alrb_delimit=" "
    elif [ "$1" = "ALRB_CONT_PRESETUP" -o "$1" = "ALRB_CONT_POSTSETUP" -o "$1" = "ALRB_CONT_RUNPAYLOAD" ]; then
	alrb_delimit=";"
    elif [ "$1" = "ALRB_CONT_CONDUCT" ]; then
	alrb_delimit=","
    elif [ "$1" = "ALRB_CONT_SWTYPE" -o "$1" = "ALRB_CONT_USERCONFIGBATCH" -o "$1" = "ALRB_CONT_GUESSMOD" -o "$1" = "ALRB_CONT_SETUPFILE" ]; then
	alrb_setIt="YES"
    elif [ "$1" = "ALRB_CONT_RUNTIMEALT" ]; then
	local alrb_oldifs="$IFS"
	IFS=$'\n'
	local alrb_tmpAr=( `\echo $2 | tr '&' '\n'` )
	local alrb_tmpVal
	for alrb_tmpVal in "${alrb_tmpAr[@]}"; do
	    alrb_runtimeAltAr=( "${alrb_runtimeAltAr[@]}" "$alrb_tmpVal" )
	done
	IFS="$alrb_oldifs"
	# skip setting this as it will be done later depending on runtime
	return 0	
    elif [ "$1" = "ALRB_CONT_RUNTIMEOPT" ]; then
	local alrb_oldifs="$IFS"
	IFS=$'\n'
	local alrb_tmpAr=( `\echo $2 | tr '&' '\n'` )
	local alrb_tmpVal
	for alrb_tmpVal in "${alrb_tmpAr[@]}"; do
	    alrb_runtimeOptAr=( "${alrb_runtimeOptAr[@]}" "$alrb_tmpVal" )
	done
	IFS="$alrb_oldifs"
	# skip setting this as it will be done later depending on runtime
	return 0
    else
	\echo "Error: unknown ALRB Container variable $1, see"
	\echo "       https://twiki.atlas-canada.ca/bin/view/AtlasCanada/Containers#Advanced_Options"
	return 64
    fi

    if eval test -z \"\${$1}\" ; then
	alrb_setIt="YES"
    fi
    
    if [ "$alrb_setIt" = "YES" ]; then
	export $1="$2"
    else
	eval \echo "\${$1}" | \grep -e "$2" > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	    local alrb_cmd="\${$1}"
	    export $1="`eval \echo $alrb_cmd`$alrb_delimit$2" 
	fi
    fi
    
    return 0
}


run_ALRB_UserContainerConfig() 
{
    alrb_userConfigDone="YES"
    if [ -e "$HOME/alrb_container.cfg.sh" ]; then
	source $HOME/alrb_container.cfg.sh
	return $?
    fi
    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_printMsg()
#!---------------------------------------------------------------------------- 
{
   
    if [[ $alrb_contVerbose -eq 0 ]] && [[ $1 -eq 0 ]]; then
	\echo "$2"
    elif [ $1 -le $alrb_contVerbose ]; then
	printf "%-s | %s \n" "`date -u '+%F %T'` (ALRB: `expr \`date +%s\` - $alrb_time0` s)" "$2"
    fi
    
    return 0
}


alrb_fn_cleanup()
{
    if [ -e $alrb_contDummyHome/timing.sh ]; then
	source $alrb_contDummyHome/timing.sh
	alrb_fn_printMsg 1 "Container was ready for payload after `expr $alrb_timePayload0 - $alrb_time0` s"
    fi

    if [ "$alrb_pipeExecutor" != "" ]; then
	kill -9 $alrb_pipeExecutor > /dev/null 2>&1
    fi
    if [ ! -z $ALRB_CONT_PIPEDIRHOST ]; then
	\rm -rf $ALRB_CONT_PIPEDIRHOST
    fi
    if [ "$alrb_contDummyHome" != "" ]; then
	\rm -rf $alrb_contDummyHome
    fi

    alrb_fn_printMsg 1 "Finished running container"
    
    return 0
}


alrb_fn_krbBind()
{
    local alrb_result

    if [ ! -z $KRB5CCNAME ]; then
	alrb_krbType="`\echo $KRB5CCNAME | \cut -f 1 -d ':'`"
	if [[ "$alrb_krbType" != "FILE" ]] && [[ "$alrb_krbType" != "DIR" ]]; then
	    alrb_bindKRB5CCNAME="NO"
	    return 0
	fi
	alrb_bindKRB5CCNAME="YES" 
	alrb_krbNameHost="`\echo $KRB5CCNAME | \cut -f 2 -d ':'`"
	alrb_krbNameContainer="$alrb_krbNameHost"
#	if [ "$alrb_containMinimal" = "YES" ]; then
#	    alrb_bindKRB5CCNAME="YES"
#	    alrb_krbNameContainer="/ALRB_HOSTBIND_KRB5CCNAME"
#	else
#	    \echo $KRB5CCNAME | \grep -E ":/tmp/" > /dev/null 2>&1
#	    if [ $? -ne 0 ]; then
#		alrb_bindKRB5CCNAME="YES"
#		alrb_krbNameContainer="/ALRB_HOSTBIND_KRB5CCNAME"
#	    fi
#	fi
	alrb_result=`type -t alrb_fn_${ALRB_CONT_SWTYPE}SetEnvVar`
	if [[ $? -eq 0 ]] && [[ "$alrb_result" = "function" ]]; then
	    alrb_fn_${ALRB_CONT_SWTYPE}SetEnvVar KRB5CCNAME "$alrb_krbType:$alrb_krbNameContainer"
	fi
	
    fi

    return 0
}


alrb_fn_saveEnvs()
{

    local alrb_tmpVal
    
# in certain circumstances, save the proxy to another dir if it exists
    if [ ! -z $X509_USER_PROXY ]; then
	\echo $X509_USER_PROXY | \grep -E "^/tmp/" > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	    alrb_copyProxy="YES"
	fi
	if [[ "$alrb_copyProxy" = "YES" ]] && [[ -e $X509_USER_PROXY ]]; then
	    \cp $X509_USER_PROXY $alrb_contDummyHome/
	    alrb_tmpVal=`basename $X509_USER_PROXY`
	    X509_USER_PROXY="$ALRB_CONT_DUMMYHOME/$alrb_tmpVal"
	fi    
    fi
	     
    if [ "$alrb_localALRB" = "YES" ]; then
	export ALRB_CONT_HOSTALRBDIR=$ATLAS_LOCAL_ROOT_BASE
    fi

    alrb_envFile="$alrb_contDummyHome/envs"
    touch $alrb_envFile
    alrb_tmpVal='env | \grep 
 -e "KRB[[:alnum:]]*="
 -e "USER="
 -e "DISPLAY="
 -e "SITE_NAME="
 -e "PANDA_SITE_NAME="
 -e "PANDA_RESOURCE="
 -e "SSH_AUTH_SOCK="
 -e "ATLAS_SITE_NAME="
 -e "X509_USER_PROXY="
 -e "RUCIO_ACCOUNT="
 -e "FRONTIER_SERVER="
 -e "FRONTIER_LOG_[[:alnum:]]*="
 -e "ALRB_[[:alnum:]]*Version="
 -e "ALRB_menuFmtSkip="
 -e "ALRB_CONT_[[:alnum:]]*="
 -e "ALRB_localConfigDir="
 -e "ALRB_testPath="
 -e "ALRB_doPostASetup="
 -e "^http_proxy="
 -e "^https_proxy="
 -e "ALRB_USE_PY2="
 -e "ALRB_USE_PY3="
 -e "ALRB_noFrontierSetup="
 -e "ATLAS_LOCAL_ROOT_ARCH_OVERRIDE="
 -e "ALRB_allowOverrides="
 -e "ALRB_XCACHE_PROXY.*="
 -e "COOL_ORA_ENABLE_ADAPTIVE_OPT="
 -e "ALRB_asetupPacparser="
| \cut -f 1 -d "="
'

    for alrb_item in `eval $alrb_tmpVal`; do
	\echo "export ${alrb_item}=\"${!alrb_item}\"" >> $alrb_envFile
    done

    if [ "$alrb_krbNameContainer" != "" ]; then
	\echo "export KRB5CCNAME=\"$alrb_krbType:$alrb_krbNameContainer\"" >> $alrb_envFile
    fi

    alrb_postEnvFile="$alrb_contDummyHome/postEnvs"
    \echo "# this needs to run post setupATLAS to avoid eg LD_PRELOAD issues" > $alrb_postEnvFile
    alrb_tmpVal='env | \grep 
 -e "ALRB_CONTENV_.*="
| \cut -f 1 -d "="   	
'

    for alrb_item in `eval $alrb_tmpVal`; do
	alrb_itemMod=`\echo $alrb_item | \sed -e 's|ALRB_CONTENV_||g'`
	\echo "export ${alrb_itemMod}=\"${!alrb_item}\"" >> $alrb_postEnvFile
    done
    
    if [ "$alrb_cont_display" != "" ]; then
	\echo "export DISPLAY=\"$alrb_cont_display\"" >> $alrb_envFile
    fi
    \echo "export ALRB_CONT_HOSTOS=\"$ALRB_OSTYPE\"" >> $alrb_envFile
    \echo "export ALRB_CONT_HOSTARCH=\"$ATLAS_LOCAL_ROOT_ARCH\"" >> $alrb_envFile
    
# changes for ensuring that time is set correctly (fails on some machines)
# would be nice to bind mount this but needs underlay at all sites
    if [ -e "/alrb/localtime" ]; then
	\cp /alrb/localtime $alrb_contDummyHome/
	export ALRB_CONT_TZ="/alrb/localtime"
    elif [ -e "/etc/localtime" ]; then
	\cp /etc/localtime $alrb_contDummyHome/
	export ALRB_CONT_TZ="/alrb/localtime"
    fi    
    if [ ! -z "$ALRB_CONT_TZ" ]; then
	\echo "export TZ=\"$ALRB_CONT_TZ\"" >> $alrb_envFile
    fi

    if [ "$alrb_containerType" != "" ]; then
	\echo "export ALRB_containerType=\"$alrb_containerType\"" >> $alrb_envFile
    fi
    
    return 0
}


alrb_fn_createDummyHome()
{

    alrb_fn_printMsg 1 "Creating dummy home dir and scripts"

    alrb_contDummyHome=`\mktemp -d $alrb_contWorkdir/home.XXXXXX`
    if [ $? -ne 0 ]; then
	return 64
    fi
    chmod a+rx $alrb_contDummyHome
    \mkdir -p $alrb_contDummyHome/ATLASLocalRootBase
    
    \cp $ATLAS_LOCAL_ROOT_BASE/etc/postATLASReleaseSetup*.sh $alrb_contDummyHome/
    
    \cat << EOF >> $alrb_contDummyHome/probeContainer.sh
#!/bin/sh

if [ \$HOME ]; then
 cd
fi

alrb_str=""

if [ -d /cvmfs/atlas.cern.ch/repo/sw ]; then
    alrb_str="\$alrb_str;alrb_containerSkipCvmfs=\"YES\""
fi

if [ ! -e /etc/profile.d/container-date.sh ]; then 
    alrb_str="\$alrb_str;export ALRB_CONT_SKIPALRB=\"YES\""
    if [ -e /release_setup.sh ]; then
      alrb_str="\$alrb_str;alrb_containerType=\"non-atlas-standalone\""
    else
      alrb_str="\$alrb_str;alrb_containerType=\"non-atlas\""
    fi
else
    if [ -e /release_setup.sh ]; then
      alrb_str="\$alrb_str;export ALRB_CONT_SKIPALRB=\"YES\""	
      alrb_str="\$alrb_str;alrb_containerType=\"atlas-standalone\""
    else
      alrb_str="\$alrb_str;alrb_containerType=\"atlas-derived\""
    fi
fi

if [ ! -e /bin/$ALRB_SHELL ]; then
    if [ -e /bin/bash ]; then
      alrb_str="\$alrb_str;export ALRB_SHELL=\"bash\""
    elif [ -e /usr/bin/bash ]; then
      alrb_str="\$alrb_str;export ALRB_SHELL=\"bash\";alrb_shellPath=/usr/bin"
    else
      alrb_str="\$alrb_str;export ALRB_SHELL=\"sh\""
    fi
fi

alrb_checkHome=\`\find /home -maxdepth 1 -type d | \egrep -v -e "^/home/$alrb_whoami\$" -e "^/home\$"\`
if [ "\$alrb_checkHome" != "" ]; then
   alrb_str="\$alrb_str;alrb_containerSkipHome=\"YES\"" 
fi

#\echo -n "\$alrb_str" | \sed -e 's|^;||g'
\echo -n "ALRB_PROBECONTAINERRESULTS \$alrb_str"

exit 0
EOF

    chmod +x $alrb_contDummyHome/probeContainer.sh

    return 0
}


alrb_fn_createLoginScripts()
{
    alrb_fn_printMsg 1 "Create login scripts"

    \cat << EOF >> $alrb_contDummyHome/checkArch.sh
#! /bin/bash

if [ -e \$ATLAS_LOCAL_ROOT_BASE/logDir/installed ]; then
  let alrb_installedTools=\`\grep -e "\$ATLAS_LOCAL_ROOT_ARCH/" \$ATLAS_LOCAL_ROOT_BASE/logDir/installed | wc -l\`
  if [ \$alrb_installedTools -eq 0 ]; then
      \echo "Error: no tools available for this platform \$ATLAS_LOCAL_ROOT_ARCH"
      \echo "       The host platform was $ATLAS_LOCAL_ROOT_ARCH ($ALRB_OSTYPE)"
      \echo "       Host had ALRB on $ATLAS_LOCAL_ROOT_BASE"
      \echo "       Install ALRB tools for \$ATLAS_LOCAL_ROOT_ARCH (\$ALRB_OSTYPE) on host."
      exit 64
  fi
fi
exit 0
EOF

    chmod +x $alrb_contDummyHome/checkArch.sh 

    alrb_rcFile=""
    local alrb_shellCmd="$alrb_shellPath/$ALRB_SHELL"
    if [ "$ALRB_SHELL" = "bash" ]; then
	alrb_rcFile=".bashrc"
    elif [ "$ALRB_SHELL" = "zsh" ]; then
	alrb_rcFile=".zshrc"
    elif [ "$ALRB_SHELL" = "sh" ]; then
	alrb_rcFile=".bashrc"
    else
	\echo "Error unknown shell type $ALRB_SHELL" 1>&2
	return 64
    fi
    
    touch $alrb_contDummyHome/$alrb_rcFile
    if [ ! -z "$ALRB_CONT_RUNPAYLOAD" ]; then
	\echo "#! $alrb_shellCmd" > $alrb_contDummyHome/$alrb_rcFile
	chmod +x $alrb_contDummyHome/$alrb_rcFile
	alrb_cont_payload="$ALRB_CONT_DUMMYHOME/$alrb_rcFile"
    fi
    
    \cat <<EOF >> $alrb_contDummyHome/$alrb_rcFile

alrb_fn_contPostSetupMenu()
{
  $ALRB_CONT_DUMMYHOME/checkArch.sh
  if [ \$? -ne 0 ]; then
    return 64
  fi

  # temporary solution for singularity missing this
  \grep -e "CERN\.CH" /etc/krb5.conf > /dev/null 2>&1
  if [ \$? -ne 0 ]; then
    export KRB5_CONFIG=\$ATLAS_LOCAL_ROOT_BASE/user/krb5.conf
  fi

  if [ ! -z "\$ALRB_CONT_BATCHPATH" ]; then
    eval \$ALRB_CONT_BATCHPATH
  fi

  appendPath PATH \$ATLAS_LOCAL_ROOT_BASE/containerUtils

  return 0
}


. $ALRB_CONT_DUMMYHOME/envs

if [[ "$alrb_whoami" = "nobody" ]] || [[ "$alrb_whoami" = "nfsnobody" ]]; then
  alrb_whoamiReally="\`whoami\`"
  if [[ ! -z \$USER ]] && [[ "\$USER" != "\$alrb_whoamiReally" ]]; then
    export USER="\$alrb_whoamiReally"
  fi   
fi

if [ ! -z \$ALRB_CONT_DUMMYALRB ]; then
   export ALRB_localRelocateDir="$ALRB_CONT_DUMMYHOME/relocate"
   \mkdir -p \$ALRB_localRelocateDir
   export ALRB_RELOCATEALRB="YES"
fi

if [ -e /etc/profile.d/container-date.sh ]; then
  . /etc/profile.d/container-date.sh
fi

if [ ! -z \$ALRB_CONT_REALHOME ]; then
    export HOME=\$ALRB_CONT_REALHOME
    if [ -d \$ALRB_CONT_REALHOME/.globus ]; then
        if [ ! -e \$ALRB_CONT_DUMMYHOME/.globus ]; then
  	  ln -s \$ALRB_CONT_REALHOME/.globus  \$ALRB_CONT_DUMMYHOME/
        fi
    fi
    if [ ! -e \$ALRB_CONT_DUMMYHOME/.ssh ]; then
      if [ -d \$ALRB_CONT_REALHOME/.ssh.container ]; then
        ln -s \$ALRB_CONT_REALHOME/.ssh.container \$ALRB_CONT_DUMMYHOME/.ssh
      else
        mkdir -p \$ALRB_CONT_REALHOME/.ssh
        ln -s \$ALRB_CONT_REALHOME/.ssh  \$ALRB_CONT_DUMMYHOME/
      fi
    fi
fi

if [ ! -z \$ALRB_CONT_REALTMPDIR ]; then
    export TMPDIR=\$ALRB_CONT_REALTMPDIR
fi

# workaround for ssh keys for MacOS
if [ "\$ALRB_CONT_HOSTOS" = "MacOSX" ]; then
  which ssh-agent > /dev/null 2>&1
  if [ \$? -eq 0 ]; then
    eval \`ssh-agent -s | \sed -e 's/^echo/#echo/'\`
  fi
fi

if [ -e \$HOME/${alrb_rcFile}.container ]; then
  . \$HOME/${alrb_rcFile}.container
fi

cd \$ALRB_CONT_CHANGEPWD

if [ ! -z "\$ALRB_CONT_PRESETUP" ]; then
    eval \$ALRB_CONT_PRESETUP
fi

if [ "$alrb_localALRB" = "YES" ]; then
   if [ -e \$ALRB_CONT_DUMMYALRB/logDir/lastUpdate ]; then
    export ATLAS_LOCAL_ROOT_BASE=\$ALRB_CONT_DUMMYALRB
  else	   
    \echo "Warning: localalrb specified but could not find local ALRB release"
  fi
fi

if [ -z \$ATLAS_LOCAL_ROOT_BASE ]; then
  if [ -d /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ];  then
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  elif [ -e \$ALRB_CONT_DUMMYALRB/logDir/lastUpdate ]; then 
    export ATLAS_LOCAL_ROOT_BASE=\$ALRB_CONT_DUMMYALRB	
  fi
fi

\echo "let alrb_timePayload0=\`date +%s\`" > \$ALRB_CONT_DUMMYHOME/timing.sh

if [ -z \$ALRB_CONT_SKIPALRB ]; then
  . \$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh $alrb_contSetupATLASOpts
  if [ \$? -ne 0 ]; then
    exit 64
  fi  
  alrb_fn_contPostSetupMenu 
elif [ ! -z \$ATLAS_LOCAL_ROOT_BASE ]; then
  if [ -e \$ATLAS_LOCAL_ROOT_BASE/logDir/limitedInstallation ]; then
    \echo " setupATLAS is available; note that available tools are limited.
          "
  else
    \echo " setupATLAS is available for this \$ALRB_containerType container type.
          "
  fi   
  setupATLAS()
  {
      . \$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh \$@
      if [ \$? -ne 0 ]; then
         return 64
       fi     
      alrb_fn_contPostSetupMenu
      return \$?
  }
fi

if [ "\$ALRB_noGridMW" != "YES" -a  -d \$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates ]; then
  export X509_CERT_DIR=\$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates
fi

if [ ! -z "\$ALRB_CONT_POSTSETUP" ]; then
    eval \$ALRB_CONT_POSTSETUP
fi

. $ALRB_CONT_DUMMYHOME/postEnvs

# post release setups
alrb_postRelFile=""
if [ -e /release_setup.sh ]; then
  alrb_result=\`\echo "\$ALRB_testPath" | \grep -e ",postRel-dev,"\` 
  if [ \$? -eq 0 ]; then
    alrb_postRelFile="/alrb/postATLASReleaseSetup-dev.sh"
  else
    alrb_postRelFile="/alrb/postATLASReleaseSetup.sh"
  fi
fi

if [ -e /release_setup.sh ]; then
  if [ -e /etc/motd ]; then
    \cat /etc/motd
    if [ "\$alrb_postRelFile" != "" ]; then
      \echo "also do afterwords" 
      \echo "      source \$alrb_postRelFile"
    fi
  fi
fi

if [ ! -z "\$ALRB_CONT_RUNPAYLOAD" ]; then
  if [ ! -z "\$ALRB_CONT_SETUPFILE" ]; then 
     if [ -e /release_setup.sh ]; then
       \echo " sourcing /release_setup.sh \$ALRB_CONT_SETUPFILEARGS"
       . /release_setup.sh \$ALRB_CONT_SETUPFILEARGS
       if [ \$? -ne 0 ]; then
         exit 64
       fi     
       if [ "\$alrb_postRelFile" != "" ]; then
         \echo " sourcing \$alrb_postRelFile "
         eval source \$alrb_postRelFile
       fi
     elif [ "\$ALRB_CONT_SETUPFILE" != "none" ]; then
       if [ -e "\$ALRB_CONT_SETUPFILE" ]; then
         \echo " sourcing \$ALRB_CONT_SETUPFILE \$ALRB_CONT_SETUPFILEARGS"
         . \$ALRB_CONT_SETUPFILE \$ALRB_CONT_SETUPFILEARGS 
         if [ \$? -ne 0 ]; then
           exit 64
         fi     
       else
          \echo "Error: unable to source setupfile \$ALRB_CONT_SETUPFILE"
	  exit 64
       fi
     fi   

     if [ ! -z "\$ALRB_CONT_SPOSTSETUP" ]; then
       \echo " running post release setup $ALRB_CONT_SPOSTSETUP ..."
       $ALRB_CONT_SPOSTSETUP
     fi
  fi   

  unset alrb_postRelFile

  eval \$ALRB_CONT_RUNPAYLOAD
  exit \$?

elif [ -e /release_setup.sh ]; then 

  # from https://its.cern.ch/jira/browse/ATCONDDB-60 for Oracle 19c
  # can be overwritten when user source /alrb/postATLASReleaseSetup*.sh
  #  in the future if we refine this for ATLAS versions
  if [ -z \$COOL_ORA_ENABLE_ADAPTIVE_OPT ]; then
      \echo "Setting COOL_ORA_ENABLE_ADAPTIVE_OPT=Y"
      export COOL_ORA_ENABLE_ADAPTIVE_OPT=Y
  else
      \echo "Unchanged: COOL_ORA_ENABLE_ADAPTIVE_OPT=$COOL_ORA_ENABLE_ADAPTIVE_OPT"
  fi

fi

EOF

    return 0
}


alrb_fn_parseContainerOptions()
{
# this is obsolete since we can now pass args through opts
    
    local alrb_item
    local alrb_itemLower
    for alrb_item in $(\echo $alrb_guessContainer | \sed "s/+/ /g"); do
	alrb_itemLower=`\echo $alrb_item | tr '[:upper:]' '[:lower:]'`
	case "$alrb_itemLower" in
            images)
		alrb_imageType="images"
		;;         
            nocvmfs)
		alrb_containerSkipCvmfs="YES"
		;;
	    localalrb)
		alrb_localALRB="YES"
		;;
            batch)
		alrb_containerBatch="YES"
		;;
            noalrb)
# backward compatbility
		export ALRB_CONT_SKIPALRB="YES"
		;;
            noalrbsetup)
		export ALRB_CONT_SKIPALRB="YES"
		;;
            bournesh)		
		export ALRB_SHELL="sh"
		;;
            *)
		alrb_containerCandidate="$alrb_item"
		;;
	esac
    done

    return 0
}


alrb_fn_X11Support()
{    
    
    if [ "$ALRB_OSTYPE" = "MacOSX" ]; then	
	if [ "$alrb_Quiet" = "NO" ]; then
	    \echo "MacOS X: Wait for XQuartz to start"
	fi
	open -a XQuartz
	if [ $? -ne 0 ]; then
	    return 64
	fi
	osascript -e 'activate application "Terminal"'

	local alrb_idx
	for alrb_idx in `seq 1 10`; do
	    sleep 1
	    alrb_result=`\ps -eo command | \grep -e "^/Applications/Utilities/XQuartz.app/Contents/MacOS/X11.bin"`
	    if [ $? -eq 0 ]; then
		break
	    fi    
        done    

	alrb_cont_display="docker.for.mac.localhost:0"

	sleep 1
	xhost + localhost
    fi

    return 0

}


alrb_fn_BatchInterface()
{
    
    ALRB_CONT_BATCHPATH=""
    if [ "$alrb_containerBatch" = "YES" ]; then
	alrb_fn_printMsg 1 "Startup batch interface to host"
	which bsub > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    ALRB_CONT_BATCHPATH="$ALRB_CONT_BATCHPATH appendPath PATH \\\$ATLAS_LOCAL_ROOT_BASE/wrappers/containers/lsf;"
	fi
	which condor_submit > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    ALRB_CONT_BATCHPATH="$ALRB_CONT_BATCHPATH appendPath PATH \\\$ATLAS_LOCAL_ROOT_BASE/wrappers/containers/condor;"
	fi
	which sbatch > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    ALRB_CONT_BATCHPATH="$ALRB_CONT_BATCHPATH appendPath PATH \\\$ATLAS_LOCAL_ROOT_BASE/wrappers/containers/slurm;"
	fi
	which qsub > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    ALRB_CONT_BATCHPATH="$ALRB_CONT_BATCHPATH appendPath PATH \\\$ATLAS_LOCAL_ROOT_BASE/wrappers/containers/pbs;"
	fi
    fi

    if [ "$ALRB_CONT_BATCHPATH" != "" ]; then

	alrb_fn_getFifoDir

	$ATLAS_LOCAL_ROOT_BASE/wrappers/containers/executor.sh > $alrb_contDummyHome/executor.log 2>&1 &
	alrb_pipeExecutor=$!
	disown
	local alrb_tmpVal=`\ps $alrb_pipeExecutor > /dev/null`
	if [ $? -ne 0 ]; then
	    \echo "Error: Batch system not integrated into container"
	    \echo "         because executor failed."
	    return 64
	fi
	export ALRB_CONT_BATCHPATH
    fi

    return $?
}


alrb_fn_getFifoDir()
{

    mkfifo $ALRB_CONT_HOSTDUMMYHOME/testpipeDeleteMe > /dev/null 2>&1
    local alrb_retCode=$?
    if [ "$alrb_containMinimal" != "YES" ]; then
	\mkdir -p /tmp/$alrb_whoami/.alrb/pipe
	local alrb_tmpVal=`\mktemp -d /tmp/$alrb_whoami/.alrb/pipe/pipe.XXXXX`
	if [ $? -ne 0 ]; then
	    \echo "Error: unable to create dir on /tmp/$alrb_whoami"
	    return 64
	fi
	export ALRB_CONT_PIPEDIRHOST="$alrb_tmpVal"
	export ALRB_CONT_PIPEDIR="$alrb_tmpVal"
    elif [ $alrb_retCode -eq 0 ]; then
	\rm -f $ALRB_CONT_HOSTDUMMYHOME/testpipeDeleteMe
	\mkdir -p $ALRB_CONT_HOSTDUMMYHOME/.pipe
	export ALRB_CONT_PIPEDIRHOST="$ALRB_CONT_HOSTDUMMYHOME/.pipe"
	export ALRB_CONT_PIPEDIR="/alrb/.pipe"	
    else
	\echo "Error: Unable to create pipe.  \$HOME filesystem dows not allow."
	\echo "       Also, you started container with contain/containall option"
	\echo "       That prevents using /tmp."
	return 64
    fi

    return 0
}


alrb_fn_constructCmd()
{

    alrb_fn_printMsg 1 "Construct commamd"
    
    # migration of delimiters from : to ?
    \echo "$alrb_guessContainer" | \grep -e "images:" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	\echo "Warning: please use '+' for delimitor instead of ':'"
	alrb_guessContainer=`\echo $alrb_guessContainer | \sed -e 's/images:/images+/'`
	\echo "         container should be specified as : $alrb_guessContainer" 
    fi
    
    alrb_pwd=`pwd`
    alrb_uid=`id -u`
    let alrb_gid=`id -g`
    if [[ "$ALRB_OSTYPE" = "MacOSX" ]] && [[ $alrb_gid -lt 1000 ]] ; then
# macos has low range of gid that may be incompatible with Linux
	alrb_gid=$alrb_uid
    fi
    alrb_whoami=`$ATLAS_LOCAL_ROOT_BASE/utilities/getUsername.sh`
    if [[ "$alrb_whoami" = "nobody" ]] || [[ "$alrb_whoami" = "nfsnobody" ]]; then
	\echo "Warning: host machine user is $alrb_whoami which is not recommended."
    fi
    alrb_cont_display=""
    alrb_cont_payload=""
    alrb_copyProxy="NO"
    alrb_bindKRB5CCNAME="NO"
    alrb_setContainerPwd=""
    alrb_containMinimal=""
    alrb_pipeExecutor=""
    alrb_containerType="unknown"

    alrb_result=`type -t alrb_fn_${ALRB_CONT_SWTYPE}OptsParser`
    if [[ $? -eq 0 ]] && [[ "$alrb_result" = "function" ]]; then
	alrb_fn_${ALRB_CONT_SWTYPE}OptsParser
    fi
    
    if [[ -z $HOME ]] || [[ "$HOME" = "/" ]]; then
	alrb_containerSkipHome="YES"
	alrb_contWorkdir="${ALRB_tmpScratch}/container/$ALRB_CONT_SWTYPE"
	alrb_currentDir=`pwd`
	cd > /dev/null 2>&1
	alrb_homeDir=`pwd`
	cd $alrb_currentDir
    else
	alrb_contWorkdir="$HOME/.alrb/container/$ALRB_CONT_SWTYPE"
	alrb_homeDir=$HOME
    fi
    if [ ! -z $ALRB_CONT_CHOME ]; then
	alrb_contWorkdir="$ALRB_CONT_CHOME/.alrb/container/$ALRB_CONT_SWTYPE"
    fi
    \mkdir -p $alrb_contWorkdir
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    alrb_fn_parseContainerOptions

    alrb_fn_krbBind
    
    export ALRB_CONT_DUMMYHOME="/alrb"
    if [ ! -e "$ATLAS_LOCAL_ROOT_BASE/logDir/cvmfsInstallation" ]; then
	export ALRB_CONT_DUMMYALRB="/alrb/ATLASLocalRootBase"
    fi
    
    alrb_container=""
    alrb_containerAsSingularity=""
    alrb_fn_${ALRB_CONT_SWTYPE}SetContainer
    if [[ $? -ne 0 ]] || [[ "$alrb_container" = "" ]]; then
	\echo "Error: unable to guess what ${ALRB_CONT_SWTYPE} container to use for $alrb_guessContainer" 1>&2
	return 64
    fi
    # alrb_containerName="my-`\echo $alrb_container | \sed -e 's|\:|\-|g' | rev | \cut -f 1 -d "/" |rev`"
    alrb_containerName="my-`\echo $alrb_container | \sed -e 's|\:|\-|g' -e 's|/|_|g' | \tr '[:upper:]' '[:lower:]'`"
    
    alrb_result=`type -t alrb_fn_${ALRB_CONT_SWTYPE}JoinRunning`
    if [[ $? -eq 0 ]] && [[ "$alrb_result" = "function" ]]; then
	alrb_joinedRunningContainer="NO"
	alrb_fn_${ALRB_CONT_SWTYPE}JoinRunning
	alrb_rc=$?
	if [ "$alrb_joinedRunningContainer" = "YES" ]; then
	    return $alrb_rc
	fi
    fi
    
    alrb_contDummyHome=""
    alrb_fn_createDummyHome
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    alrb_result=`type -t alrb_fn_${ALRB_CONT_SWTYPE}Build`
    if [[ $? -eq 0 ]] && [[ "$alrb_result" = "function" ]]; then
	alrb_fn_${ALRB_CONT_SWTYPE}Build
	if [ $? -ne 0 ]; then
	    return 64
	fi    
    fi

    if [ "$alrb_containerType" = "atlas-default" ]; then
	alrb_knownContainerType="YES"
    else
	alrb_knownContainerType="NO"
    fi
       
    if [[ "$alrb_knownContainerType" != "YES" ]] && [[ "$alrb_guessmod" = "YES" ]]; then
	alrb_contModifiers="`alrb_fn_${ALRB_CONT_SWTYPE}GuessModifiers | \grep -e \"^ALRB_PROBECONTAINERRESULTS \"`"
	if [ $? -ne 0 ]; then
	    \echo "Warning: unable to guess modifiers for this container.  Will resume anyway."
	else
	    alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} probe container for modifiers"
	    alrb_contModifiers="`\echo $alrb_contModifiers | \sed -e 's|ALRB_PROBECONTAINERRESULTS ;||g'`"
	    if [ "$alrb_contModifiers" != "" ]; then
		eval "$alrb_contModifiers"

		alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} finished probe container"
	    else
		alrb_fn_printMsg 1 "${ALRB_CONT_SWTYPE} probe container skipped as results are '$alrb_contModifiers'"
	    fi
	fi

    fi
    
    alrb_fn_createLoginScripts
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    export ALRB_CONT_IMAGE="$ALRB_CONT_SWTYPE $alrb_contVer $alrb_container"
    export ALRB_CONT_IMAGESINGULARITY="$alrb_containerAsSingularity"
    
    if [ "$alrb_containerSkipHome" != "YES" ]; then
	alrb_username=`basename $alrb_homeDir`
	alrb_userParent=`\dirname $alrb_homeDir`

	\echo $alrb_homeDir | \grep -e "^/home/.*/$alrb_username" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
# preserve HOME if it is nestled in /home subdirs ...	    
	    alrb_userParentMnt="$alrb_userParent"
	    alrb_userParentMntCont="$alrb_userParent"
	    alrb_homeDirNew="$alrb_homeDir"
	elif [ "$alrb_userParent" != "/" ]; then
	    alrb_userParentMnt="$alrb_userParent"
	    alrb_userParentMntCont="/home"
	else
	    alrb_userParentMnt="$alrb_homeDir"
	    alrb_userParentMntCont="/home/$alrb_username"    
	fi
	if [ "$alrb_homeDirNew" = "" ]; then
	    alrb_homeDirNew="/home/$alrb_username"
	fi
	export ALRB_CONT_REALHOME="$alrb_homeDirNew"
    fi
    export ALRB_CONT_HOSTDUMMYHOME="$alrb_contDummyHome"
    
    if [ "$alrb_setContainerPwd" != "" ]; then
	export ALRB_CONT_CHANGEPWD="$alrb_setContainerPwd"
    elif [ "$alrb_pwd" != "$HOME" ]; then
	export ALRB_CONT_CHANGEPWD="/srv"
    elif [ -z "$ALRB_CONT_REALHOME" ]; then
	export ALRB_CONT_CHANGEPWD="/srv"
    else
	export ALRB_CONT_CHANGEPWD=""
    fi
    alrb_pwd=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_pwd`
    
    unset ALRB_CONT_REALTMPDIR
    if [ ! -z $TMPDIR ]; then
	export TMPDIR=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $TMPDIR`
	alrb_result=`\echo $TMPDIR | \grep -e "^/tmp" 2>&1`
	if [ $? -eq 0 ]; then
	    export ALRB_CONT_REALTMPDIR=$TMPDIR
	else
	    alrb_tmpdirNew=`\echo $TMPDIR | \sed -e 's|^'$alrb_homeDir'|'$alrb_homeDirNew'|g' -e 's|^'$alrb_pwd'|\/srv|g'`
	    if [ "$alrb_tmpdirNew" != "$TMPDIR" ]; then
		export ALRB_CONT_REALTMPDIR=$alrb_tmpdirNew
	    else
		export ALRB_CONT_REALTMPDIR="/scratch"
	    fi
	fi
    fi
        
    alrb_fn_X11Support
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    if [[ ! -z "$ATLAS_SW_BASE" ]] && [[ "$ALRB_RELOCATECVMFS" = "YES" ]]; then
	alrb_cvmfs_mount="$ATLAS_SW_BASE"
    else
	alrb_cvmfs_mount="/cvmfs"
    fi

    if [ ! -e "$alrb_cvmfs_mount/atlas.cern.ch/repo/ATLASLocalRootBase/logDir/lastUpdate" ]; then
	alrb_containerSkipCvmfs="YES"
    fi

    alrb_cmdRunPayload=""
    alrb_fn_${ALRB_CONT_SWTYPE}ConstructCommand
    if [ $? -ne 0 ]; then
	return 64
    fi
    export ALRB_CONT_STARTCMD="$alrb_cmdRunPayload"
    
    alrb_fn_${ALRB_CONT_SWTYPE}GetPathLookup
    
    alrb_fn_BatchInterface
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    alrb_fn_saveEnvs
    
    return 0
}


alrb_fn_constructCmdDumb()
{

    alrb_fn_printMsg 1 "Construct command for -x option"
    
    if [[ -z "$ALRB_CONT_RUNPAYLOAD" ]] || [[ "$ALRB_CONT_RUNPAYLOAD" = "" ]]; then
	\echo "Error: -x/--dumb option requires payload be specified (-r)"
	return 64
    fi  
    
    alrb_container="$alrb_guessContainer"
    alrb_containerCandidate="$alrb_guessContainer"
    alrb_fn_${ALRB_CONT_SWTYPE}ConstructCommandDumb  
    return $?
}


#!---------------------------------------------------------------------------- 
alrb_fn_getLocalExec()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_execFile
    alrb_execFile="`type -p $1`"
    if [ $? -eq 0 ];then
	alrb_execFile="`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_execFile`"
	\echo $alrb_execFile | \grep -e "$ALRB_cvmfs_repo" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    return 64
	else
	    \echo "$alrb_execFile"
	fi
    else
	return 64
    fi

    return 0
}


alrb_fn_buildSedPath()
{
    local alrb_hostPath="$1"
    local alrb_contPath="$2"
    
# need to be more robust here because we support OS which may not have regex
    if [ "$alrb_hostPath" != "$alrb_contPath" ]; then
	ALRB_CONT_SED2HOST="$ALRB_CONT_SED2HOST -e 's|\([:;,[:space:]]\)$alrb_contPath|\1$alrb_hostPath|g' -e 's|^$alrb_contPath|$alrb_hostPath|g'"
	ALRB_CONT_SED2CONT="$ALRB_CONT_SED2CONT -e 's|\([:;,[:space:]]\)$alrb_hostPath|\1$alrb_contPath|g' -e 's|^$alrb_hostPath|$alrb_contPath|g'"
    fi

    return 0    
}


alrb_fn_getProxyCachedRegistryURL()
{

    # defines alrb_proxyCachedContainer if relevant
    alrb_proxyCachedContainer="$1"
        
    if [[ -z $ALRB_CONT_PROXYREGISTRY ]] || [[ "$ALRB_CONT_PROXYREGISTRY" = "" ]]; then
	return 0
    elif [ "$alrb_proxyCachedContainer" = "localhost" ]; then
	return 0
    fi

    local alrb_savedPrefix=""
    \echo $alrb_proxyCachedContainer | \grep -e '://' > /dev/null 2>&1
    if [ ${PIPESTATUS[1]} -eq 0 ]; then
	alrb_savedPrefix="`\echo $alrb_proxyCachedContainer | \cut -f 1 -d ':'`://"
	alrb_proxyCachedContainer="`\echo $alrb_proxyCachedContainer | \sed -e 's|'$alrb_savedPrefix'||g'`"
    fi
    
    # project name would not have dot according to OCI specs
    local alrb_registryIs=`\echo $alrb_proxyCachedContainer | \cut -f 1 -d '/'`
    \echo $alrb_registryIs | \grep -e "\." > /dev/null 2>&1
    if [ ${PIPESTATUS[1]} -ne 0 ]; then
	# if not URL, assume it is dockerhub
	alrb_proxyCachedContainer="docker.io/$alrb_proxyCachedContainer"
    fi
    
    local alrb_recreateFile="YES"
    local alrb_sedFile="$alrb_contWorkdir/convertToproxySed.txt"
    if [ -e $alrb_sedFile ]; then
	\grep -e "##$ALRB_CONT_PROXYREGISTRY##" $alrb_sedFile > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    alrb_recreateFile="NO"
	fi
    fi

    if [ "$alrb_recreateFile" = "YES" ]; then
	\rm -f $alrb_sedFile.new
	\echo "##$ALRB_CONT_PROXYREGISTRY##" > $alrb_sedFile.new
	local alrb_proxyItem
	local alrb_regItem
	for alrb_proxyItem in ${alrb_proxyCacheAr[@]}; do
	    local alrb_proxy=`\echo $alrb_proxyItem | \cut -f 1 -d '|'`
	    local alrb_regItemAr=( `\echo $alrb_proxyItem | \cut -f 2- -d '|' | \tr ',' '\n'` )
	    for alrb_regItem in ${alrb_regItemAr[@]}; do
		 \echo "s|^$alrb_regItem/|$alrb_proxy/$alrb_regItem/|g" >> $alrb_sedFile.new
	    done
	done
	\mv $alrb_sedFile.new $alrb_sedFile
    fi

    local alrb_proxyCachedContainerNew="$alrb_savedPrefix`\echo $alrb_proxyCachedContainer | eval \sed -E -f $alrb_sedFile`"
    if [ "$alrb_proxyCachedContainerNew" != "$alrb_proxyCachedContainer" ]; then
	\echo "Info: proxy caching $alrb_proxyCachedContainerNew"
	alrb_proxyCachedContainer="$alrb_proxyCachedContainerNew"
    fi
    
    return 0
}


#!----------------------------------------------------------------------------
# main
#!----------------------------------------------------------------------------

let alrb_time0=`date +%s`

alrb_runtimeOptAr=()
alrb_runtimeAltAr=()

alrb_userConfigDone=""
if [ -e $HOME/alrb_override_container.cfg.sh ]; then
    source $HOME/alrb_override_container.cfg.sh
elif [[ ! -z $ALRB_localConfigDir ]] && [[ -e "$ALRB_localConfigDir/alrb_container.cfg.sh" ]]; then
    source $ALRB_localConfigDir/alrb_container.cfg.sh
fi
if [ "$alrb_userConfigDone" != "YES" ]; then
    run_ALRB_UserContainerConfig
fi

# transition (temporary)
if [ ! -z "$ALRB_SING_OPTS" ]; then
    \echo "Warnng: \$ALRB_SING_OPTS obsolete; replace with \$ALRB_CONT_OPTS"
    export ALRB_CONT_OPTS="$ALRB_SING_OPTS"
fi
if [ ! -z "$ALRB_SING_PRESETUP" ]; then
    \echo "Warnng: \$ALRB_SING_PRESETUP obsolete; replace with \$ALRB_CONT_PRESETUP"
    export ALRB_CONT_PRESETUP="$ALRB_SING_PRESETUP"
fi
if [ ! -z "$ALRB_SING_POSTSETUP" ]; then
    \echo "Warnng: \$ALRB_SING_POSTSETUP obsolete; replace with \$ALRB_CONT_POSTSETUP"
    export ALRB_CONT_POSTSETUP="$ALRB_SING_POSTSETUP"
fi
if [ ! -z "$ALRB_SING_RUNPAYLOAD" ]; then
    \echo "Warnng: \$ALRB_SING_RUNPAYLOAD obsolete; replace with \$ALRB_CONT_RUNPAYLOAD"
    export ALRB_CONT_RUNPAYLOAD="$ALRB_SING_RUNPAYLOAD"
fi
if [ ! -z "$ALRB_CONT_RUNTIMEOPT" ]; then
    alrb_oldifs="$IFS"
    IFS=$'\n'
    alrb_tmpAr=( `\echo $ALRB_CONT_RUNTIMEOPT | tr '&' '\n'` )
    for alrb_tmpVal in "${alrb_tmpAr[@]}"; do
	alrb_runtimeOptAr=( "${alrb_runtimeOptAr[@]}" "$alrb_tmpVal" )
    done
    IFS="$alrb_oldifs"
fi
if [ ! -z "$ALRB_CONT_RUNTIMEALT" ]; then
    alrb_oldifs="$IFS"
    IFS=$'\n'
    alrb_tmpAr=( `\echo $ALRB_CONT_RUNTIMEALT | tr '&' '\n'` )
    for alrb_tmpVal in "${alrb_tmpAr[@]}"; do
	alrb_runtimeAltAr=( "${alrb_runtimeAltAr[@]}" "$alrb_tmpVal" )
    done
    IFS="$alrb_oldifs"
fi

# can override this in docker/podman functions with -P/--platform option
alrb_archType="x86_64"
if [ "$ATLAS_LOCAL_ROOT_ARCH" = "aarch64-Linux" ]; then
    alrb_archType="aarch64"
elif [ "$ATLAS_LOCAL_ROOT_ARCH" = "arm64-MacOS" ]; then
    alrb_archType="aarch64"
fi

alrb_prettyHostName=""
if [ -e /etc/os-release ]; then
    alrb_prettyHostName="`\grep -e 'PRETTY_NAME=' /etc/os-release 2> /dev/null | \cut -f 2 -d '=' | \sed -e 's|\"||g'`"
fi
if [ "$alrb_prettyHostName" = "" ]; then
    alrb_prettyHostName="$ALRB_OSFLAVOR$ALRB_OSMAJORVER"
fi

export ALRB_CONT_HOSTINFO="$ALRB_OSTYPE, $alrb_prettyHostName, `uname -m`, `uname -r`"

alrb_cvmfsRepoAr=(
    "atlas.cern.ch"
    "atlas-condb.cern.ch"
    "atlas-nightlies.cern.ch"
    "sft.cern.ch"
    "sft-nightlies.cern.ch"
    "unpacked.cern.ch"
)

alrb_shellPath="/bin"
alrb_Quiet="NO"
alrb_guessContainer=""
alrb_QuietOpt=""
alrb_contSetupATLASOpts=""
alrb_disableDefaultMounts=""
alrb_containerSkipCvmfs=""
alrb_containerSkipHome=""
alrb_containerCandidate=""
alrb_imageType=""
alrb_containerBatch=""
if [ -z $ALRB_CONT_LOCALALRB ]; then
    alrb_localALRB=""
else
    alrb_localALRB="$ALRB_CONT_LOCALALRB"
fi
alrb_constructCmdType="default"
alrb_beforerun=""
alrb_afterrun=""
alrb_optContainerBuilld=""
alrb_forceBuild=""
alrb_bindmount=""
alrb_setContainerPwd=""
alrb_labelBuild=""
alrb_labelUse=""
alrb_buildFile=""
alrb_platform=""
alrb_runtimeInitialized=""
alrb_krbNameHost=""
alrb_krbNameContainer=""
alrb_krbType=""
if [ -z $ALRB_CONT_VERBOSE ]; then
    let alrb_contVerbose=0
else
    let alrb_contVerbose=$ALRB_CONT_VERBOSE
fi
let alrb_timePayload0=0
alrb_patch=""
alrb_patchBindStr=""
unset ALRB_CONT_SKIPALRB
if [ -z $ALRB_CONT_PROXYREGISTRY ]; then
    alrb_proxyCacheAr=()
else
    alrb_proxyCacheAr=( `\echo "$ALRB_CONT_PROXYREGISTRY" | \tr '&' '\n'` )
fi

if [ -z $ALRB_CONT_TIMEOUTD ]; then
    export ALRB_CONT_TIMEOUTD="1200"
fi
if [ -z $ALRB_CONT_TIMEOUTR ]; then
    export ALRB_CONT_TIMEOUTR="20"
fi
if [ -z $ALRB_CONT_TIMEOUTS ]; then
    export ALRB_CONT_TIMEOUTS="30"
fi


alrb_shortopts="h,q,p,c:,t:,2,3,d,b,g,e:,E:,o:,O:,r:,s:,x,A:,B:,m:,w:,l:,P:,v" 
alrb_longopts="help,showVersions,quiet,noLocalPostSetup,container:,test:,disable-default-mounts,batch,images,localalrb,noalrb,nocvmfs,nohome,swtype:,conduct:,execopt:,Execopt:,runopt:,Runopt:,postsetup:,presetup:,runpayload:,setupfile:,cfgbatch:,dumb,afterrun:,beforerun:,swVersion:,buildopt:,forceBuild,mount:,runtimeOpt:,runtimeAlt:,pwd:,unpackeddir:,sargs:,writeLabel:,label:,buildFile:,platform:,sPostSetup:,defaultContDir:,defaultContRepo:,verbose,patch:,shell:,pidFile:,chome:,proxyReg:,timeoutD:,timeoutR:,timeoutS:"
alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_parseOptions.sh bash $alrb_shortopts $alrb_longopts $alrb_progname "$@"`
if [ $? -ne 0 ]; then
    \echo " If it is an option for a tool, you need to put it in double quotes." 1>&2
    exit 64
fi
eval set -- "$alrb_opts"

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
	-c|--container)
	    alrb_guessContainer="$2"
	    export ALRB_CONT_SETUPATLASOPT="$alrb_guessContainer"
	    shift 2
	    ;;
        -2)
	    alrb_contSetupATLASOpts="$alrb_contSetupATLASOpts $1"
            shift
            ;;
        -3)
	    alrb_contSetupATLASOpts="$alrb_contSetupATLASOpts $1"
            shift
            ;;
	-A|--afterrun)
	    alrb_afterrun="$2"
	    shift 2
	    ;;
	-B|--beforerun)
	    alrb_beforerun="$2"
	    shift 2
	    ;;
	-b|--batch)
	    alrb_containerBatch="YES"
	    shift
	    ;;
	--buildopt)
	    alrb_optContainerBuilld="$2"
	    shift 2
	    ;;
	--buildFile)
	    alrb_buildFile=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh "$2"`
	    if [ $? -ne 0 ]; then
		\echo "Error: $2 in --buildFile option does not exist." 
		exit 64
	    fi
	    shift 2
	    ;;
	--cfgbatch)
	    export ALRB_CONT_USERCONFIGBATCH="$2"
	    shift 2
	    ;;
	--chome)
	    export ALRB_CONT_CHOME="$2"
	    shift 2
	    ;;
	--conduct)
	    export ALRB_CONT_CONDUCT="$2"
	    shift 2
	    ;;
	--defaultContDir)
	    export ALRB_CONT_DEFAULTCONTDIR="$2"
	    shift 2
	    ;;
	--defaultContRepo)
	    export ALRB_CONT_DEFAULTCONTREPO="$2"
	    shift 2
	    ;;
	-d|--disable-default-mounts)
	    export ALRB_CONT_DISABLEDEFAULTMOUNTS="YES"
	    shift
	    ;;
	-e|--execopt)
	    export ALRB_CONT_CMDOPTS="$ALRB_CONT_CMDOPTS $2"
	    shift 2
	    ;;
	-E|--Execopt)
	    export ALRB_CONT_CMDOPTS="$2"
	    shift 2
	    ;;
	--forceBuild)
	    alrb_forceBuild="YES"
	    shift
	    ;;
	-g)
	    export ALRB_CONT_GUESSMOD="NO"
	    shift
	    ;;
        -h|--help)
	    alrb_fn_startContainerHelp
	    exit 0
            ;;
	--images)
	    alrb_imageType="images"
	    shift
	    ;;
	-w|--writeLabel)
	    alrb_labelBuild="$2"
	    shift 2
	    ;;
	-l|--label)
	    alrb_labelUse="$2"
	    shift 2
	    ;;
	--localalrb)
	    alrb_localALRB="YES"
	    shift
	    ;;
	-m|--mount)
	    alrb_bindmount="$alrb_bindmount,$2"
	    shift 2
	    ;;
	--noalrb)
	    export ALRB_CONT_SKIPALRB="YES"
	    shift
	    ;;
	--nocvmfs)
	    alrb_containerSkipCvmfs="YES"
	    shift
	    ;;
	--nohome)
	    alrb_containerSkipHome="YES"
	    shift
	    ;;
	-o|--runopt)
	    export ALRB_CONT_OPTS="$ALRB_CONT_OPTS $2"
	    shift 2
	    ;;
	-O|--Runopt)
	    export ALRB_CONT_OPTS="$2"
	    shift 2
	    ;;
	-p|--noLocalPostSetup)
	    alrb_contSetupATLASOpts="$alrb_contSetupATLASOpts $1"
	    shift
            ;;
	--patch)
	    alrb_patch=",$2,"
	    shift 2
	    ;;
	--pidFile)
	    export ALRB_CONT_PIDFILE="$2"
	    shift 2
	    ;;
	-P|--platform)
	    alrb_platform="$2"
	    shift 2
	    ;;
	--postsetup)
	    export ALRB_CONT_POSTSETUP="$2"
	    shift 2
	    ;;
	--presetup)
	    export ALRB_CONT_PRESETUP="$2"
	    shift 2
	    ;;
	--proxyReg)
	    if [ "$2" != "none" ]; then
		alrb_proxyCacheAr=( `\echo "$2" | \tr '&' '\n'` )
		export ALRB_CONT_PROXYREGISTRY="$2"
	    else
		alrb_proxyCacheAr=()
		unset ALRB_CONT_PROXYREGISTRY
	    fi
	    shift 2
	    ;;
	--pwd)
	    alrb_setContainerPwd="$2"
	    shift 2
	    ;;
        -q|--quiet)
	    alrb_contSetupATLASOpts="$alrb_contSetupATLASOpts $1"
            alrb_Quiet="YES"
	    alrb_QuietOpt="-q"
            shift 
            ;;
	-r|--runpayload)
	    export ALRB_CONT_RUNPAYLOAD="$2"
	    shift 2
	    ;;
	--runtimeAlt)
	    alrb_runtimeAltAr=( "${alrb_runtimeAltAr[@]}" "$2" )
	    shift 2
	    ;;
	--runtimeOpt)
	    alrb_runtimeOptAr=( "${alrb_runtimeOptAr[@]}" "$2" )
	    shift 2
	    ;;
	-s|--setupfile)
	    export ALRB_CONT_SETUPFILE="$2"
	    shift 2
	    ;;
	--sargs)
	    export ALRB_CONT_SETUPFILEARGS="$2"
	    shift 2
	    ;;
	--sPostSetup)
	    export ALRB_CONT_SPOSTSETUP="$2"
	    shift 2
	    ;;
	--shell)	    
	    export ALRB_SHELL="$2"
	    shift 2
	    ;;
	--showVersions)
	    if [ ! -z $ALRB_CONT_HOSTALRBDIR ]; then
		source ${ATLAS_LOCAL_ROOT_BASE}/relocate/container.sh
	    elif [ "$ALRB_RELOCATECVMFS" = "YES" ]; then
		source ${ATLAS_LOCAL_ROOT_BASE}/relocate/relocateCvmfs.sh
	    fi
	    source ${ATLAS_LOCAL_ROOT_BASE}/utilities/checkAtlasLocalRoot.sh
	    if [ -e $ALRB_cvmfs_unpacked_repo/logDir/list.txt ]; then
		shift
 		alrb_searchStr="`\echo $@ | \sed -e 's|^--||' -e 's|[[:space:]]||g' -e 's|\.|\\\.|g' | \sed -e 's|,|\",\"|g' -e 's|^|\"|' -e 's|$|\"|' `"
		if [ "$alrb_searchStr" != "" ]; then
		    alrb_searchStr="| \grep -i -e `\echo $alrb_searchStr | \sed -e 's|,| \| \\\grep -e |g'`"
		fi
	    else
		alrb_searchStr=""
	    fi	    
	    eval ${ATLAS_LOCAL_ROOT_BASE}/container/printContainerList.sh $alrb_searchStr
	    exit 0
	    ;;
	--swtype)
	    export ALRB_CONT_SWTYPE="`\echo $2 | tr '[:upper:]' '[:lower:]'`"
	    shift 2
	    ;;
	--swVersion)
	    export ALRB_CONT_SWVERSION="$2"
	    shift 2
	    ;;
        -t|--test)
	    export ALRB_testPath=",$2,"
            shift 2
            ;;
	--timeoutD)
	    export ALRB_CONT_TIMEOUTD="$2"
	    shift 2
	    ;;
	--timeoutR)
	    export ALRB_CONT_TIMEOUTR="$2"
	    shift 2
	    ;;
	--timeoutS)
	    export ALRB_CONT_TIMEOUTS="$2"
	    shift 2
	    ;;
	--unpackeddir)
	    export ALRB_CONT_UNPACKEDDIR="$2"
	    shift 2
	    ;;
	-v|--verbose)
	    let alrb_contVerbose+=1
	    shift
	    ;;
	-x|--dumb)
	    alrb_constructCmdType="dumb"
	    shift
	    ;;
        --)
            shift
            break
            ;;
        *)
            \echo "Internal Error: option processing error: $1" 1>&2
            exit 1
            ;;
    esac
done

    alrb_fn_printMsg 1 "Beginning container setup"
    
if [ "$alrb_guessContainer" = "" ]; then
    \echo "Error: container to guess not specified" 1>&2
    alrb_fn_startContainerHelp
    exit 64
fi

if [ ! -z $ALRB_CONT_CHOME ]; then
    alrb_bindmount="$alrb_bindmount,$ALRB_CONT_CHOME:$ALRB_CONT_CHOME"
fi

if [ ! -z $ALRB_CONT_PIDFILE ]; then
    alrb_pidFile="`eval \echo $ALRB_CONT_PIDFILE`"
    \echo "epoch=`date +%s`; pid=$$" >> $alrb_pidFile
    # do this to avoid propagation to nested containers
    unset ALRB_CONT_PIDFILE
fi

# unset these in case user forgets to submt batch jobs with envs
unset ALRB_CONT_PIPEDIR
unset ALRB_CONT_PIPEDIRHOST
unset ALRB_CONT_BATCHPATH

if [[ ! -z "$ALRB_CONT_DISABLEDEFAULTMOUNTS" ]] && [[ "$ALRB_CONT_DISABLEDEFAULTMOUNTS" = "YES" ]]; then
    alrb_containerSkipCvmfs="YES"
    alrb_containerSkipHome="YES"
fi

if [ -z "$ALRB_CONT_SWTYPE" ]; then
    if [ "$ALRB_OSTYPE" = "MacOSX" ]; then
	ALRB_CONT_SWTYPE="docker"
    else
	# try apptainer (cvmfs,local). If fails, singularity (local)
	ALRB_CONT_SWTYPE="apptainer"
	alrb_fn_getLocalExec apptainer > /dev/null 2>&1	    
	let alrb_localApptainer=$?
	alrb_fn_getLocalExec singularity > /dev/null 2>&1	    
	let alrb_localSingularity=$?
	if [[ $alrb_localApptainer -eq 0 ]] || [[ $alrb_localSingularity -eq 0 ]]; then
	    source $ATLAS_LOCAL_ROOT_BASE/container/${ALRB_CONT_SWTYPE}Functions.sh
	    alrb_fn_${ALRB_CONT_SWTYPE}Try
	    if [ $? -ne 0 ]; then
		if [ $alrb_localSingularity -ne 0 ]; then		
		    \echo "Error: There is no local singularity as fallback for apptainer."
		    alrb_fn_cleanup
		    exit 64
		fi
		ALRB_CONT_SWTYPE="singularity"
	    fi
	fi
    fi
fi

export ALRB_CONT_SWTYPE=`\echo $ALRB_CONT_SWTYPE | tr '[:upper:]' '[:lower:]'`

alrb_fn_printMsg 1 "Container runtime $ALRB_CONT_SWTYPE selected"

if [ ! -z $ALRB_CONT_GUESSMOD ]; then
    alrb_guessmod="$ALRB_CONT_GUESSMOD"
else
    alrb_guessmod="YES"
fi

export ALRB_CONT_CONDUCT=",$ALRB_CONT_CONDUCT,"

if [ ! -e $ATLAS_LOCAL_ROOT_BASE/container/${ALRB_CONT_SWTYPE}Functions.sh ]; then 
    \echo "Error: container type $ALRB_CONT_SWTYPE unsupported"
    alrb_fn_startContainerHelp
    exit 64
else
    alrb_result=`type -t alrb_fn_${ALRB_CONT_SWTYPE}Version`
    if [[ $? -ne 0 ]] || [[ "$alrb_result" != "function" ]]; then
	source $ATLAS_LOCAL_ROOT_BASE/container/${ALRB_CONT_SWTYPE}Functions.sh
    fi
fi

alrb_guessContainerOriginal=$alrb_guessContainer

# keyword getarch
if [ "$alrb_platform" = "" ]; then
    \echo $alrb_guessContainer | \grep -e "getarch" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	alrb_guessContainer="`\echo $alrb_guessContainer | \sed -e 's|getarch|'$alrb_archType'|g'`"
	\echo "Info: Container $alrb_guessContainerOriginal == $alrb_guessContainer"
    fi
fi

# if find container
\echo $alrb_guessContainer | \grep -e "find" > /dev/null 2>&1
if [ $? -eq 0 ]; then
    alrb_result=`type -t alrb_fn_findReleaseUI`
    if [[ $? -ne 0 ]] || [[ "$alrb_result" != "function" ]]; then
	source $ATLAS_LOCAL_ROOT_BASE/container/findFunctions.sh
	alrb_fn_initializeFindFunctions
    fi
    alrb_fn_findReleaseUI "NO" "$alrb_guessContainer"
    if [ $? -ne 0 ]; then
	alrb_fn_cleanup
	exit 64
    else
	\echo "
Info: Container to be used is $alrb_runtimeUrl
      To skip find next time, use the above container
        setupATLAS -c $alrb_runtimeUrl
      Or
        setupATLAS -c $alrb_aliasUrl
"
	alrb_guessContainer=$alrb_runtimeUrl
    fi
fi

# if container is alias prefixed
\echo $alrb_guessContainer | \grep -E -e "\%" > /dev/null 2>&1
if [ $? -eq 0 ]; then
    alrb_result=`type -t alrb_fn_whatIs`
    if [[ $? -ne 0 ]] || [[ "$alrb_result" != "function" ]]; then
	source $ATLAS_LOCAL_ROOT_BASE/container/findFunctions.sh
	alrb_fn_initializeFindFunctions
    fi    
    alrb_fn_whatIs "$alrb_guessContainer"
    if [ $? -ne 0 ]; then
	alrb_fn_cleanup
	exit 64
    else
	\echo "
Info: Container to be used is
        $alrb_runtimeUrl
      You can also do
        setupATLAS -c $alrb_runtimeUrl
"
	alrb_guessContainer=$alrb_runtimeUrl
    fi
fi

if [ "$alrb_runtimeInitialized" != "$ALRB_CONT_SWTYPE" ]; then
    alrb_fn_${ALRB_CONT_SWTYPE}Initialize
    alrb_fn_${ALRB_CONT_SWTYPE}Version
    if [ $? -ne 0 ]; then
	alrb_fn_cleanup
	exit 64
    fi
fi

if [ "$alrb_bindmount" != "" ]; then
    alrb_bindmount=`\echo $alrb_bindmount | \sed -e 's|,| '$alrb_runtimeBindMountOpt' |g'`
    export ALRB_CONT_CMDOPTS="$ALRB_CONT_CMDOPTS $alrb_bindmount"
fi

if [ "$alrb_setContainerPwd" != "" ]; then
    if [ "$alrb_runtimeWorkdirOpt" != "" ]; then
	export ALRB_CONT_CMDOPTS="$ALRB_CONT_CMDOPTS ${alrb_runtimeWorkdirOpt}${alrb_setContainerPwd}"
    fi
fi

for alrb_runtimeOptStr in "${alrb_runtimeOptAr[@]}"; do
    alrb_runtimeType=`\echo $alrb_runtimeOptStr | \cut -f 1 -d "|" | \tr '[:upper:]' '[:lower:]'`
    if [ "$alrb_runtimeType" = "${ALRB_CONT_SWTYPE}" ]; then
	export ALRB_CONT_CMDOPTS="$ALRB_CONT_CMDOPTS `\echo $alrb_runtimeOptStr | \cut -f 2- -d "|"`"
    fi
done

if [ "$alrb_constructCmdType" = "default" ]; then
    alrb_fn_constructCmd
    alrb_rc=$?
elif [ "$alrb_constructCmdType" = "dumb" ]; then
    alrb_fn_constructCmdDumb
    alrb_rc=$?
else
    \echo "Error: unknown constructCmdType \"$alrb_constructCmdType\""
    let alrb_rc=64
fi

if [ $alrb_rc -eq 0 ]; then
    if [ "$alrb_beforerun" != "" ]; then
	alrb_fn_printMsg 1 "Container before payload run"
	eval $alrb_beforerun
	alrb_rc=$?
    fi
fi

if [ $alrb_rc -eq 0 ]; then
    alrb_fn_printMsg 1 "Container payload run"
    eval $alrb_cmdRunPayload
    alrb_rc=$?
fi

if [ "$alrb_afterrun" != "" ]; then
    alrb_fn_printMsg 1 "Container after payload run"
    eval $alrb_afterrun
    alrb_rc1=$?
    if [ $alrb_rc -eq 0 ]; then
	alrb_rc=$alrb_rc1
    fi
fi

alrb_fn_cleanup

exit $alrb_rc
