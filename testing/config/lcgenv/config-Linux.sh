#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## lcgenv configurations:

##
## ALRB_lcgenvVersion=version to test 
# no testing version defined; users can override to specific one
#if [ -z "$ALRB_lcgenvVersion" ]; then
#    if [ "$alrb_Mode" = "test" ]; then
#	export ALRB_lcgenvVersion="testing"
#    fi
#fi

alrb_tmpVal="
arch=x86_64?os=6?
LCG_93#x86_64-slc6-gcc62-opt#pyanalysis
&
arch=x86_64?os=7?
LCG_102#x86_64-centos7-gcc11-opt#ROOT
&
arch=x86_64?os=8?
LCG_102#x86_64-centos8-gcc11-opt#ROOT
&
arch=x86_64?os=9?
LCG_102#x86_64-centos9-gcc11-opt#ROOT
&
arch=aarch64-Linux?os=7?
LCG_102#aarch64-centos7-gcc11-opt#ROOT 
&
arch=aarch64-Linux?os=8?
LCG_102#aarch64-centos7-gcc11-opt#ROOT 
"

alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ $? -ne 0 ]; then
    \echo "lcgenv not supported on this platform/os"
    return 64
else
    alrb_testLCGRelease="$alrb_result"
fi

unset alrb_tmpVal alrb_result

