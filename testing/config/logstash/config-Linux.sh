#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## logstash configurations:

##
## ALRB_logstashVersion=version to test 
if [ -z "$ALRB_logstashVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_logstashVersion="testing"
    fi
fi

alrb_tmpVal="
os=6?
skip	
&
do
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ "$alrb_result" != "do" ]; then
    \echo "logstash not supported on this platform"
    return 64
fi

unset alrb_tmpVal alrb_result


    

