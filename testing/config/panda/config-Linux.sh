#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## panda configurations:

##
## ALRB_pandaVersion=version to test 
if [ -z "$ALRB_pandaVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_pandaVersion="testing"
    fi
fi

##
## ALRB_testPandaAsetupRelease= release to use
if [ -z $ALRB_testPandaAsetupRelease ]; then
    alrb_tmpVal="
arch=x86_64?os=6?
AtlasOffline,21.0.8,slc6
&
arch=x86_64?os=7?
Athena,22.0.50
&
arch=x86_64?os=8?
Athena,22.0.50,centos7
&
arch=x86_64?os=9?
Athena,23.0.38
&
arch=aarch64-Linux?os=7?
master,AthSimulation,latest
&
arch=aarch64-Linux?os=8?
Athena,23.0.1,centos7
&
arch=aarch64-Linux?os=9?
Athena,25.0.21
&
skip
"
    alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
    if [ $? -eq 0 ]; then
	export ALRB_testPandaAsetupRelease="$alrb_result"
    fi
    unset alrb_tmpVal alrb_result
fi

##
## ALRB_testPrunContainer= for container to use for prun test
export ALRB_testPrunContainer="library/busybox:latest"


##
## ALRB_testPathenaContainer= for container to use for pathena test
export ALRB_testPathenaContainer="atlas/athanalysis:21.2.108"




