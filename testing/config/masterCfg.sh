#!----------------------------------------------------------------------------
#!
#! masterCfg.sh
#!
#! The master configuration file which will be run 
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# Which shells to test by default
alrb_testShellAr=( "bash" "zsh" )

# python versions to explicitly setup for testing (for specific tools only)
##
## ALRB_testPython3Ver="versions (comma delimited) of python3 to use in testing"
if [ -z $ALRB_testPython3Ver ]; then
    alrb_tmpVal1="recommended,pilot-default,pilot-testing"
else
    alrb_tmpVal1="$ALRB_testPython3Ver"
fi
alrb_thisPython="none"
alrb_tmpVal="
arch=x86_64?os=6?
none
&
arch=x86_64?os=7?
none,$alrb_tmpVal1
&
arch=aarch64-Linux?os=7?
$alrb_tmpVal1 
&
arch=x86_64?os=8?
$alrb_tmpVal1
&
arch=aarch64-Linux?os=8?
$alrb_tmpVal1 
&
arch=x86_64?os=9?
$alrb_tmpVal1
&
arch=aarch64-Linux?os=9?
$alrb_tmpVal1 
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ $? -eq 0 ]; then
    alrb_testPython3Ar=( `\echo "$alrb_result" | \sed -e 's|,| |g'`  )
fi

# default timeout for test commands
alrb_runShellScriptTimeout="300"

unset alrb_tmpVal alrb_tmpVal1 alrb_result
