#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## psutil configurations:

##
## ALRB_psutilVersion=version to test 
if [ -z "$ALRB_psutilVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_psutilVersion="testing"
    fi
fi

alrb_tmpVal="
os=6?
skip	
&
do
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ "$alrb_result" != "do" ]; then
    \echo "psutil not supported on this platform"
    return 64
fi

unset alrb_tmpVal alrb_result


    

