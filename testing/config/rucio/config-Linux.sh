#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## rucio configurations:

##
## ALRB_testRucioRseList="rucio sites to use in order of priority separated by ;"
if [ -z $ALRB_testRucioRseList ]; then
    alrb_tmpVal="
arch=aarch64-Linux?domain=cern.ch?
CERN-PROD_SCRATCHDISK;TRIUMF-LCG2_SCRATCHDISK;CA-VICTORIA-WESTGRID-T2_SCRATCHDISK;CA-SFU-T2_SCRATCHDISK
&
CERN-PROD_SCRATCHDISK;TRIUMF-LCG2_SCRATCHDISK;CA-VICTORIA-WESTGRID-T2_SCRATCHDISK;CA-SFU-T2_SCRATCHDISK
"	
    alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
    if [ $? -eq 0 ]; then
	export ALRB_testRucioRseList="$alrb_result"	
    fi
    unset alrb_tmpVal alrb_result
fi

##
## ALRB_rucioVersion=version to test 
if [ -z "$ALRB_rucioVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_rucioVersion="testing"
    fi
fi





