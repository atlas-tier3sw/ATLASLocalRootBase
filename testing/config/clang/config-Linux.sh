#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## clang configurations:

##
## ALRB_clangVersion=version to test 
if [ -z "$ALRB_clangVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_clangVersion="testing"
    fi
fi

alrb_tmpVal="
arch=x86_64?os=7?
do
&
arch=x86_64?os=8?
do
&
arch=x86_64?os=9?
do
&
arch=aarch64-Linux?os=9?
do
"

alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ $? -ne 0 ]; then
    \echo "clang not supported on this os/platform"
    return 64
fi

unset alrb_tmpVal alrb_result


    

