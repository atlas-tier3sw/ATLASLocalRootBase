#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## root configurations:

##
## ALRB_rootVersion=version to test 

if [ -z $ALRB_rootVersion ]; then
    export ALRB_rootVersion="recommended"
fi

alrb_tmpVal="
arch=x86_64?os=6?
do
&
arch=x86_64?os=7?
do
&
arch=x86_64?os=8?
do
&
arch=x86_64?os=9?
do
&
arch=aarch64-Linux?os=7?
do
&
arch=aarch64-Linux?os=8?
do
&
arch=aarch64-Linux?os=9?
do
"

alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ $? -ne 0 ]; then
    \echo "ROOT not supported on this platform/os"
    return 64
fi

unset alrb_tmpVal alrb_result
