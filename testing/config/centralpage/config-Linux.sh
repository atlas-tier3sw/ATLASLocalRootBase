#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## centralpage configurations:

##
## ALRB_centralpageVersion=version to test 
if [ -z "$ALRB_centralpageVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_centralpageVersion="testing"
    fi
fi

alrb_tmpVal="
os=6?
skip	
&
arch=aarch64-Linux?os=7?
skip	
&
os=8?
skip	
&
do
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ "$alrb_result" != "do" ]; then
    \echo "centralpage not supported on this platform"
    return 64
fi

unset alrb_tmpVal alrb_result


    

