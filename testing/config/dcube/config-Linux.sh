#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## dcube configurations:

##
## ALRB_dcubeVersion=version to test 
if [ -z "$ALRB_dcubeVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_dcubeVersion="testing"
    fi
fi

alrb_tmpVal="
arch=x86_64?os=7?
do
&
arch=x86_64?os=9?
do
&
arch=aarch64-Linux?os=9?
do
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ $? -ne 0 ]; then
    \echo "dcube not supported on this platform"
    return 64
fi

unset alrb_tmpVal alrb_result
