#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## pyami configurations:

##
## ALRB_pyamiVersion=version to test 
if [ -z "$ALRB_pyamiVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_pyamiVersion="testing"
    fi
fi

alrb_tmpVal="
os!=6?
do
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ $? -ne 0 ]; then
    \echo "pyami supported only for RHEL >= 7"
    return 64
fi

unset alrb_tmpVal alrb_result

