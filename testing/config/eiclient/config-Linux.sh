#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## eiclient configurations:

##
## ALRB_eiclientVersion=version to test 
if [ -z "$ALRB_eiclientVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_eiclientVersion="testing"
    fi
fi

alrb_tmpVal="
domain=cern.ch?
do
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ $? -ne 0 ]; then
    \echo "eiclient can run only on cern.ch domains at the moment"
    return 64
fi

unset alrb_tmpVal alrb_result

