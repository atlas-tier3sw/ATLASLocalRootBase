#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## stomp configurations:

##
## ALRB_stompVersion=version to test 
if [ -z "$ALRB_stompVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_stompVersion="testing"
    fi
fi

alrb_tmpVal="
os=6?
skip	
&
arch=aarch64-Linux?os=7?
skip
&
do
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ "$alrb_result" != "do" ]; then
    \echo "stomp not supported on this platform"
    return 64
fi

unset alrb_tmpVal alrb_result


    

