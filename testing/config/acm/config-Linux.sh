#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## acm configurations:

##
## ALRB_acmVersion=version to test 
if [ -z "$ALRB_acmVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_acmVersion="testing"
    fi
fi


##
## alrb_acmAthenaRel=Release version to setup
if [ -z $alrb_acmAthenaRel ]; then

    alrb_tmpVal="
arch=x86_64?os=6?
AthAnalysis,21.2.39
&	
arch=x86_64?os=7?
AthAnalysis,21.2.39
&
arch=x86_64?os=9?
AthAnalysis,25.2.19
"
    alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
    if [ $? -eq 0 ]; then
	alrb_acmAthenaRel="$alrb_result"	
    else
	\echo "release not available for this platform for acm testing"
	return 64
    fi
    unset alrb_tmpVal alrb_result
fi
