#!----------------------------------------------------------------------------
#!
#! config-Linux.sh 
#!
#! configs for tool testing
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------
##
## scikit configurations:

##
## ALRB_scikitVersion=version to test 
if [ -z "$ALRB_scikitVersion" ]; then
    if [ "$alrb_Mode" = "test" ]; then
	export ALRB_scikitVersion="testing"
    fi
fi

alrb_tmpVal="
os=6?
skip	
&
arch=aarch64-Linux?os=7?
skip	
&
os=8?
skip	
&
do
"
alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
if [ "$alrb_result" != "do" ]; then
    \echo "scikit not supported on this platform"
    return 64
fi

unset alrb_tmpVal alrb_result


    

