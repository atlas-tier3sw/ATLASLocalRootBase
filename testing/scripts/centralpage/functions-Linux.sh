#!----------------------------------------------------------------------------
#!
#! functions-Linux.sh
#!
#! functions for testing the tools
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


#!---------------------------------------------------------------------------- 
alrb_fn_centralpageTest1()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="centralpageTest1"

    \mkdir -p $alrb_relTestDir/workdir/$alrb_runName

    local alrb_cmdCentralpageRun="$alrb_relTestDir/centralpage-test1.sh"
    local alrb_cmdCentralpageRunName="$alrb_relTestDir/centralpage-test1.out"
   
    \cat  << EOF >> $alrb_cmdCentralpageRun
centralpage --scope=mc23_13p6TeV WeakBoson Vjets Baseline Sherpa_2214 -m
EOF
    chmod +x $alrb_cmdCentralpageRun
    
    local alrb_runScript="$alrb_relTestDir/centralpage-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/centralpage-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/workdir

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdCentralpageRun" $alrb_cmdCentralpageRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_centralpageTestSetupEnv()
#!---------------------------------------------------------------------------- 
{

    \rm -f $alrb_relTestDir/centralpage-script-setup.sh
    \cat << EOF >> $alrb_relTestDir/centralpage-script-setup.sh
source $alrb_envFile.sh
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q $alrb_set3
$alrb_pythonSetup
lsetup "centralpage $alrb_centralpageVal" $alrb_VerboseOpt
if [ \$? -ne 0 ]; then
  exit 64
fi
EOF
    
    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_centralpageTestRun()
#!---------------------------------------------------------------------------- 
{
    local alrb_retCode=0
    local alrb_thisEnv
    local alrb_thisShell

    local alrb_centralpageVal="default"
    if [ ! -z $ALRB_centralpageVersion ]; then
	alrb_centralpageVal="$ALRB_centralpageVersion"
    fi    
    
    \echo -e "
\e[1mcentralpage test\e[0m"
    (
	export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
	source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q
	lsetup "centralpage $alrb_centralpageVal" -q
	echo $ATLAS_LOCAL_CENTRALPAGE_VERSION
    )

    # this is used only with python3
    let alrb_osVersionN=`\echo $ALRB_OSMAJORVER`
    if [ $alrb_osVersionN -le 7 ]; then
	touch $alrb_toolWorkdir/python3Only
    fi
    
    local alrb_thisPython
    for alrb_thisPython in ${alrb_testPython3Ar[@]}; do

	if [ "$alrb_thisPython" = "none" ]; then
	    if [ -e $alrb_toolWorkdir/python3Only ]; then
		continue
	    fi
	    local alrb_pythonSetupName=""
	    local alrb_pythonSetup=""	    
	elif [ "$alrb_thisPython" != "recommended" ]; then
	    \echo "Skipping python $alrb_thisPython as scikit is not ready"
	    continue
	else
	    local alrb_pythonSetupname="(python $alrb_thisPython)"
	    local alrb_pythonSetup="lsetup \"python $alrb_thisPython\""
	fi
    
	for alrb_thisShell in ${alrb_testShellAr[@]}; do
	
	    local alrb_addStatus=""
	    local alrb_relTestDir="$alrb_toolWorkdir/$alrb_thisShell"
	    \mkdir -p $alrb_relTestDir

	    alrb_fn_centralpageTestSetupEnv
	    if [ $? -ne 0 ]; then
		return 64
	    fi

	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "centralpage test1  $alrb_pythonSetupName"
	    alrb_fn_centralpageTest1
	    alrb_fn_addSummary $? exit
	done
	
    done

    return 0
}


