#!----------------------------------------------------------------------------
#!
#! functions-Linux.sh
#!
#! functions for testing the tools
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


#!---------------------------------------------------------------------------- 
alrb_fn_psutilImport()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="psutilImport"

    \mkdir -p $alrb_relTestDir/workdir/$alrb_runName

    local alrb_cmdPsutilRun="$alrb_relTestDir/psutil-import.sh"
    local alrb_cmdPsutilRunName="$alrb_relTestDir/psutil-import.out"
   
    \cat  << EOF >> $alrb_cmdPsutilRun
python3 -c "import psutil"
EOF
    chmod +x $alrb_cmdPsutilRun
    
    local alrb_runScript="$alrb_relTestDir/psutil-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/psutil-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/workdir

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPsutilRun" $alrb_cmdPsutilRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_psutilTestSetupEnv()
#!---------------------------------------------------------------------------- 
{
    
    if [ "$alrb_thisPython" = "none" ]; then
	local alrb_pythonSetup=""
	if [ "$ALRB_OSMAJORVER" = "7" ]; then	    
            local alrb_set3="-2"  # centos7 default is now python3	    
        else
            local alrb_set3=""
        fi
    else  
	local alrb_pythonSetup="lsetup \"python $alrb_thisPython\""
	local alrb_set3="-3"
    fi

    \rm -f $alrb_relTestDir/psutil-script-setup.sh
    \cat << EOF >> $alrb_relTestDir/psutil-script-setup.sh
source $alrb_envFile.sh
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q $alrb_set3
$alrb_pythonSetup
lsetup "psutil" $alrb_VerboseOpt
if [ \$? -ne 0 ]; then
  exit 64
fi
EOF
    
    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_psutilTestRun()
#!---------------------------------------------------------------------------- 
{
    local alrb_retCode=0
    local alrb_thisEnv
    local alrb_thisShell

    \echo -e "
\e[1mpsutil test\e[0m"
    (
	export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
	source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q
	lsetup psutil -q
	echo $ATLAS_LOCAL_PSUTIL_VERSION
    )

    # this is used only with python3
    let alrb_osVersionN=`\echo $ALRB_OSMAJORVER`
    if [ $alrb_osVersionN -le 7 ]; then
	touch $alrb_toolWorkdir/python3Only
    fi
    
    local alrb_thisPython
    for alrb_thisPython in ${alrb_testPython3Ar[@]}; do

	if [ "$alrb_thisPython" = "none" ]; then
	    if [ -e $alrb_toolWorkdir/python3Only ]; then
		continue
	    fi
	    local alrb_pythonSetup=""	    
	else
	    local alrb_pythonSetup="(python $alrb_thisPython)"
	fi
    
	for alrb_thisShell in ${alrb_testShellAr[@]}; do
	
	    local alrb_addStatus=""
	    local alrb_relTestDir="$alrb_toolWorkdir/$alrb_thisShell"
	    \mkdir -p $alrb_relTestDir

	    alrb_fn_psutilTestSetupEnv
	    if [ $? -ne 0 ]; then
		return 64
	    fi

	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "psutil import $alrb_pythonSetup"
	    alrb_fn_psutilImport
	    alrb_fn_addSummary $? exit
	done
	
    done

    return 0
}


