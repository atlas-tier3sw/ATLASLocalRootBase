#!----------------------------------------------------------------------------
#!
#! functions-Linux.sh
#!
#! functions for testing the tools
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

#!---------------------------------------------------------------------------- 
alrb_fn_pandaGetToken()
#!---------------------------------------------------------------------------- 
{

    if [ "$alrb_authType" = "tokens" ]; then
	(
	    export X509_USER_PROXY=/dummyShouldNotExist
	    export ALRB_tokensPanda="YES"
	    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
	    source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh  panda -q
	    pbook -c "exit"
	)
    fi

    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_pandaTestPathena()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="pathena"

    if [ "$ALRB_testPandaAsetupRelease" = "skip" ]; then
	\echo "no ATLAS release available for this platform/os for pathena test"
	\echo " Skipping this test"
	alrb_setStatus="SKIP"
	return 64
    fi
    
    \mkdir -p $alrb_relTestDir/$alrb_runName

    local alrb_cmdPandaSetupRel="asetup $ALRB_testPandaAsetupRelease"
    local alrb_cmdPandaSetupRelName="$alrb_relTestDir/panda-relSetup.out"

    local alrb_cmdPandaSetupRel2="get_files -jo HelloWorldOptions.py"
    local alrb_cmdPandaSetupRelName2="$alrb_relTestDir/panda-relSetup2.out"

    
    local alrb_cmdPandaRun="pathena HelloWorldOptions.py --outDS=user.$RUCIO_ACCOUNT.test.`uuidgen`.$alrb_jobNameSuffix --noOutput"
    local alrb_cmdPandaRunName="$alrb_relTestDir/panda-pathena.out"
    
    local alrb_runScript="$alrb_relTestDir/panda-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
cd $alrb_relTestDir/$alrb_runName

alrb_exitCode=0

source $alrb_relTestDir/panda-script-setup.sh

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaSetupRel" $alrb_cmdPandaSetupRelName $alrb_Verbose
alrb_exitCode=\$?

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaSetupRel2" $alrb_cmdPandaSetupRelName2 $alrb_Verbose
alrb_exitCode=\$?

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaRun" $alrb_cmdPandaRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?


    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_pandaTestPrun()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="prun"

    \mkdir -p $alrb_relTestDir/prun
    local alrb_prunTestScript="$alrb_relTestDir/prun/myPunJob.sh"
    \cat  <<  EOF >> $alrb_prunTestScript
#! /bin/bash

\echo 'testJob.sh running...'
\echo "----------------------"
\echo "date is " \`date\`
\echo "hostname is " \`hostname -f\`
\echo "id is " \`id\`
\echo "uname is : "\`uname -a\` 
\echo "pwd is : " \`pwd\`
\echo "----------------------"
\echo "Voms proxy is : "
voms-proxy-info -all
\echo "----------------------"
\echo "Env is : "
env | sort
\echo "----------------------"
exit 0

EOF

    local alrb_cmdPandaRun="prun --exec=myPunJob.sh --outDS=user.$RUCIO_ACCOUNT.test.`uuidgen`.$alrb_jobNameSuffix --noBuild"
    local alrb_cmdPandaRunName="$alrb_relTestDir/panda-prun.out"
    
    local alrb_runScript="$alrb_relTestDir/panda-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/panda-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/prun

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaRun" $alrb_cmdPandaRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    # saving this task id for pbook testing
    if [ $alrb_retCode -eq 0 ]; then
	local alrb_tmpVal
	alrb_tmpVal=`\grep -e jediTaskID $alrb_cmdPandaRunName`
	if [ $? -eq 0 ]; then
	    alrb_prunJediTaskId=`\echo $alrb_tmpVal | \sed -e 's|.*jediTaskID=\([[:digit:]]*\).*|\1|g'`
	fi
    fi
    

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_pandaTestPbook()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="pbook"

    if [ "$alrb_prunJediTaskId" = "" ]; then
	\echo "Error: unable to run pbook test as prun job from last stage did not show a TaskId"
	return 64
    fi

    \mkdir -p $alrb_relTestDir/pbook

    local alrb_cmdPandaRunShow="pbook -c 'show()'"
    local alrb_cmdPandaRunShowName="$alrb_relTestDir/panda-pbook-show.out"

    local alrb_cmdPandaRunKR="pbook -c 'killAndRetry($alrb_prunJediTaskId)'"
    local alrb_cmdPandaRunKRName="$alrb_relTestDir/panda-pbook-kr.out"

    local alrb_runScript="$alrb_relTestDir/panda-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/panda-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/pbook

# so that we have new .pathena dir for testing
export HOME=$alrb_relTestDir/pbook

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaRunShow" $alrb_cmdPandaRunShowName $alrb_Verbose
alrb_exitCode=\$?

# kill and retry disabled by ATLAS ... 
#if [ \$alrb_exitCode -eq 0 ]; then
#  source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaRunKR" $alrb_cmdPandaRunKRName $alrb_Verbose
#  if [ \$? -ne 0 ]; then
#    \echo "Ignore error if from kill as task can be done already"
#    alrb_exitCode=64
#  fi
#fi

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?
       
    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_pandaTestPrunContainer()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="prunContainer"

    \mkdir -p $alrb_relTestDir/prunContainer
    local alrb_prunTestScript="$alrb_relTestDir/prunContainer/myPunJob.sh"
    \cat  <<  EOF >> $alrb_prunTestScript
#! /bin/sh

\echo 'testJob.sh running...'
\echo "----------------------"
\echo "date is " \`date\`
\echo "hostname is " \`hostname -f\`
\echo "id is " \`id\`
\echo "uname is : "\`uname -a\` 
\echo "pwd is : " \`pwd\`
\echo "----------------------"
\echo "Voms proxy is : "
voms-proxy-info -all
\echo "----------------------"
\echo "Env is : "
env | sort
\echo "----------------------"
exit 0

EOF
    chmod +x $alrb_prunTestScript
    
    local alrb_cmdPandaRun="prun --exec='./myPunJob.sh' --outDS=user.$RUCIO_ACCOUNT.test.`uuidgen`.$alrb_jobNameSuffix --noBuild --containerImage $ALRB_testPrunContainer"
    local alrb_cmdPandaRunName="$alrb_relTestDir/panda-prunContainer.out"
    
    local alrb_runScript="$alrb_relTestDir/panda-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/panda-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/prunContainer

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaRun" $alrb_cmdPandaRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?
   
    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_pandaTestPathenaContainer()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="pathenaContainer"

    \mkdir -p $alrb_relTestDir/pathenaContainer
    \cat << EOF >> $alrb_relTestDir/pathenaContainer/myFirstJobOptions.py 
theApp.EvtMax=10
EOF
    
    local alrb_cmdPandaRun="pathena --containerImage $ALRB_testPathenaContainer --outDS=user.$RUCIO_ACCOUNT.test.`uuidgen`.$alrb_jobNameSuffix --trf 'athena ./myFirstJobOptions.py' --noOutput"
    local alrb_cmdPandaRunName="$alrb_relTestDir/panda-pathenaContainer.out"
    
    local alrb_runScript="$alrb_relTestDir/panda-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/panda-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/pathenaContainer

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaRun" $alrb_cmdPandaRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?


    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_pandaTestPChain()
#!---------------------------------------------------------------------------- 
{

    local alrb_tmpVal="
os=6?
skip
&
do
"
    local alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
    if [ "$alrb_result" != "do" ]; then
	\echo "pchain not supported on this platform"
	\echo " Skipping this test"
	alrb_setStatus="SKIP"
	return 64
    fi
    
    local alrb_retCode=0
    local alrb_runName="pchain"

    \mkdir -p $alrb_relTestDir/pchain
    \cat << EOF >> $alrb_relTestDir/pchain/simple_chain.cwl
cwlVersion: v1.0
class: Workflow

inputs: []

outputs:
  outDS:
    type: string
    outputSource: bottom/outDS


steps:
  top:
    run: prun
    in:
      opt_exec:
        default: "echo %RNDM:10 > seed.txt"
      opt_args:
        default: "--outputs seed.txt --nJobs 3 --avoidVP"
    out: [outDS]

  bottom:
    run: prun
    in:
      opt_inDS: top/outDS
      opt_exec:
        default: "echo %IN > results.root"
      opt_args:
        default: "--outputs results.root --forceStaged --avoidVP"
    out: [outDS]

EOF
    touch $alrb_relTestDir/pchain/dummy.yaml
    
    local alrb_cmdPandaRun="pchain --cwl simple_chain.cwl --yaml dummy.yaml --outDS user.$RUCIO_ACCOUNT.testPchain.`uuidgen`.$alrb_jobNameSuffix"
    local alrb_cmdPandaRunName="$alrb_relTestDir/panda-pchain.out"
    
    local alrb_runScript="$alrb_relTestDir/panda-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/panda-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/pchain

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaRun --check" $alrb_cmdPandaRunName $alrb_Verbose
alrb_exitCode=\$?
if [ \$alrb_exitCode != 0 ]; then
   \$alrb_exitCode
fi

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdPandaRun" $alrb_cmdPandaRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_pandaTestSetupEnv()
#!---------------------------------------------------------------------------- 
{
        
    if [ "$alrb_thisPython" = "none" ]; then
	local alrb_pythonSetup=""
	if [ "$ALRB_OSMAJORVER" = "7" ]; then
	    local alrb_set3="-2"  # centos7 default is now python3
	else
            local alrb_set3=""
	fi
    else
        local alrb_pythonSetup="lsetup \"python $alrb_thisPython\""
        local alrb_set3="-3"
    fi
    
    \rm -f $alrb_relTestDir/panda-script-setup.sh
    \cat << EOF >> $alrb_relTestDir/panda-script-setup.sh
source $alrb_envFile.sh
if [ "$alrb_authType" = "tokens" ]; then
   export X509_USER_PROXY=/dummyShouldNotExist
   export ALRB_tokensPanda="YES"
else
   export ALRB_tokensPanda="NO"
fi
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q $alrb_set3
$alrb_pythonSetup
lsetup "panda" $alrb_VerboseOpt
if [ \$? -ne 0 ]; then
  exit 64
fi
EOF
    
    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_pandaTestRun()
#!---------------------------------------------------------------------------- 
{
    local alrb_retCode=0
    local alrb_thisEnv
    local alrb_thisShell
    
    \echo -e "
\e[1mpanda test\e[0m"
    (
	export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
	source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q
	lsetup panda -q
	prun --version
    )

    if [ "$alrb_testTokens" = "YES" ]; then
	local alrb_authTypeAr=( "x509" "tokens" )
    else
	local alrb_authTypeAr=( "x509" )
    fi
    local alrb_authType
    local alrb_thisPython
    for alrb_thisPython in ${alrb_testPython3Ar[@]}; do

	if [ "$alrb_thisPython" = "none" ]; then
            local alrb_pythonSetup=""
        else
            local alrb_pythonSetup="(python $alrb_thisPython)"
        fi
	
	for alrb_authType in ${alrb_authTypeAr[@]}; do
	    for alrb_thisShell in ${alrb_testShellAr[@]}; do
	
		local alrb_prunJediTaskId=""
		local alrb_addStatus=""
		local alrb_relTestDir="$alrb_toolWorkdir/$alrb_thisShell/$alrb_thisPython/$alrb_authType"
		\mkdir -p $alrb_relTestDir

		local alrb_jobNameSuffix="$ALRB_OSMAJORVER-$ATLAS_LOCAL_ROOT_ARCH-$alrb_thisShell-python$alrb_thisPython-$alrb_authType"
		
		alrb_fn_pandaTestSetupEnv
		if [ $? -ne 0 ]; then
		    return 64
		fi
		
		alrb_fn_initSummary $alrb_tool $alrb_thisShell "panda prun ($alrb_authType) $alrb_pythonSetup"
		alrb_fn_pandaGetToken
		alrb_fn_pandaTestPrun
		alrb_fn_addSummary $? continue
		
		alrb_fn_initSummary $alrb_tool $alrb_thisShell "panda pathena ($alrb_authType) $alrb_pythonSetup"
		alrb_fn_pandaGetToken
		alrb_fn_pandaTestPathena
		alrb_fn_addSummary $? continue
		
		alrb_fn_initSummary $alrb_tool $alrb_thisShell "panda pbook ($alrb_authType) $alrb_pythonSetup"
		alrb_fn_pandaGetToken
		alrb_fn_pandaTestPbook
		local alrb_rc=$?
		if [ $alrb_rc  -ne 0 ]; then
		    alrb_addStatus="READ"
		fi
		alrb_fn_addSummary $alrb_rc continue $alrb_addStatus
		alrb_addStatus=""
		
		alrb_fn_initSummary $alrb_tool $alrb_thisShell "panda prun container ($alrb_authType) $alrb_pythonSetup"
		alrb_fn_pandaGetToken
		alrb_fn_pandaTestPrunContainer
		alrb_fn_addSummary $? continue
		
		alrb_fn_initSummary $alrb_tool $alrb_thisShell "panda pathena container ($alrb_authType) $alrb_pythonSetup"
		alrb_fn_pandaGetToken
		alrb_fn_pandaTestPathenaContainer
		alrb_fn_addSummary $? continue
		
		alrb_fn_initSummary $alrb_tool $alrb_thisShell "panda pathena pchain ($alrb_authType) $alrb_pythonSetup"
		alrb_fn_pandaGetToken
		alrb_fn_pandaTestPChain
		alrb_fn_addSummary $? continue 

	    done
	done
    done

    return 0
}


