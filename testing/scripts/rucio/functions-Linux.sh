#!----------------------------------------------------------------------------
#!
#! functions-Linux.sh
#!
#! functions for testing the tools
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


#!---------------------------------------------------------------------------- 
alrb_fn_rucioCheckID()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="id"

    local alrb_cmdWhoami="rucio whoami"
    local alrb_cmdWhoamiName="$alrb_relTestDir/rucio-whoami.out"

    local alrb_cmdVomsId="voms-proxy-info -identity"
    local alrb_identityFile="$alrb_relTestDir/rucio-identity.out"

    local alrb_cmdRucioId="rucio-admin account list-identities $RUCIO_ACCOUNT"
    local alrb_cmdRucioIdName="$alrb_relTestDir/rucio-list-identity.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"

    \rm -f $alrb_runScript
    \cat << EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdWhoami" $alrb_cmdWhoamiName $alrb_Verbose
alrb_retCode=\$?
if [ \$alrb_retCode -eq 0 ]; then 
    \grep -e " \${RUCIO_ACCOUNT}" $alrb_cmdWhoamiName > /dev/null 2>&1
    alrb_retCode=\$?
    if [ \$alrb_retCode -ne 0 ]; then
	\cat $alrb_cmdWhoamiName
	\echo "Error: rucio whoami does not have identity \$RUCIO_ACCOUNT"
    fi
fi

if [ \$alrb_retCode -eq 0 ]; then
    source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdVomsId" $alrb_identityFile $alrb_Verbose
    alrb_retCode=\$?
fi    

if [ \$alrb_retCode -eq 0 ]; then
    alrb_identity=\`\cat $alrb_identityFile\`
    source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh  "$alrb_cmdRucioId" $alrb_cmdRucioIdName $alrb_Verbose
    alrb_retCode=\$?
fi
# cannot do identity check as format is different with IAM
#if [ \$alrb_retCode -eq 0 ]; then
#    \grep -e "\$alrb_identity" $alrb_cmdRucioIdName > /dev/null 2>&1
#    alrb_retCode=\$?  
#    if [ \$alrb_retCode -ne 0 ]; then
#	\echo "Error: unable to find identity \$alrb_identity in rucio-admin:"
#	\cat $alrb_cmdRucioIdName
#    fi
#fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?
    
    return $alrb_retCode
}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioListRse()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="listRse"

    local alrb_cmdListRse="rucio list-rses"
    local alrb_cmdListRseName="$alrb_relTestDir/rucio-list-rses.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdListRse" $alrb_cmdListRseName $alrb_Verbose
alrb_retCode=\$?

if [ \$alrb_retCode -ne 0 ]; then 
  \grep -e "$alrb_rseName" $alrb_cmdListRseName > /dev/null 2>&1
  alrb_retCode=\$? 
  if [ \$alrb_retCode -ne 0 ]; then
      \cat $alrb_cmdListRseName
      \echo "Error: $alrb_rseName not found in rucio list-rses"
  fi
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?
    
    return $alrb_retCode
}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioPrepareUploadFiles()
#!---------------------------------------------------------------------------- 
{
    \mkdir -p $alrb_rucioUploadDir
    local alrb_uploadFileAr=( `\find $alrb_rucioUploadDir -mindepth 1 -maxdepth 1 -type f | \awk -F/ '{print $NF}' | \sed -e 's|^|'$alrb_rucioScope':|g'` )
    if [ ${#alrb_uploadFileAr[@]} -eq 0 ]; then
	\echo $alrb_rucioUuidGen > $alrb_rucioUploadDir/myFile.${alrb_rucioUuidGen}.log
	alrb_uploadFileAr=( "$alrb_rucioScope:myFile.${alrb_rucioUuidGen}.log" )
    fi
    alrb_rucioDiDs="\"${alrb_uploadFileAr[@]}\""

    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioUploadDS()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="upload"

    if [ "$alrb_rucioDiDs" = "" ]; then
	alrb_fn_rucioPrepareUploadFiles
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi

    alrb_cmdUploadFile="rucio upload --scope $alrb_rucioScope --rse $alrb_rseName $alrb_rucioUploadDir"
    alrb_cmdUploadFileName="$alrb_relTestDir/rucio-upload.out"

    alrb_cmdAddataset="rucio add-dataset $alrb_rucioMyDataset"
    alrb_cmdAddatasetName="$alrb_relTestDir/rucio-add-dataset.out"

    alrb_cmdAttachDS="rucio attach $alrb_rucioMyDataset $alrb_rucioDiDs"
    alrb_cmdAttachDSName="$alrb_relTestDir/rucio-attadh.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdUploadFile" $alrb_cmdUploadFileName $alrb_Verbose
alrb_retCode=\$? 

if [ \$alrb_retCode -eq 0 ]; then
  source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdAddataset" $alrb_cmdAddatasetName $alrb_Verbose
  alrb_retCode=\$?
fi

if [ \$alrb_retCode -eq 0 ]; then
  source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdAttachDS" $alrb_cmdAttachDSName $alrb_Verbose
  alrb_retCode=\$?
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?
    
    return $alrb_retCode
}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioListDS()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="list"

    local alrb_cmdListDS="rucio list-files $alrb_rucioMyDataset"
    local alrb_cmdListDSName="$alrb_relTestDir/rucio-list-files.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdListDS" $alrb_cmdListDSName $alrb_Verbose
alrb_retCode=\$? 

if [ \$alrb_retCode -eq 0 ]; then 
  \grep -e "$alrb_rucioMyFile" $alrb_cmdListDSName > /dev/null 2>&1
  alrb_retCode=\$? 
  if [ \$alrb_retCode -ne 0 ]; then
    \cat $alrb_cmdListDSName 
    \echo "Error: $alrb_rucioMyFile is not in file listing"
  fi
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode
}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioGetDS()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="get"

    local alrb_cmdGetDS="rucio get $alrb_rucioMyDataset --dir $alrb_relTestDir"
    local alrb_cmdGetDSName="$alrb_relTestDir/rucio-get.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript 
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdGetDS" $alrb_cmdGetDSName $alrb_Verbose
alrb_retCode=\$? 

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode
}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioGetPfns()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="pfns"

    local alrb_cmdGetDS="rucio list-file-replicas $alrb_rucioDiDs --protocols root,davs,srm --pfns --domain wan"
    local alrb_cmdGetDSName="$alrb_relTestDir/rucio-pfns.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript 
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdGetDS" $alrb_cmdGetDSName $alrb_Verbose
alrb_retCode=\$? 

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode
}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioEraseDS()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="erase"

    local alrb_cmdEraseFile="rucio erase $alrb_rucioDiDs"
    local alrb_cmdEraseFileName="$alrb_relTestDir/rucio-erase-file.out"

    local alrb_cmdEraseDS="rucio erase $alrb_rucioMyDataset"
    local alrb_cmdEraseDSName="$alrb_relTestDir/rucio-erase-dataset.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat << EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdEraseFile" $alrb_cmdEraseFileName $alrb_Verbose
alrb_retCode=\$?

if [ \$alrb_retCode -eq 0 ]; then
  source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdEraseDS" $alrb_cmdEraseDSName $alrb_Verbose
  alrb_retCode=\$?
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode
}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioMultilevelContainers()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="MultilevelContainers"

    local alrb_cmdListDS="rucio download --dir $alrb_relTestDir user.mlassnig:user.mlassnig.container.level_two"
    local alrb_cmdListDSName="$alrb_relTestDir/rucio-multilevelcontainers.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdListDS" $alrb_cmdListDSName $alrb_Verbose
alrb_retCode=\$? 

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioCERNReplicaCheck()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="CERNReplicaCheck"

    local alrb_cmdListDS="SITE_NAME=CERN-PROD rucio list-file-replicas --rses \"CERN-PROD_DATADISK\" --pfn --protocols root mc15_13TeV:HITS.06828093._000096.pool.root.1"
    local alrb_cmdListDSName="$alrb_relTestDir/rucio-cernreplicacheck.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdListDS" $alrb_cmdListDSName $alrb_Verbose
alrb_retCode=\$? 

if [ \$alrb_retCode -eq 0 ]; then
  \grep -e "root://eosatlas.cern.ch:1094//eos/atlas/atlasdatadisk/rucio/mc15_13TeV/6a/54/HITS.06828093._000096.pool.root.1" $alrb_cmdListDSName > /dev/null 2>&1
  alrb_retCode=\$? 
  if [ \$alrb_retCode -ne 0 ]; then
    \cat $alrb_cmdListDSName 
    \echo "Error: root://eosatlas.cern.ch:1094//eos/atlas/atlasdatadisk/rucio/mc15_13TeV/6a/54/HITS.06828093._000096.pool.root.1 is not found in listing"
  fi
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioBNLReplicaCheck()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="BNLReplicaCheck"

    local alrb_cmdListDS="SITE_NAME=BNL-ATLAS rucio list-file-replicas --rses \"CERN-PROD_DATADISK\" --pfn --protocols root mc15_13TeV:HITS.06828093._000096.pool.root.1"
    local alrb_cmdListDSName="$alrb_relTestDir/rucio-bnlreplicacheck.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdListDS" $alrb_cmdListDSName $alrb_Verbose
alrb_retCode=\$? 

if [ \$alrb_retCode -eq 0 ]; then
  \grep -e "root://xrootdproxy.usatlas.bnl.gov:1096//root://eosatlas.cern.ch:1094//eos/atlas/atlasdatadisk/rucio/mc15_13TeV/6a/54/HITS.06828093._000096.pool.root.1" $alrb_cmdListDSName > /dev/null 2>&1
  alrb_retCode=\$? 
  if [ \$alrb_retCode -ne 0 ]; then
    \cat $alrb_cmdListDSName 
    \echo "Error: root://xrootdproxy.usatlas.bnl.gov:1096//root://eosatlas.cern.ch:1094//eos/atlas/atlasdatadisk/rucio/mc15_13TeV/6a/54/HITS.06828093._000096.pool.root.1 is not found in listing"
  fi
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioScopelessDownload()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="ScopelessDownload"

    local alrb_cmdTest="rucio download --dir $alrb_relTestDir user.mlassnig.pilot.test.single.hits"
    local alrb_cmdTestOut="$alrb_relTestDir/rucio-${alrb_runName}.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdTest" $alrb_cmdTestOut $alrb_Verbose
alrb_retCode=\$? 

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioParentCheck()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="ParentCheck"

    local alrb_cmdTest="rucio list-parent-dids user.mlassnig.pilot.test.single.hits"
    local alrb_cmdTestOut="$alrb_relTestDir/rucio-${alrb_runName}.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript

    local alrb_expectedOut="$alrb_relTestDir/rucio-expectedOutput-$alrb_runName"
    \rm -f $alrb_expectedOut
    \cat <<EOF >> $alrb_expectedOut
+--------------------------------------------------------------+--------------+
| SCOPE:NAME                                                   | [DID TYPE]   |
|--------------------------------------------------------------+--------------|
| user.mlassnig:user.mlassnig.container.level_one              | CONTAINER    |
| user.mlassnig:user.mlassnig.pilot.test.single.hits.container | CONTAINER    |
+--------------------------------------------------------------+--------------+
EOF

    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdTest" $alrb_cmdTestOut $alrb_Verbose
alrb_retCode=\$? 

if [ \$alrb_retCode -eq 0 ]; then
   diff -b -w $alrb_cmdTestOut $alrb_expectedOut > /dev/null 2>&1
   alrb_retCode=\$?
   if [ \$alrb_retCode -ne 0 ]; then
     \echo "$alrb_cmdTest"
     \cat $alrb_cmdTestOut
     \echo "Error: Differences seen; instead of expected output:"
     \cat $alrb_expectedOut
   fi 
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioMetadata()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="Metadata"

    local alrb_cmdTest="rucio get-metadata mc15_13TeV:HITS.06828093._000096.pool.root.1"
    local alrb_cmdTestOut="$alrb_relTestDir/rucio-${alrb_runName}.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript

    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdTest" $alrb_cmdTestOut $alrb_Verbose
alrb_retCode=\$? 

if [ \$alrb_retCode -eq 0 ]; then
   \grep -e ac5b3759b606ba4286814bd86455ae02 $alrb_cmdTestOut >/dev/null 2>&1
   if [ \$? -ne 0 ]; then
     alrb_retCode=64
     \echo "Error: guid:ac5b3759b606ba4286814bd86455ae02 incorrect ?" 
   fi   
   \grep -e ParticleGunEvtGen_single_B0_flatPt10to1000 $alrb_cmdTestOut >/dev/null 2>&1
   if [ \$? -ne 0 ]; then
     alrb_retCode=64
     \echo "Error: stream_name:ParticleGunEvtGen_single_B0_flatPt10to1000 incorrect ?" 
   fi
   
   if [ \$alrb_retCode -ne 0 ]; then 
     \echo "$alrb_cmdTest"
     \cat $alrb_cmdTestOut
   fi    	   
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioContentCheck()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="ContentCheck"

    local alrb_cmdTest="rucio list-content --short user.mlassnig.pilot.test.multi.hits | \sort"
    local alrb_cmdTestOut="$alrb_relTestDir/rucio-${alrb_runName}.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript

    local alrb_expectedOut="$alrb_relTestDir/rucio-expectedOutput-$alrb_runName"
    \rm -f $alrb_expectedOut
    \cat <<EOF >> $alrb_expectedOut
mc15_14TeV:HITS.10075481._000432.pool.root.1
mc15_14TeV:HITS.10075481._000433.pool.root.1
mc15_14TeV:HITS.10075481._000434.pool.root.1
mc15_14TeV:HITS.10075481._000435.pool.root.1
mc15_14TeV:HITS.10075481._000444.pool.root.1
mc15_14TeV:HITS.10075481._000445.pool.root.1
mc15_14TeV:HITS.10075481._000451.pool.root.1
mc15_14TeV:HITS.10075481._000454.pool.root.1
mc15_14TeV:HITS.10075481._000455.pool.root.1
EOF

    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdTest" $alrb_cmdTestOut $alrb_Verbose
alrb_retCode=\$? 

if [ \$alrb_retCode -eq 0 ]; then
   diff -b -w $alrb_cmdTestOut $alrb_expectedOut > /dev/null 2>&1
   alrb_retCode=\$?
   if [ \$alrb_retCode -ne 0 ]; then
     \echo "$alrb_cmdTest"
     \cat $alrb_cmdTestOut   
     \echo "Error: Differences seen; instead of expected output:"
     \cat $alrb_expectedOut
   fi 
fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioRseAttribute()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="RseAttribute"

    local alrb_cmdTest="rucio list-rse-attributes CERN-PROD_DATADISK"
    local alrb_cmdTestOut="$alrb_relTestDir/rucio-${alrb_runName}.out"

    local alrb_runScript="$alrb_relTestDir/rucio-script-$alrb_runName"
    \rm -f $alrb_runScript

    \cat <<EOF >> $alrb_runScript
source $alrb_relTestDir/rucio-script-setup.sh
source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdTest" $alrb_cmdTestOut $alrb_Verbose
alrb_retCode=\$? 

if [ \$alrb_retCode -eq 0 ]; then
   \grep -e "CERN-PROD_DATADISK:[[:space:]]*True" $alrb_cmdTestOut >/dev/null 2>&1
   if [ \$? -ne 0 ]; then
     alrb_retCode=64
     \echo "Error: CERN-PROD_DATADISK:True incorrect ?" 
   fi   
   \grep -e "tier:[[:space:]]*0" $alrb_cmdTestOut >/dev/null 2>&1
   if [ \$? -ne 0 ]; then
     alrb_retCode=64
     \echo "Error: tier:0 incorrect ?" 
   fi   

   if [ \$alrb_retCode -ne 0 ]; then 
     \echo "$alrb_cmdTest"
     \cat $alrb_cmdTestOut
   fi

fi

exit \$alrb_retCode

EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript 
    alrb_retCode=$?

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioTestSetupEnv()
#!---------------------------------------------------------------------------- 
{

    (
	if [ ! -e $alrb_workDir/rseToUse.sh ]; then
	    alrb_fn_rucioCheckRse
	    exit $?
	fi
	exit 0
    )
    if [ $? -ne 0 ]; then
	return 64
    fi

    source $alrb_workDir/rseToUse.sh
    alrb_rucioUuidGen=`uuidgen`
    alrb_rucioScope="user.${RUCIO_ACCOUNT}"
    alrb_rucioMyDataset="${alrb_rucioScope}:user.${RUCIO_ACCOUNT}.dataset.${alrb_rucioUuidGen}"
    alrb_rucioUploadDir=$alrb_relTestDir/rucioUpload
    alrb_rucioDiDs=""

    if [ "$alrb_thisPython" = "none" ]; then
	local alrb_pythonSetup=""
	if [ "$ALRB_OSMAJORVER" = "7" ]; then
	    local alrb_set3="-2"  # centos7 default is now python3
	else
            local alrb_set3=""
	fi
    else	
	local alrb_pythonSetup="lsetup \"python $alrb_thisPython\""
	local alrb_set3="-3"
    fi
    
    \rm -f $alrb_relTestDir/rucio-script-setup.sh 
    \cat << EOF >> $alrb_relTestDir/rucio-script-setup.sh
source $alrb_envFile.sh
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q $alrb_set3
$alrb_pythonSetup
lsetup rucio $alrb_VerboseOpt
if [ \$? -ne 0 ]; then
  exit 64
fi
source $alrb_workDir/rseToUse.sh
alrb_rucioUuidGen=$alrb_rucioUuidGen
alrb_rucioScope=$alrb_rucioScope
alrb_rucioMyDataset=$alrb_rucioMyDataset
alrb_rucioUploadDir=$alrb_rucioUploadDir
alrb_rucioDiDs="$alrb_rucioDiDs"
EOF

    return 0
}


#!----------------------------------------------------------------------------
alrb_fn_rucioCheckRse()
#!----------------------------------------------------------------------------
{

    local let alrb_retCode=0
    local alrb_result
    local alrb_toolWorkdir="$alrb_workDir/rucio"

    \echo "
Getting appropriate Rucio RSE to use for testing ...
 from: $ALRB_testRucioRseList
"

    mkdir -p $alrb_toolWorkdir
    source $alrb_envFile.sh
    export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
    source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q -3
    lsetup rucio -q
    if [ $? -ne 0 ]; then
	return 64
    fi
    
    local let alrb_retCode=64
    local alrb_rseName
    local alrb_result

    \rm -f $alrb_toolWorkdir/ddmendpt.out
    $ATLAS_LOCAL_ROOT_BASE/utilities/listDDMSites.sh > $alrb_toolWorkdir/ddmendpt.out
    for alrb_rseName in `\echo $ALRB_testRucioRseList | \sed -e 's|\;| |g'`; do
	\echo "
Checking $alrb_rseName ..."
	alrb_result=`\grep -e $alrb_rseName $alrb_toolWorkdir/ddmendpt.out 2>&1`
	if [ $? -ne 0 ]; then
	    \echo "$alrb_rseName not found as DDM site.  Checking next in list."
	    continue
	fi

	local alrb_tier=`\echo $alrb_result | \cut -f 3 -d ":" | \sed -e 's| ||g'`
	if [ "$alrb_tier" = "3" ]; then
	    \echo "Warning: Site for $alrb_rseName is a Tier3 and may not be completely accessible"
	    continue
	fi
	local alrb_siteName=`\echo $alrb_result | \cut -f 2 -d ":" | \sed -e 's| ||g'`

	alrb_result=`$ATLAS_LOCAL_ROOT_BASE/utilities/listDDMBlacklists.sh| \grep -e $alrb_rseName`
	if [ $? -eq 0 ]; then
	    \echo " "
	    \echo "Found possible information for services that could affect $alrb_rseName:"
	    \echo $alrb_result | \tr '|' '\n'
	    \echo " "
	    \echo "These are not definitive and you should read the above description to be sure."
	    continue
	fi

	\rm -f $alrb_toolWorkdir/downtime.out
	local alrb_cmd="$ATLAS_LOCAL_ROOT_BASE/utilities/listDowntimes.sh $alrb_siteName"
	eval $alrb_cmd > $alrb_toolWorkdir/downtime.out 2>&1 
	alrb_result=`\grep -e "site not in downtime" $alrb_toolWorkdir/downtime.out`
	if [ $? -ne 0 ]; then 
	    \echo " "
	    \echo "Found possible information for services that could affect $alrb_rseName:"
	    \echo $alrb_cmd
	    \cat $alrb_toolWorkdir/downtime.out
	    \echo " "
	    \echo "These are not definitive and you should read the above description to be sure."
	    continue
	else
	    let alrb_retCode=0
	    \echo "alrb_rseName=$alrb_rseName" > $alrb_workDir/rseToUse.sh
	    break
	fi
	
    done

    return $alrb_retCode

}


#!---------------------------------------------------------------------------- 
alrb_fn_rucioTestRun()
#!---------------------------------------------------------------------------- 
{
    local alrb_retCode=0

    \echo -e "
\e[1mrucio test\e[0m"
    (
	export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
	source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q -3
	lsetup rucio -q
	rucio --version

	let alrb_osVersionN=`\echo $ALRB_OSMAJORVER`
	alrb_tmpVal="`\echo $ATLAS_LOCAL_RUCIOCLIENTS_VERSION | \cut -f 1 -d "-" | \sed -e 's/\([0-9\.]*\).*/\1/g'`"
	let alrb_tmpValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpVal 3`
	if [[ $alrb_osVersionN -le 7 ]] && [[ $alrb_tmpValN -ge 12901 ]]; then
	    touch $alrb_toolWorkdir/python3Only
	fi
    )

    local alrb_thisPython
    for alrb_thisPython in ${alrb_testPython3Ar[@]}; do

	if [ "$alrb_thisPython" = "none" ]; then
	    if [ -e $alrb_toolWorkdir/python3Only ]; then
		continue
	    fi
	    local alrb_pythonSetup=""
	else
	    local alrb_pythonSetup="(python $alrb_thisPython)"
	fi
	    
	local alrb_thisShell
	for alrb_thisShell in ${alrb_testShellAr[@]}; do
	
	    local alrb_addStatus=""
	    local alrb_relTestDir="$alrb_toolWorkdir/$alrb_thisShell/$alrb_thisPython"
	    \mkdir -p $alrb_relTestDir
	    
	    alrb_fn_rucioTestSetupEnv
	    if [ $? -ne 0 ]; then
		return 64
	    fi

	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio checkID $alrb_pythonSetup"
	    alrb_fn_rucioCheckID
	    alrb_fn_addSummary $? exit
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio list rse $alrb_pythonSetup"
	    alrb_fn_rucioListRse
	    alrb_fn_addSummary $? exit	
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio upload dataset $alrb_pythonSetup"
	    alrb_fn_rucioUploadDS
	    alrb_fn_addSummary $? exit	
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio list dataset $alrb_pythonSetup"
	    alrb_fn_rucioListDS
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio get pfns $alrb_pythonSetup"
	    alrb_fn_rucioGetPfns
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio get dataset $alrb_pythonSetup"
	    alrb_fn_rucioGetDS
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio erase dataset $alrb_pythonSetup"
	    alrb_fn_rucioEraseDS
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio multilevel containers $alrb_pythonSetup"
	    alrb_fn_rucioMultilevelContainers
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio CERN Replica check $alrb_pythonSetup"
	    alrb_fn_rucioCERNReplicaCheck
	    alrb_fn_addSummary $? continue
	    
	    #	alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio BNL Replica check $alrb_pythonSetup"
	    #	alrb_fn_rucioBNLReplicaCheck
	    #	alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio scopeless download $alrb_pythonSetup"
	    alrb_fn_rucioScopelessDownload
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio parent check $alrb_pythonSetup"
	    alrb_fn_rucioParentCheck
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio metadata $alrb_pythonSetup"
	    alrb_fn_rucioMetadata
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio content check $alrb_pythonSetup"
	    alrb_fn_rucioContentCheck
	    alrb_fn_addSummary $? continue
	    
	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "rucio RSE attribute boolean/integer check $alrb_pythonSetup"
	    alrb_fn_rucioRseAttribute
	    alrb_fn_addSummary $? continue
	done
    done

    return 0
}


