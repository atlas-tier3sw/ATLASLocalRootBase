#!----------------------------------------------------------------------------
#!
#! functions-Linux.sh
#!
#! functions for testing the tools
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


#!---------------------------------------------------------------------------- 
alrb_fn_dcubeArtTest1()
#!---------------------------------------------------------------------------- 
{

    local alrb_tmpVal="
arch=x86_64?os=7?
do
&
skip
"
    local alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
    if [ "$alrb_result" != "do" ]; then
	\echo "art-test1 is not supported for RHEL$ALRB_OSMAJORVER $ATLAS_LOCAL_ROOT_ARCH"
	\echo " Skipping this test"
	alrb_setStatus="SKIP"
	return 64
    fi
    
    local alrb_retCode=0
    local alrb_runName="dcubeArtTest1"

    \mkdir -p $alrb_relTestDir/workdir

    local alrb_cmdDcubeRun="\$ATLAS_DCUBE_DIR/test/art-test1.sh"
    local alrb_cmdDcubeRunName="$alrb_relTestDir/dcube-artTest1.out"
    
    local alrb_runScript="$alrb_relTestDir/dcube-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/dcube-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/workdir

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdDcubeRun" $alrb_cmdDcubeRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_dcubeArtTest2()
#!---------------------------------------------------------------------------- 
{

    local alrb_tmpVal="
arch=x86_64?os=7?
do
&
skip
"
    local alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
    if [ "$alrb_result" != "do" ]; then
	\echo "art-test2 is not supported for RHEL$ALRB_OSMAJORVER $ATLAS_LOCAL_ROOT_ARCH"
	\echo " Skipping this test"
	alrb_setStatus="SKIP"
	return 64
    fi

    
    local alrb_retCode=0
    local alrb_runName="dcubeArtTest2"

    \mkdir -p $alrb_relTestDir/workdir

    local alrb_cmdDcubeRun="\$ATLAS_DCUBE_DIR/test/art-test2.sh"
    local alrb_cmdDcubeRunName="$alrb_relTestDir/dcube-artTest2.out"
    
    local alrb_runScript="$alrb_relTestDir/dcube-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/dcube-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/workdir

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdDcubeRun" $alrb_cmdDcubeRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_dcubeArtTest3()
#!---------------------------------------------------------------------------- 
{

        local alrb_tmpVal="
arch=x86_64?os=9?
do
&
arch=aarch64-Linux?os=9?
do
&
skip
"
    local alrb_result="`alrb_fn_setTestVar \"$alrb_tmpVal\"`"
    if [ "$alrb_result" != "do" ]; then
	\echo "art-test1 is not supported for RHEL$ALRB_OSMAJORVER $ATLAS_LOCAL_ROOT_ARCH"
	\echo " Skipping this test"
	alrb_setStatus="SKIP"
	return 64
    fi

    local alrb_retCode=0
    local alrb_runName="dcubeArtTest3"

    \mkdir -p $alrb_relTestDir/workdir

    local alrb_testOpt=""
    if [[ "$ATLAS_LOCAL_ROOT_ARCH" = "aarch64-Linux" ]] && [[ "$ALRB_OSMAJORVER" = "9" ]]; then
	alrb_testOpt='-r \"views LCG_106 aarch64-el9-gcc13-opt\"'
    fi
    
    local alrb_cmdDcubeRun="\$ATLAS_DCUBE_DIR/test/art-test3.sh $alrb_testOpt"
    local alrb_cmdDcubeRunName="$alrb_relTestDir/dcube-artTest3.out"
    
    local alrb_runScript="$alrb_relTestDir/dcube-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/dcube-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/workdir

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdDcubeRun" $alrb_cmdDcubeRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_dcubeCheckPath()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="dcubeCheckPath"

    \mkdir -p $alrb_relTestDir/workdir

    local alrb_cmdDcubeRun="\$ATLAS_DCUBE_DIR/test/check-path.sh"
    local alrb_cmdDcubeRunName="$alrb_relTestDir/dcube-CheckPath.out"
    
    local alrb_runScript="$alrb_relTestDir/dcube-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/dcube-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/workdir

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdDcubeRun" $alrb_cmdDcubeRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_dcubeTestSetupEnv()
#!---------------------------------------------------------------------------- 
{

    \rm -f $alrb_relTestDir/dcube-script-setup.sh
    \cat << EOF >> $alrb_relTestDir/dcube-script-setup.sh
source $alrb_envFile.sh
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q
lsetup "dcube" $alrb_VerboseOpt
if [ \$? -ne 0 ]; then
  exit 64
fi
EOF
    
    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_dcubeTestRun()
#!---------------------------------------------------------------------------- 
{
    local alrb_retCode=0
    local alrb_thisEnv
    local alrb_thisShell

    \echo -e "
\e[1mdcube test\e[0m"
    (
	export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
	source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q
	lsetup dcube -q
	\echo $ALRB_requestedVersions | \tr ' ' '\n' | \grep -e dcube  | \head -n 1
	if [ ! -d "$ATLAS_DCUBE_DIR/test" ]; then
	    \echo "Info: Unable to test this older version of dcube."
	    return 1
	fi
	return 0
    )
    if [ $? -eq 1 ]; then
	return 0
    fi
    
    for alrb_thisShell in ${alrb_testShellAr[@]}; do
	
	local alrb_addStatus=""
	local alrb_relTestDir="$alrb_toolWorkdir/$alrb_thisShell"
	\mkdir -p $alrb_relTestDir

	alrb_fn_dcubeTestSetupEnv
	if [ $? -ne 0 ]; then
	    return 64
	fi

	alrb_fn_initSummary $alrb_tool $alrb_thisShell "dcube check-path"
	alrb_fn_dcubeCheckPath
	alrb_fn_addSummary $? continue

	alrb_fn_initSummary $alrb_tool $alrb_thisShell "dcube art-test1"
	alrb_fn_dcubeArtTest1
	alrb_fn_addSummary $? continue
	
	alrb_fn_initSummary $alrb_tool $alrb_thisShell "dcube art-test2"
	alrb_fn_dcubeArtTest2
	alrb_fn_addSummary $? continue

	alrb_fn_initSummary $alrb_tool $alrb_thisShell "dcube art-test3"
	alrb_fn_dcubeArtTest3
	alrb_fn_addSummary $? continue

    done

    return 0
}


