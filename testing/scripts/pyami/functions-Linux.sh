#!----------------------------------------------------------------------------
#!
#! functions-Linux.sh
#!
#! functions for testing the tools
#!
#! Usage:
#!     not directly
#!
#! History:
#!   08Mar18: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

#!---------------------------------------------------------------------------- 
alrb_fn_pyamiList()
#!---------------------------------------------------------------------------- 
{

    local alrb_retCode=0
    local alrb_runName="list"

    local alrb_cmdListPyami="ami list dataperiods -y 2017"
    local alrb_cmdListPyamiName="$alrb_relTestDir/pyami-list.out"

    local alrb_runScript="$alrb_relTestDir/pyami-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat << EOF >> $alrb_runScript
source $alrb_relTestDir/pyami-script-setup.sh

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdListPyami" $alrb_cmdListPyamiName $alrb_Verbose
alrb_retCode=\$?

exit \$alrb_retCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode
}


#!---------------------------------------------------------------------------- 
alrb_fn_pyamiTestSetupEnv()
#!---------------------------------------------------------------------------- 
{
    
    if [ "$alrb_thisPython" = "none" ]; then
	local alrb_pythonSetup=""
	if [ "$ALRB_OSMAJORVER" = "7" ]; then
	    local alrb_set3="-2"  # centos7 default is now python3
	else
            local alrb_set3=""
	fi
    else	
	local alrb_pythonSetup="lsetup \"python $alrb_thisPython\""
	local alrb_set3="-3"
    fi

    \rm -f $alrb_relTestDir/pyami-script-setup.sh
    \cat << EOF >> $alrb_relTestDir/pyami-script-setup.sh
source $alrb_envFile.sh
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q $alrb_set3
$alrb_pythonSetup
lsetup "pyami" "rucio -w" $alrb_VerboseOpt
if [ \$? -ne 0 ]; then
  exit 64
fi
EOF
    
    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_pyamiTestRun()
#!---------------------------------------------------------------------------- 
{
    local alrb_retCode=0
    local alrb_thisEnv
    local alrb_thisShell

    \echo -e "
\e[1mpyami test\e[0m"
    (
	export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
	source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q
	lsetup pyami -q
	ami --version
    )

    local alrb_thisPython
    for alrb_thisPython in ${alrb_testPython3Ar[@]}; do
	
	if [ "$alrb_thisPython" = "none" ]; then
	    local alrb_pythonSetup=""	    
	else
	    local alrb_pythonSetup="(python $alrb_thisPython)"
	fi

	for alrb_thisShell in ${alrb_testShellAr[@]}; do
	
	    local alrb_addStatus=""
	    local alrb_relTestDir="$alrb_toolWorkdir/$alrb_thisShell/$alrb_thisPython"
	    \mkdir -p $alrb_relTestDir
	    
	    alrb_fn_pyamiTestSetupEnv
	    if [ $? -ne 0 ]; then
		return 64
	    fi

	    alrb_fn_initSummary $alrb_tool $alrb_thisShell "ami list $alrb_pythonSetup"
	    alrb_fn_pyamiList
	    alrb_fn_addSummary $? exit

	done
	
    done

    return 0
}


