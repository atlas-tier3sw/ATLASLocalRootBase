#!----------------------------------------------------------------------------
#!
#! functions-Linux.sh
#!
#! functions for testing the tools
#!
#! Usage:
#!     not directly
#!
#! History:
#!   15Mar21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


#!---------------------------------------------------------------------------- 
alrb_fn_clangCompile()
#!---------------------------------------------------------------------------- 
{
    
    local alrb_retCode=0
    local alrb_runName="clangCompile"

    \mkdir -p $alrb_relTestDir/workdir/$alrb_runName

    local alrb_cmdClangRun="$alrb_relTestDir/clang-compile.sh"
    local alrb_cmdClangRunName="$alrb_relTestDir/clang-compile.out"
   
    \cat  << EOF >> $alrb_cmdClangRun
#! /bin/sh
cd $alrb_relTestDir/workdir/$alrb_runName
rm -f hello.cxx
printf "#include <iostream>\nint main(){std::cout<<\"Hello World\"<<std::endl;return 0;}" > hello.cxx
\$CXX hello.cxx
./a.out
EOF
    chmod +x $alrb_cmdClangRun
    
    local alrb_runScript="$alrb_relTestDir/clang-script-$alrb_runName"
    \rm -f $alrb_runScript
    \cat  << EOF >> $alrb_runScript
source $alrb_relTestDir/clang-script-setup.sh
alrb_exitCode=0
\cd $alrb_relTestDir/workdir

source $ATLAS_LOCAL_ROOT_BASE/utilities/evaluator.sh "$alrb_cmdClangRun" $alrb_cmdClangRunName $alrb_Verbose
alrb_exitCode=\$?

exit \$alrb_exitCode
EOF

    alrb_fn_runShellScript $alrb_thisShell $alrb_runScript
    alrb_retCode=$?

    return $alrb_retCode   
}


#!---------------------------------------------------------------------------- 
alrb_fn_clangTestSetupEnv()
#!---------------------------------------------------------------------------- 
{

    \rm -f $alrb_relTestDir/clang-script-setup.sh
    \cat << EOF >> $alrb_relTestDir/clang-script-setup.sh
source $alrb_envFile.sh
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q
lsetup "clang" $alrb_VerboseOpt
if [ \$? -ne 0 ]; then
  exit 64
fi
EOF
    
    return 0
}


#!---------------------------------------------------------------------------- 
alrb_fn_clangTestRun()
#!---------------------------------------------------------------------------- 
{
    local alrb_retCode=0
    local alrb_thisEnv
    local alrb_thisShell

    \echo -e "
\e[1mclang test\e[0m"
    (
	export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
	source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -q
	lsetup clang -q
	clang --version
    )
    
    for alrb_thisShell in ${alrb_testShellAr[@]}; do
	
	local alrb_addStatus=""
	local alrb_relTestDir="$alrb_toolWorkdir/$alrb_thisShell"
	\mkdir -p $alrb_relTestDir

	alrb_fn_clangTestSetupEnv
	if [ $? -ne 0 ]; then
	    return 64
	fi

	alrb_fn_initSummary $alrb_tool $alrb_thisShell "clang compile"
	alrb_fn_clangCompile
	alrb_fn_addSummary $? continue

    done

    return 0
}


