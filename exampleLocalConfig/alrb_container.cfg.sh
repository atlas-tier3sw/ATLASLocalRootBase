# This is an example site config file for containers.
# It needs to live in a dir that is pointed to by the env variable 
#  $ALRB_localConfigDir that sites need to define for local configurations.

# for mounting dirs in different locations, Singularity at your site needs to 
#  set the configurations for setuid and underlay as shown below. 
#  Type the command
#    'egrep "setuid|underlay" /etc/singularity/singularity.conf'
#  and it needs to show:
#    allow setuid = no
#    enable underlay = yes

# Details of the advanced configurations can be found here:
#  https://twiki.atlas-canada.ca/bin/view/AtlasCanada/Containers#Advanced_Options

# If your home dir is not /home/<username>, then you can mount the same point
#set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS "-B $HOME:$HOME"
# Set the HOME dir to the above for the container user
#set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "export HOME='$HOME'"


