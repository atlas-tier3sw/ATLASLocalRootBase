#! An example of a configuration file for Frontier-squid.
#!
#! The file, localFrontierSquid.sh should reside in 
#! $ATLAS_LOCAL_ROOT_BASE/config (this is preferred)
#! or $ALRB_localConfigDir (if it is defined for all users.)
#!  

#(If your nearest Tier2 allows it,! you can use that Tier2 squid server instead for proxyurl.)
# note that if you are a Tier1 or a Tier2, you should not use this file.  The
# pilot sets FRONTIER_SERVER or else the env SITE_NAME will auto config it.

# eg if you are in North America
#   If you have access to a local squid:
#export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(YOUR-NEREST-LOCAL-SQUID-IF-ANY)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)(proxyurl=http://atlasbpfrontier.cern.ch:3127)"
#   or else
#export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)(proxyurl=http://atlasbpfrontier.cern.ch:3127)" 

# eg if you are in Europe
#   If you have access to a local squid:
#export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(YOUR-NEREST-LOCAL-SQUID-IF-ANY)(proxyurl=http://atlasbpfrontier.cern.ch:3127)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)"
#   or else
#export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(proxyurl=http://atlasbpfrontier.cern.ch:3127)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)"

