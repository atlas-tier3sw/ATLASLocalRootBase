#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! getOSType.sh
#!
#! A simple script to print out the OS type and version of the machine
#!
#! Usage:
#!     getOSType.sh
#! return 
#!   Linux|MacOSX OSMajorVersion OSMinorVersion
#!
#! History:
#!    14Mar14: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# default
alrb_osType="Linux"
alrb_osMajorVer=7
alrb_osMinorVer=9
alrb_osFlavor="centos"

alrb_tmpGLV=`getconf  GNU_LIBC_VERSION 2>&1`
if [ $? -eq 0 ]; then
    alrb_osType="Linux"
    alrb_glv="`\echo $alrb_tmpGLV | \awk '{print $NF}' | \awk -F. '{printf "%d%02d", $1, $2}'`"
    
    if [ $alrb_glv -le 205 ] ; then
	alrb_osMajorVer=5
	alrb_osFlavor="slc"
    elif [ $alrb_glv -le 216 ] ; then
	alrb_osMajorVer=6
	alrb_osFlavor="slc"
    elif [ $alrb_glv -le 227 ]; then
	alrb_osMajorVer=7
	alrb_osFlavor="centos"
    elif [ $alrb_glv -le 233 ]; then
	alrb_osMajorVer=8
	alrb_osFlavor="centos"
    else
	alrb_osMajorVer=9
	alrb_osFlavor="el"
    fi
    
    if [ -e /etc/redhat-release ]; then
	alrb_osVersion="`\sed -e 's/[[:alpha:]]*[[:space:]]* //g' -e 's|[^[:digit:]\.]*||g' /etc/redhat-release`"
    elif [ -e /etc/os-release ]; then
	\grep -e "^ID_LIKE=" /etc/os-release | \grep -e "rhel" -e "centos" -e "fedora" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    alrb_osVersion="`\grep -e "^VERSION=" /etc/os-release | sed -e 's|[^0-9.]*||g'`"
	fi
    else
	alrb_osVersion=""
    fi
    \echo "$alrb_osVersion" | \grep -e "\." > /dev/null 2>&1 
    if [ $? -eq 0 ]; then
	alrb_osMinorVer=`\echo "$alrb_osVersion" | \cut -f 2 -d '.'`
    else
	alrb_osMinorVer=0
    fi	    
    
    \echo "$alrb_osType $alrb_osMajorVer $alrb_osMinorVer $alrb_osFlavor"
    exit 0
fi

# Mac (BSD); ignore the "10" 
# fix since Apple changed product name from BigSur and version bumped to 11
alrb_result=`which sw_vers 2>&1`
if [ $? -eq 0 ]; then
    alrb_osFlavor="macOS"
    alrb_osType=`sw_vers -productName | \sed -e 's/ //g'`
    if [ "$alrb_osType" = "macOS" ]; then
	alrb_osType="MacOSX"
    fi
    alrb_osMajorVer=`sw_vers -productVersion | \cut -f 1 -d "."`
    if [ "$alrb_osMajorVer" = "10" ]; then
	alrb_osMajorVer=`sw_vers -productVersion | \cut -f 2 -d "."`
	alrb_osMinorVer=`sw_vers -productVersion | \cut -f 3 -d "."`
	\echo "$alrb_osType $alrb_osMajorVer $alrb_osMinorVer $alrb_osFlavor"
	exit 0
    else
	alrb_osMinorVer=`sw_vers -productVersion | \cut -f 2 -d "."`
	\echo "$alrb_osType $alrb_osMajorVer $alrb_osMinorVer $alrb_osFlavor"
	exit 0
    fi
fi

\echo "$alrb_osType $alrb_osMajorVer $alrb_osMinorVer $alrb_osFlavor"
exit 0
