#! /bin/bash

alrb_print_ROOTBugMsg() 
{
    \echo -e "
\033[1m\033[4m30 Nov 2023\033[0m 

A serious security issue in ROOT's web-based GUI has been identified:

  You should not use the web-based browser until further notice and instead 
  set the old-style TBrowser as the default. Please see the Twiki page linked 
  below for instructions on how to do this.

  https://twiki.cern.ch/twiki/bin/view/AtlasComputing/AtlasComputingArchive/RootBrowserSecurityIssue

  More details of this issue:
  https://root.cern/about/security/#2023-11-26-open-port-for-control-of-web-gui-allows-read-and-write-access-to-file-system

  \033[0;31mReason for this message: $alrb_addMsg\033[0m 
"

    return 0
}

alrb_addMsg=""

# HOME can be undefined so accept that and quit
if [ -z $HOME ]; then
    exit 0

elif [ ! -e $HOME/.rootrc ]; then
    alrb_addMsg="\$HOME/.rootrc is missing"
    alrb_print_ROOTBugMsg

else
    # get rid of empty lines / spaces, comments
    alrb_tmpBuffer="`\sed -e 's/[[:space:]]//g' -e '/^$/d' -e '/^#/d' $HOME/.rootrc`"
    let alrb_intances=`\echo "$alrb_tmpBuffer" | \grep -e "Browser.Name:" | wc -l`
    if [ $alrb_intances -eq 1 ]; then
	alrb_field=`\echo "$alrb_tmpBuffer" | \grep -e "Browser.Name:" | \cut -f 2- -d ":" | \sed -e 's|[[:space:]]||g'`
	if [ "$alrb_field" != "TRootBrowser" ]; then
	    alrb_addMsg="\$HOME/.rootrc - Browser.Name is NOT set to TRootBrowser"
	    alrb_print_ROOTBugMsg
	fi
    elif [ $alrb_intances -eq 0 ]; then
	alrb_addMsg="\$HOME/.rootrc - has not set Browser.Name"
	alrb_print_ROOTBugMsg

    elif [ $alrb_intances -gt 1 ]; then
	alrb_addMsg="\$HOME/.rootrc - Browser.Name set more than once"
	alrb_print_ROOTBugMsg

    fi
fi

exit 0
