#!----------------------------------------------------------------------------
#!
#! checkShell.sh
#!
#! check bash shell for various things
#!
#! History:
#!   28Mar12: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

shopt -s expand_aliases
alias | \grep -e "setupATLAS" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    typeset -f setupATLAS > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	function setupATLAS
	{
            source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh $@
            return $?
	}
    fi    
fi

