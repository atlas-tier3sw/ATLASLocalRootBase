#!----------------------------------------------------------------------------
#!
#!  getCurrentEnv.sh
#!
#!    Print out a comma delimited string of compiler and python versions
#!     format will be compiler/python:arch:version
#!
#!  Usage:
#!    source getCurrentEnv.sh
#!
#!  History:
#!    21Jan2015: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

alrb_currentEnv=""

for alrb_pyName in python python2 python3; do
    # python format: python:ver=version::arch=arch:
    alrb_type="`type $alrb_pyName 2>&1`"
    if [ $? -eq 0 ]; then
	\echo $alrb_type | \grep -e "alias" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	    continue
	fi
	alrb_version=`$alrb_pyName -V 2>&1 | \awk '{print $2}'`
	alrb_exeArch="`uname -m`"
	alrb_currentEnv="${alrb_currentEnv};$alrb_pyName:ver=${alrb_version}:arch=${alrb_exeArch}:"
    fi
done

if [ "$ALRB_OSTYPE" = "MacOSX" ]; then
# nothing needed for MacOSX yet; using defaults
    alrb_currentEnv="$alrb_currentEnv"
else    
# gcc format: gcc:ver=version:
    alrb_version=`gcc -dumpversion 2>&1`
    if [ $? -eq 0 ]; then
	alrb_currentEnv="${alrb_currentEnv};gcc:ver=${alrb_version}:"
    else
	alrb_currentEnv="${alrb_currentEnv};gcc:ver=0.0:"
    fi
fi


\echo "$alrb_currentEnv;"
unset alrb_version alrb_type alrb_exeArch alrb_currentEnv alrb_pyName
return 0

