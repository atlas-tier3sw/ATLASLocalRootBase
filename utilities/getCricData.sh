#! /bin/bash 
#!----------------------------------------------------------------------------
#!
#! a script to download CRIC json files
#!
#! Usage:
#!    getCricData.sh -h
#!
#! History:
#!   08jan20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_progname=getCricData

alrb_fn_getCricDataHelp()
{
    \cat <<EOF

    getCricData.sh <type> <dir>

    fetch json file from CRIC to download dir.
    successful result will be a <dir>/<type>.json file

    valid type: downtime, ddmendpoint, ddmendpointstatus, pandaqueue

    Options (to override defaults) are:
     -h  --help                Print this help message
     -q  --quiet 	       Quiet option

EOF
}

if [[ -z $X509_USER_PROXY ]] || [[ -z $X509_CERT_DIR ]]; then
    \echo "Error: X509 env not defined"
    exit 64
fi

let alrbExitCode=0
alrb_shortopts="h,q"
alrb_longopts="help,quiet"
alrb_result=`getopt -T >/dev/null 2>&1`
if [ $? -eq 4 ] ; then # New longopts getopt.
    alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_progname" -- "$@")
    alrbExitCode=$?
else # use wrapper
    alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt.sh $alrb_shortopts $alrb_longopts $*`
    alrbExitCode=$?
fi
    
# do we have an error here ?
if [ $alrbExitCode -ne 0 ]; then
    \echo $alrb_opts 1>&2
    \echo "'$alrb_progname --help' for more information" 1>&2
    exit $alrbExitCode
fi
    
eval set -- "$alrb_opts"

alrb_curlOpt=""

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
	    alrb_fn_getCricDataHelp
            exit 0
            ;;
        -q|--quiet)
	    alrb_curlOpt="-s"
	    shift
            ;;
	--)
            shift
            break
            ;;
        *)
            \echo "Internal Error: option processing error: $1" 1>&2
            exit 1
            ;;
    esac
done

if [ $# -ne 2 ]; then
    \echo "Error: incorrect arguments ..." 1>&2
    alrb_fn_getCricDataHelp 1>&2
    exit 64
fi

alrb_type="$1"
alrb_dir="$2"

mkdir -p $alrb_dir

if [ "$alrb_type" = "downtime" ]; then
    alrb_url="https://atlas-cric.cern.ch/api/core/downtime/query/?json&preset=sites"
elif [ "$alrb_type" = "ddmendpoint" ]; then
    alrb_url="https://atlas-cric.cern.ch/api/atlas/ddmendpoint/query/list/?json"
elif [ "$alrb_type" = "ddmendpointstatus" ]; then
    alrb_url="https://atlas-cric.cern.ch/api/atlas/ddmendpointstatus/query/list/?json"
elif [ "$alrb_type" = "pandaqueue" ]; then
    alrb_url="https://atlas-cric.cern.ch/api/atlas/pandaqueue/query/?json"
else
    \echo "Error: unknown Cric fetch type $alrb_type"
    exit 64
fi
    
curl $alrb_curlOpt -o ${alrb_dir}/${alrb_type}.json.new  --capath $X509_CERT_DIR --cacert $X509_USER_PROXY --cert $X509_USER_PROXY "$alrb_url"
if [ $? -eq 0 ]; then
    \mv ${alrb_dir}/${alrb_type}.json.new ${alrb_dir}/${alrb_type}.json
    exit 0
else
    exit 64
fi

exit 0
