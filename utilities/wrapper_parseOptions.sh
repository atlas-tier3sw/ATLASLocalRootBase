#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! wrapper_parseOptions.sh
#!
#! A wrapper script to provide consistent arg parsing
#!
#! Usage: 
#!     wrapper_parseOptions.sh <shell> <shortopts> <longopts> <progname> <args>
#!
#! For bash/zsh, return delimited string of parsed options and args
#!  usage: wrapper_parseOptions.sh bash $SHORTOPTS $LONGOPTS $progname "$@"
#!
#! History:
#!   10Sep15: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_shell=$1
alrb_shortopts=$2
alrb_longopts=$3
alrb_prog=$4
shift 4

alrb_result=`getopt -T >/dev/null 2>&1`
if [ $? -eq 4 ] ; then # New longopts getopt.
    alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_prog" -- "$@")
    alrb_returnVal=$?
else # use wrapper
    alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt_parser.sh $alrb_shortopts $alrb_longopts "$@"`
    alrb_returnVal=$?
    if [ $alrb_returnVal -ne 0 ]; then
	\echo $alrb_opts 1>&2
    fi
fi

# do we have an error here ?
if [ $alrb_returnVal -ne 0 ]; then
    \echo "'$alrb_prog --help' for more information" 1>&2
    exit 1
fi


\echo ${alrb_opts[*]}

exit 0
