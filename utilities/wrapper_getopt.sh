#! /bin/sh
#!----------------------------------------------------------------------------
#!
#! A wrapper to mimic Linux bash getopt for both long and short opts
#! (this is primarily for MacOSX which cannot otherwise parse long opts) 
#!
#! Usage:
#!    wrapper_getopt.sh shortopts longopts arg1 [arg2 ...] 
#!
#! History:
#!   17Mar14: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

"exec" "$(  check_interpreter() { unalias $1 2> /dev/null; unset $1; ALRB_envPython=$(command -v $1); [ $ALRB_envPython ] && $ALRB_envPython -c 'import getopt' > /dev/null 2>&1 && { echo $ALRB_envPython; unset ALRB_envPython; }; }; [ $ALRB_envPython ] && echo $ALRB_envPython || check_interpreter python || check_interpreter python3 || check_interpreter python2 || echo /usr/bin/python )" "-u" "-Wignore" "$0" "$@"

from __future__ import print_function
import getopt, sys

myArgs = str(sys.argv)

myShortOpts = sys.argv[1]
myLongOpts = sys.argv[2].split(',')
myLongOpts = [ x.replace(':','=') for x in myLongOpts ]

try:
    opts, args = getopt.gnu_getopt( sys.argv[3:], myShortOpts, myLongOpts )

except getopt.GetoptError as err:
    print(str(err))
    sys.exit(64)

resultOpts = "".join(" ".join("%s %s" % tup for tup in opts)) 
resultArgs = " ".join( args )

print("%s -- %s " % (resultOpts,resultArgs))

sys.exit(0)
