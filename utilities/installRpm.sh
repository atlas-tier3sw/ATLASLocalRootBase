#! /bin/sh
#!----------------------------------------------------------------------------
#!
#! installRpm.sh
#!
#! install fron yum repos software that is missing
#!
#! Usage:
#!     installRpm.sh <dir to install> <rpm packages delimited by commas>
#!
#! History:
#!    26Aug22: A. De Silva
#!
#!----------------------------------------------------------------------------

if [ "$#" -ne 2 ]; then 
    \echo "
Usage: 
       installRpm <local dir to install> <rpm packages delimited by commas>

	This is provided as a best effort basis, not all rpms installed this 
	way will work.

        You can run this once
 	  inside a container 
	  or bare metal RHEL machine
          or use a container command line 
	    eg: 
	      setupATLAS -c centos7 -r \"installRpm <local dir to install> <rpm packages delimited by commas>\"
	(ie install once and reuse multiple times.)

	Note that anything installed previously will be removed 

	To use; source this script after everything else is setup:
	 source <local dir to install>/setup.sh

	The approprite directories will be appended to paths so that the 
	 priorities are not overridden and should  mimic a system/os 
         installation.

        To give additional options to dnf or yum, define them in an env
	    export ALRB_RPMINSTALL_OPTS=\"...\"

        The installed directory and its contents should be relocatable.
        That is, if uploaded to the grid, it can be setup and used in the
        same manner.

       "
    exit 64
fi


alrb_fn_createSetupScript()
{
    
    alrb_dirList=(
	"PATH:/usr/bin"
	"PATH:/bin"
	"PATH:/usr/sbin"
	"PATH:/sbin"
	"LD_LIBRARY_PATH:/usr/lib64"
	"LD_LIBRARY_PATH:/usr/lib"
	"LD_LIBRARY_PATH:/lib64"
	"LD_LIBRARY_PATH:/lib"
    )

    alrb_pythonAr=()
    alrb_pythonVersions=()
    for alrb_item in ${alrb_dirList[@]}; do
	alrb_pathname=`\echo $alrb_item | \cut -f 1 -d ":"`
	alrb_path=`\echo $alrb_item | \cut -f 2 -d ":"`
	if [ -d "$alrb_workdir$alrb_path" ]; then
	    \echo "appendPath $alrb_pathname \$alrb_scriptHome$alrb_path" >> $alrb_workdir/setup.sh.tmp

	    if [ "$alrb_pathname" = "LD_LIBRARY_PATH" ]; then
		alrb_tmpAr=( `\find $alrb_workdir$alrb_path -name python* -type d` )
		for alrb_item2 in ${alrb_tmpAr[@]}; do
		    \echo "		    		    
if [ -d \$alrb_scriptHome$alrb_path/\$alrb_scriptPython ]; then
    appendPath PYTHONPATH \$alrb_scriptHome$alrb_path/\$alrb_scriptPython
fi
if [ -d \$alrb_scriptHome$alrb_path/\$alrb_scriptPython/site-packages ]; then
    appendPath PYTHONPATH \$alrb_scriptHome$alrb_path/\$alrb_scriptPython/site-packages
fi
" >> $alrb_workdir/setup.sh.tmp2
		    alrb_tmpVal=`\basename $alrb_item2`
		    \echo ${alrb_pythonVersions[@]} | \grep -e $alrb_tmpVal > /dev/null 2>&1
		    if [ $? -ne 0 ]; then
			alrb_pythonVersions=( ${alrb_pythonVersions[@]}  $alrb_tmpVal )
		    fi
		done
		
	    fi
	fi
    done    


    if [ ${#alrb_pythonVersions[@]} -gt 1 ]; then
	\cat <<EOF >> $alrb_workdir/setup.sh
	if [ "\$#" -ne 1 ]; then
\echo "Error: You need to specify an argument for python versions, available:
         ${alrb_pythonVersions[@]}"
           return 64
        else
	   alrb_scriptPython="\$1"
	fi

EOF
    else
	\cat <<EOF >> $alrb_workdir/setup.sh
alrb_scriptPython="${alrb_pythonVersions[0]}"

EOF
    fi

    
    \cat <<EOF >> $alrb_workdir/setup.sh
if [ ! -z \$BASH_SOURCE ]; then
    alrb_scriptHome=\`\dirname \${BASH_SOURCE[0]}\`
else
    alrb_scriptHome=\`\dirname \$0\`
fi
if [ "\$alrb_scriptHome" = "." ]; then
    alrb_scriptHome=\`pwd\`
fi

# this is a minimal ALRB setup
if [ -z \$ATLAS_LOCAL_ROOT_BASE ]; then
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
fi
source \$ATLAS_LOCAL_ROOT_BASE/utilities/setupAliases.sh

EOF

    touch $alrb_workdir/setup.sh.tmp
    \cat $alrb_workdir/setup.sh.tmp >> $alrb_workdir/setup.sh  
    \rm -f $alrb_workdir/setup.sh.tmp
    touch $alrb_workdir/setup.sh.tmp2
    \cat $alrb_workdir/setup.sh.tmp2 >> $alrb_workdir/setup.sh  
    \rm -f $alrb_workdir/setup.sh.tmp2
    
    \cat <<EOF >> $alrb_workdir/setup.sh
unset alrb_scriptHome alrb_scriptPython
EOF


    if [ ${#alrb_pythonVersions[@]} -gt 1 ]; then
	\echo "
Usage:
	source $alrb_installDir/setup.sh <python version>
	
	       where <python version> is one of these:
               ${alrb_pythonVersions[@]}	      
"
    else
	\echo "
Usage:	
	source $alrb_installDir/setup.sh 
"
    fi

    return 0
}

if [ ! -z "$ALRB_RPMINSTALL_OPTS" ]; then
    \echo "\$ALRB_RPMINSTALL_OPTS=$ALRB_RPMINSTALL_OPTS"
fi

alrb_installDir="$1"
\mkdir -p $alrb_installDir
if [ $? -ne 0 ]; then
    exit 64
fi
alrb_installDir=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_installDir`

alrb_rpms="`\echo $2 | \sed -e 's|,| |g'`"

if [ -e /etc/os-release ]; then
    source /etc/os-release
    alrb_osver="`\echo $VERSION | \cut -f 1 -d ' ' | \cut -d "." -f 1`"
else	  
    alrb_osver=`lsb_release  -r | \awk '{print $2}' | \cut -d "." -f 1`
fi
alrb_unameM="`uname -m`"
alrb_platform="$alrb_unameM-centos$alrb_osver"

alrb_workdir=$alrb_installDir/$alrb_platform
\rm -rf $alrb_workdir
alrb_logdir=$alrb_workdir/logs
\mkdir -p $alrb_logdir
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir
which yumdownloader > /dev/null 2>&1
if [ $? -eq 0 ]; then
    yumdownloader --resolve $ALRB_RPMINSTALL_OPTS $alrb_rpms
else
    dnf download --resolve $ALRB_RPMINSTALL_OPTS $alrb_rpms
fi
if [ $? -ne 0 ]; then
    exit 64
fi

for alrb_item in `ls -1 *.rpm`; do
    rpm2cpio $alrb_item | cpio -dim;
done

cd $alrb_workdir
ls -1 *.rpm > ./os-rpms.txt
\rm *.rpm

cd $alrb_workdir
alrb_fn_createSetupScript

\cat <<EOF >> $alrb_installDir/setup.sh.tmp

if [ ! -z \$BASH_SOURCE ]; then
    alrb_scriptInstallDirBase=\`\dirname \${BASH_SOURCE[0]}\`
else
    alrb_scriptInstallDirBase=\`\dirname \$0\`
fi
if [ "\$alrb_scriptInstallDirBase" = "." ]; then
    alrb_scriptInstallDirBase=\`pwd\`
fi

if [ ! -e \$alrb_scriptInstallDirBase/\$ATLAS_LOCAL_ROOT_ARCH-centos\$ALRB_OSMAJORVER/setup.sh ]; then
  \echo "This is not installed for \$ATLAS_LOCAL_ROOT_ARCH-centos\$ALRB_OSMAJORVER"
  return 64
fi
source \$alrb_scriptInstallDirBase/\$ATLAS_LOCAL_ROOT_ARCH-centos\$ALRB_OSMAJORVER/setup.sh \$@
return \$?
EOF
if [ $? -eq 0 ]; then
    \mv $alrb_installDir/setup.sh.tmp $alrb_installDir/setup.sh
fi

exit 0
