#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! createLockFile.sh
#!
#! create a lockfile if one does not already exist
#!
#! Usage:
#!     createLockFile.sh <options>
#!
#! History:
#!    28Oct19: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_progname=createLockFile

alrb_fn_createLockFileHelp()
{
    \cat <<EOF 

    $alrb_progname [options] 

    This will create a lockfile if it does not exists.
    If it exists, sleep for a specified time and retry the specified amount.
    If the caller process on the same host no longer exists, remove the lock
   
    important Notes: 
      - user is responsible for deleting filename

    Options to override default:
     -d=INTEGER     delete lockfile and continue if older than (default 600 s) 
                     will not delete if the process which created the lock 
                     exists. 
     -f=STRING      lock file with complete path
     -h --help      print this help message
     -r=INTEGER     retries (default 10)
     -s=INTEGER     sleeptime in seconds (default 60s)
     -v --verbose   verbose output

EOF

    return 0
}


alrb_fn_clearlockfile() 
{
    
    local alrb_result
    if [ -e $alrb_lockFile ]; then
	source $alrb_lockFile
	local let alrb_wait=`expr $alrb_timeNow - $alrb_lockTime`
	if [ "$alrb_oldHost" = "$alrb_thisHost" ]; then
	    alrb_result=`ps -p $alrb_oldPid 2>&1`
	    if [ $? -ne 0 ]; then
		if [ "$alrb_verbose" = "YES" ]; then
	            \echo "lockfile: lock deleted since pid $alrb_oldPid that created it does not exist"
		fi
		\rm -f $alrb_lockFile
		return 0
	    else
		if [ "$alrb_verbose" = "YES" ]; then
	            \echo "lockfile: locked by pid $alrb_oldPid"
		fi
	    fi	    
	elif [ $alrb_wait -gt $alrb_deleteLock ]; then
	    if [ "$alrb_verbose" = "YES" ]; then
	        \echo "lockfile: old lock deleted after $alrb_wait s"
	    fi
	    \rm -f $alrb_lockFile
	    return 0
	fi
    else
	return 0
    fi

    return 1
}


alrb_fn_getlockfile() 
{
    
    alrb_fn_clearlockfile    
    local alrb_result=$?

    if [ $alrb_result -eq 0 ]; then
	if [ "$alrb_verbose" = "YES" ]; then
	    \echo "lockfile: creating $alrb_lockFile"	
	fi
	\echo "alrb_oldPid=$alrb_parentPid; alrb_oldHost=$alrb_thisHost; let alrb_lockTime=$alrb_timeNow" >> $alrb_lockFile
	alrb_decision=""
	return 0
    fi

    return $alrb_result
}



alrb_shortopts="f:,s:,q,r:,h,v,d:"
alrb_longopts="help,verbose"
alrb_result=`getopt -T >/dev/null 2>&1`
if [ $? -eq 4 ] ; then # New longopts getopt.
    alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_progname" -- "$@")
    alrb_returnVal=$?
else # use wrapper
    alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt.sh $alrb_shortopts $alrb_longopts $*`
    alrb_returnVal=$?
fi
    
# do we have an error here ?
if [ $alrb_returnVal -ne 0 ]; then
    \echo $alrb_opts 1>&2    
    \echo "$alrb_progname --help for usage" 1>&2
    return 1
fi

eval set -- "$alrb_opts"

alrb_lockFile=""
let alrb_sleepTime=60
let alrb_repeat=10
alrb_verbose=""
let alrb_deleteLock="600"

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
	    alrb_fn_createLockFileHelp
	    exit 0
            ;;
	-f)
	    alrb_lockFile=$2
	    shift 2
	    ;;
        -s)
	    let alrb_sleepTime=$2
	    shift 2
	    ;;
        -r)
	    let alrb_repeat=$2
	    shift 2
	    ;;
	-d)
	    let alrb_deleteLock=$2
	    shift 2
	    ;;
	-v|--verbose)
	    alrb_verbose="YES"
	    shift
	    ;;
	--)
	    shift
	    break
	    ;;
        *)
	    \echo "Internal Error: option processing error: $1" 1>&2
	    exit 1
	    ;;
    esac
done

if [ "$alrb_lockFile" = "" ]; then
    \echo "Error: filename is not specified for lockfile" 1>&2
    exit 64
fi
    
let alrb_maxWait=`expr $alrb_repeat \* $alrb_sleepTime`
alrb_parentPid=$PPID
alrb_thisHost=`hostname -f`
let alrb_procTimestamp=`date +%s`
let alrb_timeNow=$alrb_procTimestamp
alrb_decision="get"
let alrb_rc=0

while [ "$alrb_decision" = "get" ]; do
    let alrb_wait=`expr $alrb_timeNow - $alrb_procTimestamp`
    if [ $alrb_wait -gt $alrb_maxWait ]; then
	if [ "$alrb_verbose" = "YES" ]; then
	    \echo "lockfile: giving up after $alrb_wait s"
	fi
	exit 1
    fi    

    alrb_fn_getlockfile
    alrb_rc=$?
    if [ "$alrb_decision" = "get" ]; then
	if [ "$alrb_verbose" = "YES" ]; then
	    \echo "lockfile: sleeping for $alrb_sleepTime s"
	fi
	sleep $alrb_sleepTime
	let alrb_timeNow=`date +%s`
    fi
done

exit $alrb_rc


