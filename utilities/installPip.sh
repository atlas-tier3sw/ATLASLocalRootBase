#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! installPip.sh
#!
#! install from pip
#!
#! Usage:
#!     installPip.sh <dir to install>/<file containing required packages>
#!
#! History:
#!    13Mar23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_progname=installPip

alrb_fn_installPipHelp()
{

        \cat <<EOF 

    $alrb_progname [options] <dir to install>/<file containing required packages>

    The file containing the required pip installs should be in the directory
    that you want the packages to be installed. It is passed to pip as the
    arg to the -r option.  See pip install document for details.

    Note that this is meant only for python3 and recommended 3.9 and newer.

    You can install once and reuse:
      inside a container (after setupATLAS -c ...)
        eg:
	  installPip <dir to install>/<file containing required packages>
      or bare metal RHEL machine (after setupATLAS)
        eg:
          installPip <dir to install>/<file containing required packages>
      or use a container command line from bare metal RHEL machine
        eg: 
          setupATLAS -c centos7 -r "installPip <dir to install>/<file containing required packages>"

     You can use the same installation dir for different OS and arch.
     It will preserve different os/arch types but recreate the specified one.

     To use; source this script after everything else is setup:
     source <dir to install>/setup.sh

    The approprite directories will be appended to paths so that the 
    existing path priorities are not overridden.

    Options to override default:
     -h --help      Print this help message
     -c --cOpts     Options to pass to setupATLAS -c
     -C --container Container to use
     -O --Options   Options for pip install		     
     -o --os        Comma delimited os versions (default $ALRB_OSMAJORVER)
     		     eg 7,9
     -p --pyVersion Comma delimited ALRB versions of python (default 3.9)
     		     eg 3.7,3.8,3.9 or 3.7:3.7.13-fix1-x86_64-centos7
		     where a specific ALRB python version can be passed 

    The installed directory and its contents should be relocatable.  That is,
    if uploaded to the grid, it can be setup and used in the same manner.

EOF

    return 0
    
}


alrb_fn_createSetupScript()
{

    \cat <<EOF >> $alrb_workdir/setup.sh
if [ ! -z \$BASH_SOURCE ]; then
    alrb_scriptHome=\`\dirname \${BASH_SOURCE[0]}\`
else
    alrb_scriptHome=\`\dirname \$0\`
fi
if [ "\$alrb_scriptHome" = "." ]; then
    alrb_scriptHome=\`pwd\`
fi

EOF
    
    alrb_dirList=(
	"PATH:/usr/bin"
	"PATH:/bin"
	"PATH:/usr/sbin"
	"PATH:/sbin"
	"LD_LIBRARY_PATH:/usr/lib"
	"LD_LIBRARY_PATH:/usr/lib64"
	"LD_LIBRARY_PATH:/lib"
	"LD_LIBRARY_PATH:/lib64"
    )

    alrb_donePyPath=","
    for alrb_item in ${alrb_dirList[@]}; do
	alrb_pathname=`\echo $alrb_item | \cut -f 1 -d ":"`
	alrb_path=`\echo $alrb_item | \cut -f 2 -d ":"`
	if [ -d "$alrb_workdir$alrb_path" ]; then
	    \echo "appendPath $alrb_pathname \$alrb_scriptHome$alrb_path" >> $alrb_workdir/setup.sh.tmp

	    if [ "$alrb_pathname" = "LD_LIBRARY_PATH" ]; then
		alrb_tmpAr=( `\find $alrb_workdir$alrb_path -name site-packages -type d` )
		for alrb_item2 in ${alrb_tmpAr[@]}; do
		    \echo "$alrb_donePyPath" | \grep -e "$alrb_path" > /dev/null 2>&1
		    if [ $? -eq 0 ]; then
			continue
		    fi
		    \echo "		    		    
if [ -d \$alrb_scriptHome$alrb_path/python\$alrb_scriptPython ]; then
    appendPath PYTHONPATH \$alrb_scriptHome$alrb_path/python\$alrb_scriptPython
fi
if [ -d \$alrb_scriptHome$alrb_path/python\$alrb_scriptPython/site-packages ]; then
    appendPath PYTHONPATH \$alrb_scriptHome$alrb_path/python\$alrb_scriptPython/site-packages
fi
" >> $alrb_workdir/setup.sh.tmp2	
		    alrb_donePyPath="$alrb_donePyPath,$alrb_path,"
		done
		
	    fi
	fi
    done    

    let alrb_nPythonVer=`\wc -l $alrb_workdir/pythonBuild.txt | \cut -f 1 -d ' '`
    
    if [ $alrb_nPythonVer -gt 1 ]; then
	\cat <<EOF >> $alrb_workdir/setup.sh
if [ "\$#" -ne 1 ]; then
  \echo "There is more than one python version so this is ambigious ..."
  \echo "Error: You need to specify an argument for python version, available:"
  \cat \$alrb_scriptHome/pythonBuild.txt | \cut -f 1 -d "|"  

  return 64
else
  alrb_scriptPython="\$1"
fi

EOF
    else
	\cat <<EOF >> $alrb_workdir/setup.sh
alrb_scriptPython="\`\tail -n 1 \$alrb_scriptHome/pythonBuild.txt | \cut -f 1 -d '|'\`"  			
EOF
    fi

    \cat <<EOF >> $alrb_workdir/setup.sh

alrb_tmpVer="\`\grep -e \"^\$alrb_scriptPython\" \$alrb_scriptHome/pythonBuild.txt | \cut -f 2 -d '|'\`"
if [ \$? -eq 0 ]; then

  alrb_thisPy3=""
  alrb_tmpVal="\`python3 -V 2>&1\`"
  if [ \$? -eq 0 ]; then
     alrb_thisPy3=\`\echo \$alrb_tmpVal | \cut -f 2 -d " " | \cut -f -2 -d "."\`
  fi
  if [ "\$alrb_thisPy3" != "\$alrb_scriptPython" ]; then
    lsetup "python \$alrb_tmpVer"
  fi
fi

EOF

    touch $alrb_workdir/setup.sh.tmp
    \cat $alrb_workdir/setup.sh.tmp >> $alrb_workdir/setup.sh  
    \rm -f $alrb_workdir/setup.sh.tmp
    touch $alrb_workdir/setup.sh.tmp2
    \cat $alrb_workdir/setup.sh.tmp2 >> $alrb_workdir/setup.sh  
    \rm -f $alrb_workdir/setup.sh.tmp2
    
    \cat <<EOF >> $alrb_workdir/setup.sh
unset alrb_scriptHome alrb_scriptPython alrb_tmpVer alrb_tmpVal alrb_thisPy3
EOF


    if [ $alrb_nPythonVer -gt 1 ]; then
	\echo "
Usage:
	source $alrb_installDir/setup.sh <python version>
	
	       where <python version> is one of these:
"
\cat $alrb_installDir/pythonBuild.txt | \cut -f 1 -d '|'
    else
	\echo "
Usage:	
	source $alrb_installDir/setup.sh 
"
    fi

    return 0
}


# * * * main * * * 

alrb_shortopts="h,o:,p:,O:,c:,C:"
alrb_longopts="help,os:,pyVersion:,Options:,cOpts:,container:"
alrb_result=`getopt -T >/dev/null 2>&1`
if [ $? -eq 4 ] ; then # New longopts getopt.
    alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_progname" -- "$@")
    alrb_returnVal=$?
else # use wrapper
    alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt.sh $alrb_shortopts $alrb_longopts $*`
    alrb_returnVal=$?
fi
    
# do we have an error here ?
if [ $alrb_returnVal -ne 0 ]; then
    \echo $alrb_opts 1>&2    
    \echo "$alrb_progname --help for usage" 1>&2
    return 1
fi

eval set -- "$alrb_opts"

alrb_osVersionAr=( "$ALRB_OSMAJORVER" )
alrb_pyVersionAr=( "3.9" )
alrb_pipOpts=""
alrb_contOpt=""
alrb_useContainer=""

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
	    alrb_fn_installPipHelp
	    exit 0
            ;;
	-C|--container)
	    alrb_useContainer="$2"
	    shift 2
	    ;;
	-o|--os)
	    alrb_osVersionAr=( `\echo "$2" | \sed -e 's|,| |g'`)
	    shift 2
	    ;;
        -O|--Options)
	    alrb_pipOpts="-O \"$2\""
	    shift 2
	    ;;
        -p|--pyVersion)
	    alrb_pyVersionAr=( `\echo "$2" | \sed -e 's|,| |g'`)
	    shift 2
	    ;;
	-c|--cOpts)
	    alrb_contOpt="$2"
	    shift 2
	    ;;
	--)
	    shift
	    break
	    ;;
        *)
	    \echo "Internal Error: option processing error: $1" 1>&2
	    exit 1
	    ;;
    esac
done

if [[ "$#" -eq 0 ]] || [[ "$#" -gt 1 ]] ; then 
    alrb_fn_installPipHelp
    \echo "Error: missing argument"
    exit 64
else
    alrb_argFile="`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $1`"
    if [ ! -e "$alrb_argFile" ]; then
	alrb_fn_installPipHelp
	\echo "Error: unable to find $alrb_argFile"    
	exit 64
    fi
    alrb_installDir="`\dirname $alrb_argFile`"
    alrb_pipRFile="`basename $alrb_argFile`"
fi

alrb_unameM="`uname -m`"
for alrb_osVersion in ${alrb_osVersionAr[@]}; do

    # Asoka, 20 Mat 2023.  Fix me !
    # temporary workaround to avoid Apptainer and /eos issue
    # https://github.com/apptainer/apptainer/issues/1217
    pwd | \grep -e "^/eos" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	cd 
    fi	   
	    
    alrb_platform="$alrb_unameM-centos$alrb_osVersion"

    alrb_workdir=$alrb_installDir/$alrb_platform
    \rm -rf $alrb_workdir

    for alrb_pyVersionVal in ${alrb_pyVersionAr[@]}; do
	alrb_pyVersion=`\echo $alrb_pyVersionVal | \cut -f 1 -d ":"`
	alrb_pyALRBVersion=`\echo $alrb_pyVersionVal | \cut -f 2 -d ":"`
	if [ "$alrb_pyVersion" = "$alrb_pyALRBVersion" ]; then
	    alrb_pyALRBVersion="centos$alrb_osVersion-$alrb_pyVersion"
	fi
	\echo $alrb_pyALRBVersion | \grep -e "centos$alrb_osVersion" > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	    continue
	fi

	\echo "

 * * * Installing for centos$alrb_osVersion and python $alrb_pyVersionVal ***
"
	
	alrb_logdir=$alrb_workdir/logs/$alrb_pyVersion
	\mkdir -p $alrb_logdir
	if [ $? -ne 0 ]; then
	    exit 64
	fi
    
	\cp $alrb_argFile $alrb_workdir/userRequirements.txt
	if [ "$alrb_osVersion" = "7" ]; then
	    alrb_devtoolset="scl enable devtoolset-7"
	    alrb_thisContainer="$ALRB_cvmfs_repo/containers/fs/singularity/$alrb_unameM-alrbdev-centos7"
	else
	    alrb_devtoolset=""
	    alrb_thisContainer="centos$alrb_osVersion"
	fi
	if [ "$alrb_useContainer" != "" ]; then
	     alrb_thisContainer="$alrb_useContainer"
	fi

	if [ "$alrb_devtoolset" = "" ]; then
	    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $alrb_thisContainer $alrb_contOpt --noalrb -r "$ATLAS_LOCAL_ROOT_BASE/utilities/make-genericPip.sh $alrb_workdir/userRequirements.txt $alrb_pyVersionVal $alrb_pipOpts" 
	else
	    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $alrb_thisContainer $alrb_contOpt --noalrb -r "$alrb_devtoolset '$ATLAS_LOCAL_ROOT_BASE/utilities/make-genericPip.sh $alrb_workdir/userRequirements.txt $alrb_pyVersionVal $alrb_pipOpts'"
	fi
	if [ $? -ne 0 ]; then
	    \echo "FAILED: OS: $alrb_osVersion, ARCH: $alrb_unameM, PYTHON: $alrb_pyVersion" | tee -a $alrb_workdir/status
	    \echo "        Will continue with next ..."
	    continue
	else
	    \echo "OK: OS: $alrb_osVersion, ARCH: $alrb_unameM, PYTHON: $alrb_pyVersionVal" |  tee -a $alrb_workdir/status
	    \echo "$alrb_pyVersion|$alrb_pyALRBVersion|" >> $alrb_workdir/pythonBuild.txt
	fi
    done

    cd $alrb_workdir
    alrb_fn_createSetupScript

done	

	
\cat <<EOF >> $alrb_installDir/setup.sh.tmp

if [ ! -z \$BASH_SOURCE ]; then
    alrb_scriptHomeBase=\`\dirname \${BASH_SOURCE[0]}\`
else
    alrb_scriptHomeBase=\`\dirname \$0\`
fi
if [ "\$alrb_scriptHomeBase" = "." ]; then
    alrb_scriptHomeBase=\`pwd\`
fi

if [ ! -e \$alrb_scriptHomeBase/\$ATLAS_LOCAL_ROOT_ARCH-centos\$ALRB_OSMAJORVER/setup.sh ]; then	
  \echo "This is 								    not installed for \$ATLAS_LOCAL_ROOT_ARCH-centos\$ALRB_OSMAJORVER"
  return 64
fi	
source \$alrb_scriptHomeBase/\$ATLAS_LOCAL_ROOT_ARCH-centos\$ALRB_OSMAJORVER/setup.sh \$@	
return \$?
EOF
if [ $? -eq 0 ]; then
    \mv $alrb_installDir/setup.sh.tmp $alrb_installDir/setup.sh
fi

\cat $alrb_installDir/*/status 2> /dev/null
\grep -e "FAILED:" $alrb_installDir/*/status > /dev/null 2>&1
if [ $? -eq 0 ]; then
    exit 64
fi

exit 0
