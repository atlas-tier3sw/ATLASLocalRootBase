#! /bin/bash
#!----------------------------------------------------------------------------
#! 
#! make-genericPip.sh
#!
#!  Install pip packages in PWD
#!
#! Usage:
#!     cd <installation dir>
#!     setupATLAS -c centos$ALRB_OSMAJORVER --noalrb -r "$ATLAS_LOCAL_ROOT_BASE/utilities/make-genericPip.sh <full path to install dir>/<pip requirement file containing what to install> <pythonversion>"
#!  should be run inside a container for the os/arch that you need
#!
#!
#! History:
#!    08Mar23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_progname="genericPip"
alrb_pkgName="pipInstalls"

alrb_fn_genericPipHelp()
{
    \cat <<EOF 
    $alrb_progname [options] <full path to install dir>/<pip requirement file containing what to install> <python Version>

    will use the file and python version to install the contents in \$PWD.
    \$PWD will not be erased.

    Options to override default:
     -h --help      Print this help message
     -O --Options   Options for pip install

EOF
    return 0
}


# * * * main * * *

alrb_shortopts="h,O:"
alrb_longopts="help,Options:"
alrb_result=`getopt -T >/dev/null 2>&1`
if [ $? -eq 4 ] ; then # New longopts getopt.
    alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_progname" -- "$@")
    alrb_returnVal=$?
else # use wrapper
    alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt.sh $alrb_shortopts $alrb_longopts $*`
    alrb_returnVal=$?
fi
    
# do we have an error here ?
if [ $alrb_returnVal -ne 0 ]; then
    \echo $alrb_opts 1>&2    
    \echo "$alrb_progname --help for usage" 1>&2
    return 1
fi

eval set -- "$alrb_opts"

alrb_pipOpts=""

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
	    alrb_fn_genericPipHelp
	    exit 0
            ;;
        -O|--Options)
	    alrb_pipOpts="$2"
	    shift 2
	    ;;
	--)
	    shift
	    break
	    ;;
        *)
	    \echo "Internal Error: option processing error: $1" 1>&2
	    exit 1
	    ;;
    esac
done

if [ "$#" -ne 2 ]; then 
    alrb_fn_genericPipHelp
    \echo "Error: incorrect number of args"
    exit 64
fi

alrb_reqFile="$1"
if [ ! -e $alrb_reqFile ]; then
    \echo "Error: unable to find file of pip stuff to install $alrb_reqFile"
    exit 64
else
    \echo "    * * * Contents of $alrb_reqFile   * * *"
    \cat $alrb_reqFile
    \echo "    * * * * * *"
fi
alrb_buildDir="`\dirname $alrb_reqFile`"
alrb_pyVersionVal="$2"

if [ -e /etc/os-release ]; then
    source /etc/os-release
    alrb_osVersion="`\echo $VERSION | \cut -f 1 -d ' ' | cut -f 1 -d '.'`"
else  
    alrb_osVersion=`lsb_release  -r | \awk '{print $2}' | \cut -d "." -f 1`
fi
alrb_unameM=`uname -m`
alrb_tag="$alrb_pkgName-$alrb_unameM-centos$alrb_osVersion"

alrb_pyVersion=`\echo $alrb_pyVersionVal | \cut -f 1 -d ":"`
alrb_pyALRBVersion=`\echo $alrb_pyVersionVal | \cut -f 2 -d ":"`
if [ "$alrb_pyVersion" = "$alrb_pyALRBVersion" ]; then
    alrb_pyALRBVersion="centos$alrb_osVersion-$alrb_pyVersion"
fi
alrb_pyVersionMajor=`\echo $alrb_pyVersion | \cut -d "." -f 1`

alrb_lsetupOption=""
if [ "$alrb_pyVersion" = "2.7" ]; then
    alrb_lsetupOption="-f"
fi

if [[ "$alrb_osVersion" = "9" ]] && [[ "$alrb_pyALRBVersion" = "centos9-3.9" ]]; then
    \echo "Skipping seting up python $alrb_pyALRBVersion since 3.9 is available on system"
else
    type -a lsetup > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
    fi
    lsetup $alrb_lsetupOption "python $alrb_pyALRBVersion"
    if [ $? -ne 0 ]; then
	exit 64
    fi
fi

alrb_logDir=$alrb_buildDir/logs/$alrb_pyVersion
mkdir -p $alrb_logDir
 
if [ -z $TMPDIR ]; then
    \echo TMPDIR not defined.  Will use PWD=$PWD
    alrb_workdir=$PWD/deleteMe-$alrb_pkgName/$alrb_tag
else
    alrb_workdir=$TMPDIR/deleteMe-$alrb_pkgName/$alrb_tag
fi
\rm -rf $alrb_workdir
mkdir -p $alrb_workdir
cd $alrb_workdir

alrb_virtualEnvDir=$alrb_workdir/VirtualEnvDir
mkdir -p $alrb_virtualEnvDir
cd $alrb_virtualEnvDir
export HOME=$alrb_virtualEnvDir

\echo "
python$alrb_pyVersionMajor -m pip install $alrb_pipOpts --user virtualenv" 2>&1 | tee -a $alrb_logDir/virtualenv.log
python$alrb_pyVersionMajor -m pip install $alrb_pipOpts --user virtualenv 2>&1 | tee -a $alrb_logDir/virtualenv.log
if [ $? -ne 0 ]; then
    exit 64
fi
\echo "
.local/bin/virtualenv -p python$alrb_pyVersionMajor myVenv" 2>&1 | tee -a $alrb_logDir/virtualenv.log
.local/bin/virtualenv -p python$alrb_pyVersionMajor myVenv 2>&1 | tee -a $alrb_logDir/virtualenv.log
if [ $? -ne 0 ]; then
    exit 64
fi
\echo "
. myVenv/bin/activate" >> $alrb_logDir/virtualenv.log
. myVenv/bin/activate
if [ $? -ne 0 ]; then
    exit 64
fi
\echo "
pip install $alrb_pipOpts -r $alrb_reqFile" 2>&1 | tee -a $alrb_logDir/pipinstall.log
pip install $alrb_pipOpts -r $alrb_reqFile 2>&1 | tee -a $alrb_logDir/pipinstall.log
if [ $? -ne 0 ]; then
    exit 64
fi

\echo "pip freeze ...
`pip freeze`
"

alrb_tmpAr=( `pip freeze | \sed -e 's|==.*||g'` )

cd $alrb_virtualEnvDir/myVenv
for alrb_item in "${alrb_tmpAr[@]}"; do
    \echo "packing $alrb_item" | tee -a $alrb_logDir/packaging.log
    pip show -f $alrb_item > $alrb_logDir/$alrb_item.list
    alrb_location=`\grep -e "Location: " $alrb_logDir/$alrb_item.list | \sed -e 's|Location:[[:space:]]||g'`
    alrb_oldifs="$IFS"
    IFS=$'\n'
    alrb_fileListAr=( `\sed -e '1,/^Files:/d' $alrb_logDir/$alrb_item.list | \sed -e 's/[ \t]*$//' -e 's/^[ \t]*//'` )
    IFS="$alrb_oldifs"
    for alrb_item2 in "${alrb_fileListAr[@]}"; do
	alrb_realFile=`readlink -f "$alrb_location/$alrb_item2"`
	alrb_relativeFile=`\echo $alrb_realFile | \sed -e 's|'$alrb_virtualEnvDir'/myVenv/||g'`
	alrb_newDir=`\dirname $alrb_relativeFile`
	mkdir -p $alrb_buildDir/$alrb_newDir
	\cp -R "$alrb_realFile" $alrb_buildDir/$alrb_newDir/
    done
done

if [ -d $alrb_buildDir/bin ]; then
    cd $alrb_buildDir/bin
    alrb_tmpAr=( `\grep -I -r -H -e "^#\!.*VirtualEnvDir/myVenv/bin/python*" * | \cut -f 1 -d ":"` )
    for alrb_item in ${alrb_tmpAr[@]}; do
	\sed -e 's|^#!.*VirtualEnvDir/myVenv/bin/python*|#\! /usr/bin/env python'$alrb_pyVersionMajor'|g' "$alrb_item" > "$alrb_item.new" 
	chmod --reference "$alrb_item" "$alrb_item.new"
	\mv "$alrb_item.new" "$alrb_item"
    done
fi

cd $alrb_buildDir

# just in case PWD was use d(if TMPDIR was missing)
\rm -rf $alrb_installDir/deleteMe-$alrb_pkgName

exit 0
