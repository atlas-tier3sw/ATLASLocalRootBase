#!----------------------------------------------------------------------------
#!
#! cleanupFrontier.sh
#!
#! remove IN2P3 and TRIUMF launchpads if defined and generate a warning
#!
#! Usage:
#!     source cleanupFrontier.sh
#!
#! History:
#!    13Apr23: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

if [[ ! -z $ALRB_noFrontierSetup ]] && [[ "$ALRB_noFrontierSetup" = "YES" ]]; then
    return 0
fi
 
\echo $FRONTIER_SERVER | \grep -e "(serverurl=http://frontier-atlas[[:alnum:]]*.lcg.triumf.ca:3128/ATLAS_frontier)" -e "(serverurl=http://ccfrontier[[:alnum:]\.]*.in2p3.fr:23128/ccin2p3-AtlasFrontier)" > /dev/null 2>&1
if [ $? -eq 0 ]; then
    \echo "Warning: FRONTIER_SERVER is defined with obsolete IN2P3 or TRIUMF launchpads."
    \echo "         removing them now but please remove them from your login setup."
    export FRONTIER_SERVER=`\echo $FRONTIER_SERVER | \sed -e 's|(serverurl=http://frontier-atlas[[:alnum:]]*.lcg.triumf.ca:3128/ATLAS_frontier)||g' -e 's|(serverurl=http://ccfrontier[[:alnum:]\.]*.in2p3.fr:23128/ccin2p3-AtlasFrontier)||g'`

    # in case we go overboard and do not have a launchpad anymore ...
    \echo $FRONTIER_SERVER | \grep -e "serverurl=" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)$FRONTIER_SERVER"
    fi

fi

return 0
