#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! fetch-gridCertificates.sh
#!
#! A script to populate the etc/grid-security/certificate dir
#!
#! Usage:
#!     fetch-gridCertificates.sh --help
#!
#! History:
#!   13Apr22: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_progname=fetch-gridCertificates.sh  

#!----------------------------------------------------------------------------
alrb_fn_fetchGridCertHelp()
#!----------------------------------------------------------------------------
{
    \cat <<EOF

Usage: fetch-gridCertificates.sh   [options]

    This application will populate the etc/grid-security/certificate dir

    You need to set the environment variable ATLAS_LOCAL_ROOT_BASE first.

    Options (to override defaults) are:
     -h  --help               Print this help message
     --force                  Override the default wait times:
     			      CA downloads: $alrb_getCATime s
			      CRL generation: $alrb_getCRLTime s 
     -v --verbose             Verbose messages
   
EOF
}


#!----------------------------------------------------------------------------
alrb_fn_cleanup()
#!----------------------------------------------------------------------------
{

    \rm -f $alrb_certLogLastUpdate
\cat <<EOF > $alrb_certLogLastUpdate
let alrb_timeLastCA=$alrb_timeLastCA
let alrb_timeLastCRL=$alrb_timeLastCRL 
EOF

    \rm -f $alrb_securityDirLock
    return 0
}


#!----------------------------------------------------------------------------
alrb_fn_rotateLogs()
#!----------------------------------------------------------------------------
{

    if [ -e $alrb_certLogFile ]; then
	local let alrb_logSize=`du -sk $alrb_certLogFile | \cut -f 1`
	if [ $alrb_logSize -gt 1000 ]; then
	    cd $alrb_certLog
	    local alrb_id
	    local let alrb_nextI=0
	    for alrb_idx in 4 3 2 1; do
		let alrb_nextI=$alrb_idx+1
		if [ -e $alrb_certLogFile.tgz.$alrb_idx ]; then
		    \mv $alrb_certLogFile.tgz.$alrb_idx $alrb_certLogFile.tgz.$alrb_nextI
		fi
	    done
	    tar zcf $alrb_certLogFile.tgz.1 $alrb_certLogFile
	    \rm -f $alrb_certLogFile
	fi
    fi
    if [ ! -e $alrb_certLogFile ]; then
	touch $alrb_certLogFile
    fi

    return 0
}


#!----------------------------------------------------------------------------
alrb_fn_getCA()
#!----------------------------------------------------------------------------
{
    local let alrb_rc=0
    \echo "`date` - start fetchCA run" >> $alrb_certLogFile
	
    mkdir -p $alrb_caDir
    cd $alrb_caDir
    \rm -rf $alrb_caDir/*
    \rm -f $alrb_certLogFile.tmp
    wget -e robots=off -l1 -nd -r --quiet "http://repository.egi.eu/sw/production/cas/1/current/RPMS" >> $alrb_certLogFile.tmp 2>&1
    if [ $? -ne 0 ]; then
	\echo "May have failed to fetch rpms .... ?"
	\cat $alrb_certLogFile.tmp
#	let alrb_rc=64
    fi
    \cat $alrb_certLogFile.tmp >> $alrb_certLogFile
    local alrb_file
    let alrb_nRpms=0
    for alrb_file in *.rpm; do
	let alrb_nRpms+=1
	\rm -f $alrb_certLogFile.tmp
	rpm2cpio $alrb_file | cpio -im --make-directories --quiet >> $alrb_certLogFile.tmp 2>&1
	if [ $? -ne 0 ]; then
	    \echo "Failed to do rpm2cpio ... $alrb_file"
	    \cat $alrb_certLogFile.tmp
	    let alrb_rc=64
	fi	
	\cat $alrb_certLogFile.tmp >> $alrb_certLogFile
    done
    if [ $alrb_nRpms -lt 30 ]; then
	\echo "Number of rpms is less than 30 so flag this as an error ..."
	let alrb_rc=64
    fi
    \rm -rf $X509_CERT_DIR.new
    mkdir -p $X509_CERT_DIR.new
    mv  $alrb_caDir/etc/grid-security/certificates/* $X509_CERT_DIR.new/
    
    \echo "`date` - end fetchCA run" >> $alrb_certLogFile

    return $alrb_rc
}


#!----------------------------------------------------------------------------
alrb_fn_getCRL()
#!----------------------------------------------------------------------------
{
    local let alrb_rc=0

    \echo "`date` - start fetchCRL run" >> $alrb_certLogFile
    
     source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh emi >> $alrb_certLogFile 2>&1
    if [ $? -ne 0 ]; then
	\echo "Error: unable to setup emi" >> $alrb_certLogFile 2>&1
	return 64
    fi

    $EMI_TARBALL_BASE/usr/sbin/fetch-crl -l $X509_CERT_DIR.new -o $X509_CERT_DIR.new >> $alrb_certLogFile 2>&1
    if [ $? -ne 0 ]; then
	let alrb_rc=64
    fi
    
    \echo "`date` - end fetchCRL run" >> $alrb_certLogFile
    
    return $alrb_rc

}


#!----------------------------------------------------------------------------
# main
#!----------------------------------------------------------------------------

alrb_shortopts="h,v" 
alrb_longopts="help,force,verbose"
alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_parseOptions.sh bash $alrb_shortopts $alrb_longopts $alrb_progname "$@"`
if [ $? -ne 0 ]; then
    exit 64
fi
eval set -- "$alrb_opts"

let alrb_rc=0
alrb_force="NO"
alrb_verbose="NO"
let alrb_getCATime=24*3600  # 1 day
let alrb_getCRLTime=6*3600  # 6 hr

alrb_securityDir=$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi
alrb_certDir=$alrb_securityDir/certificates
alrb_certLog=$ATLAS_LOCAL_ROOT_BASE/logDir
alrb_certLogFile=$alrb_certLog/fetch-gridCertificates.log
alrb_certLogLastUpdate=$alrb_certLog/gridCertificatesLastUpdate
alrb_securityDirLock=$alrb_securityDir/gridCertificateslockfile
alrb_caDir=$alrb_securityDir/ca
X509_CERT_DIR=$alrb_securityDir/certificates
mkdir -p $X509_CERT_DIR

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
            alrb_fn_fetchGridCertHelp
            exit 0
            ;;
        --force)
	    alrb_force="YES"
	    alrb_getCATime=0
	    alrb_getCRLTime=0
	    shift
            ;;
        -v|--verbose)
	    alrb_verbose="YES"
	    shift
            ;;
        --)
            shift
            break
            ;;
        *)
            \echo "Internal Error: option processing error: $1" 1>&2
            exit 1
            ;;
    esac
done

if [ -z $ATLAS_LOCAL_ROOT_BASE ]
then
    \echo "Error: ATLAS_LOCAL_ROOT_BASE not set"
    exit 64
fi

mkdir -p $alrb_certDir
mkdir -p $alrb_certLog

$ATLAS_LOCAL_ROOT_BASE/utilities/createLockFile.sh -f $alrb_securityDirLock -s 30 -r 10 -d 600 -v 

let alrb_timeLastCA=0
let alrb_timeLastCRL=0
let alrb_timeNow=`date +%s`
if [ -e $alrb_certLogLastUpdate ]; then
    source $alrb_certLogLastUpdate
fi
let alrb_timeDiffCA=$alrb_timeNow-$alrb_timeLastCA
let alrb_timeDiffCRL=$alrb_timeNow-$alrb_timeLastCRL

alrb_fn_rotateLogs

if [ $alrb_timeDiffCA -gt $alrb_getCATime ]; then
    alrb_fn_getCA
    alrb_rc=$?
    if [ $alrb_rc -eq 0 ]; then
	let alrb_timeLastCA=$alrb_timeNow
    else
	alrb_fn_cleanup
	\echo "Failed fetchCA, so exiting now."
	exit 64
    fi
fi

if [ $alrb_timeDiffCRL -gt $alrb_getCRLTime ]; then
    \find $alrb_certDir -mtime +7 -type f -name "*.crl_url" | \awk '{print "\\rm "$1""}' | sh
    alrb_fn_getCRL
    alrb_rc=$?
    if [ $alrb_rc -eq 0 ]; then
        let alrb_timeLastCRL=$alrb_timeNow
    else
	\echo "Failed fetchCRL - look at the logs"
    fi    
fi


alrb_fileListing="$alrb_securityDir/certificates-list.txt"
alrb_replace="NO"
cd $X509_CERT_DIR.new
\rm -f $alrb_fileListing.new
\find . -name "*.r0" | while read -r alrb_file; do \echo -n $alrb_file; openssl crl -text -in $alrb_file | \grep -e "Next Update" -e "Issuer" | \tr -d '\n'; \echo " "; done > $alrb_fileListing.new.tmp
sort -k 2 -u $alrb_fileListing.new.tmp > $alrb_fileListing.new
\rm -f $alrb_fileListing.new.tmp

if [ -e $alrb_fileListing ]; then
 
    # look at the second field only, not file names
    \rm -f $alrb_fileListing.f2
    \rm -f $alrb_fileListing.new.f2
    \sed -e 's/.*Issuer: \(.*\) Next .*/\1/g' $alrb_fileListing | sort -u > $alrb_fileListing.f2
#    \cut -f 2- -d " " $alrb_fileListing | sort -u > $alrb_fileListing.f2
    \sed -e 's/.*Issuer: \(.*\) Next .*/\1/g' $alrb_fileListing.new | sort -u > $alrb_fileListing.new.f2
#    \cut -f 2- -d " " $alrb_fileListing.new | sort -u > $alrb_fileListing.new.f2

    let alrb_lengthOld=`wc -l $alrb_fileListing.f2 | \cut -f 1 -d ' '`
    let alrb_lengthNew=`wc -l $alrb_fileListing.new.f2 | \cut -f 1 -d ' '`
    let alrb_lengthDiff=$alrb_lengthOld-$alrb_lengthNew
    let alrb_lengthDiffAbs=`\echo $alrb_lengthDiff | \sed -e 's|-||'`

    if [ $alrb_lengthDiffAbs -gt 5 ]; then
	\echo "Error : more than 5 differences seen.  Will not replace but write inside existing dirs"
    else
	alrb_replace="YES"
    fi

    if [ $alrb_lengthDiffAbs -ne 0 ]; then
	\echo " * * * Start Differences $alrb_lengthDiff * * *
"
#    \rm -f $alrb_fileListing.sort
#    sort -k 2 -u $alrb_fileListing > $alrb_fileListing.sort
#    mv $alrb_fileListing.sort $alrb_fileListing
	diff -w $alrb_fileListing.f2  $alrb_fileListing.new.f2
	\echo "
 * * * End Differences * * *"
    fi
    
else
    alrb_replace="YES"
fi

if [ "$alrb_replace" = "NO" ]; then
    \cp -p -r $X509_CERT_DIR.new/* $X509_CERT_DIR/
    cd $X509_CERT_DIR
    \find . -name "*.r0" | while read -r alrb_file; do \echo -n $alrb_file; openssl crl -text -in $alrb_file | \grep -e "Next Update" -e "Issuer" | \tr -d '\n'; \echo " "; done > $alrb_fileListing.tmp
    sort -u $alrb_fileListing.tmp > $alrb_fileListing
    \rm -f $alrb_fileListing.tmp
else
    \rm -rf $X509_CERT_DIR
    \mkdir -p $X509_CERT_DIR
    \cp -p -r $X509_CERT_DIR.new/* $X509_CERT_DIR/
    \cp -p $alrb_fileListing.new $alrb_fileListing
fi


alrb_fn_cleanup

exit 0
