#! /bin/bash 
#!----------------------------------------------------------------------------
#!
#! a cript to get the username
#!
#! Usage:
#!    getUsername.sh
#!
#! History:
#!   04jan20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_result=`whoami 2>&1`
if [ $? -eq 0 ]; then
    \echo $alrb_result
    exit 0
fi

if [ ! -z $USER ]; then
    \echo $USER
    exit 0
fi

alrb_result=`id -u 2>&1`
if [ $? -eq 0 ]; then
    \echo "uid$alrb_result"
    exit 0
fi

\echo "atlasuser"
exit 0
