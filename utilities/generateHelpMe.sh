#! /bin/bash

\echo -e " 


  First see: 
    \033[1;34mhttps://twiki.atlas-canada.ca/bin/view/AtlasCanada/ATLASLocalRootBase2\033[0m
  for ATLASLocalRootBase (the User Interface you are interacting with now !)


  If you are running, or want to run, containers:
  Documentation:
    \033[1;34mhttps://twiki.atlas-canada.ca/bin/view/AtlasCanada/Containers\033[0m


\033[1;36mOther links below:\033[0m
  Some of the help / references below refer to sourcing /afs/cern scripts; 
  there is no need to do this because all software is available locally and 
  can be accessed by the menu items after setupATLAS.


\033[1;36mmanageTier3SW\033[0m
  For Tier3 admins (or if you want to install on your personal laptop/desktop)
    \033[1;34mhttps://twiki.atlas-canada.ca/bin/view/AtlasCanada/ManageTier3SW\033[0m
  Help (eGroup):
    \033[1;34mhttps://groups.cern.ch/group/atlas-adc-tier3-managers/default.aspx\033[0m
       (mailing list):
    \033[1;34matlas-adc-tier3-managers@cern.ch\033[0m   
       (subscribe):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-adc-tier3-managers\033[0m 


\033[1;36mATLAS Software Documentation\033[0m
  A good place for getting started:
    \033[1;34mhttps://atlassoftwaredocs.web.cern.ch\033[0m


\033[1;36mTutorials\033[0m
  You can join these at CERN or do it on your own at your own pace !
    \033[1;34mhttps://atlassoftwaredocs.web.cern.ch/ASWTutorial/\033[0m
  setMeUp to run tutorials anywhere:
    \033[1;34mhttps://twiki.atlas-canada.ca/bin/view/AtlasCanada/SetMeUp\033[0m


\033[1;36macm\033[0m
  Information:
    \033[1;34mhttps://gitlab.cern.ch/atlas-sit/acm/blob/master/README.md\033[0m
  Help (eGroup):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/Egroup.do?egroupName=atlas-sw-acm-users\033[0m
       (mailing list):
    \033[1;34matlas-sw-acm-users@cern.ch\033[0m
       (subscribe):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-sw-acm-users\033[0m


\033[1;36mart\033[0m
  Information:
    \033[1;34mhttps://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/ART\033[0m


\033[1;36mastyle\033[0m 
  Documentation:
    \033[1;34mhttps://gitlab.cern.ch/atlas-publications-committee/atlasrootstyle\033[0m


\033[1;36mAtlantis\033[0m 
  Reference:
    \033[1;34mhttps://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/Atlantis\033[0m
    \033[1;34mhttp://atlantis.web.cern.ch\033[0m 
  Help (eGroup - see forum description in links for usage):
    \033[1;34mhttps://groups.cern.ch/group/hn-atlas-AtlantisDisplay/default.aspx\033[0m
       (mailing list):
    \033[1;34mhn-atlas-AtlantisDisplay@cern.ch\033[0m
       (subscribe):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-AtlantisDisplay\033[0m


\033[1;36mAtlasSetup\033[0m
  asetup and what it is / does:
    \033[1;34mhttps://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/AtlasSetup\033[0m
  Reference:
    \033[1;34mhttps://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/AtlasSetupReference\033[0m
  Help (eGroup - see forum description in links for usage):
    \033[1;34mhttps://groups.cern.ch/group/hn-atlas-offlineSWHelp/default.aspx\033[0m
       (mailing list):
    \033[1;34mhn-atlas-offlineSWHelp@cern.ch\033[0m 
       (subscribe):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-offlineSWHelp\033[0m 


\033[1;36mcentralpage\033[0m
  Information:
    \033[1;34mhttps://gitlab.cern.ch/atlas-physics/pmg/infrastructure/central-page/-/blob/master/new-cp/README.md#setup\033[0m
    \033[1;34mhttps://indico.cern.ch/event/1415236/contributions/5949967/attachments/2854195/4991923/2024-05-13_PC_CentralPage_mcfayden.pdf\033[0m 


\033[1;36mcppcheck\033[0m
  Information:
    \033[1;34mhttp://cppcheck.sourceforge.net\033[0m


\033[1;36memi\033[0m
  This is the new grid middleware.  You should not have to set this up.


\033[1;36mEIClients\033[0m
  Information and Tutorial
    \033[1;34mhttps://twiki.cern.ch/twiki/bin/view/AtlasComputing/EventIndex#Command_line_Services\033[0m
    \033[1;34mhttps://twiki.cern.ch/twiki/bin/view/AtlasComputing/EventIndexPicking\033[0m


\033[1;36mlcgenv\033[0m
   \033[1;34mhttps://twiki.atlas-canada.ca/bin/view/AtlasCanada/Lcgenv\033[0m 
    See also views.


\033[1;36mPandaClient\033[0m
  Information (see link to Documentation on individual tool)
    \033[1;34mhttps://panda-wms.readthedocs.io/en/latest/client/client.html\033[0m
  Monitor:
    \033[1;34mhttp://bigpanda.cern.ch\033[0m
  Help (eGroup - see forum description in links for usage):
    \033[1;34mhttps://groups.cern.ch/group/hn-atlas-dist-analysis-help/default.aspx\033[0m
       (mailing list):
    \033[1;34mhn-atlas-dist-analysis-help@cern.ch\033[0m   
       (subscribe):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-dist-analysis-help\033[0m 


\033[1;36mpyAMI\033[0m
  Note: 
      "'$HOME'"/.pyami/pyami.cfg is the location for username and password.
      If you want to use username/password instead of voms proxy, then add
        --ignore-proxy to the ami command line.
  Getting Started:
    \033[1;34m https://atlas-ami.cern.ch/?subapp=document&userdata=getting-started\033[0m 
  Help (eGroup - see forum description in links for usage):
    \033[1;34mhttps://groups.cern.ch/group/atlas-bookkeeping/default.aspx\033[0m
       (mailing list):
    \033[1;34matlas-bookkeeping@cern.ch\033[0m   
       (subscribe):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-bookkeeping\033[0m 


\033[1;36mrucio\033[0m
  HowTo:	
    \033[1;34mhttps://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/RucioClientsHowTo\033[0m
  WebUI:
    \033[1;34mhttps://rucio-ui.cern.ch\033[0m
  Help (eGroup - see forum description in links for usage):
    \033[1;34mhttps://groups.cern.ch/group/hn-atlas-dist-analysis-help/default.aspx\033[0m
       (mailing list):
    \033[1;34mhn-atlas-dist-analysis-help@cern.ch\033[0m   
       (subscribe):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-dist-analysis-help\033[0m 

 
\033[1;36mscikit\033[0m
  scikit-hep is an ecosystem for data analysis in Python embracing all major
   topics involved in a physicist's work.
  \033[1;34mhttps://scikit-hep.org\033[0m 
  \033[1;34mhttps://github.com/scikit-hep/scikit-hep\033[0m 


\033[1;36mviews\033[0m
   Set up an entire LCG release.
   Self explanatory usage : start with lsetup \"views\" to see available LCG 
    releases and then platforms.
      eg. lsetup \"views LCG_95 x86_64-centos7-gcc8-opt\"


\033[1;36mxcache\033[0m 
  Documentation:
    \033[1;34mhttps://twiki.atlas-canada.ca/bin/view/AtlasCanada/Xcache\033[0m


\033[1;36mAdditional Resources\033[0m
  DAST has useful information for users as well as experts:	
    \033[1;34mhttps://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/AtlasDAST\033[0m
       (mailing list):
    \033[1;34mhn-atlas-dist-analysis-help@cern.ch\033[0m   
       (subscribe):
    \033[1;34mhttps://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-dist-analysis-help\033[0m 


\033[1;36mNote:\033[0m
Contacts for issues or questions:

DAST (\033[1;34mhn-atlas-dist-analysis-help@cern.ch\033[0m) for distributed computing help for any ATLAS related issue if you are unsure where to ask. 

PAT (\033[1;34mhn-atlas-PATHelp@cern.ch\033[0m) for Physics Tools.

Offline (\033[1;34mhn-atlas-offlineSWHelp@cern.ch\033[0m) for offline software

"


