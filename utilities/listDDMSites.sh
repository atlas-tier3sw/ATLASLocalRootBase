#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! listDDMSites.sh
#!
#! simple list of ddm endpoints
#!
#! Usage: 
#!     listDDMSites.sh
#!
#! History:
#!   09Dec20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# -*- coding: utf-8 -*- 
"exec" "$( [ ! -z $EMI_PYTHONBIN ] && \echo $EMI_PYTHONBIN || \echo /usr/bin/python)" "-u" "-Wignore" "$0" "$@"

from __future__ import print_function 

import sys, os
import json

try:  
  jsonfileDir = os.environ['ALRB_tmpScratch'] + '/cric/'  
except: 
  sys.exit(64)

command= os.environ['ATLAS_LOCAL_ROOT_BASE'] + '/utilities/getCricData.sh -q ddmendpoint ' + jsonfileDir  
try:
  return_value = os.system(command)  
  if return_value != 0:
     sys.exit(64)
except: 
  sys.exit(64)

jsonfileLocation = jsonfileDir + 'ddmendpoint.json'
  
with open(jsonfileLocation,'r') as jsonfile:
  jsondata = json.load(jsonfile)
jsonfile.close()
  
for name in jsondata.keys():  
  print(jsondata[name]['name'],':',jsondata[name]['site'],':',jsondata[name]['tier_level'],':',jsondata[name]['state'])

sys.exit(0)
