#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! fetchCA-emi.sh
#!
#! A script to fetch the latest grid CA
#!
#! Usage:
#!     fetchCA-emi.sh --help
#!
#! History:
#!   10Jul09: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# fetch-crl does this in the new script.

exit 0

alrb_cont="centos`$ATLAS_LOCAL_ROOT_BASE/utilities/getOSType.sh | \cut -f 2 -d ' '`"
alrb_tmpScript="./myfetchCA.sh"
\rm -f $alrb_tmpScript
\echo "#! /bin/bash" > $alrb_tmpScript
chmod +x $alrb_tmpScript
\cat <<EOF >> $alrb_tmpScript
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
${ATLAS_LOCAL_ROOT_BASE}/utilities/fetch-gridCertificates.sh $@
exit \$?
EOF

set -- 

\echo $ATLAS_LOCAL_ROOT_BASE | \grep -e "^/cvmfs" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    alrb_contOpt="-m $ATLAS_LOCAL_ROOT_BASE"
else
    alrb_contOpt=""
fi

source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $alrb_cont --nohome --noalrb $alrb_contOpt -r $alrb_tmpScript -q
alrb_rc=$?

\rm -f $alrb_tmpScript

exit $alrb_rc
