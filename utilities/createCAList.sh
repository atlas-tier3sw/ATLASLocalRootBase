#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! createCAList.sh
#!
#! dump the CA information into a file for easy lookup
#!
#! Usage:
#!     createCAList.sh
#!
#! History:
#!   08Jul21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

if [ -z $ATLAS_LOCAL_ROOT_BASE ]
then
    \echo "Error: ATLAS_LOCAL_ROOT_BASE not set"
    exit 64
fi

alrb_fileListing="$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates-list.txt"
cd $ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates

\rm -f $alrb_fileListing.new
\find . -name "*.r0" | while read -r alrb_file; do \echo -n $alrb_file; openssl crl -text -in $alrb_file | \grep -e "Next Update" -e "Issuer" | \tr -d '\n'; \echo " "; done > $alrb_fileListing.new

if [ $? -eq 0 ]; then
    \mv $alrb_fileListing.new $alrb_fileListing
fi

exit 0
