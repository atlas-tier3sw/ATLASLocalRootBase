The files in this dir are meant to be bind mounted into the el9 containers.
They should be arch independent.

The files were obtainer by running installRpm (after setupATLAS) on an
el9 container and manually copying them to the gitlab repo dir.  It is
expected that this is a rare occurence to maintain this.
  installRpm ~/myOpenSSHPatch crypto-policies\*,openssh-clients

The files (to be bind mounted on the defaulyt el9 containers are):

1.  For ssh to work (ssh to remote machine or for git clone etc)
/etc/ssh/ssh_config.d/50-redhat.conf
/etc/crypto-policies/back-ends/openssh.config
  (dereferenced sym link /usr/share/crypto-policies/DEFAULT/openssh.txt)


