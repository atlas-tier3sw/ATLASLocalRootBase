#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup clang for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   04Dec20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_CLANG_VERSION=$1

alrb_tmpVal=`\find $ATLAS_LOCAL_ROOT/Clang/$ATLAS_LOCAL_CLANG_VERSION -maxdepth 3 -type f -name setup.sh`
source $alrb_tmpVal

unset alrb_tmpVal
