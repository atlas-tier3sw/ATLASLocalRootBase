#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup scikit for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   20Oct21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_SCIKIT_VERSION=$1
alrb_scikitUsePy3="$2"

if [ "$alrb_scikitUsePy3" = "dynamic" ]; then
    alrb_tmpVal="`python3 -V 2>&1`"
    if [ $? -eq 0 ]; then
	alrb_scikitUsePy3=`\echo $alrb_tmpVal | \cut -f 2 -d " " | \cut -f -2 -d "."`
    else
	\echo "Erorr: python3 (dynamic) does not seem to be available for scikit"
	return 64
    fi
    unset alrb_tmpVal
fi

source $ATLAS_LOCAL_ROOT/scikit/$ATLAS_LOCAL_SCIKIT_VERSION/setup.sh $alrb_scikitUsePy3

unset alrb_scikitUsePy3

