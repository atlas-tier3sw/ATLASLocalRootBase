#!----------------------------------------------------------------------------
#!
#!  functions.sh
#!
#!    functions for scikit
#!
#!  History:
#!   25Oct21: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------


alrb_fn_scikitHelp()
{
    \cat <<EOF

Usage: lsetup [global options] "${alrb_sw} [options] <version>"
  
    This sets up the ATLAS environment for scikit

    You need to set the environment variable ATLAS_LOCAL_ROOT_BASE first.    

    Options (to override defaults) are:
     -p  --pyVer            Specify python version if not default
EOF
    alrb_fn_glocalSetupHelp
    ${ATLAS_LOCAL_ROOT_BASE}/swConfig/showVersions.sh $alrb_sw

}


alrb_fn_scikitVersionConvert()
{
    local alrb_tmpVal=`\echo $1 | \cut -f 1 -d "-" | \sed -e 's/\([0-9\.]*\).*/\1/g'`    
    $ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpVal 3
    return 0
}


alrb_fn_scikitDepend()
{
    local alrb_sw="scikit"
    local alrb_progname="alrb_fn_${alrb_sw}Depend"
    
    local alrb_swDir
    alrb_swDir=`alrb_fn_getSynonymInfo "$alrb_sw" dir`
    if [ $? -ne 0 ]; then
	return 64
    fi

    local alrb_shortopts="h,q,s,f,c:,p:"
    local alrb_longopts="help,${alrb_sw}Version:,quiet,skipConfirm,force,pyVer:"
    local alrb_opts
    local alrb_result
    alrb_result=`getopt -T >/dev/null 2>&1`
    if [ $? -eq 4 ] ; then # New longopts getopt.
	alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_progname" -- "$@")
	local alrb_returnVal=$?
    else # use wrapper
	alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt.sh $alrb_shortopts $alrb_longopts $*`
	local alrb_returnVal=$?
    fi
    
# do we have an error here ?
    if [ $alrb_returnVal -ne 0 ]; then
	\echo $alrb_opts 1>&2
	\echo "'lsetup \"$alrb_sw --help\"' for more information" 1>&2
	return 1
    fi
    
    eval set -- "$alrb_opts"
    
    local alrb_caller="unknown"
    if [ ! -z $ALRB_scikitVersion ]; then
	local alrb_swVersion=$ALRB_scikitVersion
    else
	local alrb_swVersion="dynamic"
    fi

    local alrb_needPython=""
    
    while [ $# -gt 0 ]; do
	: debug: $1
	case $1 in
            -h|--help)
		alrb_fn_scikitHelp
		return 0
		;;
	    -p|--pyVer)
		alrb_needPython="$2"
		shift 2
		;;
	    --${alrb_sw}Version)
                local alrb_swVersion=$2
		shift 2
		;;
            -c|--caller)
		local alrb_caller=$2
		shift 2
		;;
	    -q|--quiet)	    
# backward compatibility
	        alrb_Quiet="YES"
		shift
		;;
	    -s|--skipConfirm)	    
		shift
		;;
	    -f|--force)    
		shift
		;;
            --)
		shift
		break
		;;
            *)
		\echo "Internal Error: option processing error: $1" 1>&2
		return 1
		;;
	esac
    done
    
    if [ $# -ge 1 ]; then
	local alrb_swVersion=$1
	shift
	alrb_fn_unrecognizedExtraArgs "$@"
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi
    
    if [ "$alrb_swVersion" = "" ]; then
	\echo "Error: ${alrb_sw} version not specified" 1>&2
	return 64
    fi

    local alrb_candRealVersion=""
    local alrb_candVirtVersion=""
    alrb_fn_getToolVersion "$alrb_sw" "$alrb_swVersion" ""
    if [ $? -ne 0 ]; then
	return 64
    fi
    local alrb_setVer="$alrb_candVirtVersion"

    if [ "$alrb_setVer" != "" ]; then
	alrb_fn_versionChecker "$alrb_sw" "$alrb_caller" "$alrb_setVer" -c "$alrb_swVersion"
	if [ $? -ne 0 ]; then
	    return 64
	else

# dependencies
	    alrb_fn_doOverrides "$alrb_sw" "$alrb_setVer" "$alrb_sw"
	    if [ $? -ne 0 ]; then
		return 64
	    fi

	    local alrb_tmpVal=""
	    if [ "$alrb_needPython" = "" ]; then
		# requires python 3.9 or newer by default
		local let alrb_tmpValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/getCurrentEnvVal.sh $alrb_preSetupEnv python3:ver --decimal=2`
		if [[ $? -ne 0 ]] || [[ $alrb_tmpValN -lt 309 ]]; then
		    if [ -z $ALRB_pythonVersion ]; then
			alrb_tmpVal="centos$ALRB_OSMAJORVER-3.9"
			alrb_scikit_extra="3.9"
		    else
			alrb_tmpVal="$ALRB_pythonVersion"
			alrb_scikit_extra="dynamic"
		    fi
		fi
	    else
		local alrb_InstallDir
		alrb_InstallDir=`alrb_fn_getInstallDir "$alrb_tool"`
		if [ $? -ne 0 ]; then
		    return 64
		fi
		alrb_tmpVal="`\grep -e \"^$alrb_needPython|\" $alrb_InstallDir/$alrb_setVer/pythonBuild.txt 2>&1`"
		if [ $? -eq 0 ]; then
		    alrb_tmpVal="`\echo $alrb_tmpVal | \cut -f 2 -d '|'`"
		else
		    \echo "Error: $alrb_needPython unavailable for scikit.  Valid::"
		    \cut -f 1 -d "|"  $alrb_InstallDir/$alrb_setVer/pythonBuild.txt
		    return 64
		fi
		alrb_scikit_extra="$alrb_needPython"
	    fi

	    if [ "$alrb_tmpVal" != "" ]; then
		alrb_fn_depend python "$alrb_tmpVal" -c "$alrb_sw"
		if [ $? -ne 0 ]; then
		    return 64
		fi	    
	    fi
	    
	    return 0
	fi
    else
	\echo "Error: scikit version undetermined or unavailable $alrb_swVersion" 1>&2
	return 64
    fi
}


alrb_fn_scikitGetInstallDirAttributes()
{
    \echo "setup.sh"

    return 0
}
