#! /bin/sh
#!----------------------------------------------------------------------------
#!
#! listAnalyPQ.sh
#!
#! simple list of available analysis queues
#! ( have reservations about doing this if it encourages brokerage by-passing!)
#!
#! Usage: 
#!     listAnalyPQ.sh
#!
#! History:
#!   28Jan20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# -*- coding: utf-8 -*-

"exec" "$(  check_interpreter() { unalias $1 2> /dev/null; unset $1; ALRB_envPython=$(command -v $1); [ $ALRB_envPython ] && $ALRB_envPython -c 'import sys' > /dev/null 2>&1 && { echo $ALRB_envPython; unset ALRB_envPython; }; }; [ $ALRB_envPython ] && echo $ALRB_envPython || check_interpreter python || check_interpreter python3 || check_interpreter python2 || echo /usr/bin/python )" "-u" "-Wignore" "$0" "$@"

from __future__ import print_function 

import sys, os
import json

try:
  jsonfileLocation = os.environ['ALRB_cvmfs_repo'] + '/sw/local/etc/cric_pandaqueues.json'  
except: 
  jsonfileLocation = '/cvmfs/atlas.cern.ch/repo/sw/local/etc/cric_pandaqueues.json'
  
with open(jsonfileLocation,'r') as jsonfile:
  jsondata = json.load(jsonfile)
jsonfile.close()

try:
  jsonfileLocationPQTag = os.environ['ALRB_cvmfs_repo'] + '/sw/local/etc/cric_pandaqueue_tags.json'
except:
  jsonfileLocationPQTag = '/cvmfs/atlas.cern.ch/repo/sw/local/etc/cric_pandaqueue_tags.json'

with open(jsonfileLocationPQTag,'r') as jsonfilePQTag:
  jsondataPQTag = json.load(jsonfilePQTag)
jsonfilePQTag.close()

for site in jsondata.keys():
  if (jsondata[site]['site_state'] == "ACTIVE" ) and (jsondata[site]['type'] == "analysis" or jsondata[site]['type'] == "unified") and (jsondata[site]['status'] == 'online' or jsondata[site]['status'] == 'brokeroff'):
     pandaResource=jsondata[site]['panda_resource']
     print(pandaResource,':',jsondata[site]['status'],':',json.dumps(jsondataPQTag[pandaResource]['architectures']))

sys.exit(0)
