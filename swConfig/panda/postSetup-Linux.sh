#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    25Nov2022: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ "$alrb_Quiet" = "NO" ]; then
    if [ "$ALRB_tokensPanda" = "YES" ]; then
	\echo " panda:"
	\echo "   Using tokens instead of X509 for authentication"
    fi
    if [ `uname -m` = "aarch64" ]; then
	\echo " panda:"
	\echo "   aarch64 resources are limited on the grid.  Type"
	\echo "     listAnalyPQ | grep aarch64"
	\echo "   to see list of online and brokeroff queues" 
    fi
fi

if [[ ! -z $ALRB_envPython ]] && [[ ! -z $PANDA_PYTHONPATH ]]; then
    alrb_pandaPython=`$ALRB_envPython -V 2>&1 | \cut -f 2 -d " "`
    alrb_pandaPythonMajor=`\echo $alrb_pandaPython | \cut -f 1 -d "."`
    alrb_pandaPythonMinor=`\echo $alrb_pandaPython | \cut -f 2 -d "."`
    alrb_pandaPythonLib=`\find $ATLAS_LOCAL_PANDACLIENT_PATH/lib -mindepth 2 -maxdepth 2 -name site-packages | \grep -e python$alrb_pandaPythonMajor.$alrb_pandaPythonMinor`
    if [ "$alrb_pandaPythonLib" = "" ]; then
	alrb_pandaPythonLib=`\find $ATLAS_LOCAL_PANDACLIENT_PATH/lib -mindepth 2 -maxdepth 2 -name site-packages | \grep -e python$alrb_pandaPythonMajor. | \tail -n 1`
	if [ "$alrb_pandaPythonLib" = "" ]; then
	    \echo "Error: unable to determine panda python lib path to fix"
	fi	
    fi
    if [ "$alrb_pandaPythonLib" != "" ]; then
	export PYTHONPATH=`\echo $PYTHONPATH | \sed -e 's|'$PANDA_PYTHONPATH'|'$alrb_pandaPythonLib'|g'`
	export PANDA_PYTHONPATH="$alrb_pandaPythonLib"
    fi
    unset alrb_pandaPythonMajor alrb_pandaPythonMinor alrb_pandaPythonLib alrb_pandaPython
fi

