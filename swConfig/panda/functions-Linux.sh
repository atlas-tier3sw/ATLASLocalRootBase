#!----------------------------------------------------------------------------
#!
#!  functions.sh
#!
#!    functions for PandaClient
#!
#!  History:
#!    19Mar2015: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------


alrb_fn_pandaHelp()
{
    \cat <<EOF

Usage: lsetup [global options] "${alrb_sw} [options] <version>"
  
    This sets up the ATLAS environment for PandaClient

    You need to set the environment variable ATLAS_LOCAL_ROOT_BASE first.    

    Options (to override defaults) are:
EOF
    alrb_fn_glocalSetupHelp
    ${ATLAS_LOCAL_ROOT_BASE}/swConfig/showVersions.sh $alrb_sw

}


alrb_fn_pandaVersionConvert()
{
    # 3 significant figures 
    local alrb_tmpVal=`\echo $1 | \cut -f 1 -d "-" | \sed -e 's/\([0-9\.]*\).*/\1/g'`    
    $ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpVal 3
    return $?
}


alrb_fn_pandaDepend()
{
    local alrb_sw="panda"
    local alrb_progname="alrb_fn_${alrb_sw}Depend"
    
    local alrb_swDir
    alrb_swDir=`alrb_fn_getSynonymInfo "$alrb_sw" dir`
    if [ $? -ne 0 ]; then
	return 64
    fi

    local alrb_shortopts="h,q,s,f,c:"
    local alrb_longopts="help,${alrb_sw}Version:,quiet,skipConfirm,noAthenaCheck,force,pandaClientVersion:"
    local alrb_opts
    local alrb_result
    alrb_result=`getopt -T >/dev/null 2>&1`
    if [ $? -eq 4 ] ; then # New longopts getopt.
	alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_progname" -- "$@")
	local alrb_returnVal=$?
    else # use wrapper
	alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt.sh $alrb_shortopts $alrb_longopts $*`
	local alrb_returnVal=$?
    fi
    
# do we have an error here ?
    if [ $alrb_returnVal -ne 0 ]; then
	\echo $alrb_opts 1>&2
	\echo "'lsetup \"$alrb_sw --help\"' for more information" 1>&2
	return 1
    fi
    
    eval set -- "$alrb_opts"
    
    local alrb_caller="unknown"
    if [ ! -z $ALRB_pandaVersion ]; then
	local alrb_swVersion=$ALRB_pandaVersion
    else
	local alrb_swVersion="dynamic"
    fi
    
    while [ $# -gt 0 ]; do
	: debug: $1
	case $1 in
            -h|--help)
		alrb_fn_pandaHelp
		return 0
		;;
            --${alrb_sw}Version|--pandaClientVersion)
                local alrb_swVersion=$2
		shift 2
		;;
            -c|--caller)
		local alrb_caller=$2
		shift 2
		;;
	    -q|--quiet)	    
# backward compatibility
	        alrb_Quiet="YES"
		shift
		;;
	    -s|--skipConfirm)	    
		shift
		;;
	    -f|--force)    
		shift
		;;
	    --noAthenaCheck)
		\echo "Warning: --noAthenaCheck is obsolete." 1>&2
# obsolete	    
		shift
		;;
            --)
		shift
		break
		;;
            *)
		\echo "Internal Error: option processing error: $1" 1>&2
		return 1
		;;
	esac
    done
    
    if [ $# -ge 1 ]; then
	local alrb_swVersion=$1
	shift
	alrb_fn_unrecognizedExtraArgs "$@"
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi
    
    if [ "$alrb_swVersion" = "" ]; then
	\echo "Error: ${alrb_sw} version not specified" 1>&2
	return 64
    fi

# JEDI migration is complete
    if [ "$alrb_swVersion" = "currentJedi" ]; then
	\echo "Warning: ${alrb_sw} version currentJedi is obsolete."
	\echo "         Drop it and use the default."
	alrb_swVersion="dynamic"
    fi    
    
    local alrb_candRealVersion=""
    local alrb_candVirtVersion=""
    alrb_fn_getToolVersion "$alrb_sw" "$alrb_swVersion" ""
    if [ $? -ne 0 ]; then
	return 64
    fi
    local alrb_setVer="$alrb_candVirtVersion"
        
    if [ "$alrb_setVer" != "" ]; then
	alrb_fn_versionChecker "$alrb_sw" "$alrb_caller" "$alrb_setVer" -c "$alrb_swVersion"
	if [ $? -ne 0 ]; then
	    return 64
	else

# dependencies
	    alrb_fn_doOverrides "$alrb_sw" "$alrb_setVer" "$alrb_sw"
	    if [ $? -ne 0 ]; then
		return 64
	    fi

	    local let alrb_tmpValN
	    if [[ "$ALRB_OSMAJORVER" = "6" ]] && [[ "$ALRB_tokensPanda" = "YES" ]] ; then
		let alrb_tmpValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/getCurrentEnvVal.sh $alrb_preSetupEnv python:ver --decimal=2`
		if [[ $? -ne 0 ]] || [[ $alrb_tmpValN -lt 207 ]]; then
		    alrb_fn_depend python "recommended-SL6" -c "$alrb_sw"
		    if [ $? -ne 0 ]; then
			return 64
		    fi
		fi		
	    elif [ "$PANDA_PYTHON_EXEC" = "python3" ]; then
		let alrb_tmpValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/getCurrentEnvVal.sh $alrb_preSetupEnv $PANDA_PYTHON_EXEC:ver --decimal=2`
		if [[ $? -ne 0 ]] || [[ $alrb_tmpValN -lt 309 ]]; then
		    if [ -z $ALRB_pythonVersion ]; then
			alrb_tmpVal="recommended"
		    else
			alrb_tmpVal="$ALRB_pythonVersion"
		    fi
		    alrb_fn_depend python "$alrb_tmpVal" -c "$alrb_sw"
		    if [ $? -ne 0 ]; then
			return 64
		    fi
		fi
	    fi
	    
	    return 0
	fi
    else
	\echo "Error: PandaClient version undetermined or unavailable $alrb_swVersion" 1>&2
	return 64
    fi
}

alrb_fn_pandaPostInstall()
{
    cd $alrb_InstallDir

    local alrb_retCode=0

    # additional python3 versions for each OS (3.9 is done)
    # format: OS version needed, python version needed
    alrb_tmpPyVarAr=( "9:centos9-3.11" )
    
    mkdir $alrb_InstallDir/tmp
    \mv * $alrb_InstallDir/tmp
    cd $alrb_InstallDir/tmp

    # we will not have python2 for almalinux9 so it is not an error
    which python2 > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	python2 setup.py install --prefix=$alrb_InstallDir
	alrb_retCode=$?
    else
	\echo "no python2 found.  Skipping python2 setups ..."
    fi
    if [ $alrb_retCode -eq 0 ]; then
	local alrb_tmpVal
	alrb_tmpVal=`which python3 > /dev/null 2>&1`
	alrb_thisPy3=""
	if [ $? -eq 0 ]; then
	    alrb_thisPy3="`\echo $alrb_tmpVal | \cut -f 2 -d ' ' | \cut -f -2 -d '.'`"
	fi       	
# python 3.9 is minimal required version
	if [ "$alrb_thisPy3" != "3.9" ]; then
	    (
		source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
		source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh -q "python $ALRB_OSFLAVOR$ALRB_OSMAJORVER-3.9"
		python3 setup.py install --prefix=$alrb_InstallDir
		return $?
	    )
	else
	    python3 setup.py install --prefix=$alrb_InstallDir
	fi
	alrb_retCode=$?

	if [ $alrb_retCode -eq 0 ]; then
	    for alrb_tmpPyVar in ${alrb_tmpPyVarAr[@]}; do
		alrb_useContainer="centos`\echo $alrb_tmpPyVar | cut -f 1 -d ':'`"
		alrb_requiredPythonVersion=`\echo $alrb_tmpPyVar | cut -f 2 -d ':'`
		\rm -f $alrb_InstallDir/tmp/container_script.sh
		\cat << EOF > $alrb_InstallDir/tmp/container_script.sh
#! /bin/sh
cd $alrb_InstallDir/tmp
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
alrb_InstallDir="$alrb_InstallDir"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh -q "python $alrb_requiredPythonVersion"
python3 setup.py install --prefix=$alrb_InstallDir
EOF
		\cat $alrb_InstallDir/tmp/container_script.sh
		chmod +x $alrb_InstallDir/tmp/container_script.sh
		(
		    alrb_thispwd=`pwd`
		    \echo $alrb_thispwd | \grep -e "^/eos" > /dev/null 2>&1
		    if [ $? -eq 0 ]; then
# apptainer runtime container bug - cannot start from /eos
			cd $TMPDIR
		    fi
		    unset HOME
		    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $alrb_useContainer --noalrb --runtimeOpt="apptainer|-B $ATLAS_LOCAL_ROOT_BASE" --runtimeOpt="singularity|-B $ATLAS_LOCAL_ROOT_BASE" --runtimeOpt="docker|-V $ATLAS_LOCAL_ROOT_BASE" --runtimeOpt="shifter|-V $ATLAS_LOCAL_ROOT_BASE" -r $alrb_InstallDir/tmp/container_script.sh
		)
		alrb_retCode=$?		
		if [ $alrb_retCode -ne 0 ]; then
		    break
		fi
	    done	    
	fi
    fi

    cd $alrb_InstallDir
    \rm -rf $alrb_InstallDir/tmp

    return $alrb_retCode
}

