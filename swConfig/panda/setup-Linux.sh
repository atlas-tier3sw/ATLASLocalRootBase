#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup PandaClient for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   27Apr15: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_PANDACLI_VERSION=$1

deletePath PATH $ATLAS_LOCAL_PANDACLIENT_PATH

export ATLAS_LOCAL_PANDACLIENT_PATH=${ATLAS_LOCAL_ROOT}/PandaClient/${ATLAS_LOCAL_PANDACLI_VERSION}

if [ $ALRB_RELOCATEALRB != "YES" ]; then
    source ${ATLAS_LOCAL_PANDACLIENT_PATH}/etc/panda/panda_setup.sh
else
    source ${ATLAS_LOCAL_PANDACLIENT_PATH}/etc/panda/panda_setup.sh.relocate
fi
export PATHENA_GRID_SETUP_SH=${ATLAS_LOCAL_ROOT_BASE}/user/pandaGridSetup.sh

if [ "$ALRB_tokensPanda" = "YES" ]; then
    export PANDA_AUTH=oidc
    export PANDA_AUTH_VO=atlas 

    if [ "$ALRB_OSMAJORVER" = "6" ]; then
# default slc6 python is 2.6; we need at least 2.7
	alrb_tmpVal=`python -c 'import sys; print(str(sys.version_info[0])+"."+str(sys.version_info[1]))'`
	if [[ "$alrb_tmpVal" != "2.7" ]] || [[ -z $PANDA_PYTHON_EXEC ]]; then
	    export PANDA_PYTHON_EXEC=$ALRB_pythonBinDir/python2
	fi
	unset alrb_tmpVal
    fi
else
    unset PANDA_AUTH
    unset PANDA_AUTH_VO
fi

listAnalyPQ()
{
    $ATLAS_LOCAL_ROOT_BASE/swConfig/panda/listAnalyPQ.sh "$@"
    return $?
}
