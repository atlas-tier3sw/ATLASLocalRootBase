#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup pacparser for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   04Dec20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_PACPARSER_VERSION=$1

source $ATLAS_LOCAL_ROOT/pacparser/$ATLAS_LOCAL_PACPARSER_VERSION/setup.sh

