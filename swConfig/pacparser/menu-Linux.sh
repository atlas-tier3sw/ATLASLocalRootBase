#!----------------------------------------------------------------------------
#!
#! menu.sh
#!
#! sets up the menu
#!
#! Usage:
#!     source menu.sh
#!
#! History:
#!   04Dec20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_AvailableTools="$alrb_AvailableTools pacparser"

return 0
