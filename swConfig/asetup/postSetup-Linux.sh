#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    19Mar2015: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ ! -z $PACPARSER_INSTALLEDDIR ]; then
    # need to do this because asetup is always setup and we want a clean env
    # unless a release is setup.  It is done in asetup's epilog 
    deletePath PATH $PACPARSER_INSTALLEDDIR/bin
    deletePath LD_LIBRARY_PATH $PACPARSER_INSTALLEDDIR/lib
fi
