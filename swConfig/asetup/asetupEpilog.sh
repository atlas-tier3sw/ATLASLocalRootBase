#!----------------------------------------------------------------------------
#!
#! asetupEpilog.sh
#!
#! Runs things after asetup 
#!
#! Usage:
#!     asetupEpilog.sh
#!     Note: This is meant to be run by AtlasSetup (epilog)
#!
#! History:
#!    12Jul12: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# set FRONTIER_SERVER if it is not set
if [[ -z $ALRB_noFrontierSetup ]] || [[ "$ALRB_noFrontierSetup" != "YES" ]]; then
    if [ -z $FRONTIER_SERVER ]; then
	export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(proxyurl=http://atlasbpfrontier.cern.ch:3127)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)"
    fi

    alrb_result=`$ATLAS_LOCAL_ROOT_BASE/utilities/addDevFrontier.sh`
    if [ $? -eq 0 ]; then
	export FRONTIER_SERVER=$alrb_result
    fi
fi

# if afs is setup for SITEROOT, issue a note
alrb_result=`\echo $SITEROOT | \grep '/afs/cern.ch' 2>&1`
if [ $? -eq 0 ]; then
    \echo "Note: Release is setup from /afs/cern.ch."
fi

# ASG releases are depreciated
#alrb_result=`\echo $ROOTCOREDIR | \grep '/cvmfs/atlas.cern.ch/repo/sw/ASG/'`
#if [ $? -eq 0 ]; then
#    \echo "Warning: ROOTCOREDIR points to ASG area after asetup."
#    \echo "         You are probably setting up an ASG release using asetup."
#    \echo "         This is depreciated.  Please use rcSetup instead."
#fi

# python fix
source $ATLAS_LOCAL_ROOT_BASE/swConfig/python/pythonFix-Linux.sh

# allow for users to run post release setup scripts
if [ ! -z "$ALRB_doPostASetup" ]; then
    eval $ALRB_doPostASetup
fi

# ATLAS overrides if applicable
alrb_result=`\echo $ALRB_testPath | \grep -e ",postRel-dev,"` 
if [ $? -eq 0 ]; then
    alrb_postRelFile="$ALRB_cvmfs_repo/sw/local/postATLASReleaseSetup-dev.sh"
else
    alrb_postRelFile="$ALRB_cvmfs_repo/sw/local/postATLASReleaseSetup.sh"
fi
if [ -e "$alrb_postRelFile" ]; then
    eval source $alrb_postRelFile
fi
unset alrb_postRelFile

if [ ! -z "$RUCIO_HOME" ]; then
    \echo "Warning: ATLAS release set up with rucio and the latter may not work properly."
    \echo "         Retry with rucio wrapper; eg lsetup \"rucio -w\""
fi

if [[ ! -z $ALRB_asetupPacparser ]] && [[ "$ALRB_asetupPacparser" = "YES" ]]; then
    appendPath PATH $PACPARSER_INSTALLEDDIR/bin
    appendPath LD_LIBRARY_PATH $PACPARSER_INSTALLEDDIR/lib
fi

if [ ! -z $AtlasVersion ]; then
    alrb_cpackWarning=""
    let alrb_tmpValN1=`\echo $AtlasVersion | \cut -f 1 -d '.'`    
    let alrb_tmpValN2=`\echo $AtlasVersion | \cut -f 2 -d '.'`
    let alrb_tmpValN3=`\echo $AtlasVersion | \cut -f 3 -d '.'`    
    let alrb_tmpValN=`expr $alrb_tmpValN1 \* 100000 + $alrb_tmpValN2 \* 1000 + $alrb_tmpValN3`
    if [[ $alrb_tmpValN -ge 2102249 ]] && [[ $alrb_tmpValN -le 2102251 ]]; then
	alrb_cpackWarning="YES"
    elif [[ $alrb_tmpValN -ge 2402002 ]] && [[ $alrb_tmpValN -le 2402003 ]]; then
	alrb_cpackWarning="YES"
    fi
    if [ "$alrb_cpackWarning" = "YES" ]; then
	\echo "Important for release $AtlasVersion:
    If you build code for the grid with this release, specify
      -DATLAS_USE_CUSTOM_CPACK_INSTALL_SCRIPT=TRUE
    Details: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisReleaseCPackBug2023April"
    fi
    unset alrb_tmpValN1 alrb_tmpValN2 alrb_tmpValN3 alrb_tmpValN alrb_cpackWarning
fi

unset alrb_result
