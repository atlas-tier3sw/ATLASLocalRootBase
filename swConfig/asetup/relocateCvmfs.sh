#!----------------------------------------------------------------------------
#!
#! relocateCvmfs-asetup.sh
#!
#! defines relocatable env for cvmfs
#!
#! These need to be defined:
#!   ATLAS_SW_BASE should be defined and point to somewhere other than /cvmfs
#!   ALRB_localRelocateDir needs to be defined to a writebale area  
#!
#! Usage: 
#!     source relocateCvmfs-asetup.sh
#!
#! History:
#!   16Jul14: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_originalALRB=`\grep -e "asetupEpilog.sh" $ATLAS_LOCAL_ROOT/AtlasSetup/.configCMake/.asetup.site | \sed -e 's|source||g' -e 's|/swConfig/.*||g' -e 's|[[:space:]]||g'`
alrb_conversionSed=""
if [ ! -z "$ATLAS_SW_BASE" ]; then
    alrb_conversionSed="-e 's|\([= :]\)/cvmfs|\1'$ATLAS_SW_BASE'|g'"    
elif [ "$alrb_originalALRB" != "$ATLAS_LOCAL_ROOT_BASE" ]; then
    alrb_conversionSed="-e 's|\([= :]\)$alrb_originalALRB|\1'$ATLAS_LOCAL_ROOT_BASE'|g'"
fi
unset alrb_originalALRB

if [ "$alrb_conversionSed" = "" ]; then
    unset alrb_conversionSed
    return 0
fi

# change AtlasSetupSite if it exists    
if [ ! -z $AtlasSetupSite ]; then
    alrb_asetupLocalConfigDir="$ALRB_localRelocateDir/asetup-config"
    mkdir -p $alrb_asetupLocalConfigDir
    alrb_updateIt="NO"
    \rm -f $alrb_asetupLocalConfigDir/.asetup.site.new
    eval \sed $alrb_conversionSed $AtlasSetupSite > $alrb_asetupLocalConfigDir/.asetup.site.new
    if [ -e $alrb_asetupLocalConfigDir/.asetup.site ]; then
	alrb_result=`diff $alrb_asetupLocalConfigDir/.asetup.site $alrb_asetupLocalConfigDir/.asetup.site.new 2>&1`
	if [ $? -ne 0 ]; then
	    alrb_updateIt="YES"
	fi
    else
	alrb_updateIt="YES"
    fi
    if [ "$alrb_updateIt" = "YES" ]; then
	mv $alrb_asetupLocalConfigDir/.asetup.site.new $alrb_asetupLocalConfigDir/.asetup.site 
    fi
    export AtlasSetupSite=$alrb_asetupLocalConfigDir/.asetup.site
    unset alrb_asetupLocalConfigDir
fi

# change AtlasSetupSiteCMake if it exists    
if [ ! -z $AtlasSetupSiteCMake ]; then
    alrb_asetupLocalConfigDir="$ALRB_localRelocateDir/asetup-cmake-config"
    mkdir -p $alrb_asetupLocalConfigDir
    alrb_updateIt="NO"
    \rm -f $alrb_asetupLocalConfigDir/.asetup.site.new
    eval \sed $alrb_conversionSed $AtlasSetupSiteCMake > $alrb_asetupLocalConfigDir/.asetup.site.new 
    if [ -e $alrb_asetupLocalConfigDir/.asetup.site ]; then
	alrb_result=`diff $alrb_asetupLocalConfigDir/.asetup.site $alrb_asetupLocalConfigDir/.asetup.site.new 2>&1`
	if [ $? -ne 0 ]; then
	    alrb_updateIt="YES"
	fi
    else
	alrb_updateIt="YES"
    fi
    if [ "$alrb_updateIt" = "YES" ]; then
	mv $alrb_asetupLocalConfigDir/.asetup.site.new $alrb_asetupLocalConfigDir/.asetup.site 
    fi
    export AtlasSetupSiteCMake=$alrb_asetupLocalConfigDir/.asetup.site
    unset alrb_asetupLocalConfigDir
fi

unset alrb_conversionSed

return 0
