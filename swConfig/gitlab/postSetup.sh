#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post-setup
#!
#!  History:
#!    19Aug2022: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ -e $$ATLAS_LOCAL_GITLAB_PATH/logDir/minPython3.sh ]; then
    source $ATLAS_LOCAL_GITLAB_PATH/logDir/minPython3.sh
    alrb_tmpPyVal=`python3 -c "import platform; print(platform.python_version())"`
    let alrb_tmpPyValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpPyVal 3`
    if [ $alrb_tmpPyValN -lt $python_gitlab_builtWithPyN ]; then
	\echo " gitlab:"
	\echo "   Warning:gitlab was built with python $alrb_tmpPyVal but we have $alrb_tmpPyVal"
    fi

    unset alrb_tmpPyVal alrb_tmpPyValN alrb_tmpPyVal alrb_tmpPyValN
fi
