#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup git for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   27Apr15: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_GITLAB_VERSION=$1
export ATLAS_LOCAL_GITLAB_PATH=${ATLAS_LOCAL_ROOT}/gitlab/${ATLAS_LOCAL_GITLAB_VERSION}

alrb_tmpVal=`\find $ATLAS_LOCAL_GITLAB_PATH -type d -name bin`
alrb_gitlabHome=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh "$alrb_tmpVal/.."`

insertPath PATH $alrb_gitlabHome/bin

for alrb_libDir in lib lib64; do
    if [ -d $alrb_gitlabHome/$alrb_libDir ]; then
	continue
    fi
    if [ -z "${LD_LIBRARY_PATH}" ]; then
	export LD_LIBRARY_PATH=$alrb_gitlabHome/$alrb_libDir
    else
	insertPath LD_LIBRARY_PATH $alrb_gitlabHome/$alrb_libDir
    fi
done
    
alrb_tmpPyVer=`$ALRB_envPython -V 2>&1 | \cut -f 2 -d " "  | \cut -f 1-2 -d "."`
alrb_foundPyLibs="NO"
for alrb_libDir in lib lib64; do
    alrb_tmpValPyDir="$alrb_gitlabHome/$alrb_libDir/python${alrb_tmpPyVer}/site-packages"
    if [ -d $alrb_tmpValPyDir ]; then
	alrb_foundPyLibs="YES"
        if [ -z $PYTHONPATH ]; then
	    export PYTHONPATH="$alrb_tmpValPyDir"
	else	
	    insertPath PYTHONPATH "$alrb_tmpValPyDir"
    	fi
    fi  
done
if [ "$alrb_foundPyLibs" = "NO" ]; then
   # backward compatibility ...
   alrb_tmpValPyDir=`\find $alrb_gitlabHome/lib -type d -name site-packages`
   if [ -z "${PYTHONPATH}" ]; then
       export PYTHONPATH=$alrb_tmpValPyDir
   else
       insertPath PYTHONPATH $alrb_tmpValPyDir
   fi
fi

unset alrb_tmpVal alrb_tmpPyVer alrb_libDir alrb_tmpValPyDir alrb_foundPyLibs



