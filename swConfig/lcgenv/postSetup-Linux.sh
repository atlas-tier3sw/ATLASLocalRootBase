#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    24Aug2020: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ "$alrb_Quiet" = "NO" ]; then
    \echo " lcgenv:"
    \echo "   Note that lcgenv sets up a long list of library paths."
    \echo "   This may slow down executables; they look at each library path item."
    \echo "   A faster alternative is to setup the entire LCG stack with views:"
    \echo "    lsetup \"views <LCG release> <platform>\""
fi
