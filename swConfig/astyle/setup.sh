#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup astyle for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   04Oct22: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_ASTYLE_VERSION=$1
export ATLAS_LOCAL_ASTYLE_PATH=${ATLAS_LOCAL_ROOT}/astyle/${ATLAS_LOCAL_ASTYLE_VERSION}

insertPath ROOT_INCLUDE_PATH $ATLAS_LOCAL_ASTYLE_PATH/astyle
insertPath PYTHONPATH $ATLAS_LOCAL_ASTYLE_PATH/astyle

