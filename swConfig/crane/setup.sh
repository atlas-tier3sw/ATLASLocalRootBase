#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup crane for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   04Oct22: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_CRANE_VERSION=$1
export ATLAS_LOCAL_CRANE_PATH=${ATLAS_LOCAL_ROOT}/crane/${ATLAS_LOCAL_CRANE_VERSION}

insertPath PATH $ATLAS_LOCAL_CRANE_PATH
