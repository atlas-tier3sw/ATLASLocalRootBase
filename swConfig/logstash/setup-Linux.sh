#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup logstash for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   20Oct21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_LOGSTASH_VERSION=$1

alrb_logstashHome=`\find $ATLAS_LOCAL_ROOT/logstash/$ATLAS_LOCAL_LOGSTASH_VERSION -type d -name lib | \sed -e 's|/lib||g'`

insertPath PATH "$alrb_logstashHome/bin"

alrb_tmpPyVer=`python3 -V 2>&1 | \cut -f 2 -d " "  | \cut -f 1-2 -d "."`

for alrb_libDir in lib lib64; do
    alrb_tmpVal="$alrb_logstashHome/$alrb_libDir/python${alrb_tmpPyVer}/site-packages"
    if [ -d $alrb_tmpVal ]; then
        if [ -z $PYTHONPATH ]; then
	    export PYTHONPATH="$alrb_tmpVal"
	else	
	    insertPath PYTHONPATH "$alrb_tmpVal"
    	fi
    fi  
done

unset alrb_logstashHome alrb_tmpVal alrb_tmpPyVer alrb_libDir

