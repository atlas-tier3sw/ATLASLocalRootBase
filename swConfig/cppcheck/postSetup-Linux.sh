#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    19Mar2015: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ "$alrb_Quiet" = "NO" ]; then
    if [ -z $AtlasVersion ]; then
	\echo " cppcheck:"
	\echo "   Warning: To use cppcheck you need to setup a release or a nightly first"
    elif [ ! -z $LCG_PLATFORM ]; then
	alrb_lcgPlatform=`\echo $LCG_PLATFORM | \sed -e 's|centos9|el9|g' -e 's|\(.*gcc[[:digit:]]*\)-.*|\1|g'`
	\echo $ATLAS_LOCAL_CPPCHECK_VERSION | \grep -e "$alrb_lcgPlatform" > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	    \echo " cppcheck:"
	    \echo "   Warning: cppcheck $ATLAS_LOCAL_CPPCHECK_VERSION mis-match release \$LCG_PLATFORM=$LCG_PLATFORM"
	fi
	unset alrb_lcgPlatform
    else
	\echo " cppcheck:"
	\echo "   Note: Unable to check compatibility. \$LCG_PLATFORM undefined; ask ALRB dev to fix."
    fi
fi
