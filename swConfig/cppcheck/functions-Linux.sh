#!----------------------------------------------------------------------------
#!
#!  functions.sh
#!
#!    functions for cppcheck 
#!
#!  History:
#!    19Mar2015: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------


alrb_fn_cppcheckHelp()
{
    \cat <<EOF

Usage: lsetup [global options] "${alrb_sw} [options] <version>"
  
    This sets up the ATLAS environment for cppcheck 

    You need to set the environment variable ATLAS_LOCAL_ROOT_BASE first.    

    Options (to override defaults) are:
EOF
    alrb_fn_glocalSetupHelp
    ${ATLAS_LOCAL_ROOT_BASE}/swConfig/showVersions.sh $alrb_sw

}


alrb_fn_cppcheckVersionConvert()
{
# 2 significant figures 
    local alrb_tmpVal=`\echo $1 | \cut -f 1 -d "-" | \sed -e 's/\([0-9\.]*\).*/\1/g'`
    $ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpVal 2
    return $?
}


alrb_fn_cppcheckDepend()
{
    local alrb_sw="cppcheck"
    local alrb_progname="alrb_fn_${alrb_sw}Depend"
    
    local alrb_swDir
    alrb_swDir=`alrb_fn_getSynonymInfo "$alrb_sw" dir`
    if [ $? -ne 0 ]; then
	return 64
    fi

    local alrb_shortopts="h,q,s,f,c:"
    local alrb_longopts="help,${alrb_sw}Version:,quiet,skipConfirm,force"
    local alrb_opts
    local alrb_result
    alrb_result=`getopt -T >/dev/null 2>&1`
    if [ $? -eq 4 ] ; then # New longopts getopt.
	alrb_opts=$(getopt -o $alrb_shortopts --long $alrb_longopts -n "$alrb_progname" -- "$@")
	local alrb_returnVal=$?
    else # use wrapper
	alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_getopt.sh $alrb_shortopts $alrb_longopts $*`
	local alrb_returnVal=$?
    fi
    
# do we have an error here ?
    if [ $alrb_returnVal -ne 0 ]; then
	\echo $alrb_opts 1>&2
	\echo "'lsetup \"$alrb_sw --help\"' for more information" 1>&2
	return 1
    fi
    
    eval set -- "$alrb_opts"
    
    local alrb_caller="unknown"
    if [ ! -z $ALRB_cppcheckVersion ]; then
	local alrb_swVersion=$ALRB_cppcheckVersion
    else
	local alrb_swVersion="dynamic"
    fi
    
    while [ $# -gt 0 ]; do
	: debug: $1
	case $1 in
            -h|--help)
		alrb_fn_cppcheckHelp
		return 0
		;;
            --${alrb_sw}Version)
                local alrb_swVersion=$2
		shift 2
		;;
            -c|--caller)
		local alrb_caller=$2
		shift 2
		;;
	    -q|--quiet)	    
# backward compatibility
	        alrb_Quiet="YES"
		shift
		;;
	    -s|--skipConfirm)	    
		shift
		;;
	    -f|--force)    
		shift
		;;
            --)
		shift
		break
		;;
            *)
		\echo "Internal Error: option processing error: $1" 1>&2
		return 1
		;;
	esac
    done
    
    if [ $# -ge 1 ]; then
	local alrb_swVersion=$1
	shift
	alrb_fn_unrecognizedExtraArgs "$@"
	if [ $? -ne 0 ]; then
	    return 64
	fi
    fi
    
    if [ "$alrb_swVersion" = "" ]; then
	\echo "Error: ${alrb_sw} version not specified" 1>&2
	return 64
    fi

    local alrb_candRealVersion=""
    local alrb_candVirtVersion=""
    alrb_fn_getToolVersion "$alrb_sw" "$alrb_swVersion" "alrb_gcc:alrb_slc:alrb_arch:alrb_firstVer"
    if [ $? -ne 0 ]; then
	return 64
    fi
    local alrb_setVer="$alrb_candVirtVersion"
    
    if [ "$alrb_setVer" != "" ]; then
	alrb_fn_versionChecker "$alrb_sw" "$alrb_caller" "$alrb_setVer" -c "$alrb_swVersion"
	if [ $? -ne 0 ]; then
	    return 64
	else

# dependencies
	    alrb_fn_doOverrides "$alrb_sw" "$alrb_setVer" "$alrb_sw"
	    if [ $? -ne 0 ]; then
		return 64
	    fi

	    return 0
	fi
    else
	\echo "Error: cppcheck version undetermined or unavailable $alrb_swVersion" 1>&2
	return 64
    fi
}

alrb_fn_cppcheckPostInstall()
{
    if [ "$alrb_PostInstallUseContainer" = "" ]; then
	return 0
    fi

    local alrb_useContainer=`\echo $alrb_PostInstallUseContainer | \cut -f 1 -d "|"`
    local alrb_useContainerScript=`\echo $alrb_PostInstallUseContainer | \cut -f 2 -d "|"`
    if [ ! -e "$alrb_useContainerScript" ]; then
	\echo "Error: $alrb_useContainerScript does not exist"
	return 64
    fi
        
    cd $alrb_InstallDir
    local alrb_tmpVal
    alrb_tmpVal=`\find $alrb_InstallDir -maxdepth 2 -type f -name Makefile`
    if [ $? -ne 0 ]; then
	\echo "Error: Makefile is missing in source code after unpacking ?"
	return 64
    fi

    alrb_tmpVal=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_readlink.sh $alrb_tmpVal`
    local alrb_cppInstallDir=`\dirname $alrb_tmpVal`
    local alrb_cppSrcDir="$alrb_InstallDir/src"
    \mv $alrb_cppInstallDir $alrb_cppSrcDir
    \mkdir -p $alrb_cppInstallDir
    cd $alrb_cppSrcDir

    \rm -f $alrb_cppSrcDir/container_script.sh
    \cat << EOF > $alrb_cppSrcDir/container_script.sh  
#! /bin/sh
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_LOCAL_ROOT_BASE
alrb_InstallDir="$alrb_InstallDir"
alrb_cppSrcDir="$alrb_cppSrcDir"
alrb_cppInstallDir="$alrb_cppInstallDir"

EOF
    \cat $alrb_useContainerScript >> $alrb_cppSrcDir/container_script.sh
    chmod +x $alrb_cppSrcDir/container_script.sh
    
    (
	unset HOME
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $alrb_useContainer --noalrb --runtimeOpt="apptainer|-B $ATLAS_LOCAL_ROOT_BASE" --runtimeOpt="singularity|-B $ATLAS_LOCAL_ROOT_BASE" --runtimeOpt="docker|-V $ATLAS_LOCAL_ROOT_BASE" --runtimeOpt="shifter|-V $ATLAS_LOCAL_ROOT_BASE" -r $alrb_cppSrcDir/container_script.sh
    )
    if [ $? -ne 0 ]; then
	return 64
    fi

    cd $alrb_InstallDir
    \cat << EOF > $alrb_InstallDir/setup.sh 
export CPPCHECK_SRC="$alrb_cppSrcDir"
export CPPCHECK_HOME="$alrb_cppInstallDir"
insertPath PATH \$CPPCHECK_HOME/bin        
EOF
    
    return 0
}
