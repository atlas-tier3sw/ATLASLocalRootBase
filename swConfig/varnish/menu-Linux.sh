#!----------------------------------------------------------------------------
#!
#! menu.sh
#!
#! sets up the menu
#!
#! Usage:
#!     source menu.sh
#!
#! History:
#!   07Jul22: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_AvailableTools="$alrb_AvailableTools varnish"

return 0
