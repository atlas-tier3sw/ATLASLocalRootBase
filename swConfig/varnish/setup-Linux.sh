#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup varnish for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   07Jul22: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_VARNISH_VERSION=$1

alrb_tmpVal=`\find $ATLAS_LOCAL_ROOT/varnish/$ATLAS_LOCAL_VARNISH_VERSION -type d -name bin | \sed -e 's|/bin||g'`

export ALRB_VARNISH_HOME="$alrb_tmpVal"

appendPath PATH $ALRB_VARNISH_HOME/bin
if [ -d $ALRB_VARNISH_HOME/lib ]; then
    appendPath LD_LIBRARY_PATH $ALRB_VARNISH_HOME/lib
fi
if [ -d $ALRB_VARNISH_HOME/lib64 ]; then
    appendPath LD_LIBRARY_PATH $ALRB_VARNISH_HOME/lib64
fi

unset alrb_tmpVal

