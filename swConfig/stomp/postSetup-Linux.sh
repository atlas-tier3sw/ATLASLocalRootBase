#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    07Jul2022: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ "$ALRB_envPython" != "python3" ]; then
    \echo " stomp:"
    \echo "   Warning: stomp requires python3 but python3 env is not default for this OS."
    \echo "   You should do setupATLAS -3 to default to python3 environment"
fi
