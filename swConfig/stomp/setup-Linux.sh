#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup stomp for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   07Jul22: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_STOMP_VERSION=$1

alrb_tmpVal=`\find $ATLAS_LOCAL_ROOT/stomp/$ATLAS_LOCAL_STOMP_VERSION -type d -name lib | \sed -e 's|/lib||g'`

insertPath PATH "$alrb_tmpVal/bin"

alrb_tmpPyVer=`python3 -V 2>&1 | \cut -f 2 -d " "  | \cut -f 1-2 -d "."`

if [ -z $PYTHONPATH ]; then
    export PYTHONPATH="$alrb_tmpVal/lib/python${alrb_tmpPyVer}/site-packages"
else
    insertPath PYTHONPATH "$alrb_tmpVal/lib/python${alrb_tmpPyVer}/site-packages"
fi

unset alrb_tmpVal alrb_tmpPyVer

