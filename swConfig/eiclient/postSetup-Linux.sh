#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    13Jul2022: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ "$alrb_Quiet" = "NO" ]; then

    alrb_tmpVal=`\echo $ATLAS_LOCAL_EICLIENT_VERSION | \cut -f 1 -d "-" | \sed -e 's/\([0-9\.]*\).*/\1/g'`
    let alrb_tmpValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpVal 3`

    if [ $alrb_tmpValN -ge 40000 ]; then
	\echo " eiclient:"
	\echo "  This is the new implementation of the EventIndex. 
   For a full list of options, type:
    dataset-list -h
    event-lookup -h
    event-list -h"
    fi
fi
unset alrb_tmpVal alrb_tmpValN

