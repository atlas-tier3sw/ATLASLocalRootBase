#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    19Mar2015: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ "$alrb_Quiet" = "NO" ]; then
    alrb_tmpVal=`env | \grep RUCIO_ACCOUNT`
    if [[ $? -ne 0 ]] && [[ ! -z $RUCIO_ACCOUNT ]] ; then
	\echo " rucio:"
	\echo "   Warning: RUCIO_ACCOUNT was defined but not exported."
	\echo "    Exporting it now ..."
	export RUCIO_ACCOUNT
    fi
    if [[ ! -z "$AtlasVersion" ]] && [[ ! -z "$RUCIO_HOME" ]]; then
	\echo " rucio:"
	\echo "   Warning: An ATLAS release was setup and rucio may not work."
	\echo "            If you have rucio issues, retry with the wrapper version. "
	\echo "            eg: lsetup \"rucio -w\""
    fi
    let	alrb_tmpValN=`\echo $ALRB_OSMAJORVER`
    if [ $alrb_tmpValN -le 7 ]; then
	alrb_tmpVal="`\echo $ATLAS_LOCAL_RUCIOCLIENTS_VERSION | \cut -f 1 -d "-" | \sed -e 's/\([0-9\.]*\).*/\1/g'`"
	let alrb_tmpValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpVal 3`
	if [ $alrb_tmpValN -ge 12901 ]; then
	    if [ "$RUCIO_PYTHONBIN" != "python3" ]; then
		\echo " rucio:
   Warning: This is a wrapper version of rucio (no APIs) for python2 centos7.
            rucio $ATLAS_LOCAL_RUCIOCLIENTS_VERSION and newer are python3 only.
            for python2 (older) rucio: lsetup \"rucio SL7Python2\"
	    for python3 centos7 environment, do instead \"setupATLAS -3\""
	    fi
	fi
    fi
fi
unset alrb_tmpVal alrb_tmpValN
