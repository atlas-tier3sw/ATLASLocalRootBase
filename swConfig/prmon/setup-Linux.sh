#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup prmon for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   16Jul20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_PRMON_VERSION=$1
export ATLAS_LOCAL_PRMON_PATH=${ATLAS_LOCAL_ROOT}/prmon/${ATLAS_LOCAL_PRMON_VERSION}

if [ -d "$ATLAS_LOCAL_PRMON_PATH/bin" ]; then
    insertPath PATH "$ATLAS_LOCAL_PRMON_PATH/bin"
else
    alrb_tmpVal=`\find $ATLAS_LOCAL_PRMON_PATH -type d -name bin`
    if [ $? -eq 0 ]; then
	insertPath PATH $alrb_tmpVal
    fi
fi

unset alrb_tmpVal



