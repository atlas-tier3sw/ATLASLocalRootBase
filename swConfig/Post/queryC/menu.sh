#!----------------------------------------------------------------------------
#!
#! menu.sh
#!
#! sets up the menu
#!
#! Usage:
#!     source menu.sh
#!
#! History:
#!   27Apr15: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

queryC()
{
    ${ATLAS_LOCAL_ROOT_BASE}/container/queryContainer.sh $@
    return $?
}

if [ "$alrb_Quiet" = "NO" ]; then
    $ATLAS_LOCAL_ROOT_BASE/swConfig/printMenu.sh queryC 1 "Post"
fi

alrb_AvailableToolsPost="$alrb_AvailableToolsPost queryC"

return 0
