#!----------------------------------------------------------------------------
#!
#! menu.sh
#!
#! sets up the menu
#!
#! Usage:
#!     source menu.sh
#!
#! History:
#!   27Apr15: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

installRpm()
{
    ${ATLAS_LOCAL_ROOT_BASE}/utilities/installRpm.sh $@
    return $?
}

if [ "$alrb_Quiet" = "NO" ]; then
    $ATLAS_LOCAL_ROOT_BASE/swConfig/printMenu.sh installRpm 1 "Post"
fi

alrb_AvailableToolsPost="$alrb_AvailableToolsPost installRpm"

return 0
