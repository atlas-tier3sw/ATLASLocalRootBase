#!----------------------------------------------------------------------------
#!
#! menu.sh
#!
#! sets up the menu
#!
#! Usage:
#!     source menu.sh
#!
#! History:
#!   27Apr15: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

installPip()
{
    ${ATLAS_LOCAL_ROOT_BASE}/utilities/installPip.sh $@
    return $?
}


if [ "$alrb_Quiet" = "NO" ]; then
    $ATLAS_LOCAL_ROOT_BASE/swConfig/printMenu.sh installPip 1 "Post"
fi

alrb_AvailableToolsPost="$alrb_AvailableToolsPost installPip"

return 0
