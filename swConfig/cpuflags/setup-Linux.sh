#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup cpuflags for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   20Oct21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_CPUFLAGS_VERSION=$1

export CPUFLAGS_HOME=`\find $ATLAS_LOCAL_ROOT/cpuflags/$ATLAS_LOCAL_CPUFLAGS_VERSION -type f -name README.md | \sed -e 's|/README.md||g'`

insertPath PATH $CPUFLAGS_HOME


