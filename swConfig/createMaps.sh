#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! createMaps.sh
#!
#! creates maps for installed sw
#!
#! Usage:
#!     createMaps.sh
#!
#! History:
#!   09May16: A. De Silva, First version
#!
#!----------------------------------------------------------------------------


source ${ATLAS_LOCAL_ROOT_BASE}/utilities/checkAtlasLocalRoot.sh

source ${ATLAS_LOCAL_ROOT_BASE}/swConfig/functions.sh

# these will not have any dirs in ALRB but can be made visible
alrb_availableMask="| \grep -e '^lcgenv$' -e '^sft$' -e '^views$' -e '^java$'"

alrb_SetupToolAr=( `\grep -i -e '^,[A-Z]*' $ATLAS_LOCAL_ROOT_BASE/swConfig/synonyms.txt | \cut -f 4 -d ","  |  \tr '[:upper:]' '[:lower:]' | \egrep -v "^virtual"` )

for alrb_item in ${alrb_SetupToolAr[@]}; do
    alrb_fn_createReleaseMap $alrb_item
done

\mkdir -p $ATLAS_LOCAL_ROOT/.alrb
\rm -f $ATLAS_LOCAL_ROOT/.alrb/availableMash.sh
\echo "alrb_availableMask=\"$alrb_availableMask\"" > $ATLAS_LOCAL_ROOT/.alrb/availableMash.sh

exit 0


