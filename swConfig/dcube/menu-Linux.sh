#!----------------------------------------------------------------------------
#!
#! menu.sh
#!
#! sets up the menu
#!
#! Usage:
#!     source menu.sh
#!
#! History:
#!   24Sep19: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_AvailableTools="$alrb_AvailableTools dcube"

return 0
