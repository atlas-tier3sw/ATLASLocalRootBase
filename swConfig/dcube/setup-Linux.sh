#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup dcube for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   24Sep19: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_DCUBE_DIR=${ATLAS_LOCAL_ROOT}/dcube/$1

if [ -e "$ATLAS_DCUBE_DIR/scripts/setup.sh" ]; then
    source $ATLAS_DCUBE_DIR/scripts/setup.sh
fi
