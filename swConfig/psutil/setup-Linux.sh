#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup psutil for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   20Oct21: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_PSUTIL_VERSION=$1

alrb_psutilHome=`\find $ATLAS_LOCAL_ROOT/psutil/$ATLAS_LOCAL_PSUTIL_VERSION -type d -name lib | \sed -e 's|/lib||g'`

insertPath PATH "$alrb_psutilHome/bin"

alrb_tmpPyVer=`python3 -V 2>&1 | \cut -f 2 -d " "  | \cut -f 1-2 -d "."`

for alrb_libDir in lib lib64; do
    alrb_tmpVal="$alrb_psutilHome/$alrb_libDir/python${alrb_tmpPyVer}/site-packages"
    if [ -d $alrb_tmpVal ]; then
        if [ -z $PYTHONPATH ]; then
	    export PYTHONPATH="$alrb_tmpVal"
	else	
	    insertPath PYTHONPATH "$alrb_tmpVal"
    	fi
    fi  
done

unset alrb_psutilHome alrb_tmpVal alrb_tmpPyVer alrb_libDir

