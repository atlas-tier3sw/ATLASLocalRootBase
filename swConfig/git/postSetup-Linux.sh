#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    23jan2023: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ "$alrb_Quiet" = "NO" ]; then
    alrb_tmpVal="`\echo $ATLAS_LOCAL_GIT_VERSION | \cut -f 1 -d "-" | \sed -e 's/\([0-9\.]*\).*/\1/g'`"
    let alrb_tmpValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpVal 3`
    if [ $alrb_tmpValN -ge 23900 ]; then
	\echo " git:"
	\echo "   Note that with a sparse checkout, 'git grep' now only searches within 
        currently checked out packages. To search in all packages, use 
        'git grep --cached'. You may wish to set this as an alias, 
        e.g. \"alias ggrep='git grep --cached'\"."
    fi
fi
unset alrb_tmpVal alrb_tmpValN
