#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup darshan for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   04Dec20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_DARSHAN_VERSION=$1

source $ATLAS_LOCAL_ROOT/darshan/$ATLAS_LOCAL_DARSHAN_VERSION/setup.sh

if [ -z $DARSHAN_LD_PRELOAD ]; then
    export DARSHAN_LD_PRELOAD="$DARSHAN_INSTALLEDDIR/lib/libdarshan.so"
fi

if [ -z $DARSHAN_ENABLE_NONMPI ]; then
    export DARSHAN_ENABLE_NONMPI=1
fi
