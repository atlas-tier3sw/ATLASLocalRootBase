#!----------------------------------------------------------------------------
#!
#!  postSetup.sh
#!
#!    post setup script
#!
#!  History:
#!    19Mar2015: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ "$alrb_Quiet" = "NO" ]; then
    \echo " darshan:"
    \echo "   DARSHAN_LOGDIR is set to $DARSHAN_LOGDIR"
    \echo "    Or you can 'export DARSHAN_LOGDIR=<path>' to customize the log path."
    \echo "   You must 'export LD_PRELOAD=\$DARSHAN_LD_PRELOAD' to enable instrumentation"
    \echo "    of applications."
fi 

