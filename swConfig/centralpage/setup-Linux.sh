#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup centralpage for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   01Oct24: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_CENTRALPAGE_VERSION=$1

export ATLAS_LOCAL_CENTRALPAGE_HOME=`\find ${ATLAS_LOCAL_ROOT}/centralpage/${ATLAS_LOCAL_CENTRALPAGE_VERSION} -type f -name setup.sh | \sed -e 's|/setup.sh||g'`

source $ATLAS_LOCAL_CENTRALPAGE_HOME/setup.sh

return 0
