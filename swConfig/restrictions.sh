alrb_fn_restrictions() {

    local alrb_sedExclusions=""
    local alrb_motdExclusions=""
    
    if [[ ! -z "$ALRB_CONT_IMAGE" ]] && [[ "$ALRB_CONT_IMAGE" != "" ]] && [[ -e /release_setup.sh ]]; then	

	alrb_sedExclusions="$alrb_sedExclusions -e '/^rcsetup$/d'"
	alrb_motdExclusions="$alrb_motdExclusions
Standalone container detected.  These tools are disabled:"
	
	#asetup
	\grep -e "asetup" /release_setup.sh >/dev/null 2>&1
	if [ $? -ne 0 ]; then
	    alrb_sedExclusions="$alrb_sedExclusions -e '/^asetup$/d'"
	    alrb_motdExclusions="$alrb_motdExclusions
 asetup: Releases are setup with source /release_setup.sh"
	fi
	
	# panda
	alrb_sedExclusions="$alrb_sedExclusions -e '/^panda$/d'"
	alrb_motdExclusions="$alrb_motdExclusions
 panda : Job needs to be submitted from outside the container"

	alrb_motdExclusions="$alrb_motdExclusions
         see https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ADCContainersDeployment#Standalone_containers
"
    fi
        
    if [ "$alrb_sedExclusions" != "" ]; then
	export ALRB_sedExclusions="| \sed $alrb_sedExclusions"
	export ALRB_motdExclusions="$alrb_motdExclusions"
    else
	export ALRB_sedExclusions=""
	export ALRB_motdExclusions=""
    fi
 
    return 0
}

