#!----------------------------------------------------------------------------
#!
#! setup.sh
#!
#! A simple script to setup curl for local Atlas users
#!
#! Usage:
#!     source setup.sh <version>
#!
#! History:
#!   03Apr24: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

export ATLAS_LOCAL_CURL_VERSION=$1
export ATLAS_LOCAL_CURL_PATH=${ATLAS_LOCAL_ROOT}/curl/${ATLAS_LOCAL_CURL_VERSION}

if [ -e $ATLAS_LOCAL_CURL_PATH/setup.sh ]; then
    source $ATLAS_LOCAL_CURL_PATH/setup.sh
fi
