#!----------------------------------------------------------------------------
#!
#! pythonFix-Linux.sh
#!
#! Fixes issues for python setups
#!
#! Usage:
#!     source pythonFix-Linux.sh
#!
#! History:
#!   27Apr15: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

if [ -z $SSL_CERT_DIR ]; then
    export SSL_CERT_DIR=$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates
fi

return 0
