relocate allows for /cvmfs to be mounted elsewhere; eg: /mnt/cvmfs
  or for use with cvmfsexec (see below))

In order to use ALRB with relocate, this is the procedure:

export ATLAS_SW_BASE=/mnt/cvmfs
# export ATLAS_SW_BASE=~/cvmfsexec/dist/cvmfs  # cvmfsexec location if used
export VO_ATLAS_SW_DIR=$ATLAS_SW_BASE/atlas.cern.ch/repo/sw 
export ALRB_localRelocateDir=$HOME/myLocalRelocateDir
mkdir -p $ALRB_localRelocateDir
export ALRB_RELOCATECVMFS="YES"
export ATLAS_LOCAL_ROOT_BASE=$ATLAS_SW_BASE/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS

----------

If interested in cvmfsexec, this is the cheat sheet:

From:
https://github.com/cvmfs/cvmfsexec

git clone https://github.com/cvmfs/cvmfsexec.git ~/cvmfsexec
cd cvmfsexec
git checkout tags/v4.27  # I prefer specific tags to be sure of what I install
./makedist osg   # has -o option to put in shared space.  Will it work on network file systems ?

./mountrepo config-osg.opensciencegrid.org
./mountrepo atlas.cern.ch
./mountrepo atlas-condb.cern.ch 
./mountrepo atlas-nightlies.cern.ch
./mountrepo sft.cern.ch
./mountrepo sft-nightlies.cern.ch
./mountrepo unpacked.cern.ch

For containers, you will need a local installaed runtime (apptainer/docker etc)
 as the one from cvmfs will not work with relocated cvmfsexec.
