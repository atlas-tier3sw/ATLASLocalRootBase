# these are settings that setupATLAS -2/-3 will set

if [ "$1" = "python3" ]; then
    export ALRB_envPython="python3"
    export PANDA_PY3=1
    export PANDA_PYTHON_EXEC=$ALRB_envPython
    export AMI_envPython="python3"
elif [ "$1" = "python2" ]; then
    export ALRB_envPython="python2"
    unset PANDA_PY3
    export PANDA_PYTHON_EXEC=$ALRB_envPython
    export AMI_envPython="python2"
else
    export ALRB_envPython="python"
    unset PANDA_PY3
    unset PANDA_PYTHON_EXEC
    unset AMI_envPython
fi
export RUCIO_PYTHONBIN=$ALRB_envPython
export EMI_PYTHONBIN=$ALRB_envPython

