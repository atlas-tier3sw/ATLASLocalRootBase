####
#  This file is obsolete.  It has been moved to the Tier3SWConfig package.
####
# These are versions that should be used for ADC Testing of ALRB tools
#  eg http://hammercloud.cern.ch/hc/app/atlas/#ALRB

export ALRB_asetupVersion=testing
export ALRB_emiVersion=testing
export ALRB_davixVersion=testing
#export ALRB_rucioVersion=testing
export ALRB_xrootdVersion=testing
export ALRB_cvmfsSingularityVersion=testing

