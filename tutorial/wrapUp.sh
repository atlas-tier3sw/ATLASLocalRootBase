#!----------------------------------------------------------------------------
#!
#! wrapUp.sh
#!
#! tarball the work dir for user support
#!
#! Usage:
#!     wrapUp.sh
#!
#! History:
#!   07Aug14: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

cd $ALRB_SMUDIR

source $ALRB_SMUDIR/shared.sh

\rm -f $ALRB_SMUDIR/summary.txt
touch $ALRB_SMUDIR/summary.txt

alrb_testResult=`\echo $alrb_testResult | \sed -e 's/^,//' -e 's/,$//'`

\echo " "
\echo "Tutorial : $alrb_tutorialVersion"
\echo "Domain   : $alrb_domain"
\echo "Nickname : $alrb_nickname"
\echo "Identity : $alrb_identity"

\echo "Tutorial : $alrb_tutorialVersion" >> $ALRB_SMUDIR/summary.txt
\echo "Domain   : $alrb_domain" >> $ALRB_SMUDIR/summary.txt
\echo "Nickname : $alrb_nickname" >> $ALRB_SMUDIR/summary.txt
\echo "Identity : $alrb_identity" >> $ALRB_SMUDIR/summary.txt
 
alrb_testAr=( `\echo $alrb_testResult | \sed -e 's/True/OK/g' -e 's/False/Failed/g' -e 's/,/ /g'` )
for alrb_item in ${alrb_testAr[@]}; do
    alrb_key=`\echo $alrb_item | \cut -d "=" -f 1`
    alrb_value=`\echo $alrb_item | \cut -d "=" -f 2`
    printf "  %-20s %10s\n" $alrb_key $alrb_value
    printf "  %-20s %10s\n" $alrb_key $alrb_value >> $ALRB_SMUDIR/summary.txt
done

\rm -f $ALRB_SCRATCH/smu.tar.gz
tar zcf $ALRB_SCRATCH/smu.tar.gz *
\rm -rf $ALRB_SMUDIR/*
\mv $ALRB_SCRATCH/smu.tar.gz $ALRB_SMUDIR/smu.tar.gz

\echo " "
\echo "Tarball of results is $ALRB_SMUDIR/smu.tar.gz"
alrb_result=`\echo $alrb_testResult | \grep False 2>&1`
if [ $? -eq 0 ]; then
    \echo "Please also send $ALRB_SMUDIR/smu.tar.gz to user support."
    \echo -e "Some checks have failed and this node is NOT ready for the tutorial.    "'[\033[31mFAILED\033[0m]'
else
    \echo -e "All checks are passing and this node is ready for the tutorial.         "'[\033[32m  OK  \033[0m]'
    \echo "
Please define this in your login script if not already:
export ALRB_TutorialData=$ALRB_TutorialData
"
fi

cd $alrb_thisPwd

unset alrb_key alrb_result alrb_testAr alrb_testResult alrb_value
